const path = require("path");
const WebpackBuildNotifierPlugin = require("webpack-build-notifier");

const isProduction = process.env.NODE_ENV === "production";
const isStaging = !isProduction || process.env.IS_DEBUG !== "false";
let publicPath = isProduction ? "/admin" : '/';
const BACKEND_HOST = process.env.BACKEND_HOST

let dirname = __dirname;

const plugins = [];

if (isProduction) {
    const buildType = isStaging ? "Staging" : "Production";
    plugins.push(new WebpackBuildNotifierPlugin({
        logo: path.resolve(dirname, "./src/assets/av-128x128.png"),
        suppressSuccess: true,
        suppressWarning: true,
        title: `Fuel ${buildType} Build ready`
    }));
}

module.exports = {
    publicPath,
    outputDir: path.resolve(__dirname, "dist"),
    productionSourceMap: false,
    // css: {
    //     loaderOptions: {
    //         scss: {
    //             prependData: `
    //                 @import "@/styles/scss/abstracts/index.scss";
    //             `,
    //             sassOptions: {
    //                 includePaths: [path.resolve(__dirname, "node_modules")]
    //             },
    //             implementation: require("node-sass")
    //         }
    //     }
    // },
    devServer: {
        disableHostCheck: true,
        proxy: {
            "/*.php": {
                changeOrigin: true,
                secure: false,
                target: BACKEND_HOST
            }
        },
        watchOptions: {
            poll: true
        }
    },
    chainWebpack: config => {
        config.plugin("define").tap((definitions) => {
            definitions[0].APP_IS_STAGING = isStaging;
            definitions[0].APP_IS_PRODUCTION = isProduction;
            definitions[0].APP_VERSION = JSON.stringify(require("./package.json").version);
            // definitions[0].__VUE_OPTIONS_API = true;
            // definitions[0].VUE_PROD_DEVTOOLS = false;
            return definitions;
        });
    },
    configureWebpack: {
        plugins
    }
};
