import store, {
    CREATE_TABLE,
    DELETE_DATA,
    DELETE_WARNING,
    EDIT_NAME,
    LOG_IN,
    LOG_OUT,
    REGISTER_USER,
    REMOVE_INFO,
    ADD_DATA,
    SET_LOADING,
    MachineData,
    SET_REAL_DATA,
    EDIT_DATA,
    COMPIRE_MACHNES,
    DECOMPIRE_MACHINES,
    REVERSE_DATA_TIME,
    CHANGE_DATABASE_ORDER,
    SET_MOUNTLY_DATABASE,
    DatabaseData,
    SET_ADMIN_ACCESS, SET_TRACKED_MACHINES, SET_LOGIN_BY_SESSION, EDIT_INFO,
} from '@/store';

export function addData(data: DatabaseData): void {
    store.commit(ADD_DATA, data);
}

export function setData(data: MachineData[]): void {
    store.commit(SET_REAL_DATA, data);
}

export function deleteData(): void {
    store.commit(DELETE_DATA);
}

export function createTable(data: any): void {
    store.commit(CREATE_TABLE, data);
}

export function editName(): void {
    store.commit(EDIT_NAME);
}

export function LogIn(data: {}): void {
    store.commit(LOG_IN, data);
}

export function logOut(): void {
    store.commit(LOG_OUT)
}

export function registerNewUser(data: {}): void {
    store.commit(REGISTER_USER, data);
}

export function deleteWarning(data: boolean): void {
    store.commit(DELETE_WARNING, data)
}

export function setLoading(): void {
    store.commit(SET_LOADING);
}

export function removeInfo(index: number | string): void {
    console.log('from muttations');
    store.commit(REMOVE_INFO, index);
}

export function editInfo(data: editData) {
    store.commit(EDIT_INFO, data);
}

export function editEntry(data: []): void {
    store.commit(EDIT_DATA, data)
}

export function compireMachines(): void {
    store.commit(COMPIRE_MACHNES);
}

export function decompireMachines(): void {
    store.commit(DECOMPIRE_MACHINES);
}

export function reverseDataTime(): void {
    store.commit(REVERSE_DATA_TIME);
}
// export function loadSheets(): void {
//     store.commit(LOAD_SHEET)
// }

export function removeDataFromOrderedDatabase(data: number) {
    store.commit(CHANGE_DATABASE_ORDER, data)
}

export function setMountlyDatabase(data: any[]) {
    store.commit(SET_MOUNTLY_DATABASE, data);
}

export function setAdminAccess(data: boolean) {
    store.commit(SET_ADMIN_ACCESS, data);
}

export function setLoginBySession(data: boolean) {
    store.commit(SET_LOGIN_BY_SESSION, data);
}

export function setTrackedMachines(data: any[] | undefined) {
    store.commit(SET_TRACKED_MACHINES, data);
}
