import Vue from 'vue';
import Vuex, {ActionTree, GetterTree, MutationTree, StoreOptions} from 'vuex';
import axios from 'axios';
import {stubTrue} from "lodash-es";

Vue.use(Vuex);

// Getters
export const VIEW_DATABASE: string = 'VIEW_DATABASE';
export const VIEW_INFO: string = 'VIEW_INFO';
export const GET_LOADING: string = 'GET_LOADING';
export const GET_LOGIN: string = 'GET_LOGIN';
export const GET_EMAIL: string = 'GET_EMAIL';
export const GET_WARNING_REGISTRATION: string = 'GET_WARNING_REGISTRATION';
export const GET_LOGIN_EMAIL: string = 'GET_LOGIN_EMAIL';
export const GET_SORTED_RULE: string = 'GET_SORTED_RULE';
export const GET_MOUNTLY_DATABASE: string = 'GET_MOUNTLY_DATABASE';
export const GET_ADMIN: string = 'GET_ADMIN';
export const GET_MAINTENANCE: string = "GET_MAINTENANCE";


// Mutations
export const ADD_DATA: string = 'ADD_DATA';
export const SET_REAL_DATA: string = 'SET_REAL_DATA';
export const DELETE_DATA: string = 'DELETE_DATA';
export const CREATE_TABLE: string = 'CREATE_TABLE';
export const EDIT_NAME: string = 'EDIT_NAME';
export const LOG_IN: string = 'LOG_IN';
export const LOG_OUT: string = 'LOG_OUT';
export const REGISTER_USER: string = 'REGISTER_USER';
export const DELETE_WARNING: string = 'DELETE_WARNING';
export const SET_LOADING: string = 'SET_LOADING';
export const REMOVE_INFO: string = 'REMOVE_INFO';
export const SET_LOGIN_USERS: string = 'SET_LOGIN_USERS';
export const EDIT_DATA: string = 'EDIT_DATA';
export const COMPIRE_MACHNES: string = 'COMPIRE_MACHNES';
export const DECOMPIRE_MACHINES: string = 'DECOMPIRE_MACHINES';
export const REVERSE_DATA_TIME: string = 'REVERSE_DATA_TIME';
export const CHANGE_DATABASE_ORDER: string = 'CHANGE_DATABASE_ORDER';
export const SET_MOUNTLY_DATABASE: string = 'SET_MOUNTLY_DATABASE';
export const SET_ADMIN_ACCESS: string = 'SET_ADMIN_ON';


export interface MachineData {
    id: string
    name: string
    startDate: string
    startHour: string
    endDate: string
    endHour: string
    indexId: number
}


export interface DatabaseData {
    id: number
    name: string
    registration: string
    firstKm: number
    lastKm: number
    workerName: string
    workedPlace: string
    machineDate: string
}

export interface RootState {
    machineDatabase: DatabaseData[];
    machineData: MachineData[];
    sortedMachines: MachineData[];
    idForMachine: number;
    userLogInData: User;
    isLogin: boolean;
    newRegistredUsers: any[any];
    idForNewRegistredUsers: number;
    warningRegistration: boolean;
    isLoading: boolean;
    isAdmin: boolean;
    getLoginEmail: string;
    setLoginUsers: any[];
    sortedRule: boolean;
    databaseOrder: number;
    mountlyDatabase: any[];
    viewMaintenance: any[];
}

const state: RootState = {
    machineDatabase: [{
        id: 1,
        name: 'IVEKO',
        registration: 'CA5039KC',
        firstKm: 300000,
        lastKm: 305000,
        workerName: 'Bai Pesho',
        workedPlace: 'nqkade tam',
        machineDate: '2020-05-18',
    }],
    machineData: [
        {
            name: "Валяк ASC 150 (5-11)",
            indexId: 39,
            id: "00985",
            startDate: `2020-06-26`,
            startHour: `11:32:00`,
            endDate: `2020-07-31`,
            endHour: `09:37:00`
        },
        {
            name: "Валяк VV-100 (5-5)",
            indexId: 36,
            id: "00986",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"

        },
        {
            name: "BH 6832 ВОЛВО",
            indexId: 15,
            id: "00987",
            startDate: "2020-07-07",
            startHour: "14:24:00",
            endDate: "2020-07-31",
            endHour: `15:10:00`

        },
        {
            name: "САМ.2-ОС/СА 4392/CB0679",
            indexId: 0,
            id: "012474",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"

        },
        {
            name: "ТОВАРАЧ KOMATSU WA250-1",
            indexId: 49,
            id: "02057",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "БАГЕР ВЕРИЖЕН KOMATSU PC 340LC",
            indexId: 50,
            id: "054896",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "Грейдер Sanny С 11687",
            indexId: 47,
            id: "05651",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ВОЛВО ВН3658АВ",
            indexId: 26,
            id: "065634",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "СПЕЦИАЛЕН - ЗИМНО CA6242",
            indexId: 0,
            id: "065635",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "СПЕЦИАЛЕН - ЗИМНО CA0757",
            indexId: 0,
            id: "065746",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "4_ИВЕКО CA6598КТ",
            indexId: 4,
            id: "066000",
            startDate: "2020-06-29",
            startHour: `08:51:00`,
            endDate: "2020-08-03",
            endHour: `06:49:00`
        }
        ,
        {
            name: "МЕРЦЕДЕС CA5046ХА",
            indexId: 9,
            id: "066003",
            startDate: "2020-07-01",
            startHour: `08:00:00`,
            endDate: "2020-07-21",
            endHour: `08:00:00`
        }
        ,
        {
            name: "МЕРЦЕДЕС BH1760АА / ВН8651АК",
            indexId: 17,
            id: "066010",
            startDate: "2020-07-01",
            startHour: `12:00:00`,
            endDate: "2020-07-31",
            endHour: `10:50:00`
        }
        ,
        {
            name: "3_ИВЕКО CA5039КС",
            indexId: 3,
            id: "066058",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МЕРЦЕДЕС CA3260РМ",
            indexId: 8,
            id: "066082",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МЕРЦЕДЕС BH0944АА",
            indexId: 6,
            id: "066090",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МЕРЦЕДЕС СA6492ХА",
            indexId: 11,
            id: "066093",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "JCB BH03553 (4-7)",
            indexId: 34,
            id: "066120",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "КОМАЦУ WA 250-2",
            indexId: 0,
            id: "070490",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ВОЛВО ВН3660АВ",
            indexId: 25,
            id: "07502",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МАН СА9785ХА",
            indexId: 19,
            id: "07514",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "БАГЕР КОМАЦУ 220/ С12175",
            indexId: 0,
            id: "075180",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ВОЛВО ВН3659АВ",
            indexId: 24,
            id: "075556",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ДАФ CA8718XA",
            indexId: 18,
            id: "076031",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ИВЕКО CA4042PM",
            indexId: 20,
            id: "076298",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "5_ИВЕКО СА6599КТ",
            indexId: 5,
            id: "076430",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "2_ИВЕКО CA0817KT",
            indexId: 2,
            id: "076434",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "1_ИВЕКО CA0816KT",
            indexId: 1,
            id: "076437",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ДАФ BH3018AB",
            indexId: 22,
            id: "076444",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ЩАЕР CA4052PM",
            indexId: 14,
            id: "076445",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "Т-150 BH04237",
            indexId: 42,
            id: "076464",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МЕРЦЕДЕС BH3021AB",
            indexId: 23,
            id: "076466",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "РЕНО CA3263PM",
            indexId: 12,
            id: "076467",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МЕРЦЕДЕС CA2537РМ",
            indexId: 7,
            id: "076473",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МАН BH8971АХ",
            indexId: 27,
            id: "076481",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ВОЛВО BH8972AB",
            indexId: 28,
            id: "076482",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ДАФ CA6913XA",
            indexId: 16,
            id: "076676",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МЕРЦЕДЕС CB6245KA",
            indexId: 10,
            id: "076677",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "МЕРЦЕДЕС CA4051PM",
            indexId: 13,
            id: "076679",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "4-8 БАГЕР КОМАЦУ BH03555",
            indexId: 35,
            id: "076681",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ФРЕЗА ВИРГЕН C13550",
            indexId: 48,
            id: "077055",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ИВЕКО CA4045PM",
            indexId: 21,
            id: "077056",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "АСФАЛТОПОЛАГАЧ ABG-473",
            indexId: 51,
            id: "07729",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "БАГЕР КЕЙС BH04078 (4-1",
            indexId: 32,
            id: "077379",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ТРАКТОР ТК 150 BH02593",
            indexId: 43,
            id: "077419",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "РОТОРЕН СНЕГОРИН",
            indexId: 0,
            id: "077429",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ВАЛЯК ХАМ ХД90 BH04014 (5-9)",
            indexId: 38,
            id: "077732",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "Булдозер Komatsu 155AX-2 (2-1)",
            indexId: 0,
            id: "09784",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "C9539EP",
            indexId: 0,
            id: "1000106",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "1-1 L-34 C 04657",
            indexId: 30,
            id: "1000124",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ВАЛЯК ХАМ C11692 5-4",
            indexId: 0,
            id: "1000133",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        }
        ,
        {
            name: "ФРЕЗА БИТЕЛИ C04661",
            indexId: 44,
            id: "1000137",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "4-2 БАГЕР КЕЙС",
            indexId: 33,
            id: "1000224",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "C 11694 FOGELLE !!!",
            indexId: 41,
            id: "1000263",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "5-8 BOMAG C 11693",
            indexId: 37,
            id: "1000558",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "ТОРНАДО-МОБИЛНА ТРОШАЧКА",
            indexId: 46,
            id: "1001076",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "ПРЕСЕВНА ТЕРЕКС",
            indexId: 45,
            id: "1001605",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "5-12 Aman ROLLER",
            indexId: 40,
            id: "1001639",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "1-3 ЧЕЛ. ТОВ. ХЮНДАЙ C 02950",
            indexId: 31,
            id: "1001717",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "BH 4162 BX",
            indexId: 0,
            id: "1001921",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "CA 9706 MB",
            indexId: 29,
            id: "1002105",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "БАГЕР КОМБИНИРАН CASE C01264",
            indexId: 52,
            id: "99999",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "БАГЕР ХИТАЧИ С15707",
            indexId: 53,
            id: "1000454",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },
        {
            name: "БАГЕР КАТЕРПИЛАР С00871",
            indexId: 54,
            id: "073986",
            startDate: "2020-07-01",
            startHour: "00:00:01",
            endDate: "2020-07-30",
            endHour: "15:00:00"
        },


    ],
    sortedMachines: [],
    idForMachine: 1,
    userLogInData: {email: 'pavel@pavel.bg', password: '123'},
    // default need to be false. Now just in developer mode is ok !
    isLogin: true,
    isAdmin: true,
    newRegistredUsers: [],
    idForNewRegistredUsers: 0,
    warningRegistration: false,
    // default need to be false. Now just in developer mode is ok !
    isLoading: true,
    getLoginEmail: 'test',
    setLoginUsers: [],
    sortedRule: false,
    databaseOrder: 0,
    mountlyDatabase: [],
    viewMaintenance: [],
};

const getters: GetterTree<RootState, RootState> = {
    [VIEW_INFO](state: RootState): Object | undefined {
        if (state.sortedMachines.length < state.machineData.length) {
            return state.machineData;
        } else if (state.sortedMachines.length === state.machineData.length) {
            return state.sortedMachines;

        }
    },
    [VIEW_DATABASE](state: RootState): Object | any {
        return state.machineDatabase


    },
    [GET_LOGIN](state: RootState): boolean {
        return state.isLogin;
    },
    [GET_EMAIL](state: RootState): string {
        return state.userLogInData.email;
    },
    [GET_WARNING_REGISTRATION](state: RootState): boolean {
        return state.warningRegistration;
    },
    [GET_LOADING](state: RootState): boolean {
        return state.isLoading;
    },
    [GET_LOGIN_EMAIL](state: RootState): string {
        return state.getLoginEmail;
    },
    [GET_SORTED_RULE](state: RootState): boolean {
        return state.sortedRule;
    },
    [GET_MOUNTLY_DATABASE](state: RootState): Object | undefined {
        return state.mountlyDatabase;
    },
    [GET_ADMIN](state: RootState): Object | undefined {
        return state.isAdmin;
    },
    // viewMaintenance: object[] = [];

//
// getMaintenance(parameter: any) {
//     this.viewMaintenance = [];
//     console.log("PARAMETER", parameter);
//     axios.get('/find_maintenance.php', {params: parameter})
//         .then((res) => {
//             console.log('from database', res.data);
//             // return this.viewDatabase = res.data;
//             for (let key of res.data) {
//                 this.viewMaintenance.push(
//                     {
//                         id: key.id,
//                         registration: key.registration,
//                         start_date: key.start_date,
//                         end_date: key.end_date
//                     }
//                 )
//             }
//         })
//         .catch((err) => {
//             console.log(err);
//         })
// }
    [GET_MAINTENANCE](state: RootState, payload): void {
        axios.get('find_maintenance.php', {params: payload})
            .then((res) => {
                for (let key of res.data) {
                    state.viewMaintenance.push({
                        id: key.id,
//                         registration: key.registration,
//                         start_date: key.start_date,
//                         end_date: key.end_date
                    })
                }
            })
    }

};

const mutations: MutationTree<RootState> = {

    [REVERSE_DATA_TIME](state: RootState) {
        for (let i = 0; i < state.machineData.length; i++) {
            state.machineData[i].startDate = state.machineData[i].endDate;
            state.machineData[i].startHour = state.machineData[i].endHour;

            state.machineData[i].endDate = '';
            state.machineData[i].endHour = '';


        }
    },

    [COMPIRE_MACHNES](state: RootState) {

        function compare(a: any, b: any) {
            if (a.indexId < b.indexId) {
                return -1;
            }
            if (a.indexId > b.indexId) {
                return 1;
            }
            return 0;
        }


        // @ts-ignore
        state.sortedMachines = state.machineData.sort(compare);
        console.log('Machine data-------> ', state.machineData);
        state.sortedRule = true;
    },
    [DECOMPIRE_MACHINES](state: RootState) {
        state.sortedMachines = state.machineData;
        state.sortedRule = false;
    },

    [ADD_DATA](state: RootState, payload: DatabaseData) {
        console.log(payload);

        console.log('from payload', payload);
        return state.machineDatabase.push({
            ...payload,
        });
    },

    [EDIT_DATA](state: RootState, payload: []) {
        console.log(payload)

        // @ts-ignore
        const fieldName = payload[2];
        // @ts-ignore
        const index = payload[0];
        // @ts-ignore
        const newValue = payload[1];

        // @ts-ignore
        state.machineData[index][fieldName] = newValue

    },

    [SET_ADMIN_ACCESS](state: RootState, payload): boolean {
        if (payload === true) {
            return state.isAdmin = true;
        } else {
            return state.isAdmin = false;
        }
    },


    [SET_REAL_DATA](state: RootState, payload: MachineData[]) {
        console.log('This is from payload', payload);
        return state.machineData = [
            ...payload
        ]
    },
    [SET_LOGIN_USERS](state: RootState, payload: {}) {
        return state.setLoginUsers.push({
            ...payload
        });
    },
    [DELETE_DATA](state: RootState) {
        state.machineData = [];
    },
    [DELETE_WARNING](state: RootState, payload) {
        state.warningRegistration = payload;
    },
    [CREATE_TABLE](state: RootState) {
        let xlsx = require('xlsx');

        let jsonMachineData = JSON.stringify(this.psm);

        let data = xlsx.utils.sheet_to_json(jsonMachineData);

        let newData = data.map((record: any) => {
            record.Net = record.Sales - record.Cost;
            return record;
        });

        let newWB = xlsx.utils.book_new();
        newWB.Props = {
            Title: 'SheetJS tutorial',
            Subject: 'Register.vue',
            Author: 'Pavel',
            CreatedDate: new Date(2020, 6, 20),
        };
        newWB.SheetNames.push('Test_Sheet');
        let pureArrayDataFromMachineData: any = [
            [
                ['ID'], ['NAME'], ['START DATE'], ['START HOUR'], ['END DATE'], ['END HOUR'],
                ['INDEX ID']
            ]
        ];
        // @ts-ignore
        let arayToArrayFromMachineData = (): [] => {
            for (let data of state.machineData) {
                pureArrayDataFromMachineData.push([[data.id], [data.name], [data.startDate],
                    [data.startHour], [data.endDate], [data.endHour], [data.indexId]]);
            }

            return pureArrayDataFromMachineData;
        }

        newWB.Sheets['Test_Sheet'] = xlsx.utils.aoa_to_sheet(arayToArrayFromMachineData());

        let newWS = xlsx.utils.json_to_sheet(newData);
        xlsx.utils.book_append_sheet(newWB, newWS, 'machine_DATA');

        xlsx.writeFile(newWB, 'tableBase.xlsx');
    },
    [EDIT_NAME](state: RootState) {
        console.log(state);
        console.log('edit Todo');
    },

    [LOG_IN](state: RootState, payload) {
        let email = payload.email;

        function validateEmail(email: string) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                console.log('email OK')
                return (true);
            }
            state.isLoading = false;
            state.warningRegistration = true;
            return (false);
        }

        validateEmail(email);
        console.log(payload);
        console.log(state.userLogInData);

        let verifyedLogin = false;
        let findUserData = state.newRegistredUsers.find((element: any): any => element = payload);
        if (findUserData) {
            verifyedLogin = true;
        }
        state.getLoginEmail = email;
        console.log('Login OK');
        // tslint:disable-next-line:no-unused-expression
        return state.isLogin = true;

    },
    [LOG_OUT](state: RootState) {
        state.isLogin = false;
    },
    [REGISTER_USER](state: RootState, payload) {
        let email = payload.email;

        function validateEmail(email: string) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                state.newRegistredUsers.push(payload);
                console.log(state.newRegistredUsers);
                state.isLoading = false;
                return (true);
            } else {
                state.warningRegistration = true;
                return (false);
            }
        }

        let findUserData = state.newRegistredUsers.find((element: any): any => element = payload);
        if (findUserData === payload) {
            console.log('User already registered!');
            return;
        } else {
            validateEmail(email);
            return axios.post('http://localhost:3000/users', {
                id: state.idForNewRegistredUsers++,
                email: payload.email,
                password: payload.password,
            });
        }
    },
    [SET_LOADING](state: RootState) {
        state.isLoading = !state.isLoading;
    },

    [CHANGE_DATABASE_ORDER](state: RootState, payload): void {

        return state.databaseOrder = payload;
    },

    [REMOVE_INFO](state: RootState, payload) {
        if (state.databaseOrder === 1) {
            state.machineData.splice(payload, 1);
            axios.post('/remove_machine.php', {
                registration: payload
            })
                .then((res) => {
                    console.log(res)
                })
                .catch((err) => {
                    console.log(err);
                })

        } else {
            if (state.databaseOrder === 2) {
                console.log("remove info:", payload);
                state.machineData.splice(payload, 1);
                axios.post('/remove_worker.php', {
                    worker: payload
                })
                    .then((res) => {
                        console.log(res)
                    })
                    .catch((err) => {
                        console.log(err);
                    })

            } else {
                if (state.databaseOrder === 3) {
                    console.log("remove info:", payload);
                    state.machineData.splice(payload, 1);
                    axios.post('/remove_place.php', {
                        place: payload
                    })
                        .then((res) => {
                            console.log(res)
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                }
            }
        }
    },
    [SET_MOUNTLY_DATABASE](state: RootState, payload) {
        state.mountlyDatabase = payload
    }
};

const actions: ActionTree<RootState, RootState> = {};

const debug = process.env.NODE_ENV !== 'production';

const store: StoreOptions<RootState> = {
    state,
    getters,
    mutations,
    actions,
    strict: debug,
};

export default new Vuex.Store<RootState>(store);
