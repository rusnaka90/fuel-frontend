import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store, {GET_LOGIN} from './store';
import { logErrorEvent, logVueError } from '@/helpers/error.ts';
import './components';

// tslint:disable-next-line:only-arrow-functions
Vue.config.errorHandler = function(err, vm, info) {
    console.error(err);
    logVueError(err, vm, info);
};

window.addEventListener('error', (e: ErrorEvent) => {
    console.error(e);
    logErrorEvent(e);
});


Vue.config.productionTip = false;

require ('bulma/css/bulma.css');
// import "bulma-switch/dist/css/bulma-switch.min.css";
require("bulma-switch/dist/css/bulma-switch.min.css");
require("bulma-calendar/dist/css/bulma-calendar.min.css");

router.beforeEach((to, from, next) => {
    const isLogged = store.getters[GET_LOGIN];
    const anonymous = to.meta && to.meta.anonymous;

    // if (!isLogged && !anonymous && to.name !== 'Login') {
    //     return next({name: 'AccessHelp'});
    // }
    //
    // if(!isLogged && to.name !== 'Login') {
    //     return next({name: 'Login'});
    // }

    next();
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');


