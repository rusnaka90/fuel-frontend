import {RawLocation} from 'vue-router'

export interface BulmaMenuItemWithRouterLink {
    to: RawLocation
}
export interface BulmaMenuItemWithHref {
    href: string
}

export type BulmaMenuItemTypes = BulmaMenuItemWithRouterLink | BulmaMenuItemWithHref

export type BulmaMenuItem = BulmaMenuItemTypes & {
    label: string
    children?: BulmaMenuItem[]
}

export interface BulmaMenuList {
    label?: string
    items: BulmaMenuItem[]
}

export interface BulmaSelectOption extends Object {
    value: string | number;
    name: string;
}
