declare interface Data {
    id: number;
    // name: string;
    machine_id: number;
    firstKm: number;
    lastKm: number;
    worker_id: number;
    place_id: number;
    machineDate: string;

    // todo
    // otherKmOne: number;
    // otherPlaceOne: string;
    // otherKmTwo: number;
    // otherPlaceTwo: string;
    // otherKmThree: number;
    // otherPlaceThree: string;
}

declare interface machineData {
    machineName_id: number | null,
    machineRegistration_id: number | null,
    first_km: number | null,
    last_km: number | null,
    workerName_id: number | null,
    place_id: number | null,
    machineDate: string
}

declare interface editData {
    id: number | null,
    registration: string | null,
    name: string | null,
    type: string | null,
    index_id: number | null
}



// machineName: number | null = null;
// machineRegistrationNumber: number | null = null;
// firstKM: number = 0;
// lastKM: number = 0;
// workerName: number | null = null;
// workedPlace: number | null = null;
// machineDate: string = "";
declare interface User {
    email: string;
    password: string;
}

declare interface AuditMachine {
    plate: string
    largestColumn: number
    work: Record<string, DriverWork[]>
}
declare interface DriverWork {
    worker: string
    location: string
    km: number
}
type Audit = Record<string, AuditMachine>;


declare interface Maintenance {
    id: number,
    registration: string,
    start_date: string,
    end_date: string,
    kilometers: number | undefined,
    additional_info: string | undefined
}




