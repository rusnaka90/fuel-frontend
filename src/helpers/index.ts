
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
export function isNumeric(number: any): boolean {
    return !isNaN(number) && null !== number
}
