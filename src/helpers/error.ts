import Vue, { VNode } from 'vue';

export function logVueError(err: Error, vm: Vue, info: string): void {
    const data = {
        component: JSON.stringify(serializeVm(vm)),
        trace: JSON.stringify({
            name: err.name,
            message: err.message,
            trace: err.stack || '',
        }),
        info,
        url: location.href,
    };

    console.error(
            err,
            ...Object.values(data)
        );
}

function serializeVm(vm: Vue): Object {
    return {
        vnode: serializeVnode(vm.$vnode),
        props: vm.$props,
        children: vm.$children && vm.$children.map((child) => serializeVm(child))
    };
}

function serializeVnode(node: VNode): Object {
    return {
        tag: node.tag,
        key: node.key,
        children: node.children && node.children.map((node) => {
            return serializeVnode(node);
        })
    }
}

export function logError(
    e: Error
) {
    let type = 'unknown';
    if (e.constructor && e.constructor.name) {
        type = e.constructor.name
    }

    // @ts-ignore
    const fileName = e.fileName || '';
    // @ts-ignore
    const lineNumber = e.lineNumber || '';
    // @ts-ignore
    const columnNumber = e.columnNumber || '';

    const data = {
        component: type,
        trace: JSON.stringify(((error: any) => {
            if (error instanceof Error) {
                return {
                    name: e.name,
                    message: e.message,
                    trace: e.stack || '',
                }
            }

            return error
        })(e)),
        info: `Error: ${e.message} @ File ${fileName}:${lineNumber}:${columnNumber}`,
        url: location.href,
    };

    console.error(
            e,
            ...Object.values(data)
        );
}

export function logErrorEvent(e: ErrorEvent) {
    let type = 'unknown';
    if (e.target instanceof HTMLElement) {
        type = e.target.nodeName;
    }

    const data = {
        component: type,
        trace: JSON.stringify(((error: any) => {
            if (error instanceof Error) {
                return {
                    name: e.error.name,
                    message: e.error.message,
                    trace: e.error.stack || '',
                }
            }

            return error
        })(e.error)),
        info: `Error: ${e.message} @ File ${e.filename}:${e.lineno}:${e.colno}`,
        url: location.href,
    };

    console.error(
            e.error,
            ...Object.values(data)
        );
}
