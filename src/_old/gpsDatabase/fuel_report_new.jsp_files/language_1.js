//-----------------------------------------------------------------------------
// Превод на програмата
//-----------------------------------------------------------------------------


var texts = new Array();
texts['bg'] = new Object();
texts['en'] = new Object();
texts['de'] = new Object();
texts['ro'] = new Object();
texts['es'] = new Object();
texts['sr'] = new Object();
texts['gr'] = new Object();




texts['bg'].january = "Януари";
texts['en'].january = "January";
texts['de'].january = "Januar"; 
texts['ro'].january = "Ianuarie"; 
texts['es'].january = "Enero."; 
texts['sr'].january = "Januar"; 
texts['gr'].january = "Ιανουάριος";

texts['bg'].february = "Февруари";
texts['en'].february = "February";
texts['de'].february = "Februar"; 
texts['ro'].february = "Februarie"; 
texts['es'].february = "Febrero"; 
texts['sr'].february = "Februar"; 
texts['gr'].february = "Φεβρουάριος";

texts['bg'].march = "Март";
texts['en'].march = "March";
texts['de'].march = "Marz";
texts['ro'].march = "Martie";
texts['es'].march = "Marzo";
texts['sr'].march = "Mart";
texts['gr'].march = "Μάρτιος";

texts['bg'].april = "Април";
texts['en'].april = "April";
texts['de'].april = "April"; 
texts['ro'].april = "Aprilie"; 
texts['es'].april = "Abril";
texts['sr'].april = "April";
texts['gr'].april = "Απρίλιος";

texts['bg'].may = "Май";
texts['en'].may = "May";
texts['de'].may = "Mai"; 
texts['ro'].may = "Mai"; 
texts['es'].may = "Mayo"; 
texts['sr'].may = "Maj";
texts['gr'].may = "Μάιος";

texts['bg'].june = "Юни";
texts['en'].june = "June";
texts['de'].june = "Juni"; 
texts['ro'].june = "Iunie"; 
texts['es'].june = "Junio"; 
texts['sr'].june = "Jun";
texts['gr'].june = "Ιούνιος";

texts['bg'].july = "Юли";
texts['en'].july = "July";
texts['de'].july = "Juli";  
texts['ro'].july = "Iulie";  
texts['es'].july = "Julio";  
texts['sr'].july = "Jul";
texts['gr'].july = "Ιούλιος";

texts['bg'].august = "Август";
texts['en'].august = "August";
texts['de'].august = "August"; 
texts['ro'].august = "August"; 
texts['es'].august = "Agosto";
texts['sr'].august = "Avgust";
texts['gr'].august = "Αύγουστος";

texts['bg'].september = "Септември";
texts['en'].september = "September";
texts['de'].september = "September"; 
texts['ro'].september = "Septembrie"; 
texts['es'].september = "Septiembre"; 
texts['sr'].september = "Septembar"; 
texts['gr'].september = "Σεπτέμβριος";

texts['bg'].october = "Октомври";
texts['en'].october = "October";
texts['de'].october = "Oktober";
texts['ro'].october = "Octombrie"; 
texts['es'].october = "Octubre"; 
texts['sr'].october = "Oktobar";
texts['gr'].october = "Οκτώβριος"; 

texts['bg'].november = "Ноември";
texts['en'].november = "November";
texts['de'].november = "November"; 
texts['ro'].november = "Noiembrie"; 
texts['es'].november = "Noviembre";
texts['sr'].november = "Novembar";
texts['gr'].november = "Νοέμβριος";

texts['bg'].december = "Декември";
texts['en'].december = "December";
texts['de'].december = "Dezember";
texts['ro'].december = "Decembrie";
texts['es'].december = "Diciembre";
texts['sr'].december = "Decembar";
texts['gr'].december = "Δεκέμβριος";

texts['bg'].sunday = "Неделя";
texts['en'].sunday = "Sunday";
texts['de'].sunday = "Sonntag"; 
texts['ro'].sunday = "Duminică"; 
texts['es'].sunday = "Domingo"; 
texts['sr'].sunday = "Nedelja";
texts['gr'].sunday = "Κυριακή"; 

texts['bg'].monday = "Понеделник";
texts['en'].monday = "Monday";
texts['de'].monday = "Montag";
texts['ro'].monday = "Luni";
texts['es'].monday = "Lunes";
texts['sr'].monday = "Ponedeljak";
texts['gr'].monday = "Δευτέρα";

texts['bg'].tuesday = "Вторник";
texts['en'].tuesday = "Tuesday";
texts['de'].tuesday = "Dienstag"; 
texts['ro'].tuesday = "Marți"; 
texts['es'].tuesday = "Martes"; 
texts['sr'].tuesday = "Utorak";
texts['gr'].tuesday = "Τρίτη";

texts['bg'].wednesday = "Сряда";
texts['en'].wednesday = "Wednesday";
texts['de'].wednesday = "Mittwoch";
texts['ro'].wednesday = "Miercuri";
texts['es'].wednesday = "Miércoles";
texts['sr'].wednesday = "Sreda";
texts['gr'].wednesday = "Τετάρτη";

texts['bg'].thursday = "Четвъртък";
texts['en'].thursday = "Thursday";
texts['de'].thursday = "Donnerstag";
texts['ro'].thursday = "Joi";
texts['es'].thursday = "Jueves";
texts['sr'].thursday = "Četvrtak";
texts['gr'].thursday = "Πέμπτη";

texts['bg'].friday = "Петък";
texts['en'].friday = "Friday";
texts['de'].friday = "Freitag"; 
texts['ro'].friday = "Vineri"; 
texts['es'].friday = "Viernes"; 
texts['sr'].friday = "Petak";
texts['gr'].friday = "Παρασκευή";

texts['bg'].saturday = "Събота";
texts['en'].saturday = "Saturday";
texts['de'].saturday = "Samstag"; 
texts['ro'].saturday = "Sâmbăta"; 
texts['es'].saturday = "Sabado"; 
texts['sr'].saturday = "Subota";
texts['gr'].saturday = "Σάββατο"; 

texts['bg'].vehicle = "превозно средство";
texts['en'].vehicle = "vehicle";
texts['de'].vehicle = "Fahrzeug"; 
texts['ro'].vehicle = "Vehicul"; 
texts['es'].vehicle = "vehículo";
texts['sr'].vehicle = "prevozno sredstvo";
texts['gr'].vehicle = "όχημα";

texts['bg'].vehicles = "превозни средства";
texts['en'].vehicles = "vehicles";
texts['de'].vehicles = "Fahrzeuge"; 
texts['ro'].vehicles = "Vehicule"; 
texts['es'].vehicles = "vehículos"; 
texts['sr'].vehicles = "prevozna sredstva"; 
texts['gr'].vehicles = "οχήματα";

texts['bg'].c_vehicle = "Превозно средство";
texts['en'].c_vehicle = "Vehicle";
texts['de'].c_vehicle = "Fahrzeug"; 
texts['ro'].c_vehicle = "Vehicul"; 
texts['es'].c_vehicle = "Vehículo"; 
texts['sr'].c_vehicle = "Prevozno sredstvo"; 
texts['gr'].c_vehicle = "Όχημα";

texts['bg'].c_vehicles = "Превозни средства";
texts['en'].c_vehicles = "Vehicles";
texts['de'].c_vehicles = "Fahrzeuge"; 
texts['ro'].c_vehicles = "Vehicule"; 
texts['es'].c_vehicles = "Vehículos"; 
texts['sr'].c_vehicles = "Prevozna sredstva"; 
texts['gr'].c_vehicles = "Οχήματα";

texts['bg'].close = "затвори";
texts['en'].close = "close";
texts['de'].close = "Schliessen"; 
texts['ro'].close = "Închide"; 
texts['es'].close = "cerrar"; 
texts['sr'].close = "zatvori"; 
texts['gr'].close = "κλείστε";

texts['bg'].c_close = "Затвори";
texts['en'].c_close = "Close";
texts['de'].c_close = "Schliessen"; 
texts['ro'].c_close = "Închide"; 
texts['es'].c_close = "Cerrar"; 
texts['sr'].c_close = "Zatvori"; 
texts['gr'].c_close = "Κλείστε";

texts['bg'].searchInput = "търсене";
texts['en'].searchInput = "search";
texts['de'].searchInput = "suchen"; 
texts['ro'].searchInput = " caută"; 
texts['es'].searchInput = "buscar"; 
texts['sr'].searchInput = "traži"; 
texts['gr'].searchInput = "αναζήτηση";

texts['bg'].c_searchInput = "Tърсене";
texts['en'].c_searchInput = "Search";
texts['de'].c_searchInput = "Suchen"; 
texts['ro'].c_searchInput = "Caută"; 
texts['es'].c_searchInput = "Buscar"; 
texts['sr'].c_searchInput = "Traži"; 
texts['gr'].c_searchInput = "Αναζήτηση";

texts['bg'].c_searchInput2 = "Tърсене...";
texts['en'].c_searchInput2 = "Search...";
texts['de'].c_searchInput2 = "Suchen..."; 
texts['ro'].c_searchInput2 = "Caută..."; 
texts['es'].c_searchInput2 = "Buscando..."; 
texts['sr'].c_searchInput2 = "Traži...";
texts['gr'].c_searchInput2 = "Αναζήτηση..."; 

texts['bg'].hideTable = "скрий таблицата";
texts['en'].hideTable = "hide table";
texts['de'].hideTable = "Tabelle Ausblenden"; 
texts['ro'].hideTable = "Ascunde tabelul";
texts['es'].hideTable = "ocultar tabla";
texts['sr'].hideTable = "sakrij tabelu";
texts['gr'].hideTable = "απόκρυψη πίνακα";

texts['bg'].c_hideTable = "Скрий таблицата";
texts['en'].c_hideTable = "Hide table";
texts['de'].c_hideTable = "Tabelle Ausblenden"; 
texts['ro'].c_hideTable = "Ascunde tabelul"; 
texts['es'].c_hideTable = "Ocultar tabla";
texts['sr'].c_hideTable = "Sakrij tabelu";
texts['gr'].c_hideTable = "Απόκρυψη πίνακα";

texts['bg'].reload = "обнови";
texts['en'].reload = "reload";
texts['de'].reload = "Aktualisieren"; 
texts['ro'].reload = "Actualizează ";
texts['es'].reload = "recargar ";
texts['sr'].reload = "obnovi ";
texts['gr'].reload = "ανανέωση";

texts['bg'].c_reload = "Обнови";
texts['en'].c_reload = "Reload";
texts['de'].c_reload = "Aktualisieren"; 
texts['ro'].c_reload = "Actualizează";
texts['es'].c_reload = "Recargar";
texts['sr'].c_reload = "Obnovi";
texts['gr'].c_reload = "Ανανέωση";

texts['bg'].reload2 = "обновяване";
texts['en'].reload2 = "reload";
texts['de'].reload2 = "Aktualisieren"; 
texts['ro'].reload2 = "Reîncarcare"; 
texts['es'].reload2 = "recargar"; 
texts['sr'].reload2 = "obnavljanje"; 
texts['gr'].reload2 = "ανανέωση";

texts['bg'].c_reload2 = "Обновяване";
texts['en'].c_reload2 = "Reload";
texts['de'].c_reload2 = "Aktualisieren"; 
texts['ro'].c_reload2 = "Reîncarcare"; 
texts['es'].c_reload2 = "Recargar"; 
texts['sr'].c_reload2 = "Obnavljanje"; 
texts['gr'].c_reload2 = "Ανανέωση";

texts['bg'].refreshReload = "обнови и презареди";
texts['en'].refreshReload = "refresh & reload";
texts['de'].refreshReload = "Aktualisieren"; 
texts['ro'].refreshReload = "Actualizează și reîncarcă"; 
texts['es'].refreshReload = "refrescar y recargar"; 
texts['sr'].refreshReload = "obnovi i osveži";
texts['gr'].refreshReload = "ανανέωση και επαναφόρτωση";

texts['bg'].c_refreshReload = "Обнови и презареди";
texts['en'].c_refreshReload = "Refresh & reload";
texts['de'].c_refreshReload = "Aktualisieren"; 
texts['ro'].c_refreshReload = "Actualizează și reîncarcă"; 
texts['es'].c_refreshReload = "Refrescar y recargar"; 
texts['sr'].c_refreshReload = "Obnovi i osveži"; 
texts['gr'].c_refreshReload = "Ανανέωση και επαναφόρτιση";

texts['bg'].radius = "радиус";
texts['en'].radius = "radius";
texts['de'].radius = "Radius"; 
texts['ro'].radius = "Rază"; 
texts['es'].radius = "radio"; 
texts['sr'].radius = "radijus"; 
texts['gr'].radius = "ακτίνα";

texts['bg'].c_radius = "Радиус";
texts['en'].c_radius = "Radius";
texts['de'].c_radius = "Radius";
texts['ro'].c_radius = "Rază";
texts['es'].c_radius = "Radio";
texts['sr'].c_radius = "Radijus";
texts['gr'].c_radius = "Ακτίνα";

texts['bg'].terrain = "терен";
texts['en'].terrain = "terrain";
texts['de'].terrain = "Gebiet"; 
texts['ro'].terrain = "Teren"; 
texts['es'].terrain = "terreno"; 
texts['sr'].terrain = "teren"; 
texts['gr'].terrain = "έδαφος"

texts['bg'].c_terrain = "Терен";
texts['en'].c_terrain = "Terrain";
texts['de'].c_terrain = "Gebiet"; 
texts['ro'].c_terrain = "Teren"; 
texts['es'].c_terrain = "Terreno"; 
texts['sr'].c_terrain = "Teren";
texts['gr'].c_terrain = "Εδαφος";

texts['bg'].data = 'данни';
texts['en'].data = 'data';
texts['de'].data = "daten"; 
texts['ro'].data = "date"; 
texts['es'].data = "datos"; 
texts['sr'].data = "podaci"; 
texts['gr'].data = 'δεδομένα';

texts['bg'].c_data = 'Данни';
texts['en'].c_data = 'Data';
texts['de'].c_data = "Daten"; 
texts['ro'].c_data = "Date"; 
texts['es'].c_data = "Datos"; 
texts['sr'].c_data = "Podaci";
texts['gr'].c_data = 'Δεδομένα'; 

texts['bg'].service = 'сервиз';
texts['en'].service = 'service';
texts['de'].service = 'service'; 
texts['ro'].service = 'service'; 
texts['es'].service = 'servicio'; 
texts['sr'].service = 'servis';
texts['gr'].service = 'σέρβις '; 

texts['bg'].c_service = 'Сервиз';
texts['en'].c_service = 'Service';
texts['de'].c_service = 'Service'; 
texts['ro'].c_service = 'Service'; 
texts['es'].c_service = 'Servicio'; 
texts['sr'].c_service = 'Servis';
texts['gr'].c_service = 'Σέρβις ';

texts['bg'].online = 'oнлайн';
texts['en'].online = 'online';
texts['de'].online = 'online'; 
texts['ro'].online = 'online'; 
texts['es'].online = 'en línea'; 
texts['sr'].online = 'onlajn'; 
texts['gr'].online = 'σε απευθείας σύνδεση';

texts['bg'].c_online = 'Онлайн';
texts['en'].c_online = 'Online';
texts['de'].c_online = 'Online'; 
texts['ro'].c_online = 'Online'; 
texts['es'].c_online = 'En línea'; 
texts['sr'].c_online = 'Onlajn';
texts['gr'].c_online = 'Σε απευθείας σύνδεση';

texts['bg'].notifications = "известия";
texts['en'].notifications = "notifications";
texts['de'].notifications = "Meldungen"; 
texts['ro'].notifications = "Notificări"; 
texts['es'].notifications = "notificaciónes"; 
texts['sr'].notifications = "obaveštenja"; 
texts['gr'].notifications = "ειδοποιήσεις";

texts['bg'].c_notifications = "Известия";
texts['en'].c_notifications = "Notifications";
texts['de'].c_notifications = "Meldungen"; 
texts['ro'].c_notifications = "Notificări"; 
texts['es'].c_notifications = "Notificaciónes";
texts['sr'].c_notifications = "Obaveštenja";
texts['gr'].c_notifications = "Ειδοποιήσεις";

texts['bg'].c_notification2 = "Известяване";
texts['en'].c_notification2 = "Notifications";
texts['de'].c_notification2 = ""; 
texts['ro'].c_notification2 = "Notificare"; 
texts['es'].c_notification2 = "Notificaciónes";  
texts['sr'].c_notification2 = "Obaveštenja"; 
texts['gr'].c_notification2 = "Ειδοποίηση"; 

texts['bg'].route = "маршрут";
texts['en'].route = "route";
texts['de'].route = "Route"; 
texts['ro'].route = "Rută";
texts['es'].route = "ruta";
texts['sr'].route = "maršruta";
texts['gr'].route = "διαδρομή";

texts['bg'].c_route = "Маршрут";
texts['en'].c_route = "Route";
texts['de'].c_route = "Route"; 
texts['ro'].c_route = "Rută";
texts['es'].c_route = "Ruta";
texts['sr'].c_route = "Maršruta";
texts['gr'].c_route = "Διαδρομή";

texts['bg'].c_service_data = "Сервизни данни";
texts['en'].c_service_data = "Service data";
texts['de'].c_service_data = "Service Daten"; 
texts['ro'].c_service_data = " Date Service"; 
texts['es'].c_service_data = "Datos de servicio"; 
texts['sr'].c_service_data = "Servisni podaci"; 
texts['gr'].c_service_data = "Δεδομένα υπηρεσίας";

texts['bg'].c_oil_change = "Смяна на масло";
texts['en'].c_oil_change = "Oil change";
texts['de'].c_oil_change = "Olwechsel"; 
texts['ro'].c_oil_change = "Schimbarea uleiului"; 
texts['es'].c_oil_change = "Cambio de aceite"; 
texts['sr'].c_oil_change = "Zamena ulja";
texts['gr'].c_oil_change = "Αλλαγή λαδιού";

texts['bg'].oil = "Масло";
texts['en'].oil = "Oil";
texts['de'].oil = ""; 
texts['ro'].oil = "Ulei"; 
texts['es'].oil = "Aceite"; 
texts['sr'].oil = "Ulje";
texts['gr'].oil = "Λάδι"; 

texts['bg'].tires_c = "Гуми";
texts['en'].tires_c = "Tires";
texts['de'].tires_c = ""; 
texts['ro'].tires_c = "Anvelope"; 
texts['es'].tires_c = "Neumáticos"; 
texts['sr'].tires_c = "Gume";
texts['gr'].tires_c = "Ελαστικά";

texts['bg'].c_civil_insurance = "Гражданска";
texts['en'].c_civil_insurance = "c. Insurance";
texts['de'].c_civil_insurance = "Versicherung";
texts['ro'].c_civil_insurance = "Asigurare auto";
texts['es'].c_civil_insurance = "Seguro civil de vehículo";
texts['sr'].c_civil_insurance = "Osiguranje građana";
texts['gr'].c_civil_insurance = "Αστική ασφάλιση";

texts['bg'].c_civil_insurance2 = "Гражданска застраховка";
texts['en'].c_civil_insurance2 = "Civil Liability Insurance";
texts['de'].c_civil_insurance2 = "Umweltplakette";
texts['ro'].c_civil_insurance2 = "Asigurare";
texts['es'].c_civil_insurance2 = "Seguro civil";
texts['sr'].c_civil_insurance2 = "Osiguranje građana od odgovornosti";
texts['gr'].c_civil_insurance2 = "Αστική ασφάλιση";

texts['bg'].c_autokasko = "Автокаско";
texts['en'].c_autokasko = "Vehicle Insurance";
texts['de'].c_autokasko = "Autokasko Versicherung"; 
texts['ro'].c_autokasko = "Auto CASCO"; 
texts['es'].c_autokasko = "Seguro"; 
texts['sr'].c_autokasko = "Osiguranje vozila"; 
texts['gr'].c_autokasko = "Ασφάλεια αυτοκινήτου";

texts['bg'].c_vignette = "Винетка";
texts['en'].c_vignette = "Vignette";
texts['de'].c_vignette = "Maut"; 
texts['ro'].c_vignette = "Rovineta"; 
texts['es'].c_vignette = "Telepeaje"; 
texts['sr'].c_vignette = "Vinjeta"; 
texts['gr'].c_vignette = "Βινιέτα";

texts['bg'].c_gtp = "Г.Т.П.";
texts['en'].c_gtp = "A.T.R.";
texts['de'].c_gtp = "ТUV"; 
texts['ro'].c_gtp = "I.T.P"; 
texts['es'].c_gtp = "I.T.V."; 
texts['sr'].c_gtp = "A.D.R.";
texts['gr'].c_gtp = "Κ.Τ.Ε.Ο."; 

texts['bg'].c_tachometer = "Тахограф";
texts['en'].c_tachometer = "Tachometer";
texts['de'].c_tachometer = "Tachokalibrierung"; 
texts['ro'].c_tachometer = "Tahograf"; 
texts['es'].c_tachometer = "Tacógrafo";  
texts['sr'].c_tachometer = "Tahograf";  
texts['gr'].c_tachometer = "Ταχογράφος";

texts['bg'].label = "етикет";
texts['en'].label = "label";
texts['de'].label = "Etikett"; 
texts['ro'].label = "Etichetă"; 
texts['es'].label = "etiqueta";
texts['sr'].label = "etiketa";
texts['gr'].label = "ετικέτα";

texts['bg'].c_label = "Етикет";
texts['en'].c_label = "Label";
texts['de'].c_label = "Etikett";
texts['ro'].c_label = "Etichetă";
texts['es'].c_label = "Etiqueta";
texts['sr'].c_label = "Etiketa";
texts['gr'].c_label = "Ετικέτα";

texts['bg'].description = "описание";
texts['en'].description = "description";
texts['de'].description = "Beschreibung"; 
texts['ro'].description = "Decriere";
texts['es'].description = "descripción";
texts['sr'].description = "opis";
texts['gr'].description = "περιγραφή";

texts['bg'].c_description = "Описание";
texts['en'].c_description = "Description";
texts['de'].c_description = "Beschreibung"; 
texts['ro'].c_description = "Descriere"; 
texts['es'].c_description = "Descripción"; 
texts['sr'].c_description = "Opis";
texts['gr'].c_description = "Περιγραφή";

texts['bg'].c_type = "Тип";
texts['en'].c_type = "Type";
texts['de'].c_type = "Typ"; 
texts['ro'].c_type = "Tip"; 
texts['es'].c_type = "tipo"; 
texts['sr'].c_type = "Tip";
texts['gr'].c_type = "Τύπος";

texts['bg'].type = "тип";
texts['en'].type = "type";
texts['de'].type = "Typ"; 
texts['ro'].type = "Tip"; 
texts['es'].type = "Tipo"; 
texts['sr'].type = "tip"; 
texts['gr'].type = "τύπος";

texts['bg'].rm = "р.(м)";
texts['en'].rm = "r.(m)";
texts['de'].rm = "r.(m)"; 
texts['ro'].rm = "r.(m)";
texts['es'].rm = "r.(m)";
texts['sr'].rm = "r.(m)";
texts['gr'].rm = "r.(m)";

texts['bg'].radius_m = "Радиус (м)";
texts['en'].radius_m = "Radius (m)";
texts['de'].radius_m = "Radius (m)"; 
texts['ro'].radius_m = "Rază (m)"; 
texts['es'].radius_m = "Radio (m)"; 
texts['sr'].radius_m = "Radijus (m)"; 
texts['gr'].radius_m = "Ακτίνα (м)";

texts['bg'].lat = "lat";
texts['en'].lat = "lat";
texts['de'].lat = "lat";
texts['ro'].lat = "lat";
texts['es'].lat = "lat";
texts['sr'].lat = "lat";
texts['gr'].lat = "lat";

texts['bg'].lng = "lng";
texts['en'].lng = "lng";
texts['de'].lng = "lng";
texts['ro'].lng = "lng";
texts['es'].lng = "lng";
texts['sr'].lng = "lng";
texts['gr'].lng = "lng";

texts['bg'].latitude = "гео-ширина";
texts['en'].latitude = "latitude";
texts['de'].latitude = "geo. Breite"; 
texts['ro'].latitude = "latitudine"; 
texts['es'].latitude = "latitud"; 
texts['sr'].latitude = "geografska širina"; 
texts['gr'].latitude = "γεωγραφικό πλάτος";

texts['bg'].longtitude = "гео-дължина";
texts['en'].longtitude = "longtitude";
texts['de'].longtitude = "geo. Lange"; 
texts['ro'].longtitude = "longitudine";
texts['es'].longtitude = "longitud";
texts['sr'].longtitude = "geografska dužina";
texts['gr'].longtitude = "γεωγραφικό μήκος";

texts['bg'].c_latitude = "Гео-ширина";
texts['en'].c_latitude = "Latitude";
texts['de'].c_latitude = "Geo. Breite"; 
texts['ro'].c_latitude = "Latitudine"; 
texts['es'].c_latitude = "Latitud"; 
texts['sr'].c_latitude = "Geografska širina"; 
texts['gr'].c_latitude = "Γεωγραφικό πλάτος";

texts['bg'].c_longtitude = "Гео-дължина";
texts['en'].c_longtitude = "Longtitude";
texts['de'].c_longtitude = "Geo. Lange"; 
texts['ro'].c_longtitude = "Longitudine"; 
texts['es'].c_longtitude = "Longitud"; 
texts['sr'].c_longtitude = "Geografska dužina"; 
texts['gr'].c_longtitude = "Γεωγραφικό μήκος";

texts['bg'].perimeter = "периметър";
texts['en'].perimeter = "perimeter";
texts['de'].perimeter = "perimeter"; 
texts['ro'].perimeter = "perimetru"; 
texts['es'].perimeter = "perímetro"; 
texts['sr'].perimeter = "obim"; 
texts['gr'].perimeter = "περίμετρο";

texts['bg'].c_perimeter = "Периметър";
texts['en'].c_perimeter = "Perimeter";
texts['de'].c_perimeter = "Perimeter"; 
texts['ro'].c_perimeter = "Perimetru"; 
texts['es'].c_perimeter = "Perímetro"; 
texts['sr'].c_perimeter = "Obim";
texts['gr'].c_perimeter = "Περίμετρο"; 

texts['bg'].area = "лице";
texts['en'].area = "area";
texts['de'].area = "Gebiet"; 
texts['ro'].area = "arie";
texts['es'].area = "área"; 
texts['sr'].area = "oblast"; 
texts['gr'].area = "περιοχή";

texts['bg'].c_area = "Лице";
texts['en'].c_area = "Area";
texts['de'].c_area = "Gebiet"; 
texts['ro'].c_area = "Arie";
texts['es'].c_area = "Área";
texts['sr'].c_area = "Oblast";
texts['gr'].c_area = "Περιοχή";

texts['bg'].terrain_contour_color = "Цвят на терен / контур";
texts['en'].terrain_contour_color = "terrain / contour color";
texts['de'].terrain_contour_color = "Farbe des Gebiets/Kontur"; 
texts['ro'].terrain_contour_color = "Teren/Culoare Contur"; 
texts['es'].terrain_contour_color = "Terreno/color de contorno"; 
texts['sr'].terrain_contour_color = "Boja terena / oblik"; 
texts['gr'].terrain_contour_color = "Χρώμα εδάφους / περιγράμματος";


texts['bg'].traceOptions = "Опции за следата";
texts['en'].traceOptions = "Trace options";
texts['de'].traceOptions = "Spur optionen"; 
texts['ro'].traceOptions = "Opțiuni Marcare";
texts['es'].traceOptions = "Opciones de rastro";
texts['sr'].traceOptions = "Opcije za putanju";
texts['gr'].traceOptions = "Επιλογές παρακολούθησης";

texts['bg'].tail = "oпашка";
texts['en'].tail = "tail";
texts['de'].tail = "Spur"; 
texts['ro'].tail = "Urmă";
texts['es'].tail = "cola";
texts['sr'].tail = "rep";
texts['gr'].tail = "ουρά";

texts['bg'].c_tail = "Опашка";
texts['en'].c_tail = "Tail";
texts['de'].c_tail = "Spur"; 
texts['ro'].c_tail = "Urmă"; 
texts['es'].c_tail = "Cola"; 
texts['sr'].c_tail = "Rep"; 
texts['gr'].c_tail = "Ουρά";

texts['bg'].from_date_time = "От дата и час";
texts['en'].from_date_time = "Since date";
texts['de'].from_date_time = "Datum und Uhrzeit"; 
texts['ro'].from_date_time = "Dată și oră"; 
texts['es'].from_date_time = "Desde fecha y hora"; 
texts['sr'].from_date_time = "Od datuma i sata"; 
texts['gr'].from_date_time = "Από την ημερομηνία και ώρα";

texts['bg'].from_parking = "От паркинг";
texts['en'].from_parking = "Since parked";
texts['de'].from_parking = "Vom Parkplatz"; 
texts['ro'].from_parking = "De când parcat"; 
texts['es'].from_parking = "Desde aparcamiento"; 
texts['sr'].from_parking = "Od parkiranja"; 
texts['gr'].from_parking = "Από το σταθμευμένο";

texts['bg'].parking_since = "Паркинг от";
texts['en'].parking_since = "Parked since";
texts['de'].parking_since = "Geparkt seit:"; 
texts['ro'].parking_since = "Staționat de la:"; 
texts['es'].parking_since = "Aparcamiento de:"; 
texts['sr'].parking_since = "Parkiran od:"; 
texts['gr'].parking_since = "Σταθμευμένα από το";

texts['bg'].roaming_since = "В роуминг от";
texts['en'].roaming_since = "Roaming since";
texts['de'].roaming_since = "Roaming since"; 
texts['ro'].roaming_since = "Roaming since"; 
texts['es'].roaming_since = "Roaming since"; 
texts['gr'].roaming_since = "Περιαγωγή από το";

texts['bg'].moving_sience = "Движение от:";
texts['en'].moving_sience = "Moving since:";
texts['de'].moving_sience = "In Bewegung seit:"; 
texts['ro'].moving_sience = "În mișcare de la:"; 
texts['es'].moving_sience = "Movimiento de:"; 
texts['sr'].moving_sience = "Kreće se od:"; 
texts['gr'].moving_sience = "Σε κίνηση από:";

texts['bg'].color = "цвят";
texts['en'].color = "color";
texts['de'].color = "farbe"; 
texts['ro'].color = "culoare"; 
texts['es'].color = "color"; 
texts['sr'].color = "boja"; 
texts['gr'].color = "χρώμα";

texts['bg'].c_color = "Цвят";
texts['en'].c_color = "Color";
texts['de'].c_color = "Farbe"; 
texts['ro'].c_color = "Culoare"; 
texts['es'].c_color = "Color"; 
texts['sr'].c_color = "Boja";
texts['gr'].c_color = "Χρώμα";

texts['bg'].width = "дебелина";
texts['en'].width = "width";
texts['de'].width = "breite"; 
texts['ro'].width = "grosime"; 
texts['es'].width = "anchura"; 
texts['sr'].width = "širina"; 
texts['gr'].width = "πλάτος";

texts['bg'].c_width = "Дебелина";
texts['en'].c_width = "Width";
texts['de'].c_width = "Breite"; 
texts['ro'].c_width = "grosime"; 
texts['es'].c_width = "Anchura"; 
texts['sr'].c_width = "Širina"; 
texts['gr'].c_width = "Πλάτος";

texts['bg'].opacity = "прозрачност";
texts['en'].opacity = "opacity";
texts['de'].opacity = "undurchsichtigkeit"; 
texts['ro'].opacity = "Opacitate"; 
texts['es'].opacity = "opacidad"; 
texts['sr'].opacity = "neprozirnost";
texts['gr'].opacity = "διαφάνεια";

texts['bg'].c_opacity = "Прозрачност";
texts['en'].c_opacity = "Opacity";
texts['de'].c_opacity = "Undurchsichtigkeit"; 
texts['ro'].c_opacity = "Opacitate";
texts['es'].c_opacity = "Opacidad";
texts['sr'].c_opacity = "Neprozirnost";
texts['gr'].c_opacity = "Διαφάνεια";

texts['bg'].stop = "престой";
texts['en'].stop = "outage";
texts['de'].stop = "aufenthalt"; 
texts['ro'].stop = "perioadă de oprire/staționare"; 
texts['es'].stop = "paradas"; 
texts['sr'].stop = "prekid rada"; 
texts['gr'].stop = "παραμονή";

texts['bg'].c_stop = "Престой";
texts['en'].c_stop = "Outage";
texts['de'].c_stop = "Aufenthalt"; 
texts['ro'].c_stop = "Perioadă de oprire/staționare"; 
texts['es'].c_stop = "Parada"; 
texts['sr'].c_stop = "Prekid rada"; 
texts['gr'].c_stop = "Παραμονή";

texts['bg'].c_stop2 = "Престой";
texts['en'].c_stop2 = "Outage";
texts['de'].c_stop2 = "Aufenthalt"; 
texts['ro'].c_stop2 = "Staționare"; 
texts['es'].c_stop2 = "Parada"; 
texts['gr'].c_stop2 = "Παραμονή";

texts['bg'].stops = "престои";
texts['en'].stops = "stops";
texts['de'].stops = "aufenthalte"; 
texts['ro'].stops = "Opriri"; 
texts['es'].stops = "paradas";
texts['sr'].stops = "pauza";
texts['gr'].stops = "παραμονές";
 
texts['bg'].c_stops = "Престои";
texts['en'].c_stops = "Stops";
texts['de'].c_stops = "Aufenthalte"; 
texts['ro'].c_stops = "Opriri"; 
texts['es'].c_stops = "Paradas"; 
texts['sr'].c_stops = "Pauza"; 
texts['gr'].c_stops = "Παραμονές";

texts['bg'].work_zero_speed = "На място";
texts['en'].work_zero_speed = "Zero speed";
texts['de'].work_zero_speed = "Zero speed"; 
texts['ro'].work_zero_speed = "Staționare"; 
texts['es'].work_zero_speed = ""; 
texts['gr'].work_zero_speed = "Μηδενική ταχύτητα";

texts['bg'].work_on_speed = "В движение";
texts['en'].work_on_speed = "Moving";
texts['de'].work_on_speed = ""; 
texts['ro'].work_on_speed = "In miscare"; 
texts['es'].work_on_speed = ""; 
texts['gr'].work_on_speed = "Εν κινήσει";

texts['bg'].travelled_km = "Измн. км.";
texts['en'].travelled_km = "Travelled km.";
texts['de'].travelled_km = "Gefahrene km."; 
texts['ro'].travelled_km = "Distanță parcursă."; 
texts['es'].travelled_km = "Kilometraje."; 
texts['sr'].travelled_km = "Pređeni kilometri."; 
texts['gr'].travelled_km = "Απόσταση που διανύθηκε.";

texts['bg'].max_speed = "Макс. км/ч.";
texts['en'].max_speed = "Max km/h.";
texts['de'].max_speed = "Maximale Geschwindigkeit"; 
texts['ro'].max_speed = "Viteză maximaă Km/oră"; 
texts['es'].max_speed = "Velocidad máxima km/h"; 
texts['sr'].max_speed = "Maks. km/h"; 
texts['gr'].max_speed = "Μέγ. km/h.";

texts['bg'].avg_speed = "Срдн. км/ч.";
texts['en'].avg_speed = "Avrg km/h.";
texts['de'].avg_speed = "Durchschnittliche Geschwindigkeit"; 
texts['ro'].avg_speed = "Medie Km/oră";
texts['es'].avg_speed = "Velocidad media km/h";
texts['sr'].avg_speed = "Prosečno km/h";
texts['gr'].avg_speed = "Μέσος όρος km/h";

texts['bg'].trace = "следа";
texts['en'].trace = "trace";
texts['de'].trace = "spur"; 
texts['ro'].trace = "urmă"; 
texts['es'].trace = "rastro"; 
texts['sr'].trace = "putanja";
texts['gr'].trace = "ίχνος";

texts['bg'].c_trace = "Следа";
texts['en'].c_trace = "Trace";
texts['de'].c_trace = "Spur"; 
texts['ro'].c_trace = "Urmă"; 
texts['es'].c_trace = "Rastro"; 
texts['sr'].c_trace = "Putanja"; 
texts['gr'].c_trace = "Ίχνος ";

texts['bg'].routeName = "Наименование на маршрута";
texts['en'].routeName = "Route name";
texts['de'].routeName = "Name der Route"; 
texts['ro'].routeName = "Nume de itinerariu"; 
texts['es'].routeName = "Nombre de la ruta"; 
texts['sr'].routeName = "Naziv maršrute"; 
texts['gr'].routeName = "Όνομα διαδρομής";

texts['bg'].length_km = "Дължина(км.)";
texts['en'].length_km = "Length(km.)";
texts['de'].length_km = "Lange (km.)"; 
texts['ro'].length_km = "Lungime (km.)"; 
texts['es'].length_km = "Distancia (km.)";  
texts['sr'].length_km = "Dužina (km.)";  
texts['gr'].length_km = "Μήκος (km)";

texts['bg'].map = "карта";
texts['en'].map = "map";
texts['de'].map = "Landk."; 
texts['ro'].map = "Hartă."; 
texts['es'].map = "mapa."; 
texts['sr'].map = "mapa."; 
texts['gr'].map = "χάρτης";

texts['bg'].c_map = "Карта";
texts['en'].c_map = "Map";
texts['de'].c_map = "Landk."; 
texts['ro'].c_map = "Hartă."; 
texts['es'].c_map = "Mapa."; 
texts['sr'].c_map = "Mapa."; 
texts['gr'].c_map = "Χάρτης";

texts['bg'].routeSelectionAndStopsConfiguration = "Избор на маршрут и конфигуриране на престоите";
texts['en'].routeSelectionAndStopsConfiguration = "Route selection and stops configuration";
texts['de'].routeSelectionAndStopsConfiguration = "Routenauswahl und Aufenthaltskonfiguration"; 
texts['ro'].routeSelectionAndStopsConfiguration = "Selectare traseu și configurare opriri/staționari"; 
texts['es'].routeSelectionAndStopsConfiguration = "Seleccionar ruta y configurar  paradas"; 
texts['sr'].routeSelectionAndStopsConfiguration = "Izbor maršrute i konfiguracija pauza"; 
texts['gr'].routeSelectionAndStopsConfiguration = "Επιλέξτε μια διαδρομή και διαμορφώστε τις διαμονής ";

texts['bg'].start = "Начало";
texts['en'].start = "Start";
texts['de'].start = "Anfang"; 
texts['ro'].start = "Început"; 
texts['es'].start = "Inicio"; 
texts['sr'].start = "Početak"; 
texts['gr'].start = "Αρχή";

texts['bg'].progress = "Прогрес";
texts['en'].progress = "Progress";
texts['de'].progress = "Fortschritt"; 
texts['ro'].progress = "Progres"; 
texts['es'].progress = "Progreso";
texts['sr'].progress = "Tok";
texts['gr'].progress = "Πρόοδος";

texts['bg'].end = "Край";
texts['en'].end = "End";
texts['de'].end = "Ende"; 
texts['ro'].end = "Sfârșit"; 
texts['es'].end = "Fin"; 
texts['sr'].end = "Kraj"; 
texts['gr'].end = "Τέλος";

texts['bg'].accept = "Приемане";
texts['en'].accept = "Accept";
texts['de'].accept = "Annehmen"; 
texts['ro'].accept = "Accept"; 
texts['es'].accept = "Aceptar"; 
texts['sr'].accept = "Prihvatanje"; 
texts['gr'].accept = "Αποδοχή";

texts['bg'].cancel = "Отказ";
texts['en'].cancel = "Cancel";
texts['de'].cancel = "Ablehnen"; 
texts['ro'].cancel = "Refuz"; 
texts['es'].cancel = "Cancelar"; 
texts['sr'].cancel = "Otkazivanje"; 
texts['gr'].cancel = "Ακύρωση ";

texts['bg'].showLabel = "Покажи етикета";
texts['en'].showLabel = "Show label";
texts['de'].showLabel = "Etikett einblenden"; 
texts['ro'].showLabel = "Arată eticheta"; 
texts['es'].showLabel = "Mostrar etiqueta"; 
texts['sr'].showLabel = "Pokaži oznaku";
texts['gr'].showLabel = "Εμφάνιση ετικέτας"; 

texts['bg'].hideLabel = "Скрий етикета";
texts['en'].hideLabel = "Hide label";
texts['de'].hideLabel = "Etikett ausblenden"; 
texts['ro'].hideLabel = "Ascunde etichetă"; 
texts['es'].hideLabel = "Ocultar etiqueta"; 
texts['sr'].hideLabel = "Sakrij oznaku"; 
texts['gr'].hideLabel = "Απόκρυψη ετικέτας";

texts['bg'].showBoundaries = "Покажи очертанието";
texts['en'].showBoundaries = "Show contour";
texts['de'].showBoundaries = "Gebiet einblenden"; 
texts['ro'].showBoundaries = "Afișază Zonă"; 
texts['es'].showBoundaries = "Mostrar contorno"; 
texts['sr'].showBoundaries = "Pokaži oblik";
texts['gr'].showBoundaries = "Εμφάνιση του περιγράμματος";

texts['bg'].hideBoundaries = "Скрий очертанието";
texts['en'].hideBoundaries = "Hide contour";
texts['de'].hideBoundaries = "Gebiet ausblenden"; 
texts['ro'].hideBoundaries = "Ascunde zonă"; 
texts['es'].hideBoundaries = "Ocultar contorno"; 
texts['sr'].hideBoundaries = "Sakrij oblik";
texts['gr'].hideBoundaries = "Απόκρυψη του περιγράμματος"; 



texts['bg'].answer = "отговор";
texts['en'].answer = "answer";
texts['de'].answer = "Antwort"; 
texts['ro'].answer = "Răspuns"; 
texts['es'].answer = "respuesta"; 
texts['sr'].answer = "odgovor"; 
texts['gr'].answer = "απάντηση";

texts['bg'].answers = "отговора";
texts['en'].answers = "answers";
texts['de'].answers = "Antworten"; 
texts['ro'].answers = "Răspunsuri"; 
texts['es'].answers = "Respuesta"; 
texts['sr'].answers = "Odgovori"; 
texts['sr'].answers = "Odgovori"; 
texts['gr'].answers = "απαντήσεις";

texts['bg'].current_time = "Данни към";
texts['en'].current_time = "Current time";
texts['de'].current_time = "Aktuelle Uhrzeit"; 
texts['ro'].current_time = "Date curente";
texts['es'].current_time = "Datos actuales";
texts['sr'].current_time = "Trenutno vreme";
texts['gr'].current_time = "Τρέχουσα ώρα";

texts['bg'].speed = "скорост";
texts['en'].speed = "speed";
texts['de'].speed = "Geschwindigkeit"; 
texts['ro'].speed = "Viteză"; 
texts['es'].speed = "velocidad";  
texts['sr'].speed = "brzina";
texts['gr'].speed = "ταχύτητα";

texts['bg'].c_speed = "Скорост";
texts['en'].c_speed = "Speed";
texts['de'].c_speed = "Geschwindigkeit"; 
texts['ro'].c_speed = "Viteză"; 
texts['es'].c_speed = "Velocidad"; 
texts['sr'].c_speed = "Brzina"; 
texts['gr'].c_speed = "Ταχύτητα";

texts['bg'].revolutions = "обороти";
texts['en'].revolutions = "revolutions";
texts['de'].revolutions = "Umdrehungen"; 
texts['ro'].revolutions = "Turatii"; 
texts['es'].revolutions = "revoluciones"; 
texts['sr'].revolutions = "obrtaji"; 
texts['gr'].revolutions = "περιστροφές";

texts['bg'].c_revolutions = "Обороти";
texts['en'].c_revolutions = "Revolutions";
texts['de'].c_revolutions = "Umdrehungen"; 
texts['ro'].c_revolutions = "Turații"; 
texts['es'].c_revolutions = "Revoluciones"; 
texts['sr'].c_revolutions = "Obrtaji"; 
texts['gr'].c_revolutions = "Περιστροφές";

texts['bg'].km_h = "км/ч.";
texts['en'].km_h = "km/h.";
texts['de'].km_h = "km/h"; 
texts['ro'].km_h = "km/oră"; 
texts['es'].km_h = "km/h"; 
texts['sr'].km_h = "km/h"; 
texts['gr'].km_h = "km/h"; 

texts['bg'].r_m = "о/м.";
texts['en'].r_m = "r/m.";
texts['de'].r_m = "r/m."; 
texts['ro'].r_m = "r/m."; 
texts['es'].r_m = "r/m."; 
texts['sr'].r_m = "o/m."; 
texts['gr'].r_m = "o/m."; 

texts['bg'].km = "км.";
texts['en'].km = "km.";
texts['de'].km = "km."; 
texts['ro'].km = "km."; 
texts['es'].km = "km."; 
texts['sr'].km = "km."; 
texts['gr'].km = "χλμ."; 

texts['bg'].km2 = "(км)";
texts['en'].km2 = "(km)";
texts['de'].km2 = "(km)"; 
texts['ro'].km2 = "(km)"; 
texts['es'].km2 = "(km)"; 
texts['sr'].km2 = "(km)"; 
texts['gr'].km2 = "(χλμ)";

texts['bg'].m = "м.";
texts['en'].m = "m.";
texts['de'].m = "m."; 
texts['ro'].m = "m."; 
texts['es'].m = "m."; 
texts['sr'].m = "m."; 
texts['gr'].m = "m.";

texts['bg'].metres = "метра";
texts['en'].metres = "meters";
texts['de'].metres = "";
texts['ro'].metres = "metrii";
texts['es'].metres = "metros";
texts['sr'].metres = "metara";
texts['gr'].metres = "μέτρα";

texts['bg'].h = "ч.";
texts['en'].h = "h.";
texts['de'].h = "h"; 
texts['ro'].h = "oră"; 
texts['es'].h = "h"; 
texts['sr'].h = "h"; 
texts['gr'].h = "h.";

texts['bg'].l = "л.";
texts['en'].l = "l.";
texts['de'].l = "l.";
texts['ro'].l = "l.";
texts['es'].l = "l.";
texts['sr'].l = "l.";
texts['gr'].l = "l.";

texts['bg'].l2 = "(л)";
texts['en'].l2 = "(l)";
texts['de'].l2 = "(l)";
texts['ro'].l2 = "(l)";
texts['es'].l2 = "(l)";
texts['sr'].l2 = "(l)";
texts['gr'].l2 = "(l)";

texts['bg'].ltr = "лтр.";
texts['en'].ltr = "ltr.";
texts['de'].ltr = "ltr."; 
texts['ro'].ltr = "ltr."; 
texts['es'].ltr = "ltr."; 
texts['sr'].ltr = "ltr."; 
texts['gr'].ltr = "ltr.";

texts['bg'].min = "мин.";
texts['en'].min = "min.";
texts['de'].min = "Min."; 
texts['ro'].min = "Min.";
texts['es'].min = "Min."; 
texts['sr'].min = "Min."; 
texts['gr'].min = "min.";

texts['bg'].duration = "времетраене";
texts['en'].duration = "duration";
texts['de'].duration = "dauer"; 
texts['ro'].duration = "durată de timp"; 
texts['es'].duration = "duración de tiempo"; 
texts['sr'].duration = "trajanje"; 
texts['gr'].duration = "διάρκεια";

texts['bg'].c_duration = "Времетраене";
texts['en'].c_duration = "Duration";
texts['de'].c_duration = "Dauer"; 
texts['ro'].c_duration = "Durată de timp"; 
texts['es'].c_duration = "Duración de tiempo"; 
texts['sr'].c_duration = "Trajanje"; 
texts['gr'].c_duration = "Διάρκεια";

texts['bg'].duration2 = "продължителност";
texts['en'].duration2 = "duration";
texts['de'].duration2 = "dauer"; 
texts['ro'].duration2 = "durată"; 
texts['es'].duration2 = "duración"; 
texts['sr'].duration2 = "trajanje"; 
texts['gr'].duration2 = "διάρκεια";

texts['bg'].c_duration2 = "Продължителност";
texts['en'].c_duration2 = "Duration";
texts['de'].c_duration2 = "Dauer"; 
texts['ro'].c_duration2 = "Durată"; 
texts['es'].c_duration2 = "Duración"; 
texts['sr'].c_duration2 = "Trajanje"; 
texts['gr'].c_duration2 = "Διάρκεια";

texts['bg'].elevation = "Н-ка височина";
texts['en'].elevation = "Elevation";
texts['de'].elevation = "l.";
texts['ro'].elevation = "Altitudine.";
texts['es'].elevation = "Elevación.";
texts['sr'].elevation = "Visina.";
texts['gr'].elevation = "Υψόμετρο";

texts['bg'].day = "ден";
texts['en'].day = "day";
texts['de'].day = "Tag"; 
texts['ro'].day = "zi"; 
texts['es'].day = "día"; 
texts['sr'].day = "dan"; 
texts['gr'].day = "ημέρα";

texts['bg'].c_day = "Ден";
texts['en'].c_day = "Day";
texts['de'].c_day = "Tag"; 
texts['ro'].c_day = "Zi"; 
texts['es'].c_day = "Día";
texts['sr'].c_day = "Dan";
texts['gr'].c_day = "Ημέρα";

texts['bg'].days = "дни";
texts['en'].days = "days";
texts['de'].days = "Tage"; 
texts['ro'].days = "zile"; 
texts['es'].days = "días"; 
texts['sr'].days = "dani"; 
texts['gr'].days = "ημέρες";

texts['bg'].c_days = "Дни";
texts['en'].c_days = "Days";
texts['de'].c_days = "Tage"; 
texts['ro'].c_days = "Zile"; 
texts['es'].c_days = "Días"; 
texts['sr'].c_days = "Dani"; 
texts['gr'].c_days = "Ημέρες";

texts['bg'].query = "заявка";
texts['en'].query = "query";
texts['de'].query = "Abfrage"; 
texts['ro'].query = "cerere/interogare"; 
texts['es'].query = "solicitud"; 
texts['sr'].query = "zahtev"; 
texts['gr'].query = "αίτημα";

texts['bg'].c_query = "Заявка";
texts['en'].c_query = "Query";
texts['de'].c_query = "Abfrage"; 
texts['ro'].c_query = "Cerere/Interogare"; 
texts['es'].c_query = "Solicitud"; 
texts['sr'].c_query = "Zahtev"; 
texts['gr'].c_query = "Αίτημα ";

texts['bg'].estimating = "Пресмятане...";
texts['en'].estimating = "Estimating...";
texts['de'].estimating = "Einschatzung..."; 
texts['ro'].estimating = "Calcul..."; 
texts['es'].estimating = "Calculando ..."; 
texts['sr'].estimating = "Računanje ..."; 
texts['gr'].estimating = "Υπολογισμός...";

texts['bg'].unknown_place = "Неизвестно място";
texts['en'].unknown_place = "Unknown place";
texts['de'].unknown_place = "Unbekannter Ort"; 
texts['ro'].unknown_place = "Loc necunoscut"; 
texts['es'].unknown_place = "Lugar desconocido"; 
texts['sr'].unknown_place = "Nepoznato mesto"; 
texts['gr'].unknown_place = "Άγνωστο μέρος";

texts['bg'].object = "обект";
texts['en'].object = "object";
texts['de'].object = "objekt"; 
texts['ro'].object = "obiect";
texts['es'].object = "objeto";
texts['sr'].object = "objekat";
texts['gr'].object = "τοποθεσίας";

texts['bg'].c_object = "Обект";
texts['en'].c_object = "Object";
texts['de'].c_object = "Objekt"; 
texts['ro'].c_object = "Obiect"; 
texts['es'].c_object = "Objeto"; 
texts['sr'].c_object = "Objekat"; 
texts['gr'].c_object = "Τοποθεσίας";

texts['bg'].change_me = "Промени ме";
texts['en'].change_me = "Changeme";
texts['de'].change_me = "Verandern"; 
texts['ro'].change_me = "Schimba-mă"; 
texts['es'].change_me = "Cámbiame"; 
texts['sr'].change_me = "Promeni me"; 
texts['gr'].change_me = "Άλλαξέ με";

texts['bg'].save = "запамети";
texts['en'].save = "save";
texts['de'].save = "speichern"; 
texts['ro'].save = "Salvează"; 
texts['es'].save = "guardar"; 
texts['sr'].save = "sačuvaj"; 
texts['gr'].save = "αποθήκευση ";

texts['bg'].c_save = "Запамети";
texts['en'].c_save = "Save";
texts['de'].c_save = "Speichern"; 
texts['ro'].c_save = "Salvează"; 
texts['es'].c_save = "Guardar";
texts['sr'].c_save = "Sačuvaj";
texts['gr'].c_save = "Αποθήκευση ";

texts['bg'].c_save2 = "Запази";
texts['en'].c_save2 = "Save";
texts['de'].c_save2 = "Speichern"; 
texts['ro'].c_save2 = "Salvează"; 
texts['es'].c_save2 = "Guardar"; 
texts['sr'].c_save2 = "Sačuvaj"; 
texts['gr'].c_save2 = "Αποθήκευση";

texts['bg'].save_change = "Запази промените";
texts['en'].save_change = "Accept changes";
texts['de'].save_change = ""; 
texts['ro'].save_change = "Salvează schimbările"; 
texts['es'].save_change = "Guardar los cambios";
texts['sr'].save_change = "Sačuvaj izmene";
texts['gr'].save_change = "Αποθηκεύστε τις αλλαγές";

texts['bg'].zone_name = "Наименование на обекта";
texts['en'].zone_name = "Place name";
texts['de'].zone_name = "Name des Objektes"; 
texts['ro'].zone_name = "Denumirea locație"; 
texts['es'].zone_name = "Nombre de objeto"; 
texts['sr'].zone_name = "Naziv mesta"; 
texts['gr'].zone_name = "Όνομα τοποθεσίας";

texts['bg'].choice = "Избор";
texts['en'].choice = "Choice";
texts['de'].choice = "Auswahl"; 
texts['ro'].choice = "Alegere"; 
texts['es'].choice = "Elección"; 
texts['sr'].choice = "Izbor"; 
texts['gr'].choice = "Επιλογή";

texts['bg'].stopName = "Име на престоя";
texts['en'].stopName = "Stop name";
texts['de'].stopName = "Aufenthaltsort"; 
texts['ro'].stopName = "Denumirea locului de oprire/staționare"; 
texts['es'].stopName = "Nombre de paradas";
texts['sr'].stopName = "Naziv mesta za parkiranje";
texts['es'].stopName = "Nombre de paradas";
texts['gr'].stopName = "Όνομα της διαμονής";

texts['bg'].intermediate_zones = "Междинни точки/зони";
texts['en'].intermediate_zones = "Intermidiate points/zones";
texts['de'].intermediate_zones = "Zwischenpunkte/ Zonen"; 
texts['ro'].intermediate_zones = "Puncte intermediare/ Zone"; 
texts['es'].intermediate_zones = "Puntos/Zonas intermedias"; 
texts['sr'].intermediate_zones = "Međutačke/ zone"; 
texts['gr'].intermediate_zones = "Ενδιάμεσα σημεία / ζώνες";

texts['bg'].address_search = "Търсене на адреси";
texts['en'].address_search = "Address search";
texts['de'].address_search = "Adresse suchen"; 
texts['ro'].address_search = "Cautarea adrese"; 
texts['es'].address_search = "Buscar dirección"; 
texts['sr'].address_search = "Pretraga adrese"; 
texts['gr'].address_search = "Αναζήτηση διευθύνσεων";

texts['bg'].positioning = "Позициониране";
texts['en'].positioning = "Positioning";
texts['de'].positioning = "Positionierung"; 
texts['ro'].positioning = "Poziționare";
texts['es'].positioning = "Posicionamiento";
texts['sr'].positioning = "Pozicioniranje";
texts['gr'].positioning = "Θέση";

texts['bg'].position_start = "В началото на следата";
texts['en'].position_start = "Start of the trace";
texts['de'].position_start = "Am Anfang der Spur"; 
texts['ro'].position_start = "Începutul traseului"; 
texts['es'].position_start = "En el inicio del rastro"; 
texts['sr'].position_start = "Početak putanje"; 
texts['gr'].position_start = "Αρχή του ίχνος";

texts['bg'].position_end = "В края на следата";
texts['en'].position_end = "End of the trace";
texts['de'].position_end = "Am Ende der Spur"; 
texts['ro'].position_end = "Sfârșitul traseului";
texts['es'].position_end = "En el final del rastro";
texts['sr'].position_end = "Kraj putanje";
texts['gr'].position_end = "Τέλος του ίχνος";

texts['bg'].position_vehicle = "Върху превозното средство";
texts['en'].position_vehicle = "On the vehicle's location";
texts['de'].position_vehicle = "Uber dem Fahrzeug"; 
texts['ro'].position_vehicle = "Pe vehicul"; 
texts['es'].position_vehicle = "Sobre el vehículo"; 
texts['sr'].position_vehicle = "Na lokaciju vozila";
texts['gr'].position_vehicle = "Στη θέση του οχήματος";

texts['bg'].position_trace = "Централно и събрано";
texts['en'].position_trace = "Fit the trace on screen";
texts['de'].position_trace = "Zusammengefasst"; 
texts['ro'].position_trace = "Încadrează traseul pe ecran";
texts['es'].position_trace = "Posicionar sobre el rastro";
texts['sr'].position_trace = "Prilagodi putanju ekranu";
texts['gr'].position_trace = "Τοποθετήστε το ίχνος στην οθόνη";

texts['bg'].pois = "Маркировки";
texts['en'].pois = "Points of interest";
texts['de'].pois = "POIs"; 
texts['ro'].pois = "Puncte marcate"; 
texts['es'].pois = "Puntos de intereses"; 
texts['sr'].pois = "Obeležena mesta"; 
texts['gr'].pois = "Σημάνσεις";


texts['bg'].pois2 = "Маркировки";
texts['en'].pois2 = "Geofences";
texts['de'].pois2 = "POIs"; 
texts['ro'].pois2 = "Puncte marcate"; 
texts['es'].pois2 = "Puntos de intereses"; 
texts['sr'].pois2 = "Geografske granice"; 
texts['gr'].pois2 = "Σημάνσεις";

texts['bg'].group_by = "Групирай по:";
texts['en'].group_by = "Group by:";
texts['de'].group_by = "Gruppieren nach:"; 
texts['ro'].group_by = "Grupează după:"; 
texts['es'].group_by = "Agrupar por:";  
texts['sr'].group_by = "Grupiši po:"; 
texts['gr'].group_by = "Ομαδοποίηση κατά:";

texts['bg'].zone = "зона";
texts['en'].zone = "zone";
texts['de'].zone = "zone"; 
texts['ro'].zone = "zonă"; 
texts['es'].zone = "zona"; 
texts['sr'].zone = "zona"; 
texts['gr'].zone = "ζώνη";

texts['bg'].c_zone = "зонa";
texts['en'].c_zone = "Zone";
texts['de'].c_zone = "Zone"; 
texts['ro'].c_zone = "Zonă"; 
texts['es'].c_zone = "Zona"; 
texts['sr'].c_zone = "Zona"; 
texts['gr'].c_zone = "ζώνη";

texts['bg'].zones = "Зони";
texts['en'].zones = "zones";
texts['de'].zones = "Zonen"; 
texts['ro'].zones = "Zone"; 
texts['es'].zones = "Zonas"; 
texts['sr'].zones = "Zone";
texts['gr'].zones = "Ζώνες";

texts['bg'].c_zones = "Зони";
texts['en'].c_zones = "Zones";
texts['de'].c_zones = "zonen"; 
texts['ro'].c_zones = "zone"; 
texts['es'].c_zones = "zonas"; 
texts['sr'].c_zones = "zone"; 
texts['gr'].c_zones = "Ζώνες";

texts['bg'].discharging = "разтоварване";
texts['en'].discharging = "discharging";
texts['de'].discharging = "ausladen"; 
texts['ro'].discharging = "descărcare"; 
texts['es'].discharging = "descargas"; 
texts['sr'].discharging = "istovar"; 
texts['gr'].discharging = "εκφόρτωση";

texts['bg'].c_discharging = "Разтоварване";
texts['en'].c_discharging = "Discharging";
texts['de'].c_discharging = "Ausladen"; 
texts['ro'].c_discharging = "Descărcare";
texts['es'].c_discharging = "Descargas";
texts['sr'].c_discharging = "Istovar";
texts['gr'].c_discharging = "Εκφόρτωση";

texts['bg'].leaving = "напускане";
texts['en'].leaving = "leaving";
texts['de'].leaving = "abfahrt"; 
texts['ro'].leaving = "plecare/părasire"; 
texts['es'].leaving = "salida"; 
texts['sr'].leaving = "napuštanje"; 
texts['gr'].leaving = "αναχώρηση";

texts['bg'].c_leaving = "Напускане";
texts['en'].c_leaving = "Leaving";
texts['de'].c_leaving = "Abfahrt"; 
texts['ro'].c_leaving = "Plecare/Părasire"; 
texts['es'].c_leaving = "Salida"; 
texts['sr'].c_leaving = "Napuštanje"; 
texts['gr'].c_leaving = "Αναχώρηση";

texts['bg'].hide_trace = "Скрий следата";
texts['en'].hide_trace = "Hide trace";
texts['de'].hide_trace = "Spur ausblenden"; 
texts['ro'].hide_trace = "Ascunde traseul"; 
texts['es'].hide_trace = "Ocultar rastro"; 
texts['sr'].hide_trace = "Sakrij putanju";
texts['gr'].hide_trace = "Απόκρυψη ιχνών"; 

texts['bg'].hide_filter = "Скрий филтъра";
texts['en'].hide_filter = "Hide filter";
texts['de'].hide_filter = "Filter ausblenden"; 
texts['ro'].hide_filter = "Ascunde Filtrul"; 
texts['es'].hide_filter = "Ocultar filtro"; 
texts['sr'].hide_filter = "Sakrij filter"; 
texts['gr'].hide_filter = "Απόκρυψη φίλτρου";

texts['bg'].fuel_level = "Ниво на горивото";
texts['en'].fuel_level = "Fuel level";
texts['de'].fuel_level = "Kraftstoffstand"; 
texts['ro'].fuel_level = "Nivel combustibil"; 
texts['es'].fuel_level = "Nivel de combustible"; 
texts['sr'].fuel_level = "Nivo goriva"; 
texts['gr'].fuel_level = "Επίπεδο καυσίμου";

texts['bg'].graphics = "Графики";
texts['en'].graphics = "Charts";
texts['de'].graphics = "Graphiken"; 
texts['ro'].graphics = "Grafice"; 
texts['es'].graphics = "Gráficas"; 
texts['sr'].graphics = "Grafici"; 
texts['gr'].graphics = "Γραφήματα";

texts['bg'].non_inverted = "не инвертиран";
texts['en'].non_inverted = "uninverted";
texts['de'].non_inverted = "normal";
texts['ro'].non_inverted = "normal";
texts['es'].non_inverted = "normal";
texts['sr'].non_inverted = "normalan";
texts['gr'].non_inverted = "αμετάβλητο";

texts['bg'].inverted = "инвертиран";
texts['en'].inverted = "inverted";
texts['de'].inverted = "umgekehrt";
texts['ro'].inverted = "inversat";
texts['es'].inverted = "invertida";
texts['sr'].inverted = "obrnut";
texts['gr'].inverted = "ανεστραμμένο";

texts['bg'].spent_fuel_l = "Разход (л)";
texts['en'].spent_fuel_l = "Cnsmpt (l)";
texts['de'].spent_fuel_l = "Kraftstoff (l)"; 
texts['ro'].spent_fuel_l = "Consum(l)"; 
texts['es'].spent_fuel_l = "Consumo(l)"; 
texts['sr'].spent_fuel_l = "Potrošnja(l)"; 
texts['gr'].spent_fuel_l = "Κατανάλωση (λ )";

texts['bg'].spent_fuel_l_100 = "Литри/100";
texts['en'].spent_fuel_l_100 = "Litres/100";
texts['de'].spent_fuel_l_100 = "Liter/100 km."; 
texts['ro'].spent_fuel_l_100 = "Litri/100 km."; 
texts['es'].spent_fuel_l_100 = "Litros/100 km."; 
texts['sr'].spent_fuel_l_100 = "Litri/100 km."; 
texts['gr'].spent_fuel_l_100 = "Λίτρα / 100";

texts['bg'].spent_fuel_l_100_2 = "Разход/100";
texts['en'].spent_fuel_l_100_2 = "Cnsmpt/100";
texts['de'].spent_fuel_l_100_2 = "Verbrauch pro 100 km."; 
texts['ro'].spent_fuel_l_100_2 = "Consum/ 100 km."; 
texts['es'].spent_fuel_l_100_2 = "Consumo/100 km."; 
texts['sr'].spent_fuel_l_100_2 = "Potrošnja/100 km."; 
texts['gr'].spent_fuel_l_100_2 = "Κατανάλωση / 100";

texts['bg'].spent_fuel_l_100_h = "Литри/100/ч"; //ч = час
texts['en'].spent_fuel_l_100_h = "Litres/100/h"; //ч = час
texts['de'].spent_fuel_l_100_h = "Liter/100/h";  
texts['ro'].spent_fuel_l_100_h = "Litri/100/oră";  
texts['es'].spent_fuel_l_100_h = "Litros/100/h";  
texts['sr'].spent_fuel_l_100_h = "Litri/100/h"; 
texts['gr'].spent_fuel_l_100_h = "Λίτρα / 100/ω"; 

texts['bg'].spent_fuel_l_h = "Литри/ч"; //ч = час
texts['en'].spent_fuel_l_h = "Litres/h"; //ч = час
texts['de'].spent_fuel_l_h = "Liter/h";  
texts['ro'].spent_fuel_l_h = "Litri/oră";  
texts['es'].spent_fuel_l_h = "Litros/h";  
texts['sr'].spent_fuel_l_h = "Litri/h"; 
texts['gr'].spent_fuel_l_h = "Λίτρα/ω";  

texts['bg'].period = "период";
texts['en'].period = "period";
texts['de'].period = "Periode"; 
texts['ro'].period = "Perioadă"; 
texts['es'].period = "período"; 
texts['sr'].period = "period"; 
texts['gr'].period = "περίοδος";

texts['bg'].c_period = "Период";
texts['en'].c_period = "Period";
texts['de'].c_period = "Periode"; 
texts['ro'].c_period = "Perioadă"; 
texts['es'].c_period = "Período"; 
texts['sr'].c_period = "Period"; 
texts['gr'].c_period = "Περίοδος";

texts['bg'].movement = "Движение";
texts['en'].movement = "Movement";
texts['de'].movement = "Bewegung"; 
texts['ro'].movement = "Mișcare"; 
texts['es'].movement = "Movimiento"; 
texts['sr'].movement = "Kretanje"; 
texts['gr'].movement = "Κίνηση";

texts['bg'].location_place = "Населено място/обект";
texts['en'].location_place = "Location";
texts['de'].location_place = "Ort/Objekt"; 
texts['ro'].location_place = "Locație";
texts['es'].location_place = "Localización";
texts['sr'].location_place = "Lokacija";
texts['gr'].location_place = "Τοποθεσία";

texts['bg'].distance = "Разстояние";
texts['en'].distance = "Distance";
texts['de'].distance = "Strecke"; 
texts['ro'].distance = "Distanță"; 
texts['es'].distance = "Distancia"; 
texts['sr'].distance = "Rastojanje"; 
texts['gr'].distance = "Απόσταση";

texts['bg'].distance_km = "Разстояние (км)";
texts['en'].distance_km = "Distance (km)";
texts['de'].distance_km = "Strecke (km)"; 
texts['ro'].distance_km = "Distanță (km)"; 
texts['es'].distance_km = "Distancia (km)";
texts['sr'].distance_km = "Rastojanje (km)";
texts['gr'].distance_km = "Απόσταση (χλμ)";

texts['bg'].sum = "oбщо";
texts['en'].sum = "sum";
texts['de'].sum = "gesamt"; 
texts['ro'].sum = "total"; 
texts['es'].sum = "total";  
texts['sr'].sum = "ukupno";  
texts['gr'].sum = "σύνολο";

texts['bg'].c_sum = "Общо";
texts['en'].c_sum = "Sum";
texts['de'].c_sum = "Gesamt"; 
texts['ro'].c_sum = "Total"; 
texts['es'].c_sum = "Total"; 
texts['sr'].c_sum = "Ukupno"; 
texts['gr'].c_sum = "Σύνολο ";

texts['bg'].stops_interval = "Престои в интервала";
texts['en'].stops_interval = "Stops between";
texts['de'].stops_interval = "Aufenthalte wahrend des Intervalls"; 
texts['ro'].stops_interval = "Opriri între"; 
texts['es'].stops_interval = "Paradas entre";
texts['sr'].stops_interval = "Pauze između";
texts['gr'].stops_interval = "Στάσεις μεταξύ";

texts['bg'].c_round_distance = "Закр. Дистанция";
texts['en'].c_round_distance = "Round Distance";
texts['de'].c_round_distance = "Round Distance"; 
texts['ro'].c_round_distance = "dist. rotunjita"; 
texts['es'].c_round_distance = "Round Distance"; 
texts['gr'].c_round_distance = "Στρογγυλεμένη απόσταση";

texts['bg'].date = "дата";
texts['en'].date = "date";
texts['de'].date = "datum"; 
texts['ro'].date = "dată"; 
texts['es'].date = "fecha"; 
texts['sr'].date = "datum"; 
texts['gr'].date = "ημερομηνία";

texts['bg'].c_date = "Дата";
texts['en'].c_date = "Date";
texts['de'].c_date = "Datum"; 
texts['ro'].c_date = "Dată"; 
texts['es'].c_date = "Fecha";
texts['sr'].c_date = "Datum";
texts['gr'].c_date = "Ημερομηνία";

texts['bg'].all = "всички";
texts['en'].all = "all";
texts['de'].all = "alle"; 
texts['ro'].all = "toate"; 
texts['es'].all = "todos"; 
texts['sr'].all = "sve"; 
texts['gr'].all = "όλα";

texts['bg'].c_all = "Всички";
texts['en'].c_all = "All";
texts['de'].c_all = "Alle"; 
texts['ro'].c_all = "Toate"; 
texts['es'].c_all = "Todos"; 
texts['sr'].c_all = "Sve"; 
texts['gr'].c_all = "Όλα";

texts['bg'].c_course = "Курс";
texts['en'].c_course = "Course";
texts['de'].c_course = "Course"; 
texts['ro'].c_course = "Cursa"; 
texts['es'].c_course = "Course"; 
texts['sr'].c_course = "Курс";
texts['gr'].c_course = "Διαδρομή";

texts['bg'].drivers = "Водачи";
texts['en'].drivers = "Drivers";
texts['de'].drivers = "Fahrer"; 
texts['ro'].drivers = "Șoferi"; 
texts['es'].drivers = "Choferes";
texts['sr'].drivers = "Vozači";
texts['gr'].drivers = "Οδηγοί";

texts['bg'].start_place = "Начален терен";
texts['en'].start_place = "";
texts['de'].start_place = ""; 
texts['ro'].start_place = "Teren start"; 
texts['es'].start_place = "";
texts['sr'].start_place = "Начален терен";
texts['gr'].start_place = "Αρχικό έδαφος";

texts['bg'].end_place = "Краен Терен";
texts['en'].end_place = "";
texts['de'].end_place = ""; 
texts['ro'].end_place = "Teren sfarsit"; 
texts['es'].end_place = "";
texts['sr'].end_place = "Краен Терен";
texts['gr'].end_place = "Τελικό έδαφος";

texts['bg'].date_time = "Дата/час";
texts['en'].date_time = "Date/time";
texts['de'].date_time = ""; 
texts['ro'].date_time = "Data/ora"; 
texts['es'].date_time = "";
texts['sr'].date_time = "Дата/час";
texts['gr'].date_time = "Ημερομηνία/ώρα";

texts['bg'].analys = "Анализиране";
texts['en'].analys = "Analysis";
texts['de'].analys = "Analyse"; 
texts['de'].analys = "Analiză"; 
texts['es'].analys = "Análisis"; 
texts['sr'].analys = "Analiza"; 
texts['gr'].analys = "Ανάλυση";

texts['bg'].number_of_stops = "Престои";
texts['en'].number_of_stops = "Stops";
texts['de'].number_of_stops = "Zahl der Aufenthalte"; 
texts['ro'].number_of_stops = "Perioadă de oprire"; 
texts['es'].number_of_stops = "Paradas"; 
texts['sr'].number_of_stops = "Pauze"; 
texts['gr'].number_of_stops = "Παραμονές";

texts['bg'].sum_time = "Общо време";
texts['en'].sum_time = "Sum time";
texts['de'].sum_time = "Gesamtzeit"; 
texts['ro'].sum_time = "Timp total";
texts['es'].sum_time = "Tiempo total";
texts['sr'].sum_time = "Ukupno vreme";
texts['gr'].sum_time = "Συνολικός χρόνος";

texts['bg'].chart = "Графика";
texts['en'].chart = "Chart";
texts['de'].chart = "Grafik"; 
texts['ro'].chart = "Grafic"; 
texts['es'].chart = "Gráfica"; 
texts['sr'].chart = "Gráfik"; 
texts['gr'].chart = "Γραφικά";

texts['bg'].stop_start_time = "Начало на престоя";
texts['en'].stop_start_time = "Stop start time";
texts['de'].stop_start_time = "Anfang des Aufenthalts"; 
texts['ro'].stop_start_time = "Debut perioadă de oprire"; 
texts['es'].stop_start_time = "Inicio de paradas"; 
texts['sr'].stop_start_time = "Početno vreme pauze"; 
texts['gr'].stop_start_time = "Έναρξη παραμονής";

texts['bg'].stop_end_time = "Край на престоя";
texts['en'].stop_end_time = "Stop end time";
texts['de'].stop_end_time = "Ende des Aufenthalts"; 
texts['ro'].stop_end_time = "Sfârșit perioadă de oprire "; 
texts['es'].stop_end_time = "Final de paradas "; 
texts['sr'].stop_end_time = "Završno vreme pauze"; 
texts['gr'].stop_end_time = "Τέλος παραμονής";

texts['bg'].before = "Преди";
texts['en'].before = "Before";
texts['de'].before = "Bevor"; 
texts['ro'].before = "Înainte"; 
texts['es'].before = "Antes";  
texts['sr'].before = "Pre"; 
texts['gr'].before = "Πριν";

texts['bg'].after = "След";
texts['en'].after = "After";
texts['de'].after = "Danach"; 
texts['ro'].before = "După"; 
texts['es'].before = "Después"; 
texts['sr'].before = "Posle";
texts['gr'].after = "Μετά"; 

texts['bg'].difference = "разлика";
texts['en'].difference = "difference";
texts['de'].difference = "unterschied"; 
texts['ro'].difference = "diferență"; 
texts['es'].difference = "diferencia";
texts['sr'].difference = "razlika";
texts['gr'].difference = "διαφορά";

texts['bg'].c_difference = "Разлика";
texts['en'].c_difference = "Difference";
texts['de'].c_difference = "Unterschied"; 
texts['ro'].c_difference = "Diferență"; 
texts['es'].c_difference = "Diferencia"; 
texts['sr'].c_difference = "Razlika"; 
texts['gr'].c_difference = "Διαφορά";

texts['bg'].c_difference_l = "Разлика (л)";
texts['en'].c_difference_l = "Difference (l)";
texts['de'].c_difference_l = "Unterschied (l)"; 
texts['ro'].c_difference_l = "Diferență (l)"; 
texts['es'].c_difference_l = "Diferencia (l)"; 
texts['sr'].c_difference_l = "Razlika (l)";
texts['gr'].c_difference_l = "Διαφορά (λ)";

texts['bg'].city_distance = "Градско движение";
texts['en'].city_distance = "Urban distance";
texts['de'].city_distance = "Innerorts"; 
texts['ro'].city_distance = "Distanta in oraș";
texts['es'].city_distance = "Tráfico urbano";
texts['sr'].city_distance = "gradski saobraćaj";
texts['gr'].city_distance = "Αστική κίνηση";

texts['bg'].urban_litres = "Градско (л)";
texts['en'].urban_litres = "Urban (l)";
texts['de'].urban_litres = ""; 
texts['ro'].urban_litres = "Urban"; 
texts['es'].urban_litres = "Urbano (l) "; 
texts['sr'].urban_litres = "Gradski (l) "; 
texts['gr'].urban_litres = "Αστικό (λ)";

texts['bg'].surban_litres = "Извънградско (л)";
texts['en'].surban_litres = "Suburban (l)";
texts['de'].surban_litres = ""; 
texts['ro'].surban_litres = "Extraurban"; 
texts['es'].surban_litres = "Extraurbano (l) ";
texts['sr'].surban_litres = "Vangradski (l) ";
texts['gr'].surban_litres = "Προαστιακό (λ)";

texts['bg'].outside_city_distance = "Извънградско движение";
texts['en'].outside_city_distance = "Suburban distance";
texts['de'].outside_city_distance = "Au?erorts"; 
texts['ro'].outside_city_distance = "Distanță extraurbană"; 
texts['es'].outside_city_distance = "Tráfico extraurbano"; 
texts['sr'].outside_city_distance = "Vangradski saobraćaj"; 
texts['gr'].outside_city_distance = "Προαστιακή κίνηση ";

texts['bg'].segment = "Отсечка";
texts['en'].segment = "Segment";
texts['de'].segment = "Strecke"; 
texts['ro'].segment = "Segment";
texts['es'].segment = "Tramo";
texts['sr'].segment = "Deonica";
texts['gr'].segment = "Τμήμα";

texts['bg'].travelled_distance = "Изминато разстояние";
texts['en'].travelled_distance = "Traveled distance";
texts['de'].travelled_distance = "Streckenlange"; 
texts['ro'].travelled_distance = "Distanță parcursă"; 
texts['es'].travelled_distance = "Distancia recorrida";
texts['sr'].travelled_distance = "Pređeno rastojanje";
texts['gr'].travelled_distance = "Απόσταση που διανύθηκε";

texts['bg'].spent_fuel = "Изразходвано гориво";
texts['en'].spent_fuel = "Spent fuel";
texts['de'].spent_fuel = "Kraftstoffverbrauch"; 
texts['ro'].spent_fuel = "Combustibil consumat"; 
texts['es'].spent_fuel = "Consumo de combustible"; 
texts['sr'].spent_fuel = "Potrošeno gorivo"; 
texts['bg'].spent_fuel = "Αναλωμένο καύσιμο";

texts['bg'].icon = "Икона";
texts['en'].icon = "Icon";
texts['de'].icon = "Symbol"; 
texts['ro'].icon = "Simbol"; 
texts['es'].icon = "Icono";
texts['sr'].icon = "Ikona";
texts['gr'].icon = "Εικονίδιο";

texts['bg'].deletion = "Изтриване";
texts['en'].deletion = "Delete";
texts['de'].deletion = "Loschen"; 
texts['ro'].deletion = "Ștergere"; 
texts['es'].deletion = "Borrar"; 
texts['sr'].deletion = "Brisanje"; 
texts['gr'].deletion = "Διαγραφή";

texts['bg'].del = "Изтрий";
texts['en'].del = "Delete";
texts['de'].del = "Loschen"; 
texts['ro'].del = "Șterge"; 
texts['es'].del = "Borrar";
texts['sr'].del = "Izbriši";
texts['gr'].del = "Διαγραφή";

texts['bg']._for = "за";
texts['en']._for = "for";
texts['de']._for = "fur"; 
texts['ro']._for = "pentru"; 
texts['es']._for = "para";
texts['sr']._for = "za";
texts['gr']._for = "για";

texts['bg']._for2 = "за"; //Заредил 230 литри дизелово гориво ЗА 644 лв.
texts['en']._for2 = "for"; //Заредил 230 литри дизелово гориво ЗА 644 лв.
texts['de']._for2 = "fur"; 
texts['ro']._for2 = "pentru"; 
texts['es']._for2 = "por";
texts['sr']._for2 = "za";
texts['gr']._for2 = "για";

texts['bg'].engine_off = "изключен";
texts['en'].engine_off = "off";
texts['de'].engine_off = "aus"; 
texts['ro'].engine_off = "oprit"; 
texts['es'].engine_off = "apagado"; 
texts['sr'].engine_off = "ugašen";
texts['gr'].engine_off = "απενεργοποιημενος";

texts['bg'].engine_on = "включен";
texts['en'].engine_on = "on";
texts['de'].engine_on = "an"; 
texts['ro'].engine_on = "pornit"; 
texts['es'].engine_on = "arrancado";
texts['sr'].engine_on = "upaljen";
texts['gr'].engine_on = "ενεργοποιημένος ";

texts['bg'].record = "запис";
texts['en'].record = "record";
texts['de'].record = "Speichern"; 
texts['ro'].engine_on = "salvare"; 
texts['es'].engine_on = "grabación";
texts['sr'].engine_on = "zapis";
texts['gr'].record = "εγγραφή";

texts['bg'].fuel = "Гориво";
texts['en'].fuel = "Fuel";
texts['de'].fuel = "Kraftstoff"; 
texts['ro'].fuel = "Combustibil"; 
texts['es'].fuel = "Combustible"; 
texts['sr'].fuel = "Gorivo";
texts['gr'].fuel = "Καύσιμο";

texts['bg'].address = "Адрес";
texts['en'].address = "Address";
texts['de'].address = "Adresse"; 
texts['ro'].address = "Adresa"; 
texts['es'].address = "Dirección"; 
texts['sr'].address = "Adresa"; 
texts['gr'].address = "Διεύθυνση";

texts['bg'].addresses = "Адреси";
texts['en'].addresses = "Addresses";
texts['de'].addresses = ""; 
texts['ro'].addresses = "Adrese"; 
texts['es'].addresses = "Direcciónes"; 
texts['sr'].addresses = "Adrese"; 
texts['gr'].addresses = "Διευθύνσεις";

texts['bg'].marked_places = "Маркирани обекти";
texts['en'].marked_places = "Marked places";
texts['de'].marked_places = "Markierte Orte"; 
texts['ro'].marked_places = "Locații marcate"; 
texts['es'].marked_places = "Lugares marcados";
texts['sr'].marked_places = "Obeležena mesta";
texts['gr'].marked_places = "Επισημένες  τοποθεσίες";

texts['bg'].marked_polygons = "Маркирани терени";
texts['en'].marked_polygons = "Marked terrains";
texts['de'].marked_polygons = "Markierte Gebiete"; 
texts['ro'].marked_polygons = "Terenuri marcate"; 
texts['es'].marked_polygons = "Terrenos marcados"; 
texts['sr'].marked_polygons = "Obeleženi tereni"; 
texts['gr'].marked_polygons = "Σημασμένα εδάφη";

texts['bg'].cities_villages = "Населени места";
texts['en'].cities_villages = "Living places";
texts['de'].cities_villages = "Ort"; 
texts['ro'].cities_villages = "Localități";
texts['es'].cities_villages = "Asentamientos";
texts['sr'].cities_villages = "Naseljena mesta";
texts['gr'].cities_villages = "Οικισμοί";

texts['bg'].city_l = "Градско (л)";
texts['en'].city_l = "Urban (l)";
texts['de'].city_l = "Innerorts (l)"; 
texts['ro'].city_l = "Urban (l)"; 
texts['es'].city_l = "Urbano (l)";
texts['sr'].city_l = "Gradski (l)";
texts['gr'].city_l = "Αστικό (λ )";

texts['bg'].runway_l = "Извънградско (л)";
texts['en'].runway_l = "Suburban (l)";
texts['de'].runway_l = "Au?erorts (l)"; 
texts['ro'].runway_l = "Extraurban (l)"; 
texts['es'].runway_l = "Extraurbano (l)";
texts['sr'].runway_l = "Vangradski (l)";
texts['gr'].runway_l = "Προαστιακό (λ)";

texts['bg'].city_km = "Градско (км)";
texts['en'].city_km = "Urban (km)";
texts['de'].city_km = "Innerorts (km)"; 
texts['ro'].city_km = "Urban (km)"; 
texts['es'].city_km = "Urbano (km)"; 
texts['sr'].city_km = "Gradski (km)"; 
texts['gr'].city_km = "Αστικό (χλμ)";

texts['bg'].runway_km = "Извънградско (км)";
texts['en'].runway_km = "Suburban (km)";
texts['de'].runway_km = "Au?erorts (km)"; 
texts['ro'].runway_km = "Extraurban (km)"; 
texts['es'].runway_km = "Extraurbano (km)";
texts['sr'].runway_km = "Vangradski (km)";
texts['gr'].runway_km = "Προαστιακό (χλμ)";

texts['bg'].litres_h = "Литри (ч)";
texts['en'].litres_h = "Litres (h)";
texts['de'].litres_h = "Liter pro Stunde"; 
texts['ro'].litres_h = "Litri (ora)"; 
texts['es'].litres_h = "Litros (h)";
texts['sr'].litres_h = "Litri (h)";
texts['gr'].litres_h = "Λίτρα(ω)";

texts['bg'].accumulator = "Акумулатор";
texts['en'].accumulator = "Accumulator";
texts['de'].accumulator = "Akku"; 
texts['ro'].accumulator = "Acumulator";
texts['es'].accumulator = "Acumulador";
texts['sr'].accumulator = "Akumulator";
texts['gr'].accumulator = "Συσσωρευτής";

texts['bg'].accumulator_w = "Акумулатор (w)";
texts['en'].accumulator_w = "Accumulator (w)";
texts['de'].accumulator_w = "Akku(V)"; 
texts['ro'].accumulator_w = "Acumulator(V)";
texts['es'].accumulator_w = "Acumulador(V)";
texts['sr'].accumulator_w = "Akumulator(w)";
texts['gr'].accumulator_w = "Συσσωρευτής (w)";

texts['bg'].mark = "Марка";
texts['en'].mark = "Mark";
texts['de'].mark = "Hersteller"; 
texts['ro'].mark = "Marca auto"; 
texts['es'].mark = "Marca de vehículo"; 
texts['sr'].mark = "Marka vozila"; 
texts['gr'].mark = "Μοντέλο";

texts['bg'].model = "Модел";
texts['en'].model = "Model";
texts['de'].model = "Modell"; 
texts['ro'].model = "Model"; 
texts['es'].model = "Modelo";
texts['sr'].model = "Model";
texts['gr'].model = "Μοντέλο";

texts['bg'].year = "Година";
texts['en'].year = "Year";
texts['de'].year = "Baujahr"; 
texts['ro'].year = "An"; 
texts['es'].year = "Año"; 
texts['sr'].year = "Godina";
texts['gr'].year = "Έτος"; 

texts['bg'].group = "Група";
texts['en'].group = "Group";
texts['de'].group = "Gruppe"; 
texts['ro'].group = "Grupă"; 
texts['es'].group = "Grupo"; 
texts['sr'].group = "Grupa"; 
texts['gr'].group = "Ομάδα";

texts['bg'].hour = "час";
texts['en'].hour = "hour";
texts['de'].hour = "uhrzeit"; 
texts['ro'].hour = "oră"; 
texts['es'].hour = "hora";
texts['sr'].hour = "sat";
texts['gr'].hour = "ώρα";

texts['bg'].hours = "часа";
texts['en'].hours = "hours";
texts['de'].hours = ""; 
texts['ro'].hours = "ore"; 
texts['es'].hours = "horas";
texts['sr'].hours = "sati";
texts['gr'].hours = "ώρες";

texts['bg'].c_hour = "Час";
texts['en'].c_hour = "Hour";
texts['de'].c_hour = "Uhrzeit"; 
texts['ro'].c_hour = "Oră"; 
texts['es'].c_hour = "Hora";
texts['sr'].c_hour = "Sat";
texts['gr'].c_hour = "Ώρα";

texts['bg'].minutes = "минути";
texts['en'].minutes = "minutes";
texts['de'].minutes = ""; 
texts['ro'].minutes = "minute"; 
texts['es'].minutes = "minutos";
texts['sr'].minutes = "minuti";
texts['gr'].minutes = "λεπτά";

texts['bg'].fuel_l = "Гориво (л)";
texts['en'].fuel_l = "Fuel (l)";
texts['de'].fuel_l = "Kraftstoff"; 
texts['ro'].fuel_l = "Combustibil (l)"; 
texts['es'].fuel_l = "Combustible (l)";
texts['sr'].fuel_l = "Gorivo (l)";
texts['gr'].fuel_l = "Καύσιμο (λ)";

texts['bg'].edit = "Редактиране";
texts['en'].edit = "Edit";
texts['de'].edit = "bearbeiten"; 
texts['ro'].edit = "Editare"; 
texts['es'].edit = "Editar";
texts['sr'].edit = "Izmeni";
texts['gr'].edit = "Επεξεργασία";

texts['bg'].add = "Добави";
texts['en'].add = "Add";
texts['de'].add = "hinzufugen"; 
texts['ro'].add = "Adaugă"; 
texts['es'].add = "Añadir";
texts['sr'].add = "Dodaj";
texts['gr'].add = "Προσθέστε";

texts['bg'].added = "Добавено";
texts['en'].added = "Added";
texts['de'].added = "hinzugefugt"; 
texts['ro'].added = "Adaugat"; 
texts['es'].added = "Añadido";
texts['sr'].added = "Dodato";
texts['gr'].added = "Προστέθηκε";

texts['bg'].enter_password = "Въведете парола:";
texts['en'].enter_password = "Enter the password:";
texts['de'].enter_password = "Geben Sie das Passwort ein:"; 
texts['ro'].enter_password = "Introduceți parola:"; 
texts['es'].enter_password = "Introducir contraseña :";
texts['sr'].enter_password = "Unesi lozinku :";
texts['gr'].enter_password = "Εισάγετε τον κωδικό:";

texts['bg'].status = "Статус";
texts['en'].status = "Status";
texts['de'].status = "Stand"; 
texts['ro'].status = "Status"; 
texts['es'].status = "Estado"; 
texts['sr'].status = "Status";
texts['gr'].status = "Κατάσταση"; 

texts['bg'].export = "Експорт";
texts['en'].export = "Export";
texts['de'].export = "Exportieren"; 
texts['ro'].export = "Export"; 
texts['es'].export = "Exportación"; 
texts['sr'].export = "Izvezi"; 
texts['gr'].export = "Εξαγωγή";

texts['bg'].export_to_excel = "Експорт в Excel";
texts['en'].export_to_excel = "Export to Excel";
texts['de'].export_to_excel = "Exportieren nach Excel"; 
texts['ro'].export_to_excel = "Export în Excel"; 
texts['es'].export_to_excel = "Exportar a Excel"; 
texts['sr'].export_to_excel = "Izvezi u Excel"; 
texts['gr'].export_to_excel = "Εξαγωγή σε Excel";

texts['bg'].export_to_excel_document = "Експорт в Excel документ";
texts['en'].export_to_excel_document = "Export to Excel document";
texts['de'].export_to_excel_document = ""; 
texts['ro'].export_to_excel_document = "Export în document Excel"; 
texts['es'].export_to_excel_document = "Exportar a documento de Excel";
texts['sr'].export_to_excel_document = "Izvezi u Excel dokument";
texts['gr'].export_to_excel_document = "Εξαγωγή σε έγγραφο Excel";

texts['bg'].export_to_pdf_document = "Експорт в PDF документ";
texts['en'].export_to_pdf_document = "Export to PDF document";
texts['de'].export_to_pdf_document = ""; 
texts['ro'].export_to_pdf_document = "Export în document PDF "; 
texts['es'].export_to_pdf_document = "Exportar a documento de PDF "; 
texts['sr'].export_to_pdf_document = "Izvezi u PDF dokument"; 
texts['gr'].export_to_pdf_document = "Εξαγωγή σε έγγραφο PDF";

texts['bg'].export_to_png = "Експорт в PNG формат";
texts['en'].export_to_png = "Export to PNG format";
texts['de'].export_to_png = ""; 
texts['ro'].export_to_png = "Export în format PNG "; 
texts['es'].export_to_png = "Exportar a formato PNG ";
texts['sr'].export_to_png = "Izvezi u PNG format";
texts['gr'].export_to_png = "Εξαγωγή σε μορφή PNG";

texts['bg'].print = "Принтиране";
texts['en'].print = "Print";
texts['de'].print = "Drucken"; 
texts['ro'].print = "Imprimare"; 
texts['es'].print = "imprimir";
texts['sr'].print = "Štampaj";
texts['gr'].print = "Εκτύπωση";

texts['bg'].print_mode = "Режим на принтиране";
texts['en'].print_mode = "Print mode";
texts['de'].print_mode = "Druckmodus"; 
texts['ro'].print_mode = "Mod imprimare"; 
texts['es'].print_mode = "Modo de impresión";
texts['sr'].print_mode = "Režim štampanja";
texts['gr'].print_mode = "Λειτουργία εκτύπωσης";

texts['bg'].print_page = "Принтиране на страницата";
texts['en'].print_page = "Page print";
texts['de'].print_page = "Seite drucken"; 
texts['ro'].print_page = "Printează pagina"; 
texts['es'].print_page = "Imprimir  página";
texts['sr'].print_page = "Štampanje stranica";
texts['gr'].print_page = "Εκτύπωση σελίδας ";

texts['bg'].print_grid = "Принтиране на таблицата";
texts['en'].print_grid = "Table print";
texts['de'].print_grid = "Tabelle drucken"; 
texts['ro'].print_grid = "Printează tabel"; 
texts['es'].print_grid = "Imprimir tabla";
texts['sr'].print_grid = "Štampanje tabela";
texts['gr'].print_grid = "Εκτύπωση πίνακας ";

texts['bg'].period2 = "Период";
texts['en'].period2 = "Period";
texts['de'].period2 = "Periode"; 
texts['ro'].period2 = "Perioadă"; 
texts['es'].period2 = "Período";
texts['sr'].period2 = "Períod";
texts['gr'].period2 = "Περίοδος";

texts['bg'].period_move_idle = "Период на движение/престой";
texts['en'].period_move_idle = "Move/idle duration";
texts['de'].period_move_idle = "Bewegungs-/Aufenthaltsperiode"; 
texts['ro'].period_move_idle = "Perioadă de mișcare/oprire"; 
texts['es'].period_move_idle = "Movimiento / estancia"; 
texts['sr'].period_move_idle = "Trajanje kretanja/mirovanja"; 
texts['gr'].period_move_idle = "Περίοδος κίνησης/παραμονής";

texts['bg'].work_time = "Работно време";
texts['en'].work_time = "Work time";
texts['de'].work_time = "Arbeitszeit"; 
texts['ro'].work_time = "Timp de lucru"; 
texts['es'].work_time = "Tiempo laboral";
texts['sr'].work_time = "Vreme rada";
texts['gr'].work_time = "Ώρες εργασίας";

texts['bg'].report = "Справка";
texts['en'].report = "Report";
texts['de'].report = "Bericht"; 
texts['ro'].report = "Raport"; 
texts['es'].report = "Informe"; 
texts['sr'].report = "Izveštaj"; 
texts['gr'].report = "Αναφορά";

texts['bg'].chart_access_denied = "Не сте абониран за тази справка.";
texts['en'].chart_access_denied = "You don't have the neccessary rights to do this report.";
texts['de'].chart_access_denied = "Zugang zum  Bericht verweigert"; 
texts['ro'].chart_access_denied = "Nu aveti abonament pentru acest raport"; 
texts['es'].chart_access_denied = "Usted no está suscrito a este informe";
texts['sr'].chart_access_denied = "Niste ovlašćeni da uradite ovaj izveštaj.";
texts['gr'].chart_access_denied = "Δεν έχετε τα απαραίτητα δικαιώματα για να κάνετε αυτήν την αναφορά.";

texts['bg'].delivered = "Предал";
texts['en'].delivered = "Delivered";
texts['de'].delivered = "Abgegeben von"; 
texts['ro'].delivered = "Livrat"; 
texts['es'].delivered = "Entregado"; 
texts['sr'].delivered = "Dostavljeno"; 
texts['gr'].delivered = "Παραδόθηκε";

texts['bg'].accepted = "Приел";
texts['en'].accepted = "Taked";
texts['de'].accepted = "Angenommen von";
texts['ro'].accepted = "Acceptat";
texts['es'].accepted = "Aceptado";
texts['sr'].accepted = "Preuzeto";
texts['gr'].accepted = "Παραλαβή ";

texts['bg'].price_filter = "Филтър за цена:";
texts['en'].price_filter = "Price filter:";
texts['de'].price_filter = "Preisfilter:"; 
texts['ro'].price_filter = "Filtru de preț:"; 
texts['es'].price_filter = "Filtro de precio:";
texts['sr'].price_filter = "Filter za cene:";
texts['gr'].price_filter = "Φίλτρο τιμών:";

texts['bg'].start_date = "Начална дата";
texts['en'].start_date = "Start date";
texts['de'].start_date = "Anfangsdatum"; 
texts['ro'].start_date = "Data începere"; 
texts['es'].start_date = "Fecha de inicio";
texts['sr'].start_date = "Početni datum";
texts['gr'].start_date = "Ημερομηνία έναρξης";

texts['bg'].end_date = "Крайна дата";
texts['en'].end_date = "End date";
texts['de'].end_date = "Enddatum"; 
texts['ro'].end_date = "Dată de încheiere"; 
texts['es'].end_date = "Fecha  de final";
texts['sr'].end_date = "Završni datum";
texts['gr'].end_date = "Ημερομηνία λήξης";

texts['bg'].remont_deliverer = "Предал за ремонт";
texts['en'].remont_deliverer = "Delivered for remont";
texts['de'].remont_deliverer = "Zur Reparatur abgegeben von";
texts['ro'].remont_deliverer = "Dus pentru reparare";
texts['es'].remont_deliverer = "Entregado de reparación";
texts['sr'].remont_deliverer = "Predato za popravku";
texts['gr'].remont_deliverer = "Παραδόθηκε για επισκευή";

texts['bg'].remont_taker = "Приел от ремонт";
texts['en'].remont_taker = "Taked from remont";
texts['de'].remont_taker = "Von der Reparatur abgeholt von"; 
texts['ro'].remont_taker = "Preluat de la repărare"; 
texts['es'].remont_taker = "Aceptado de reparación";
texts['sr'].remont_taker = "Preuzeto sa popravke";
texts['gr'].remont_taker = "Αποδοχή από  επισκευή";

texts['bg'].firm = "фирма";
texts['en'].firm = "firm";
texts['de'].firm = "Werkstatt";	
texts['ro'].firm = "firma";	
texts['es'].firm = "empresa";	
texts['sr'].firm = "firma";	
texts['gr'].firm = "εταιρεία";

texts['bg'].c_firm = "Фирма";
texts['en'].c_firm = "Firm";
texts['de'].c_firm = "Werkstatt"; 
texts['ro'].c_firm = "firma"; 
texts['es'].c_firm = "Empresa"; 
texts['sr'].c_firm = "Firma";
texts['gr'].c_firm = "Εταιρεία"; 

texts['bg'].city = "град";
texts['en'].city = "city";
texts['de'].city = "Stadt"; 
texts['ro'].city = "oraș"; 
texts['es'].city = "ciudad";
texts['sr'].city = "grad";
texts['gr'].city = "πόλη";

texts['bg'].c_city = "Град";
texts['en'].c_city = "City";
texts['de'].c_city = "Stadt"; 
texts['ro'].c_city = "Oraș"; 
texts['es'].c_city = "Ciudad";
texts['sr'].c_city = "Grad";
texts['gr'].c_city = "Πόλη";

texts['bg'].price = "Цена";
texts['en'].price = "Price";
texts['de'].price = "Preis"; 
texts['ro'].price = "Preț"; 
texts['es'].price = "Precio";
texts['sr'].price = "Cena";
texts['gr'].price = "Τιμή";

texts['bg'].invoice = "Фактура";
texts['en'].invoice = "Invoice";
texts['de'].invoice = "Rechnung"; 
texts['ro'].invoice = "Factură"; 
texts['es'].invoice = "Factura"; 
texts['sr'].invoice = "Faktura"; 
texts['gr'].invoice = "Τιμολόγιο";

texts['bg'].remonts = "Ремонти";
texts['en'].remonts = "Vehicle Repairs";
texts['de'].remonts = "Reparaturen";
texts['ro'].remonts = "Repărări Vehicul";
texts['es'].remonts = "Reparaciónes";
texts['sr'].remonts = "Popravke vozila";
texts['gr'].remonts = "Επισκευές";

texts['bg'].options = "Опции";
texts['en'].options = "Options";
texts['de'].options = "options"; 
texts['ro'].options = "opțiuni"; 
texts['es'].options = "opciones";
texts['sr'].options = "opcije";
texts['gr'].options = "Επιλογές";

texts['bg'].work = "Работа";
texts['en'].work = "Work";
texts['de'].work = "Arbeit"; 
texts['ro'].work = "Lucrare"; 
texts['es'].work = "Trabajo";
texts['sr'].work = "Rad";
texts['gr'].work = "Εργασία";

texts['bg'].tachometer_authentification = "Заверка на тахограф";
texts['en'].tachometer_authentification = "tachometer authentification";
texts['de'].tachometer_authentification = "Tachokalibrierung"; 
texts['ro'].tachometer_authentification = "Autentificare tahograf"; 
texts['es'].tachometer_authentification = "Certificación de tacógrafo"; 
texts['sr'].tachometer_authentification = "Autentifikacija tahometra"; 
texts['gr'].tachometer_authentification = "Πιστοποίηση ταχογράφου";

texts['bg'].kilometers = "Километри";
texts['en'].kilometers = "Kilometers";
texts['de'].kilometers = "Kilometer"; 
texts['ro'].kilometers = "Kilometri"; 
texts['es'].kilometers = "Kilómetros";
texts['sr'].kilometers = "Kilometri";
texts['gr'].kilometers = "Χιλιόμετρα";

texts['bg'].expire_on = "Изтича на";
texts['en'].expire_on = "Expire on";
texts['de'].expire_on = "Ablaufdatum "; 
texts['ro'].expire_on = "Expiră la "; 
texts['es'].expire_on = "Caduca de";
texts['sr'].expire_on = "Ističe";
texts['gr'].expire_on = "Λήγει στις";

texts['bg'].speed2 = "Скорост";
texts['en'].speed2 = "Speed";
texts['de'].speed2 = "Geschwindigkeit"; 
texts['ro'].speed2 = "Viteză"; 
texts['es'].speed2 = "Velocidad"; 
texts['sr'].speed2 = "Brzina"; 
texts['gr'].speed2 = "Ταχύτητα";

texts['bg'].previous_day = "Предишен ден";
texts['en'].previous_day = "Previous day";
texts['de'].previous_day = "Gestern"; 
texts['ro'].previous_day = "Ziuă precedentă"; 
texts['es'].previous_day = "Día anterior";
texts['sr'].previous_day = "Prethodni dan";
texts['gr'].previous_day = "Προηγούμενη ημέρα";

texts['bg'].next_day = "Следващ ден";
texts['en'].next_day = "Next day";
texts['de'].next_day = "Morgen"; 
texts['ro'].next_day = "Ziuă următoare"; 
texts['es'].next_day = "Día siguiente"; 
texts['sr'].next_day = "Sledeći dan"; 
texts['gr'].next_day = "Επόμενη μέρα";

texts['bg'].move_map = "Движи картата";
texts['en'].move_map = "Move map";
texts['de'].move_map = "Landkarte bewegen"; 
texts['ro'].move_map = "Mișcă hartă"; 
texts['sr'].move_map = "Pomeri mapu"; 
texts['gr'].move_map = "Μετακινήστε το χάρτη";

texts['bg'].time = "Време";
texts['en'].time = "Time";
texts['de'].time = "Zeit"; 
texts['ro'].time = "Timp"; 
texts['es'].time = "Timpo";
texts['sr'].time = "Vreme";
texts['gr'].time = "Χρόνος";

texts['bg'].time2 = "Час";
texts['en'].time2 = "Time";
texts['de'].time2 = "Zeit"; 
texts['ro'].time2 = "Oră"; 
texts['es'].time2 = "Hora";
texts['sr'].time2 = "Sat";
texts['gr'].time2 = "Ώρα";

texts['bg'].time3 = "часа";
texts['en'].time3 = "hours";
texts['de'].time3 = ""; 
texts['ro'].time3 = "ore"; 
texts['es'].time3 = "horas";
texts['sr'].time3 = "sati";
texts['gr'].time3 = "ώρες";

texts['bg'].check = "Касова бележка";
texts['en'].check = "Check";
texts['de'].check = "Tankquittungen"; 
texts['ro'].check = "Bon fiscal"; 
texts['es'].check = "Recibo"; 
texts['sr'].check = "Priznanica"; 
texts['gr'].check = "Απόδειξη";

texts['bg'].refueled = "Заредил";
texts['en'].refueled = "Refuel";
texts['de'].refueled = "Getankt"; 
texts['ro'].refueled = "A alimentat"; 
texts['es'].refueled = "Repostar"; 
texts['sr'].refueled = "Napunjen"; 
texts['gr'].refueled = "Εφοδιασμού";

texts['bg'].dreif = "дрейф";
texts['en'].dreif = "dreif";
texts['de'].dreif = "Drift";
texts['ro'].dreif = "Dreif";
texts['es'].dreif = "valores incorrectos";
texts['sr'].dreif = "odstupanje";
texts['gr'].dreif = "'dreif'";

texts['bg'].c_dreif = "Дрейф";
texts['en'].c_dreif = "Dreif";
texts['de'].c_dreif = "Drift";
texts['ro'].c_dreif = "Derivă";
texts['es'].c_dreif = "Valores incorrectos";
texts['sr'].c_dreif = "Odstupanje";
texts['gr'].c_dreif = "'Dreif'";

texts['bg'].temperature = "температура";
texts['en'].temperature = "temperature";
texts['de'].temperature = "temperatur"; 
texts['ro'].temperature = "temperatură";
texts['es'].temperature = "temperatura";
texts['sr'].temperature = "temperatura";
texts['gr'].temperature = "θερμοκρασία";

texts['bg'].c_temperature = "Температура";
texts['en'].c_temperature = "Temperature";
texts['de'].c_temperature = "Temperatur"; 
texts['ro'].c_temperature = "Temperatură"; 
texts['sr'].c_temperature = "Temperatura"; 
texts['gr'].c_temperature = "Θερμοκρασία";

texts['bg'].full_screen = "Цял екран";
texts['en'].full_screen = "Full screen";
texts['de'].full_screen = "Vollbild"; 
texts['ro'].full_screen = "Full screen"; 
texts['es'].full_screen = "Pantalla completa"; 
texts['sr'].full_screen = "Ceo ekran"; 
texts['gr'].full_screen = "Πλήρης οθόνη";

texts['bg'].transperant = "Прозрачна"; //линия, следа, очертание
texts['en'].transperant = "Transperant"; //линия, следа, очертание
texts['de'].transperant = "durchsichtig"; 
texts['ro'].transperant = "Transparent"; 
texts['es'].transperant = "Transparente";
texts['sr'].transperant = "Providno";
texts['gr'].transperant = "Διαφανής"; 

texts['en'].non_trasnperant = "Opaque"; //линия, следа, очертание
texts['de'].non_trasnperant = "undurchsichtig";
texts['ro'].non_trasnperant = "opac";
texts['es'].non_trasnperant = "opaco";
texts['sr'].non_trasnperant = "neprovidno";
texts['gr'].non_trasnperant = "Αδιαφανής";


texts['bg'].level = "Ниво";
texts['en'].level = "Level";
texts['de'].level = "Niveau"; 
texts['ro'].level = "Nivel"; 
texts['es'].level = "Nivel"; 
texts['sr'].level = "Nivo";
texts['gr'].level = "Επίπεδο"; 

texts['bg'].overtime = "Извънработно време";
texts['en'].overtime = "Overtime";
texts['de'].overtime = "Uberstunden";
texts['ro'].overtime = "În afară programul de lucru";
texts['es'].overtime = "Horas extras";
texts['sr'].overtime = "Prekovremeni rad";
texts['gr'].overtime = "Υπερωρία";

texts['bg'].write_access_pass_input = "Въведете парола за модифициране на фирмени данни:";
texts['en'].write_access_pass_input = "Enter the correct password to allow firm data modification";
texts['de'].write_access_pass_input = "Geben Sie ein Passwort zur Modifizierung der Firmendaten ein:"; 
texts['ro'].write_access_pass_input = "Introduceti parolă pentru modificarea datelor firmei:"; 
texts['es'].write_access_pass_input = "Introducir contraseña para 'modificación' de los datos de la empresa:"; 
texts['sr'].write_access_pass_input = "Unesite tačnu lozinku za dozvolu izmene podataka firme:"; 
texts['gr'].write_access_pass_input = "Εισαγάγετε έναν κωδικό πρόσβασης για να τροποποιήσετε δεδομένα επιχείρησης:";

texts['bg'].write_access_pass = "Парола за модифициране";    
texts['en'].write_access_pass = " Password to allow data modification";    
texts['de'].write_access_pass = "Passwort zur Modifizierung"; 
texts['ro'].write_access_pass = "Parolă de modificare"; 
texts['es'].write_access_pass = "Contraseña de 'modificación'";
texts['sr'].write_access_pass = "Lozinka za dozvolu izmene podataka";
texts['gr'].write_access_pass = "Κωδικός πρόσβασης για  τροποποίηση";   

texts['bg'].write_access_pass_tooltip = "Натиснете тук за да въведете парола за достъп за манипулиране на данните";
texts['en'].write_access_pass_tooltip = "Click here to enter password to allow data modification";
texts['de'].write_access_pass_tooltip = "Hier klicken, um ein Passwort zur Modifizierung der Firmendaten einzugeben"; 
texts['ro'].write_access_pass_tooltip = "Apasați aici ca să introduceți parolă de acces pentru modificarea datelor"; 
texts['es'].write_access_pass_tooltip = "Haga clic aquí para introducir la contraseña de 'modificación' de los datos.";
texts['sr'].write_access_pass_tooltip = "Kliknite ovde da unesete lozinku za dozvolu izmene podataka.";
texts['gr'].write_access_pass_tooltip = "Πατήστε  εδώ για να εισαγάγετε έναν κωδικό πρόσβασης για να χειριστείτε τα δεδομένα";

texts['bg'].onlineTabTooltip = "Таблица с основни данни за превозните средства, които са онлайн.";
texts['en'].onlineTabTooltip = "A table showing current data for all online vehicles.";
texts['de'].onlineTabTooltip = "Datenubersicht der Fahrzeuge, die online sind."; 
texts['ro'].onlineTabTooltip = "Tabel cu datele principale pentru autovehiculelor , care sunt online"; 
texts['es'].onlineTabTooltip = "Tabla con los  datos actuales de todos los vehículos en línea"; 
texts['sr'].onlineTabTooltip = "Tabela koja pokazuje trenutne podatke za sva dostupna vozila. "; 
texts['gr'].onlineTabTooltip = "Πίνακας βασικών δεδομένων για τα οχήματα που είναι συνδεδεμένα στο διαδίκτυο.";

texts['bg'].notificationsTabTooltip = "Известия за наближаване на крайни срокове, подмяна на масло, плащания и др.";
texts['en'].notificationsTabTooltip = "Notifications about the expiration dates of the vehicles support.";
texts['de'].notificationsTabTooltip = "Meldungen von Fristen, wie Olwechsel, Bezahlen u.a."; 
texts['ro'].notificationsTabTooltip = "Notificări de expirarea termenelor , schimbarea uleiului , plătii, etc. "; 
texts['es'].notificationsTabTooltip = "Avisos de  próximos eventos (cambio de aceite, pagos mensuales e.t.c.) ";
texts['sr'].notificationsTabTooltip = "Obaveštenja o datumu isteka za održavanje vozila.  ";
texts['gr'].notificationsTabTooltip = "Ειδοποιήσεις σχετικά με την προθεσμία, την αλλαγή λαδιού, τις πληρωμές κ.λπ.";

texts['bg'].eventNotificationTabTooltip = "Известия за настъпване на събития";
texts['en'].eventNotificationTabTooltip = "Notifications for fired events";
texts['de'].eventNotificationTabTooltip = "";
texts['ro'].eventNotificationTabTooltip = "Notificări pentru apariția unor evenimente";
texts['es'].eventNotificationTabTooltip = "Avisos de  próximos eventos";
texts['sr'].eventNotificationTabTooltip = "Obaveštenja o nastupajućim događajima";
texts['gr'].eventNotificationTabTooltip = "Ειδοποιήσεις συμβάντων";

texts['bg'].dataTabTooltip = "Подробна информация за превозното средство, което е на фокус в онлайн таблицата. В зависимост от инсталираните датчици на фокусираното превозно средство се определя дължината на списъка.";
texts['en'].dataTabTooltip = "Detailed information about the vehicle that is being currently on focus. Depending on the installed sensors the amount of information may vary.";
texts['de'].dataTabTooltip = "Ausfuhrliche Informationen zum in der Tabelle ausgewahlten Fahrzeug. Die Lange der Ubersicht ist abhangig von der Anzahl der angeschlossenen Sensoren."; 
texts['ro'].dataTabTooltip = "Informație detaliată pentru vehiculul care este monitorizat.În funcție de senzorii instalați informațiile pot varia."; 
texts['es'].dataTabTooltip = "Información detallada del vehículo que está enfocado.El tamaño de la tabla es diferente, dependiendo de los sensores instalados.";
texts['sr'].dataTabTooltip = "Detaljna informacija o vozilu koje je trenutno u fokusu. U zavisnosti od instaliranih senzora količina informacija može da varira.";
texts['gr'].dataTabTooltip = "Λεπτομερείς πληροφορίες σχετικά με το όχημα που βρίσκεται στο επίκεντρο του ηλεκτρονικού πίνακα. Ανάλογα με τους αισθητήρες που είναι εγκατεστημένοι στο εστιασμένο όχημα, προσδιορίζεται το μήκος της λίστας.";

texts['bg'].serviceTabTooltip = "Сервизна информация за превозното средство, което е на фокус в онлайн таблицата. За да виждате тези данни трябва да попълните сервизен отчет за превозните средства като смяна на масло, автокаско, гражданска и др.";
texts['en'].serviceTabTooltip = "Service information about the vehicle that is being currently on focus. In order to see this kind of information you need to fill the neccessary service data.";
texts['de'].serviceTabTooltip = "Service-Info zum in der Tabelle ausgewahlten Fahrzeug. Um Daten anzuzeigen, mussen die Serviceberichte ausgefullt werden - z. B. Wartungen, Versicherungen usw."; 
texts['ro'].serviceTabTooltip = "Informațiii despre vehiculul care este monitorizat curent. Pentru a vedea acest tip de informații trebuie sa completați datele de service";
texts['es'].serviceTabTooltip = "Información de servicio del vehículo que está enfocado. Si usted quiere visualizar estés datos anteriormente tiene que introducir los datos de servicios del vehíuclo (cambio de aceite,seguro e.t.c.)";
texts['sr'].serviceTabTooltip = "Servisna informacija o vozilu koje je trenutno u fokusu. Da biste videli ovu informaciju treba da popunite potrebne servisne podatke.";
texts['gr'].serviceTabTooltip = "Πληροφορίες υπηρεσίας για το όχημα που βρίσκεται στο επίκεντρο του ηλεκτρονικού πίνακα.Για να δείτε αυτά τα δεδομένα, θα πρέπει να συμπληρώσετε μια αναφορά υπηρεσίας για οχήματα όπως αλλαγή λαδιού , ασφάλιση κ.λπ.";

texts['bg'].routeTabTooltip = "Информация за движението на автомобила по избран маршрут.";
texts['en'].routeTabTooltip = "Information about the route which the vehicle is following.";
texts['de'].routeTabTooltip = "Info uber die Fahrzeugbewegung entlang einer ausgewahlten Route ."; 
texts['ro'].routeTabTooltip = "Informație despre traseul/ruta pe care vehiclulul îl urmează ."; 
texts['es'].routeTabTooltip = "Información para el movimiento del vehículo por la ruta elegida ."; 
texts['sr'].routeTabTooltip = "Informacija o maršruti koju vozilo prati ."; 
texts['gr'].routeTabTooltip = "Πληροφορίες για την κυκλοφορία των οχημάτων στην επιλεγμένη διαδρομή.";

texts['bg'].addRemoveAllMarkedPlacesTooltip = "Натиснете тук за да добавите/премахнете всички маркирани обекти. (Ако в някое от полетата за търсене има въведен низ, който е филтрирал списъка с маркираните обекти ще бъдат показани само тези обекти, които се виждат в таблицата!)";
texts['en'].addRemoveAllMarkedPlacesTooltip = "Click here to add/remove all marked places. (If some of the input search fields contains string filter that affects the number of visible marked places this command will display only those that are visible on the table!)";
texts['de'].addRemoveAllMarkedPlacesTooltip = "Hier klicken, um alle markierten Objekte anzuzeigen / zu entfernen. ( Wenn in einem Suchfenster ein Filter aktiviert wurde, so beeinflusst er die Anzahl der sichtbaren markierten Objekte)"; 
texts['ro'].addRemoveAllMarkedPlacesTooltip = "Apasați aici ca să adaugați/ștergeți toate obiecte marcate. ( Dacă unele din câmpurile de cautare conțin litere/cifre care pot afecta numarul de locații marcate, această comandă le va indica doar pe cele care sunt vizibile în listă)"; 
texts['es'].addRemoveAllMarkedPlacesTooltip = "Haga clic aquí para añadir / eliminar todos los objetos marcados. ( Si algunos de los campos de búsqueda contienen letras / números que se pueden filtrar los  lugares marcados, se visualizan sólo aquellos que usted puede ver en la tabla)"; 
texts['sr'].addRemoveAllMarkedPlacesTooltip = "Kliknite ovde da dodate/uklonite sva obeležena mesta. (Ako neko polje za pretragu sadrži niz filtera onda to utiče na broj vidljivih obeleženih mesta, ova komanda će pokazati samo ona mesta koja se vide u tabeli!) "; 
texts['gr'].addRemoveAllMarkedPlacesTooltip = "Πατήστε εδώ για να προσθέσετε / αφαιρέσετε όλα τα επισημασμένα μέρη. (Αν κάποιο από τα πεδία αναζήτησης έχει μια συμβολοσειρά που έχει εισάγει και έχει φιλτράρει τη λίστα των σημειωμένων θέσεων που έχουν επισημανθεί,θα εμφανίζονται μόνο εκείνα που εμφανίζονται στον πίνακα!)";

texts['bg'].addRemoveAllMarkedPolygonsTooltip = "Натиснете тук за да добавите/премахнете всички маркирани терени. (Ако в някое от полетата за търсене има въведен низ, който е филтрирал списъка с маркираните терени ще бъдат показани само тези от тях, които се виждат в таблицата!)";
texts['en'].addRemoveAllMarkedPolygonsTooltip = "Click here to add/remove all marked terrains. (If some of the input search fields contains string filter that affects the number of visible marked terrains this command will display only those that are visible on the table!)";
texts['de'].addRemoveAllMarkedPolygonsTooltip = "Hier klicken, um alle markierten Gebiete anzuzeigen/zu entfernen. ( Wenn in einem Suchfenster ein Filter aktiviert wurde, so beeinflusst er die Anzahl der sichtbaren markierten Gebiete)"; 
texts['ro'].addRemoveAllMarkedPolygonsTooltip = "Apasați aici ca să adaugați/ștergeți toate terene marcate. ( Dacă unele din câmpurile de cautare conțin litere/cifre care pot afecta numarul de terenuri marcate, această comandă le va indica doar pe cele care sunt vizibile în listă )";
texts['es'].addRemoveAllMarkedPolygonsTooltip = "Haga clic aquí para añadir / eliminar todos los terrenos marcados. ( Si algunos de los campos de búsqueda contienen letras / números que pueden filtrar los  terrenos marcados, se visualizan sólo aquellos que usted puede ver en la tabla )";
texts['sr'].addRemoveAllMarkedPolygonsTooltip = "Kliknite ovde da dodate/uklonite sve obeležene terene. (Ako neko polje za pretragu sadrži niz filtera onda to utiče na broj vidljivih obeleženih terena, ova komanda će pokazati samo one terene koji se vide u tabeli!) ";
texts['gr'].addRemoveAllMarkedPolygonsTooltip = "Πατήστε εδώ για να προσθέσετε / αφαιρέσετε όλα τα επισημασμένα εδάφη. (Αν κάποιο από τα πεδία αναζήτησης έχει μια συμβολοσειρά που έχει εισάγει και έχει φιλτράρει τη λίστα επισημασμένου εδάφους,θα εμφανίζονται μόνο εκείνα που εμφανίζονται στον πίνακα!)";

texts['bg'].markedPlacesSearchTooltip = "Въведете името на маркирания обект, който търсите или поредица от букви/цифри за да филтрирате списъка.";
texts['en'].markedPlacesSearchTooltip = "Type the name of the marked place you want to find or simply enter some string to filter the table list.";
texts['de'].markedPlacesSearchTooltip = "Geben Sie  den Namen des markierten Objektes ein, oder einige Buchstaben bzw. Zahlen, um das Objekt aus der Liste zu filtern.";
texts['ro'].markedPlacesSearchTooltip = "Introduceți numele locului marcat pe care-l cautați sau introduceți litere/cifre ca să filtrați lista.";
texts['es'].markedPlacesSearchTooltip = "Introducir el nombre del lugar marcado que desea buscar o simplemente escribir serie de números/letras para filtrar la lista.";
texts['sr'].markedPlacesSearchTooltip = "Unesite naziv obeleženog mesta koje želite da nađete ili jednostavno unesite slovo ili broj da filtrirate spisak.";
texts['gr'].markedPlacesSearchTooltip = "Καταχωρίστε το όνομα του επισημασμένου μέρος  που αναζητάτε ή μια σειρά γραμμάτων / αριθμών για να φιλτράρετε τη λίστα.";

texts['bg'].markedPlacesRadiusTooltip = "Отметка дали маркираните обекти да бъдат визуализирани с радиуса им на обхват. (По-късно може да покажете/скриете радиуса на обхват на определени обекти като кликнете два пъти върху самия обект на картата.)";
texts['en'].markedPlacesRadiusTooltip = "Choose whether you want to show/hide the radiuses of the marked places.(If a marked place is on the map you can show/hide its radius by double clicking on its icon.";
texts['de'].markedPlacesRadiusTooltip = "Bestatigen, ob Sie den Radius der markierten Objekte einblenden/ausblenden mochten. ( Spater konnen Sie den Radius der markierten Objekte durch einen Doppelklick auf das Objekt einblenden/ausblenden.)"; 
texts['ro'].markedPlacesRadiusTooltip = "Alegeți dacă doriți să afișați / ascunde Razele locurilor marcate. ( În cazul în care un loc marcat este pe harta puteți afișa / ascunde raza prin dublu click pe pictograma sa.)";
texts['es'].markedPlacesRadiusTooltip = "Marque si usted quiere ver los objetos marcados con el perímetro de covertura. ( Más tarde se puede ocultar/mostrar el perimetro de cobertura de ciertos objetos haciendo doble clic sobre el objeto mismo en el mapa.)";
texts['sr'].markedPlacesRadiusTooltip = "Izaberite da li želite da prikažete/sakrijete radijuse obeleženih mesta.(Ako je obeleženo mesto na mapi onda možete da prikažete/sakrijete njegov radijus duplim klikom na njegovu ikonu.)";
texts['gr'].markedPlacesRadiusTooltip = "Ελέγξτε αν οι σημειωμένες θέσεις εμφανίζονται με την ακτίνα τους στην περιοχή. (Εάν υπάρχει ένα επισημασμένο μέρος στον χάρτη, μπορείτε να εμφανίσετε / αποκρύψετε την ακτίνα του κάνοντας διπλό κλικ στο εικονίδιο του.)";

texts['bg'].markedPlacesLabelTooltip = "Отметка дали маркираните обекти да бъдат визуализирани с етикета показващ името им. ( Ако имате много маркирани обекти тази опция може значително да натовари процесора ви защото слоевете върху картата стават двойно повече! По-късно може да покажете/скриете етикета на определени обекти като клинете веднъж с мишката върху самия обект на картата.)";
texts['en'].markedPlacesLabelTooltip = "Choose whether you want to show/hide the marked places labels.(NOTE: If you have lots of marked places on the map this option may slow down your computer and lag may occur while moving the map! Later you can show/hide the label of particular marked place by clicking on its icon.)";
texts['de'].markedPlacesLabelTooltip = "Bestatigen, ob Sie den Namen des markierten Objektes einblenden/ausblenden mochten. (Wenn Sie viele markierte Objekte haben, kann diese Funktion Ihren Rechner verlangsamen, weil sich die Schichten auf der Landkarte verdoppeln! Spater konnen Sie den Namen der markierten Objekten durch einen Einzelmausklick auf das Objekt einblenden/ausblenden.)";
texts['ro'].markedPlacesLabelTooltip = "Alegeți dacă doriți să afișați / ascunde locurile marcate etichete (NOTA:.! Dacă aveți o mulțime de locuri de marcat pe harta, această opțiune poate încetini computerul dumneavoastră și întârziere pot să apară în timp ce se deplasează pe harta Mai târziu, puteți afișa / ascunde eticheta unui anumit marcat loc, făcând clic pe pictograma sa..)";
texts['es'].markedPlacesLabelTooltip = "Seleccionar para mostrar/ocultar la etiqueta con el nombre de los objetos marcados. (NOTA:.Si hay muchos objetos marcados  sobre del mapa, esta opción puede ralentizar el equipo. Haga doble clic  para mostrar/ocultar la etiqueta.)";
texts['sr'].markedPlacesLabelTooltip = "Izaberite da li želite da prikažete/sakrijete oznake obeleženih mesta. (UPOZORENJE: Ako imate puno obeleženih mesta na mapi ova opcija može usporiti vaš kompjuter i može doći do usporavanja dok pomerate mapu! Kasnije možete da prikažete/sakrijete oznaku određenog obeleženog mesta klikom na njegovu ikonu.)";
texts['gr'].markedPlacesLabelTooltip = "Ελέγξτε αν οι σημειωμένες θέσεις εμφανίζονται με την ετικέτα που εμφανίζει το όνομά τους. (Εάν έχετε πολλές  σημειωμένες θέσεις με ετικέτες, αυτή η επιλογή μπορεί να φορτώσει σημαντικά τον επεξεργαστή σας επειδή τα επίπεδα στο χάρτη έχουν διπλασιαστεί! Αργότερα μπορείτε να εμφανίσετε / αποκρύψετε την ετικέτα σε ορισμένα αντικείμενα κάνοντας κλικ στο ίδιο το χάρτη του αντικειμένου του χάρτη).";


texts['bg'].placesGroupSearchTooltip = "Търсене на група.";
texts['en'].placesGroupSearchTooltip = "Search for a group.";
texts['de'].placesGroupSearchTooltip = "Gruppe Suchen"; 
texts['ro'].placesGroupSearchTooltip = "Caută grupă"; 
texts['es'].placesGroupSearchTooltip = "Búsqueda de grupo";
texts['sr'].placesGroupSearchTooltip = "Pretraga grupe";
texts['gr'].placesGroupSearchTooltip = "Αναζήτηση ομάδας.";

texts['bg'].markedPolygonSearchTooltip = "Въведете име на маркиран терен или поредица от думи и цифри за да филтрирате списъка с терени.";
texts['en'].markedPolygonSearchTooltip = "Type the name of the marked terrain you want to find or simply enter some string to filter the table list.";
texts['de'].markedPolygonSearchTooltip = "Geben Sie  den Namen des markierten Gebietes ein, oder einige Buchstaben bzw. Zahlen, um das Gebiet aus der Liste zu filtern.";
texts['ro'].markedPolygonSearchTooltip = "Introduceți nume de teren marcat pe care doriți sa-l găsiți sau doar introduceți o serie de cuvinte/și cifre ca să filtrați lista cu terenuri .";
texts['es'].markedPolygonSearchTooltip = "Introducir el nombre del terreno marcado que desea buscar o simplemente escribir serie de números/letras para filtrar la lista.";
texts['sr'].markedPolygonSearchTooltip = "Ukucajte naziv obeleženog terena koji želite da nađete ili jednostavno unesite reč ili broj da filtrirate spisak iz tabele.";
texts['gr'].markedPolygonSearchTooltip = "Καταχωρίστε ένα όνομα για ένα επισημασμένο έδαφος ή μια σειρά λέξεων και αριθμών για να φιλτράρετε τη λίστα εδάφους.";

texts['bg'].markedPolygonsContourTooltip = "Отметка дали маркираните терени да бъдат визуализирани с контура им на обхват. (По-късно може да покажете/скриете контура на обхват на определени обекти като кликнете два пъти върху иконата на терена.)";
texts['en'].markedPolygonsContourTooltip = "Choose whether you want to show/hide the boundaries of the marked terrains.(If a marked terrain is on the map you can show/hide its boundaries by double clicking on its icon.";
texts['de'].markedPolygonsContourTooltip = "Bestatigen, ob Sie die Kontur der markierten Gebiete einblenden/ausblenden mochten. (Spater konnen Sie die Kontur der markierten Gebiete durch einen Doppelklick auf das Symbol des Gebietes einblenden/ausblenden)"; 
texts['ro'].markedPolygonsContourTooltip = "Alegeți dacă doriți să afișați / ascundeți limitele terenurilor marcate. (În cazul în care un teren marcat este pe harta puteți afișa / ascunde limitele sale dând dublu click pe pictograma sa"; 
texts['es'].markedPolygonsContourTooltip = "Marque si usted quiere ver los terrenos marcados con el perímetro de covertura. ( Más tarde se puede ocultar/mostrar el perimetro de cobertura de ciertos terrenos haciendo doble clic sobre el objeto mismo en el mapa.)"; 
texts['sr'].markedPolygonsContourTooltip = "Izaberite da li želite da prikažete/sakrijete granice obeleženih terena. ( Ako je obeleženi teren na mapi možete da prikažete/sakrijete njegove granice duplim klikom na njegovu ikonu.)"; 
texts['gr'].markedPolygonsContourTooltip = "Επισημαίνει αν εμφανίζονται επισημασμένα εδάφη εμφανίζονται με το περίγραμμά τους. (Αργότερα μπορείτε να εμφανίσετε / αποκρύψετε την περιοχή κάνοντας διπλό κλικ στο εικονίδιο του εδάφους.)";


texts['bg'].markedPolygonsLabelTooltip = "Отметка дали маркираните терени да бъдат визуализирани с етикета показващ името им. ( Ако имате много маркирани терени тази опция може значително да натовари процесора ви защото слоевете върху картата стават двойно повете! По-късно може да покажете/скриете етикета на определени терени като клинете веднъж с мишката върху иконата им.)";
texts['en'].markedPolygonsLabelTooltip = "Choose whether you want to show/hide the marked terrains labels.(NOTE: If you have lots of marked terrains on the map this option may slow down your computer and lag may occur while moving the map! Later you can show/hide the label of particular marked terrain by clicking on its icon.)";
texts['de'].markedPolygonsLabelTooltip = "Bestatigen, ob Sie den Namen der markierten Gebiete einblenden/ausblenden mochten. (Wenn Sie viele markierte Gebiete haben, kann diese Funktion Ihren Rechner verlangsamen, weil sich die Schichten auf der Landkarte verdoppeln! Spater konnen Sie den Namen der markierten Gebiete durch einen Einzelmausklick auf das Objekt einblenden/ausblenden.)";
texts['ro'].markedPolygonsLabelTooltip = "Alegeți dacă doriți să afișați / ascunde etichetele terenurile marcate (Notă Dacă aveți o mulțime de terenuri marcate pe hartă această opțiune poate încetini computerul dumneavoastră și întârziere pot să apară în timp ce se deplasează pe harta. Mai târziu, puteți afișa / ascunde eticheta! unui anumit marcat teren, făcând click pe pictograma sa.)";
texts['es'].markedPolygonsLabelTooltip = "Seleccionar para mostrar/ocultar la etiqueta con el nombre de los terrenos marcados. (NOTA:.Si hay muchos terrenos marcados  sobre del mapa, esta opción puede ralentizar el equipo. Haga clic dos veces para mostrar/ocultar la etiqueta.)";
texts['sr'].markedPolygonsLabelTooltip = "Izaberite da li želite da prikažete/sakrijete oznake obeleženih terena. (UPOZORENJE: Ako imate puno obeleženih terena na mapi ova opcija može usporiti vaš kompjuter i može doći do usporavanja dok pomerate mapu! Kasnije možete da prikažete/sakrijete oznaku određenog označenog terena klikom na njegovu ikonu.)";
texts['gr'].markedPolygonsLabelTooltip = "Επισημαίνει αν εμφανίζονται τα επισημασμένα εδάφη με την ετικέτα που δείχνει το όνομά τους. (Εάν έχετε πολύ επιφανειακό έδαφος, αυτή η επιλογή μπορεί να φορτώσει σημαντικά την CPU σας επειδή τα επίπεδα στο χάρτη διπλασιάζονται! Αργότερα μπορείτε να εμφανίσετε / αποκρύψετε την ετικέτα σε συγκεκριμένα εδάφη κάνοντας κλικ στο εικονίδιο).";

texts['bg'].putAllVehiclesOnlineTooltip = "Натиснете тук за да наблюдавате всички превозни средства от списъка. Ако сте въвели филтър в полетата за търсене ще се свържете само с тези превозни средства, които са видими в списъка по-долу!";
texts['en'].putAllVehiclesOnlineTooltip = "Clich here to put all vehicles on the map for real time monitoring. If you have entered a filter in the search fields there will be put on the map only those vehicles that are currently visible in the table.";
texts['de'].putAllVehiclesOnlineTooltip = "Hier klicken, um alle Fahrzeuge in der Liste anzuzeigen. Wenn Sie einen Filter im Suchfenster aktiviert haben, sehen Sie nur die Fahrzeuge, die den Suchkriterien entsprechen."; 
texts['ro'].putAllVehiclesOnlineTooltip = "Apasați aici ca să monitorizați toate vehicule de la lista. Dacă ați introdus un filtru in câmpurile de căutare vor fi puse pe hartă doar vehiculele care sunt vizibile in listă .";
texts['es'].putAllVehiclesOnlineTooltip = "Si ha introducido en el campo de búsqueda números o letras van a filtrar los vehículos y estarán disponibles solo los vehículo filtrados .";
texts['sr'].putAllVehiclesOnlineTooltip = "Kliknite ovde da stavite sva vozila na mapu za praćenje u realnom vremenu. Ako ste uneli filter u polja za pretragu onda će na mapi biti samo ona vozila koja se trenutno vide u tabeli.";
texts['gr'].putAllVehiclesOnlineTooltip = "Πατήστε εδώ για να παρακολουθήσετε όλα τα οχήματα της λίστας. Εάν έχετε εισάγει ένα φίλτρο στα πλαίσια αναζήτησης, θα συνδεθείτε μόνο σε εκείνα τα οχήματα που είναι ορατά στην παρακάτω λίστα!";

texts['bg'].groupVehiclesGroupTooltip = "Натиснете тук за да групирате превозните средства по групи с цел по-лесно навигиране";
texts['en'].groupVehiclesGroupTooltip = "Click here to group the vehicles.";
texts['de'].groupVehiclesGroupTooltip = "Hier klicken, um die Fahrzeuge in Gruppen einzuordnen. So konnen Sie die Anzeige leichter steuern."; 
texts['ro'].groupVehiclesGroupTooltip = "Apasați ca să grupați vehiculelor în grupe,  pentru o navigare mai ușoară."; 
texts['es'].groupVehiclesGroupTooltip = "Haga clic aquí para agrupación de los vehículos."; 
texts['sr'].groupVehiclesGroupTooltip = "Kliknite ovde da grupišete vozila."; 
texts['gr'].groupVehiclesGroupTooltip = "Πατήστε εδώ για να ομαδοποιήσετε τα οχήματά σας σε ομάδες για να διευκολύνετε την πλοήγηση";

texts['bg'].removeGroupVehiclesGroupTooltip = "Натиснете тук за да премахнете групирането на превозните средства";
texts['en'].removeGroupVehiclesGroupTooltip = "Click here to remove the vehicles grouping.";
texts['de'].removeGroupVehiclesGroupTooltip = "Hier klicken, um die Fahrzeuggruppen zu entfernen"; 
texts['ro'].removeGroupVehiclesGroupTooltip = "Apasați aici ca să eliminați gruparea vehiculelor"; 
texts['es'].removeGroupVehiclesGroupTooltip = "Haga clic aquí para eliminar agrupación de los vehículos";
texts['sr'].removeGroupVehiclesGroupTooltip = "Kliknite ovde da uklonite grupisanje vozila.";
texts['gr'].removeGroupVehiclesGroupTooltip = "Πατήστε  εδώ για να καταργήσετε την ομαδοποίηση των οχημάτων";

texts['bg'].searchGroupTooltip = "Въведете името на групата която търсите или поредица от букви и цифри за да филтрирате списъка.";
texts['en'].searchGroupTooltip = "Search for a group.";
texts['de'].searchGroupTooltip = "Geben Sie den Namen der gesuchten Gruppe ein,  oder einige Buchstaben bzw. Zahlen, um die Gruppe aus der Liste zu filtern.";
texts['ro'].searchGroupTooltip = "Introduceți numele grupei care cautați sau  serie de litere și cifre ca să filtrați lista. ";
texts['es'].searchGroupTooltip = "Buscar grupo.Introduzca números o letras para filtrar los grupos. ";
texts['sr'].searchGroupTooltip = "Pronađite grupu. ";
texts['gr'].searchGroupTooltip = "Πληκτρολογήστε το όνομα της ομάδας που αναζητάτε ή μια σειρά γραμμάτων και αριθμών για να φιλτράρετε τη λίστα.";

texts['bg'].searchVehicleTooltip = "Въведете името на превозното средство което търсите или поредица от букви и цифри за да филтрирате списъка.";
texts['en'].searchVehicleTooltip = "Search for a vehicle.";
texts['de'].searchVehicleTooltip = "Geben Sie den Namen des gesuchten Fahrzeuges ein, oder einige Buchstaben bzw. Zahlen, um das Fahrzeug aus der Liste zu filtern."; 
texts['ro'].searchVehicleTooltip = " Intriduceți numele de vehiculul care cautați sau serie de litere și cifre ca să filtrați lista."; 
texts['es'].searchVehicleTooltip = " Buscar vehículo.Introduzca números o letras para filtrar los vehículos."; 
texts['sr'].searchVehicleTooltip = " Pronađite vozilo."; 
texts['gr'].searchVehicleTooltip = "Καταχωρίστε το όνομα του οχήματος που αναζητάτε ή μια σειρά γραμμάτων και αριθμών για να φιλτράρετε τη λίστα.";

texts['bg'].showHideAllVehiclesTooltip = "Натиснете тук за да се скриете/покажете превозните средства от/на картата.";
texts['en'].showHideAllVehiclesTooltip = "Click here to show/hide the vehicles on the map.";
texts['de'].showHideAllVehiclesTooltip = "Hier klicken, um die Fahrzeuge auf der Landkarte einzublenden/auszublenden."; 
texts['ro'].showHideAllVehiclesTooltip = "Apasați aici ca să ascundeți/aratați vehiculelor din hartă.";
texts['es'].showHideAllVehiclesTooltip = "Haga clic para mostrar/ocultar el vehículo del mapa.";
texts['sr'].showHideAllVehiclesTooltip = "Kliknite ovde da prikažete/sakrijete vozila na mapi.";
texts['gr'].showHideAllVehiclesTooltip = "Πατήστε  εδώ για να αποκρύψετε / εμφανίσετε τα οχήματα στο χάρτη.";


texts['bg'].showHideAllGroupVehiclesTooltip = "Натиснете тук за да скриете/покажете превозните средства принадлежащи към тази група.";
texts['en'].showHideAllGroupVehiclesTooltip = "Click here to show/hide the vehicles that belong the this group.";
texts['de'].showHideAllGroupVehiclesTooltip = "Hier klicken um die Fahrzeuge, die zu dieser Gruppe gehoren, einzublenden/auszublenden."; 
texts['ro'].showHideAllGroupVehiclesTooltip = "Apasați aici ca să ascundeți/aratați vehiculelor care aparțin aceastui grup."; 
texts['es'].showHideAllGroupVehiclesTooltip = "Haga clic para mostrar/ocultar el vehículo que pertenece a este grupo."; 
texts['sr'].showHideAllGroupVehiclesTooltip = "Kliknite ovde da prikažete/sakrijete vozila koja pripadaju ovoj grupi."; 
texts['gr'].showHideAllGroupVehiclesTooltip = "Πατήστε  εδώ για να αποκρύψετε / εμφανίσετε οχήματα που ανήκουν σε αυτήν την ομάδα.";

texts['bg'].no_route_defined = "не е посочен";
texts['en'].no_route_defined = "not selected";
texts['de'].no_route_defined = "nicht ausgewahlt"; 
texts['ro'].no_route_defined = "nu este selectat"; 
texts['es'].no_route_defined = "no seleccionado";
texts['sr'].no_route_defined = "nije odabrano";
texts['gr'].no_route_defined = "δεν έχει επιλεγεί";

texts['bg'].number_of_requests = "Брой направени заявки към сървъра";
texts['en'].number_of_requests = "Number of requests sent to server";
texts['de'].number_of_requests = "Anzahl der Server Anfragen"; 
texts['ro'].number_of_requests = "Număr de cereri trimise la server"; 
texts['es'].number_of_requests = "Número de solicitudes  al servidor";
texts['sr'].number_of_requests = "Broj zahteva poslat serveru";
texts['gr'].number_of_requests = "Αριθμός αιτήσεων που υποβάλλονται στο διακομιστή";

texts['bg'].enter_unused_marked_place_name = "Желателно е да въведете неизползвано име!";
texts['en'].enter_unused_marked_place_name = "Please make sure you enter a name that is not already used!";
texts['de'].enter_unused_marked_place_name = "Geben Sie einen freien Namen ein!"; 
texts['ro'].enter_unused_marked_place_name = "Vă rog asigurațivă că introduceți un nume care nu este deja folosit"; 
texts['es'].enter_unused_marked_place_name = "¡Por favor, asegúrese de introducir un nombre que no esté ya utilizado!";
texts['sr'].enter_unused_marked_place_name = "Proverite da niste uneli ime koje je već upotrebljeno!";
texts['gr'].enter_unused_marked_place_name = "Είναι επιθυμητό να εισάγετε ένα αχρησιμοποίητο όνομα!";

texts['bg'].online_offlineVehiclesWindow_text = "Списък с превозните средства, които са достъпни за наблюдение";
texts['en'].online_offlineVehiclesWindow_text = "A list of vehicles that can be monitored.";
texts['de'].online_offlineVehiclesWindow_text = "Liste der Fahrzeuge, die uberwacht werden konnen"; 
texts['ro'].online_offlineVehiclesWindow_text = "Lista cu vehicule , care sunt disponibile pentru monitorizare"; 
texts['es'].online_offlineVehiclesWindow_text = "Lista de los vehículos que están disponibles para el monitoreo";
texts['sr'].online_offlineVehiclesWindow_text = "Spisak vozila koja mogu da se prate.";
texts['gr'].online_offlineVehiclesWindow_text = "Κατάλογος των οχημάτων που είναι διαθέσιμα για επιτήρηση";

texts['bg'].online_firmsWindow_text = "Налични фирми в системата";
texts['en'].online_firmsWindow_text = "A list with the registered firms.";
texts['de'].online_firmsWindow_text = "Firmenliste"; 
texts['ro'].online_firmsWindow_text = "Lista cu firme registrate în sistemul"; 
texts['es'].online_firmsWindow_text = "Empresas disponibles en el sistema";
texts['sr'].online_firmsWindow_text = "Spisak registrovanih firmi.";
texts['gr'].online_firmsWindow_text = "Διαθέσιμες εταιρείες στο σύστημα";

texts['bg'].online_routesWindow_text = "Списък с наличните маршрути и престои свързани с тях.";
texts['en'].online_routesWindow_text = "A list with all the routes and their intermidiate points.";
texts['de'].online_routesWindow_text = "Liste der erstellten Routen und die damit verbundenen Aufenthalte."; 
texts['ro'].online_routesWindow_text = "Lista cu toate rutele si cu punctele lor intermediare.";
texts['es'].online_routesWindow_text = "Lista con todas las rutas y sus puntos intermedios.";
texts['sr'].online_routesWindow_text = "Spisak svih maršruta i njihovih medjutačaka.";
texts['gr'].online_routesWindow_text = "Λίστα των διαθέσιμων διαδρομών και των στάσεων  που συνδέονται με αυτά.";

texts['bg'].no_online_vehicles = "Няма налични превозни средства онлайн.";
texts['en'].no_online_vehicles = "No vehicles online";
texts['de'].no_online_vehicles = "Keine Fahrzeuge online"; 
texts['ro'].no_online_vehicles = "Nu sunt vehicule online"; 
texts['es'].no_online_vehicles = "No hay vehículos en línea"; 
texts['sr'].no_online_vehicles = "Nema dostupnih vozila."; 
texts['gr'].no_online_vehicles = "Δεν υπάρχουν διαθέσιμα οχήματα σε απευθείας σύνδεση.";

texts['bg'].poi_icon_selection_description = "Изберете типът икони, които искате да заредите и натиснете два пъти избраната икона за да направите промяната.";
texts['en'].poi_icon_selection_description = "Choose the icon types you would like to load and then double click on an icon to use it.";
texts['de'].poi_icon_selection_description = "Wahlen Sie das Symbol aus, das Sie verwenden mochten und klicken Sie zweimal darauf um das ausgewahlte Symbol einzusetzen.";
texts['ro'].poi_icon_selection_description = "Alegeți tipurile de icon pe care doriți să încărcați și apoi dublu click pe o pictogramă să-l folosiți.";
texts['es'].poi_icon_selection_description = "Seleccione el tipo de iconos que desea cargar y haga doble clic en el icono seleccionado para hacer el cambio.";
texts['sr'].poi_icon_selection_description = "Izaberite tip ikone koju želite da učitate i kliknite dvaput na ikonu da je upotrebite.";
texts['gr'].poi_icon_selection_description = "Επιλέξτε τον τύπο των εικονιδίων που θέλετε να φορτώσετε και κάντε διπλό κλικ στο εικονίδιο που επιλέξατε για να κάνετε την αλλαγή.";

texts['bg'].winTitle_add_newpolygon = "Формуляр за добавяне на нов полигон в системата.";
texts['en'].winTitle_add_newpolygon = "A form for adding new polygon.";
texts['de'].winTitle_add_newpolygon = "Gebiet erstellen."; 
texts['ro'].winTitle_add_newpolygon = "Formular pentru adăugarea de noi zone de lucru.";
texts['es'].winTitle_add_newpolygon = "Formulario para añadir de nuevo polígono.";
texts['sr'].winTitle_add_newpolygon = "Obrazac za dodavanje novog poligona.";
texts['gr'].winTitle_add_newpolygon = "Φόρμα για να προσθέσετε ένα νέο πολύγωνο στο σύστημα.";

texts['bg'].confirm_marked_place_delete = "Сигурни ли сте, че искате да изтриете този обект от системата?";
texts['en'].confirm_marked_place_delete = "Are you sure you want to delete this marked place?";
texts['de'].confirm_marked_place_delete = "Sind Sie sicher, dass Sie dieses Objekt loschen mochten?"; 
texts['ro'].confirm_marked_place_delete = "Sunteți sigur, că doriți să ștergeți acest locații marcate din sistem?"; 
texts['es'].confirm_marked_place_delete = "¿Seguro que quieres eliminar este objeto del sistema?"; 
texts['sr'].confirm_marked_place_delete = "Da li ste sigurni da želite da izbrišete ovo obeleženo mesto?"; 
texts['gr'].confirm_marked_place_delete = "Είστε βέβαιοι ότι θέλετε να διαγράψετε αυτό το μέρος από το σύστημα;";

texts['bg'].confirm_marked_polygon_delete = "Сигурни ли сте, че искате да изтриете този терен от системата?";
texts['en'].confirm_marked_polygon_delete = "Are you sure you want to delete this marked terrain?";
texts['de'].confirm_marked_polygon_delete = "Sind Sie sicher, dass Sie dieses Gebiet loschen mochten?"; 
texts['ro'].confirm_marked_polygon_delete = "Sunteți sigur, că vreți să ștergeți acest teren din sistem?"; 
texts['es'].confirm_marked_polygon_delete = "¿Seguro que quieres eliminar este terreno del sistema?"; 
texts['sr'].confirm_marked_polygon_delete = "Da li ste sigurni da želite da izbrišete ovaj obeleženi teren?";
texts['gr'].confirm_marked_polygon_delete = "Είστε βέβαιοι ότι θέλετε να διαγράψετε αυτό το έδαφος από το σύστημα;";

texts['bg'].enter_refresh_marker_milliseconds = "Въведете през колко милисекунди да се опреснява маркера. (1 секунда = 1000 милисекунди)";
texts['en'].enter_refresh_marker_milliseconds = "Enter the amount of milliseconds between marker movement. (1 second = 1000 milliseconds)";
texts['de'].enter_refresh_marker_milliseconds = "Bestimmen Sie die Anzahl von Millisekunden zwischen den Marker-Bewegungen. (1 Sekunde = 1000 Millisekunden)."; 
texts['ro'].enter_refresh_marker_milliseconds = "Introduceți numărul de milisecunde intre miscările de marcare  (1 Secundă = 1000 Millisecunde)."; 
texts['es'].enter_refresh_marker_milliseconds = "Introducir  cada  cuántos milisegundos  el marcador  se va refrescar (1 segundo = 1000 milisegundos)."; 
texts['sr'].enter_refresh_marker_milliseconds = "Unesite broj milisekundi između pokreta markera. (1 sekunda = 1000 milisekundi)"; 
texts['gr'].enter_refresh_marker_milliseconds = "Καταχωρίστε πόσες χιλιοστά του δευτερολέπτου για να ανανεώνεται το δείκτη. (1 δευτερόλεπτο = 1000 χιλιοστά του δευτερολέπτου)";

texts['bg'].alert_route_end_not_defined =  "Трябва да отбележите крайна точка на маршрута. За целта движете плъзгача по времевата линия докато намерите исканото от вас място или го преместетe в края на времевата линия за да запаметите цялата следа като маршрут.";
texts['en'].alert_route_end_not_defined =  "You must select an end point for this route. Just move the slider until you decide that this is the end point you'd like to have for this route.";
texts['de'].alert_route_end_not_defined =  "Sie sollen einen Endpunkt der Route wahlen. Verschieben Sie den Cursor bis zum ausgewahlten Punkt, oder stellen Sie den Cursor ans Ende der Zeitlinie, um die ganze Spur als eine andere Route zu speichern.";
texts['ro'].alert_route_end_not_defined =  "Trebuie să selectați un punct final pentru acest traseu. Doar mutați cursorul până când decideți că acesta este punctul final pe care doriți să aibă, pentru acest traseu.";
texts['es'].alert_route_end_not_defined =  "Tenga que de marcar el punto final de la ruta. Simplemente deslizar  por la ruta hasta la posición que usted quiere marcar como punto de final .";
texts['sr'].alert_route_end_not_defined =  "Morate izabrati krajnju tačku za ovu maršrutu. Samo povucite klizač dok ne odlučite koja je krajnja tačka koju želite za ovu maršrutu.";
texts['gr'].alert_route_end_not_defined =  "Πρέπει να σημειώσετε το τελικό σημείο της διαδρομής. Για να το κάνετε αυτό, μετακινήστε το ρυθμιστικό κατά μήκος της γραμμής χρόνου μέχρι να βρείτε το σημείο που θέλετε ή να το μετακινήσετε στο τέλος της χρονικής γραμμής για να αποθηκεύσετε ολόκληρο το κομμάτι ως διαδρομή. ";

texts['bg'].route_end_point_info = "Крайната точка на маршрута ще бъде текущото положение на превозното средство по времевата линия. Сигурни ли сте, че искате да продължите?";  
texts['en'].route_end_point_info = "The end point of the route will be the current vehicle's position. Are you sure you'd like to continue?.";  
texts['de'].route_end_point_info = "Der Endpunkt der Route wird die gegenwartige Position des Fahrzeugs sein. Sind Sie sicher, dass Sie weitermachen mochten?"; 
texts['ro'].route_end_point_info = "Punctul final al traseului va fi poziția vehiculului curent. Sunteți sigur că doriți să continuați?";
texts['es'].route_end_point_info = "El punto final de la ruta será la posición actual del vehículo en la línea de tiempo. ¿Estás seguro de que quiere continuar?";
texts['sr'].route_end_point_info = "Krajnja tačka maršrute će biti trenutna pozicija vozila. Da li ste sigurni da želite da nastavite?";
texts['gr'].route_end_point_info = "Το τελικό σημείο της διαδρομής θα είναι η τρέχουσα θέση του οχήματος στη χρονολογική σειρά. Είστε βέβαιοι ότι θέλετε να συνεχίσετε?";  

texts['bg'].winTitle_stops_to_route_stops_convertion = "Избор на престоите, които да бъдат конвертирани в зони за спиране по маршрута."; 
texts['en'].winTitle_stops_to_route_stops_convertion = "List of route stops."; 
texts['de'].winTitle_stops_to_route_stops_convertion = "Auswahl der Aufenthalte, die als Aufenthaltszonen verwendet werden."; 
texts['ro'].winTitle_stops_to_route_stops_convertion = "Lista cu perioadele de oprire."; 
texts['es'].winTitle_stops_to_route_stops_convertion = "Lista de paradas por la ruta.";
texts['sr'].winTitle_stops_to_route_stops_convertion = "Izbor pauza koje će se koristiti kao parking zone duž maršrute.";
texts['gr'].winTitle_stops_to_route_stops_convertion = "Λίστα στάσεις διαδρομή.";

texts['bg'].getStopNameTooltip = "Натиснете тук за да получите наименованието на престоя ИЛИ алтернативно може да въведете име по ваш избор."; 
texts['en'].getStopNameTooltip = "Click here to reverse geocode the stop location or simply enter some name."; 
texts['de'].getStopNameTooltip = "Hier klicken, um den Aufenthaltsnamen zu bestatigen oder geben Sie einen anderen Namen nach Ihrer Wahl ein."; 
texts['ro'].getStopNameTooltip = "Apasați aici ca să primiți denumirea locului, perioadei de oprire sau introduceți numele după alegerea dvs  ."; 
texts['es'].getStopNameTooltip = "Haga clic para pueda visualizar los nombres de los paradas o introducir un nuevo nombre.  ."; 
texts['sr'].getStopNameTooltip = "Kliknite ovde da promenite geografski naziv lokacije pauze ili jednostavno unesite neko ime."; 
texts['gr'].getStopNameTooltip = "Πατήστε εδώ για να πάρετε το όνομα της διαμονής ή εναλλακτικά μπορείτε να εισάγετε ένα όνομα της επιλογής σας."; 

texts['bg'].editCellTooltip = "Натиснете тук за да редактирате полето";
texts['en'].editCellTooltip = "Click here to edit.";
texts['de'].editCellTooltip = "Hier klicken, um das Feld zu bearbeiten"; 
texts['ro'].editCellTooltip = "Apasați aici ca să editați ..";
texts['es'].editCellTooltip = "Haga clic para editar el campo..";
texts['sr'].editCellTooltip = "Kliknite ovde za izmene.";
texts['gr'].editCellTooltip = "Πατήστε  εδώ για να επεξεργαστείτε το πεδίο";

texts['bg'].centerMapTooltip = "Натиснете тук за да центрирате картата върху избраното място.";
texts['en'].centerMapTooltip = "Click here to center the map on the selected place.";
texts['de'].centerMapTooltip = "Hier klicken, um die Landkarte auf einen ausgewahlten Ort zu zentrieren."; 
texts['ro'].centerMapTooltip = "Apasați aici ca să centrați harta pe locul ales."; 
texts['es'].centerMapTooltip = "Haga clic aquí para centrar el mapa en el lugar seleccionado."; 
texts['sr'].centerMapTooltip = "Kliknite ovde da centrirate mapu na izabrano mesto."; 
texts['gr'].centerMapTooltip = "Πατήστε  εδώ για να κεντράρετε το χάρτη στην επιλεγμένη τοποθεσία.";

texts['bg'].promptRouteName = "Въведете име на маршрута, което да е повече от 3 символа.";
texts['en'].promptRouteName = "Enter a route name having atleast 3 symbols.";
texts['de'].promptRouteName = "Geben Sie einen Routennamen langer als drei Symbole ein."; 
texts['ro'].promptRouteName = "Introduceti nume unui traseu , care să conțină cel puțin 3 simboluri.";
texts['es'].promptRouteName = "Introduzca nombre de la ruta más de tres símbolos.";
texts['sr'].promptRouteName = "Unesite ime maršrute koje sadrži bar 3 simbola.";
texts['gr'].promptRouteName = "Καταχωρίστε ένα όνομα διαδρομής μεγαλύτερο από 3 χαρακτήρες.";

texts['bg'].error_routeName_length = "Името трябва да е поне 3 символа!";
texts['en'].error_routeName_length = "The name must be atleast 3 symbols!";
texts['de'].error_routeName_length = "Der Name muss mindestens drei Symbole enthalten!"; 
texts['ro'].error_routeName_length = "Numele trebuie să conțină cel puțin 3 simboluri!";
texts['es'].error_routeName_length = "¡El nombre debe ser mínimum tres símbolos!";
texts['sr'].error_routeName_length = "Ime mora da sadrži bar 3 simbola!";
texts['gr'].error_routeName_length = "Το όνομα πρέπει να είναι τουλάχιστον 3 χαρακτήρες!";

texts['bg'].routeSavedSuccessfully = "Съхраняването на маршрута приключи успешно.";
texts['en'].routeSavedSuccessfully = "The route was saved succesfully.";
texts['de'].routeSavedSuccessfully = "Die Route wurde erfolgreich gespeichert."; 
texts['ro'].routeSavedSuccessfully = "Itinerariul/traseul a fost salvat cu succes."; 
texts['es'].routeSavedSuccessfully = "La ruta se ha guardado correctamente.";
texts['sr'].routeSavedSuccessfully = "Maršruta je uspešno sačuvana.";
texts['gr'].routeSavedSuccessfully = "Η διαδρομή αποθηκεύτηκε με επιτυχία.";

texts['bg'].errorSavingRoute = "Възникна грешка при опит да бъде съхранен маршрута в базата данни.";
texts['en'].errorSavingRoute = "An error occured while trying to save the route.";
texts['de'].errorSavingRoute = "Fehler beim Versuch, die Route zu speichern."; 
texts['ro'].errorSavingRoute = "O eroare  a aparut las salvarea traseului/rutei în baza de date .";
texts['es'].errorSavingRoute = "Se ha producido un error al intentar guardar la ruta.";
texts['sr'].errorSavingRoute = "Pojavila se greška pri pokušaju da se sačuva maršruta.";
texts['gr'].errorSavingRoute = "Παρουσιάστηκε σφάλμα κατά την προσπάθεια αποθήκευσης της διαδρομής στη βάση δεδομένων.";

texts['bg'].errorNotEnoughData = "Поради липса на достатъчно данни справката не може да бъде изпълнена";
texts['en'].errorNotEnoughData = "Not enough data to finish the report.";
texts['de'].errorNotEnoughData = "Wegen Datenmangel kann der Bericht nicht erstellt werden"; 
texts['ro'].errorNotEnoughData = "Din cauză lipsei de date raportul nu poate fi făcut";
texts['es'].errorNotEnoughData = "No hay datos suficientes para terminar el informe";
texts['sr'].errorNotEnoughData = "Nema dovoljno podataka da se završi izveštaj.";
texts['gr'].errorNotEnoughData = "Λόγω έλλειψης επαρκών δεδομένων, η αναφορά δεν μπορεί να εκτελεστεί";

texts['bg'].day_road_list_for = "Дневен пътен лист за";
texts['en'].day_road_list_for = "Day road list for";
texts['de'].day_road_list_for = "Tagesfahrtenbuch"; 
texts['ro'].day_road_list_for = "Raport zilnic de"; 
texts['es'].day_road_list_for = "Informe hoja de ruta diaria para"; 
texts['sr'].day_road_list_for = "Dnevni izveštaj za"; 
texts['gr'].day_road_list_for = "Καθημερινό οδικό χάρτη για";

texts['bg'].stops_road_list_for = "Пътен лист за престои за";
texts['en'].stops_road_list_for = "Stops road list for";
texts['de'].stops_road_list_for = "Aufenthaltsberichte";
texts['ro'].stops_road_list_for = "Raport opriri pe traseu  ";
texts['es'].stops_road_list_for = "Informe de paradas para ";
texts['sr'].stops_road_list_for = "Dnevni izveštaj o pauzama za";
texts['gr'].stops_road_list_for = "Οδικό χάρτη για  διαμονή";

texts['bg'].custom_road_list_for = "Пътен лист за маршрут за периода";
texts['en'].custom_road_list_for = "Route road list between the period";
texts['de'].custom_road_list_for = "Fahrtenbuch fur eine Route und Zeitabschnitt"; 
texts['ro'].custom_road_list_for = "Itinerar/traseu pentru perioada între";
texts['es'].custom_road_list_for = "Cuaderno de bitacora de ruta por un período";
texts['sr'].custom_road_list_for = "Dnevni izveštaj o maršruti za period";
texts['gr'].custom_road_list_for = "Οδικό χάρτη διαδρομής για την περίοδο";

texts['bg'].period_road_list_for = "Пътен лист за периода";
texts['en'].period_road_list_for = "Road list for the period";
texts['de'].period_road_list_for = "Fahrtenbuch fur einen Zeitabschnitt"; 
texts['ro'].period_road_list_for = "Foaie parcursă pentru perioada"; 
texts['es'].period_road_list_for = "Informe hoja de ruta por un período"; 
texts['sr'].period_road_list_for = "Dnevni izveštaj za period"; 
texts['gr'].period_road_list_for = "Οδικό χάρτη για την περίοδο";

texts['bg'].route_road_list_from = "Пътен лист за маршрут от";
texts['en'].route_road_list_from = "Route road list for";
texts['de'].route_road_list_from = "Fahrtenbuch fur eine Route von"; 
texts['ro'].route_road_list_from = "Foaie de parcurs pentru o rută de la"; 
texts['es'].route_road_list_from = "Cuaderno de bitacora de ruta para";
texts['sr'].route_road_list_from = "Dnevni izveštaj o maršruti od";
texts['gr'].route_road_list_from = "Οδικό χάρτη διαδρομής από";

texts['bg'].fuel_fill_report_title_for_vehicle = "Справка за нивото на гориво в резервоара на превозно средство";
texts['en'].fuel_fill_report_title_for_vehicle = "Vehicle's fuel level report";
texts['de'].fuel_fill_report_title_for_vehicle = "Bericht uber den Kraftstofffullstand im Fahrzeugtank"; 
texts['ro'].fuel_fill_report_title_for_vehicle = "Raport de nivelul combustibilului în rezervorul vehiculului"; 
texts['es'].fuel_fill_report_title_for_vehicle = "Informe de nivel de combustible en le tanque ";
texts['sr'].fuel_fill_report_title_for_vehicle = "Izveštaj o nivou goriva u vozilu ";
texts['gr'].fuel_fill_report_title_for_vehicle = "Αναφορά στην στάθμη καυσίμου στη δεξαμενή ενός οχήματος";

texts['bg'].to = "до"; // Пътен лист за маршрут от 2011-09-01 12:35:10 до 2011-09-02 12:09:09
texts['en'].to = "до"; // Пътен лист за маршрут от 2011-09-01 12:35:10 до 2011-09-02 12:09:09
texts['de'].to = "bis zu"; // Пътен лист за маршрут от 2011-09-01 12:35:10 до 2011-09-02 12:09:09
texts['ro'].to = "până la"; // Пътен лист за маршрут от 2011-09-01 12:35:10 до 2011-09-02 12:09:09
texts['es'].to = "hasta"; // Пътен лист за маршрут от 2011-09-01 12:35:10 до 2011-09-02 12:09:09
texts['sr'].to = "do"; // Пътен лист за маршрут от 2011-09-01 12:35:10 до 2011-09-02 12:09:09
texts['gr'].to = "μέχρι";

texts['bg'].inside_poi_road_list_for = "Пътен лист за посещения на обекти за периода";
texts['en'].inside_poi_road_list_for = "Places visited between the period";
texts['de'].inside_poi_road_list_for = "Bericht uber die besuchten Objekte"; 
texts['ro'].inside_poi_road_list_for = "Foaie parcursa cu itinerariul pentru perioada "; 
texts['es'].inside_poi_road_list_for = "Informe de lugares visitados por un período ";
texts['sr'].inside_poi_road_list_for = "Dnevni izveštaj o posećenim mestima za period ";
texts['gr'].inside_poi_road_list_for = "Οδικό χάρτη για επιτόπιες επισκέψεις για την περίοδο";

texts['bg'].discharge_road_list_for = "Пътен лист за разтоварвания за периода";
texts['en'].discharge_road_list_for = "Пътен лист за разтоварвания за периода";
texts['de'].discharge_road_list_for = "Bericht uber die vorgenommenen Entladungen"; 
texts['ro'].discharge_road_list_for = "Foaie parcursa pentru descarcări pentru perioada";  
texts['es'].discharge_road_list_for = "Informe de descargas por un período";
texts['sr'].discharge_road_list_for = "Dnevni izveštaj o istovaru za period";
texts['gr'].discharge_road_list_for = "Οδικό χάρτη για εκφορτώσεων για την περίοδο";

texts['bg'].new_report_tooltip = "Натиснете тук за да се върнете обратно и да започнете нова справка.";
texts['en'].new_report_tooltip = "Click here to begin new report.";
texts['de'].new_report_tooltip = "Hier klicken, um einen neuen Bericht zu erstellen."; 
texts['ro'].new_report_tooltip = "Apasați aici ca să începeți un raport nou."; 
texts['es'].new_report_tooltip = "Haga clic aquí para comenzar nuevo informe.";
texts['sr'].new_report_tooltip = "Kliknite ovde da započnete novi izveštaj.";
texts['gr'].new_report_tooltip = "Πατήστε εδώ για να επιστρέψετε και να ξεκινήσετε μια νέα αναφορά.";

texts['bg'].previous_day_report_tooltip = "Натиснете тук за да направите справка за предишния ден.";
texts['en'].previous_day_report_tooltip = "Click here to do a report for the previous day.";
texts['de'].previous_day_report_tooltip = "Hier klicken, um einen Bericht fur den vorigen Tag zu erstellen."; 
texts['ro'].previous_day_report_tooltip = "Apasați aici ca să faceți raport pentru zi precedenta."; 
texts['es'].previous_day_report_tooltip = "Haga clic aquí para hacer un informe para el día anterior.";
texts['sr'].previous_day_report_tooltip = "Kliknite ovde da uradite izveštaj za prethodni dan.";
texts['gr'].previous_day_report_tooltip = "Πατήστε εδώ για να κάνετε μια αναφορά για την προηγούμενη ημέρα.";

texts['bg'].next_day_report_tooltip = "Натиснете тук за да направите справка за следващия ден.";
texts['en'].next_day_report_tooltip = "Click here to do a report for the next day.";
texts['de'].next_day_report_tooltip = "Hier klicken, um einen Bericht fur den nachsten Tag zu erstellen."; 
texts['ro'].next_day_report_tooltip = "Apasați aici ca să faceți raport pentru ziua urmatoare."; 
texts['es'].next_day_report_tooltip = "Haga clic aquí para hacer un informe para el próximo dia.";
texts['sr'].next_day_report_tooltip = "Kliknite ovde da uradite izveštaj za sledeći dan.";
texts['gr'].next_day_report_tooltip = "Πατήστε εδώ  για να κάνετε μια αναφορά για την επόμενη μέρα.";

texts['bg'].exprt_pdf_tooltip = "Натиснете тук за да запаметите пътния лист в PDF формат. (Подходяща програма за преглед на pdf файлове - Adobre Reader, Foxit Reader)";
texts['en'].exprt_pdf_tooltip = "Click here to export to PDF file. (You need to have a program like Adobre Reader or Foxit Reader installed in order to read such files.)";
texts['de'].exprt_pdf_tooltip = "Hier klicken, um den Bericht im PDF-Format zu speichern. ( Sie benotigen ein entsprechendes Programm, um die Datei nachher zu offnen – Adobe Reader, Foxit Reader)"; 
texts['ro'].exprt_pdf_tooltip = "Apasați aici ca să salvati foaie parcursa în format PDF. ( Trebuie sa aveti un program potrivit precum Acrobat Reader sau Foxit Reader pentru vizuirea de fișier pdf )"; 
texts['es'].exprt_pdf_tooltip = "Haga clic aquí para exportar a archivo PDF. (Aplicaciones recomendadas para abrir el archivo :Adobe Reader, Foxit Reader)";
texts['sr'].exprt_pdf_tooltip = "Kliknite ovde da izvezete u PDF fajl. (Morate imati instaliran Adobe Reader ili Foxit Reader da biste pročitali ovakve fajlove.)";
texts['gr'].exprt_pdf_tooltip = "Πατήστε  εδώ για εξαγωγή σε αρχείο PDF. (Πρέπει να έχετε εγκαταστήσει ένα πρόγραμμα όπως το Adobe Reader ή το Foxit Reader για να διαβάσετε τέτοια αρχεία.)";

texts['bg'].export_excel_tooltip = "Натиснете тук за да запаметите пътния лист в Excel формат. (Подходяща програма за преглед на excel файлове - Open Office, Microsoft Excell)";
texts['en'].export_excel_tooltip = "Click here to export to Excel file. (You need to have a program like Open Office or Microsoft Excel in order to read such files.)";
texts['de'].export_excel_tooltip = "Hier klicken, um den Bericht zu Excel zu exportieren.(Sie benotigen ein entsprechendes Programm, um die Datei nachher zu offnen – Open Office, Microsoft Excel)";
texts['ro'].export_excel_tooltip = "Apasați aici ca să salvați foaie parcursa în format Excel .(Program potrivit pentru vizuire de fișier în Excel - Open Office, Microsoft Excel)";
texts['es'].export_excel_tooltip = "Haga clic aquí para exportar a archivo Excel .(Aplicaciones recomendadas para abrir el archivo Excel:Open Office, Microsoft Excel)";
texts['sr'].export_excel_tooltip = "Kliknite ovde da izvezete u Excel fajl. (Morate imati program kao što je Open Office ili Microsoft Excel da biste pročitali ovakve fajlove.)";
texts['gr'].export_excel_tooltip = "Πατήστε  εδώ για εξαγωγή σε αρχείο Excel. (Πρέπει να έχετε ένα πρόγραμμα όπως το Open Office ή το Microsoft Excel για να διαβάσετε τέτοια αρχεία.)";

texts['bg'].move_map_tooltip = "Натиснете тук за да направите картата да се центрира автоматично при всяко изместване на превозното средство по неговия маршрут. (За да се възползвате от тази опция е нужно да визуализирате следата на превозното средство върху картата)";
texts['en'].move_map_tooltip = "Click here to make the map automatically center on the vehicle when it changes its coordinates.(First you need to plot a trace on the map.)";
texts['de'].move_map_tooltip = "Hier klicken, um die Landkarte automatisch  bei jeder Bewegung des Fahrzeugs entlang der Route zu zentrieren.( zuerst machen Sie die Spur auf der Landkarte sichtbar)"; 
texts['ro'].move_map_tooltip = "Apasați aici ca să faceți harta să se centreze automat pe itinerariul vehicului dupa mișcarea lui.( Pentru folosirea acestei opțiune este necesar să vizualizați ruta vehicului pe harta)"; 
texts['es'].move_map_tooltip = "Haga clic aquí para centrar el mapa automáticamente sobre el vehículo .( Primero tendrá que cargar un rastro en el mapa)"; 
texts['sr'].move_map_tooltip = "Kliknite ovde da automatski centrirate mapu na vozilo kada menja svoje koordinate. (Prvo morate da prikažete putanju na mapi.)";
texts['gr'].move_map_tooltip = "Πατήστε  εδώ για να κάνετε τον χάρτη να κεντράρη  αυτόματα κάθε φορά που το όχημα μετακινείται κατά μήκος της διαδρομής του. (Για να επωφεληθείτε από αυτήν την επιλογή, θα πρέπει να απεικονίσετε τη διαδρομή του οχήματος στο χάρτη)"; 

texts['bg'].address_search_tooltip = "Натиснете тук за да търсите адреси, заведения, хотели и т.н.";
texts['en'].address_search_tooltip = "Click here if you need to search for addresses, bars, hotels and so on.";
texts['de'].address_search_tooltip = "Hier klicken, um Adressen, Cafes, Hotels usw. zu suchen."; 
texts['ro'].address_search_tooltip = "Apasași aici ca să cautați adrese , baruri , spitale,hoteluri etc."; 
texts['es'].address_search_tooltip = "Haga clic aquí para buscar direcciones, restaurantes, hoteles, etc."; 
texts['sr'].address_search_tooltip = "Kliknite ovde da pretražite adrese, barove, hotele, itd."; 
texts['gr'].address_search_tooltip = "Πατήστε εδώ για αναζήτηση διευθύνσεων, εστιατορίων, ξενοδοχείων,κ.λπ.";

texts['bg'].full_screen_map_tooltip = "Натиснете тук за да виждате картата на целия екрен.";
texts['en'].full_screen_map_tooltip = "Click here to expand the map.";
texts['de'].full_screen_map_tooltip = "Hier klicken, um die Landkarte auf Vollbildmodus zu schalten."; 
texts['ro'].full_screen_map_tooltip = "Apasați aici ca să vedeți harta pe ecran complet."; 
texts['es'].full_screen_map_tooltip = "Haga clic aquí para ver el mapa de la pantalla completa."; 
texts['sr'].full_screen_map_tooltip = "Kliknite ovde da proširite mapu.";
texts['gr'].full_screen_map_tooltip = "Πατήστε εδώ για να δείτε τον χάρτη σε ολόκληρη την οθόνη."; 

texts['bg'].distance_trace_tooltip = "Натиснете тук за да отбележите текущото положение на превозното средсво като начална точка за измерване на разтояние и гориво до всяка една друга точка от неговия маршрут. (За да се възползвате от тази опция е нужно да визуализирате следата на превозното средство върху картата)";
texts['en'].distance_trace_tooltip = "Click here to make the current position of the vehicle a starting point for estimating spent fuel or distance to another point on the route. (First you need to plot a trace on the map.)";
texts['de'].distance_trace_tooltip = "Hier klicken, um die gegenwartige Position des Fahrzeugs als Anfangspunkt zu vermerken. So konnen Sie den Abstand und den Kraftstoffverbrauch bis zu jedem anderen Punkt der Route einschatzen.(zuerst machen Sie die Spur auf der Landkarte sichtbar)"; 
texts['ro'].distance_trace_tooltip = "Click aici pentru a face poziția curentă a vehiculului un punct de plecare pentru estimarea distanței și combustibil uzat pana la un alt punct de pe rută. (În primul rând aveți nevoie pentru a se trasează o rută pe hartă)"; 
texts['es'].distance_trace_tooltip = "Haga clic aquí para establecer  la posición actual del vehículo como punto de inicio para medición de distancia y consumo de combustible a cualquier otro punto de la ruta . (Primero tendrá que cargar un rastro en el mapa)";
texts['sr'].distance_trace_tooltip = "Kliknite ovde da podesite trenutnu poziciju vozila kao početnu tačku za merenje potrošenog goriva ili udaljenosti do druge tačke na mapi. (Prvo morate da prikažete putanju na mapi.)";
texts['gr'].distance_trace_tooltip = "Πατήστε εδώ για να επισημάνετε την τρέχουσα θέση του οχήματος ως σημείο εκκίνησης για τη μέτρηση της απόστασης και του καυσίμου σε οποιοδήποτε άλλο σημείο της διαδρομής του. (Για να επωφεληθείτε από αυτήν την επιλογή, θα πρέπει να απεικονίσετε το μονοπάτι του οχήματος στο χάρτη) ";

texts['bg'].vehicle_data_tooltip = "Натиснете тук за да се покаже прозорец с детайлна информация за поведението на превозното средство във всяка една точка от неговия маршрут.(За да се възползвате от тази опция е нужно да визуализирате следата на превозното средство върху картата)";
texts['en'].vehicle_data_tooltip = "Click here to show detailed information for the current vehicle's position along the route.(First you need to plot a trace on the map.)";
texts['de'].vehicle_data_tooltip = "Hier klicken, um ein Fenster mit detaillierten Angaben zum Verhalten des Fahrzeugs an jedem Punkt der Route anzuzeigen.(zuerst machen Sie die Spur auf der Landkarte sichtbar)"; 
texts['ro'].vehicle_data_tooltip = "Apasați aici ca să vedeți informație detaliată despre poziția curentă a vehiculului dea lungul unui traseu .(Pentru a profita de această opțiune este necesară vizualizarea traseului vehiculului pe hartă)"; 
texts['es'].vehicle_data_tooltip = "Haga clic aquí para ver información detallada para la posición del vehículo actual a lo largo de la ruta .(Primero tenga que visualizar el vehículo sobre el mapa)"; 
texts['sr'].vehicle_data_tooltip = "Kliknite ovde za prikaz detaljne informacije o trenutnoj poziciji vozila duž cele putanje. (Prvo morate da prikažete putanju na mapi.)";
texts['gr'].vehicle_data_tooltip = "Πατήστε εδώ για να εμφανίσετε ένα λεπτομερές παράθυρο πληροφοριών συμπεριφοράς οχήματος σε κάθε σημείο της διαδρομής του.(Για να επωφεληθείτε από αυτήν την επιλογή, πρέπει να απεικονίσετε την παρακολούθηση του οχήματος στον χάρτη)"; 

texts['bg'].position_tooltip = "Натиснете тук за да се зареди списък с опции за директно придвижване от едно място до друго по маршрута на превозното средство.(За да се възползвате от тази опция е нужно да визуализирате следата на превозното средство върху картата)";
texts['en'].position_tooltip = "Click here to get a list of options for advancing from one place to another along the route of the vehicle.(First you need to plot a trace on the map.)";
texts['de'].position_tooltip = "Hier klicken, um eine Liste der Optionen fur die  Bewegung von einem Punkt zum nachsten entlang der Route des Fahrzeugs anzuzeigen. (zuerst machen Sie die Spur auf der Landkarte sichtbar)"; 
texts['ro'].position_tooltip = "Click aici pentru a primi o listă de opțiuni pentru trecerea/avansarea la o locație la alta de-a lungul traseului vehiculului. (În primul rând aveți nevoie să marcați un traseu pe hartă"; 
texts['es'].position_tooltip = "Haga clic aquí para ver una lista de opciones para el movimiento directo de un lugar a otro a lo largo de la ruta del vehículo. (Primero tendrá que cargar un rastro en el mapa)"; 
texts['sr'].position_tooltip = "Kliknite ovde da dobijete listu opcija za kretanje vozila od jednog mesta do drugog duž cele putanje. (Prvo morate da prikažete putanju na mapi.)"; 
texts['gr'].position_tooltip = "Πατήστε  εδώ για να φορτώσετε μια λίστα επιλογών για να μετακινηθείτε απευθείας από το ένα μέρος στο άλλο στη διαδρομή του οχήματος (Για να επωφεληθείτε από αυτήν την επιλογή, θα πρέπει να απεικονίσετε τη διαδρομή του οχήματος στο χάρτη)";

texts['bg'].marked_places_tooltip = "Натиснете тук за да заредите списък с маркираните обекти принадлежащи на вашата фирма.";
texts['en'].marked_places_tooltip = "Click here to get a list of marked places that belong to the firm.";
texts['de'].marked_places_tooltip = "Hier klicken, um eine Liste der markierten Objekte zu laden."; 
texts['ro'].marked_places_tooltip = "Apasați aici ca să încărcați lista locurilor marcate care aparțin firmei dvs."; 
texts['es'].marked_places_tooltip = "Haga click para cargar la lista de los objetos marcados que pertenecen a su empresa.";
texts['sr'].marked_places_tooltip = "Kliknite ovde da dobijete listu obeleženih mesta koja pripadaju firmi.";
texts['gr'].marked_places_tooltip = "Πατήστε  εδώ για να φορτώσετε μια λίστα σημειωμένων χώρων που ανήκουν στην επιχείρησή σας.";

texts['bg'].marked_polygons_tooltip = "Натиснете тук за да заредите списък с маркираните терени принадлежащи на вашата фирма.";
texts['en'].marked_polygons_tooltip = "Click here to get a list of marked terrains that belong to the firm.";
texts['de'].marked_polygons_tooltip = "Hier klicken, um eine Liste der markierten Gebiete, zu laden."; 
texts['ro'].marked_polygons_tooltip = "Apasați aici ca să încarcați lista cu terenuri marcate care aparțin firmei dvs."; 
texts['es'].marked_polygons_tooltip = "Haga click para cargar la lista de los terrenos marcados que pertenecen a su empresa.";
texts['sr'].marked_polygons_tooltip = "Kliknite ovde da dobijete listu obeleženih terena koja pripadaju firmi.";
texts['gr'].marked_polygons_tooltip = "Πατήστε εδώ για να πάρετε μια λίστα με επισημασμένα εδάφη που ανήκουν στην επιχείρηση.";

texts['bg'].trace_stops_filter_input_tooltip = "Въведете филтър за показване на престоите на превозното средство за избрания маршрут. Така лесно може да филтрирате излишните спирания и да виждате само истинските (за вас) престои";
texts['en'].trace_stops_filter_input_tooltip = "Enter the minimum amount of time each stop needs to have in order to be displayed in the list. This way you can easily get rid of unnecessery information.";
texts['de'].trace_stops_filter_input_tooltip = "Geben Sie einen Filter ein, um  die Aufenthalte des Fahrzeugs fur den ausgewahlten Zeitabschnitt zu sehen."; 
texts['ro'].trace_stops_filter_input_tooltip = "Introduceți filtru pentru afișare perioadelor de oprire minima a vehicului pe itinerariul ales pentru afișate in listă.Astfel puteți ușor să filtrați informațiile inutile și să vedeți numai opriri necesare (pentru dvs.)";
texts['es'].trace_stops_filter_input_tooltip = "Introducir un filtro para mostrar los paradas de los  vehículos por la ruta seleccionada. Por esta manera se pueden filtrar solo los paradas reales del vehículo";
texts['sr'].trace_stops_filter_input_tooltip = "Unesite minimalno vreme koliko svaka pauza treba da iznosi da bi se prikazalo na listi. Na ovaj način možete se rešiti nepotrebnih informacija.";
texts['gr'].trace_stops_filter_input_tooltip = "Εισαγάγετε ένα φίλτρο για να εμφανίζονται οι στάσεις του οχήματος για την επιλεγμένη διαδρομή. Αυτό σας διευκολύνει να φιλτράρετε τις υπερβολικές στάσεις και να δείτε μόνο τις πραγματικές (για εσάς) παραμονές";

texts['bg'].trace_stops_filter_input_accept_tooltip = "Натиснете тук за да филтрирате престоите на превозното средство.";
texts['en'].trace_stops_filter_input_accept_tooltip = "Click here to filter the stops of the vehicle.";
texts['de'].trace_stops_filter_input_accept_tooltip = "Hier klicken, um einen Filter fur die Aufenthalte des Fahrzeugs einzugeben."; 
texts['ro'].trace_stops_filter_input_accept_tooltip = "Apasați aici ca să filtrați perioadele de oprire ale vehicului."; 
texts['es'].trace_stops_filter_input_accept_tooltip = "Haga clic aquí para filtrar los paradas del vehículo.";
texts['sr'].trace_stops_filter_input_accept_tooltip = "Kliknite ovde da filtrirate pauze vozila.";
texts['gr'].trace_stops_filter_input_accept_tooltip = "Πατήστε εδώ για να φιλτράρετε τις στάσεις του οχήματος.";

texts['bg'].trace_stops_filter_input_cancel_tooltip = "Натиснете тук за да изтриете филтъра за престоите на превозното средство. Така ще може да видите информация за всяко спиране на превозното средство. (НЕ СЕ ПРЕПОРЪЧВА)";
texts['en'].trace_stops_filter_input_cancel_tooltip = "Click here to remove the stops filter.";
texts['de'].trace_stops_filter_input_cancel_tooltip = "Hier klicken, um den Filter der Aufenthalte des Fahrzeugs zu loschen."; 
texts['ro'].trace_stops_filter_input_cancel_tooltip = "Apasați aici ca să ștergeți filtru pentru perioadele de oprire ale vehicului. Așa puteți vedea informație pentru fiecare oprire a vehicului (NU ESTE RECOMANDAT)"; 
texts['es'].trace_stops_filter_input_cancel_tooltip = "Haga clic aquí para borrar el filtro de las paradas del vehículo. Por esta manera  se pueden ver todos los paradas  del vehículo ( NO SE RECOMIENDA)"; 
texts['sr'].trace_stops_filter_input_cancel_tooltip = "Kliknite ovde da uklonite filter pauza. Tako ćete moći da vidite informacije za svako zaustavljanje vozila. (NE PREPORUČUJE SE)"; 
texts['gr'].trace_stops_filter_input_cancel_tooltip = "Πατήστε εδώ για να διαγράψετε το φίλτρο στάσης του οχήματος. Έτσι, μπορείτε να δείτε πληροφορίες για κάθε στάση του οχήματος. (ΔΕΝ ΣΥΝΊΣΤΑΤΑΙ)";

texts['bg'].cityNonCityDistance_chart_tooltip = "Натиснете за да видите в графичен вид съотношението на движението в градско/извънградско движение.";
texts['en'].cityNonCityDistance_chart_tooltip = "Click here to see a chart representing the correlation between urban/suburban distance.";
texts['de'].cityNonCityDistance_chart_tooltip = "Hier klicken, um einen Bericht zum Vergleich zwischen Bewegungen innerorts und au?erorts zu erstellen."; 
texts['ro'].cityNonCityDistance_chart_tooltip = "Click aici pentru a vedea graficul reprezentând corelarea intre distanta urban/suburban."; 
texts['es'].cityNonCityDistance_chart_tooltip = "Haga clic para ver gráficamente la correlación entre  tráfico urbano/extraurbano.";
texts['sr'].cityNonCityDistance_chart_tooltip = "Kliknite ovde da vidite grafik koji predstavlja odnos između gradskog/vangradskog saobraćaja.";
texts['gr'].cityNonCityDistance_chart_tooltip = "Πατήστε για να δείτε γραφικά την αναλογία της κυκλοφορίας στις αστικές / προαστιακές  κινήσεις.";

texts['bg'].movesChart_graphic_tooltip = "Натиснете за да видите в графичен вид поведението на автомобила при всеки от етапите на движение.";
texts['en'].movesChart_graphic_tooltip = "Click here to see a chart representing the vehicle's behaviour for each moving stage of the trace.";
texts['de'].movesChart_graphic_tooltip = "Hier klicken, um eine graphische Darstellung des Verhaltens der Fahrzeuge in jeder Phase der Bewegung zu erstellen."; 
texts['ro'].movesChart_graphic_tooltip = "Click aici pentru a vedea graficul reprezentând comportamentul vehiculului pe fiecare etapă a rutei."; 
texts['es'].movesChart_graphic_tooltip = "Haga clic para ver gráficamente el comportamiento del coche en cada etapa del movimiento."; 
texts['sr'].movesChart_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja ponašanje vozila pri svakoj fazi kretanja na putanji."; 
texts['gr'].movesChart_graphic_tooltip = "Πατήστε για να δείτε τη συμπεριφορά του αυτοκινήτου σε κάθε στάδιο οδήγησης.";

texts['bg'].moveIdle_graphic_tooltip = "Натиснете за да видите в графичен вид съотношението на времето при движение и престой.";
texts['en'].moveIdle_graphic_tooltip = "Click here to see a chart representing the time correlation between each stage of movement and outage.";
texts['de'].moveIdle_graphic_tooltip = "Hier klicken, um eine graphische Darstellung zwischen Bewegungs- und Aufenthaltsdauer zu erstellen.";
texts['ro'].moveIdle_graphic_tooltip = "Click aici pentru a vedea graficul reprezentând corelarea de timp care reprezintă corelația de timp între fiecare etapă de mișcare și oprire.";
texts['es'].moveIdle_graphic_tooltip = "Haga clic para ver gráficamente la correlación entre el tiempo de circulación y estaciomieno.";
texts['sr'].moveIdle_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja vremenski odnos između svake faze kretanja i pauziranja.";
texts['gr'].moveIdle_graphic_tooltip = "Πατήστε για να δείτε γραφικά την αναλογία του χρόνου στην κυκλοφορία και διαμονή.";

texts['bg'].stopsChart_graphic_tooltip = "Натиснете за да видите в графичен вид времетраенето на всеки от престойте.";
texts['en'].stopsChart_graphic_tooltip = "Click here to see a chart representing the duration of each vehicle's outage.";
texts['de'].stopsChart_graphic_tooltip = "Hier klicken, um eine graphische Darstellung der Zeitdauer von jedem Aufenthalt zu erstellen."; 
texts['ro'].stopsChart_graphic_tooltip = "Hiufenthalt zu erstellen.";
texts['es'].stopsChart_graphic_tooltip = "Haga clic aquí para ver en forma gráfico la duración de cada parada.";
texts['sr'].stopsChart_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja trajanje pauze svakog vozila .";
texts['gr'].stopsChart_graphic_tooltip = "Πατήστε για να δείτε γραφικά την διάρκεια της κάθε διαμονή.";

texts['bg'].workTimeChart_graphic_tooltip = "Натиснете за да видите в графичен вид времето на работа за всеки от дните за избрания месец.";
texts['en'].workTimeChart_graphic_tooltip = "Click here to see a chart representing the working time of the vehicle for each day.";
texts['de'].workTimeChart_graphic_tooltip = "Hier klicken, um eine graphische Darstellung der Arbeitszeit fur jeden Tag des ausgewahlten Monats zu erstellen."; 
texts['ro'].workTimeChart_graphic_tooltip = "Click aici pentru a vedea graficul reprezentând orele de lucru ale vehiculului pentru fiecare zi."; 
texts['es'].workTimeChart_graphic_tooltip = "Haga clic para ver gráficamente el tiempo laboral para cada uno de los días del mes seleccionado."; 
texts['sr'].workTimeChart_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja vreme rada vozila za svaki dan."; 
texts['gr'].workTimeChart_graphic_tooltip = "Πατήστε για να δείτε γραφικά την ώρα λειτουργίας για κάθε ημέρα της επιλεγμένης μήνα.";


texts['bg'].dailyMoveVsIdleTime_graphic_tooltip = "Натиснете за да видите в графичен вид сравнението на движенията и престоите по часове за всеки от дните.";
texts['en'].dailyMoveVsIdleTime_graphic_tooltip = "Click here to see a chart representing the duration in hours between movement and outage for each day.";
texts['de'].dailyMoveVsIdleTime_graphic_tooltip = "Hier klicken, um eine graphische Darstellung der Zeitverhaltnisse zwischen Bewegung/Aufenthalte fur jeden Tag zu erstellen."; 
texts['ro'].dailyMoveVsIdleTime_graphic_tooltip = "Click aici pentru a vedea graficul reprezentând durata în ore între orele în mișcare si cele in staționare pentru fiecare zi."; 
texts['es'].dailyMoveVsIdleTime_graphic_tooltip = "Haga clic aquí para ver en forma gráfico la correlación entre movimiento y paradas  separado de horas por cada día."; 
texts['sr'].dailyMoveVsIdleTime_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja trajanje kretanja i pauziranja u satima za svaki dan."; 
texts['gr'].dailyMoveVsIdleTime_graphic_tooltip = "Πατήστε για να δείτε το γράφημα της σύγκρισης των κινήσεων και των στάσεων ανά ώρα για κάθε μία από τις ημέρες.";


texts['bg'].dischargeVsInsideCargoZonesTime_graphic_tooltip = "Натиснете за да видите в графичен вид сравнението на престоите в зоните за разтоварване и времето на разтоварване.";
texts['en'].dischargeVsInsideCargoZonesTime_graphic_tooltip = "Click here to create a graph for comparison between length of stay and discharge duration in the zone of discharge.";
texts['de'].dischargeVsInsideCargoZonesTime_graphic_tooltip = "Hier klicken, um eine graphische Darstellung zum Vergleich zwischen Aufenthaltsdauer und Entladedauer in der Zone des Entladens zu erstellen."; 
texts['ro'].dischargeVsInsideCargoZonesTime_graphic_tooltip = "Click pentru a vizualiza graficul comparativ între perioada de staționare și durata de descărcare, în zonele de descărcare.";
texts['es'].dischargeVsInsideCargoZonesTime_graphic_tooltip = "Haga clic aquí para ver en forma gráfico la correlación de los estaciomienos en las zonas de descarga  y la duración de esta descarga.";
texts['sr'].dischargeVsInsideCargoZonesTime_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja upoređivanje trajanja pauze i trajanja istovara u zoni za istovar.";
texts['gr'].dischargeVsInsideCargoZonesTime_graphic_tooltip = "Πατήστε εδώ για να δείτε ένα γράφημα για τη σύγκριση μεταξύ της διάρκειας παραμονής και της διάρκειας της εκφόρτισης στη περιοχή εκφόρτωσης.";

texts['bg'].dischargeMoveIdleTime_graphic_tooltip = "Натиснете за да видите в графичен вид времетраенето на движенията и престоите за всички маршрути, който имат разтоварване в някоя зона.";
texts['en'].dischargeMoveIdleTime_graphic_tooltip = "Click here to create a graphical representation between exercise duration and length of stay of all routes that unloading was found.";
texts['de'].dischargeMoveIdleTime_graphic_tooltip = "Hier klicken, um eine graphische Darstellung zwischen Bewegungsdauer und Aufenthaltsdauer von allen Routen, bei denen Ausladen festgestellt wurde, zu erstellen."; 
texts['ro'].dischargeMoveIdleTime_graphic_tooltip = "Click aici pentru a crea o reprezentare grafică între durata de circulație pentru toate rutele și duratele staționărilor care a fost găsită la descărcare pentru toate rutele.";
texts['es'].dischargeMoveIdleTime_graphic_tooltip = "Haga clic aquí para ver en forma gráfico la correlación entre los movimientos y los estacionamientos para todas las rutas en las zonas de descarga."; 
texts['sr'].dischargeMoveIdleTime_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja trajanje kretanja i pauze za sve maršrute koje imaju istovar u jednoj zoni."; 
texts['gr'].dischargeMoveIdleTime_graphic_tooltip = "Πατήστε για να δείτε τη διάρκεια των κινήσεων και των στάσεων για όλες τις διαδρομές που έχουν εκφόρτωση  σε μια περιοχή.";

texts['bg'].speedChart_graphic_tooltip = "Натиснете тук за да визуализирате графика за скоростта.";
texts['en'].speedChart_graphic_tooltip = "Click here to see a chart representing the speed of the vehicle.";
texts['de'].speedChart_graphic_tooltip = "Hier klicken, um eine graphische Darstellung der Geschwindigkeit des Fahrzeugs zu erstellen."; 
texts['ro'].speedChart_graphic_tooltip = "Apasați aici ca să vizualizați graficul de viteză al vehicului."; 
texts['es'].speedChart_graphic_tooltip = "Haga clic para visualizar un gráfico de la velocidad."; 
texts['sr'].speedChart_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja brzinu vozila.";
texts['gr'].speedChart_graphic_tooltip = "Πατήστε εδώ για να απεικονίσετε το γράφημα ταχύτητας."; 

texts['bg'].accumulatorChart_graphic_tooltip = "Натиснете тук за да визуализирате графика за акумулатора.";
texts['en'].accumulatorChart_graphic_tooltip = "Click here to see a chart representing the accumulator of the vehicle.";
texts['de'].accumulatorChart_graphic_tooltip = "Hier klicken, um eine graphische Darstellung der Akkuleistung zu erstellen."; 
texts['ro'].accumulatorChart_graphic_tooltip = "Apasați aici ca să vizualizați graficul acumulatorului vehiculului."; 
texts['es'].accumulatorChart_graphic_tooltip = "Haga clic para visualizar unu gráfico del acumulador.";
texts['sr'].accumulatorChart_graphic_tooltip = "Kliknite ovde da vidite grafik koji predstavlja akumulator vozila.";
texts['gr'].accumulatorChart_graphic_tooltip = "Πατήστε  εδώ για να δείτε το διάγραμμα που αντιπροσωπεύει τον συσσωρευτή του οχήματος.";

texts['bg'].include_trace_arrows_tooltip = "Натиснете тук за да изберете дали следата на превозното средство да бъде с включена посока на движение. (Ако вече е визуализирана следата и натиснете бутона ефекта ще бъде налице при следващото и изчертаване.)";
texts['en'].include_trace_arrows_tooltip = "Click here to change the appearance of the vehicle's trace. (If the trace is already on the map you will have to plot it again in order to see the difference.)";
texts['de'].include_trace_arrows_tooltip = "Hier klicken, um auszuwahlen, ob die Richtung der Spur angezeigt werden soll. ( Wenn die Spur bereits angezeigt wird, muss diese Option neu geladen werden, damit die Anderung wirksam wird)"; 
texts['ro'].include_trace_arrows_tooltip = "Apasați aici ca să alegeți reprezentarea grafică a traseului urmat de vehicul si să fie inclusă și direcție de mișcăre . ( Dacă traseul este deja pe hartă trebuie sa-l vizualizați dinou pentru a se vedea diferența)";
texts['es'].include_trace_arrows_tooltip = "Haga clic para seleccionar  si el rastro del vehculo sea incuido la dirección de movimiento . ( Si el rastro ya esta visualizado, tenga que recargarlé de nuevo)";
texts['sr'].include_trace_arrows_tooltip = "Kliknite ovde da promenite izgled putanje vozila.(Ako je putanja već na mapi moraćete opet da je prikažete da biste videli razliku.)";
texts['gr'].include_trace_arrows_tooltip = "Πατήστε εδώ για να αλλάξετε την εμφάνιση του ίχνους του οχήματος. (Αν το ίχνος βρίσκεται ήδη στο χάρτη, θα πρέπει να το σχεδιάσετε ξανά για να δείτε τη διαφορά.)";


texts['bg'].show_bypass_over_trace_tooltip = "Ако имате инсталиран разходомер може да видите къде по трасето има байпас.";
texts['en'].show_bypass_over_trace_tooltip = "If this vehicle has fuel consumption sensor you can see where on the trace there were bypass.";
texts['de'].show_bypass_over_trace_tooltip = "Bypassschaltung des Kraftstoffverbrauchmessgerates entlang einer Route anzeigen."; 
texts['ro'].show_bypass_over_trace_tooltip = "Dacă aveți debimetru instalat puteți vedea unde pe traseu este bypassat."; 
texts['es'].show_bypass_over_trace_tooltip = "Si del vehículo ha instalado medidor de flujo de combustible se puede ver en qué parte del rastro había derivación.";
texts['sr'].show_bypass_over_trace_tooltip = "Ako ovo vozilo ima protokomer možete videti gde se na putanji nalazi bajpas.";
texts['gr'].show_bypass_over_trace_tooltip = "Αν αυτό το όχημα διαθέτει αισθητήρα κατανάλωσης καυσίμου, μπορείτε να δείτε πού βρίσκεται το ίχνος παράκαμψη.";

texts['bg'].stops_filter_bigger_than = "Престои по-големи от:";
texts['en'].stops_filter_bigger_than = "Outage bigger than:";
texts['de'].stops_filter_bigger_than = "Aufenthalte langer als:"; 
texts['ro'].stops_filter_bigger_than = "Perioade de oprire mai mare de:"; 
texts['es'].stops_filter_bigger_than = "Paradas más de :"; 
texts['sr'].stops_filter_bigger_than = "Pauza veća od :";
texts['gr'].stops_filter_bigger_than = "Διακοπή λειτουργίας μεγαλύτερη από:"; 

texts['bg'].fuel_level_tooltip = "Натиснете тук за да видите кривата на горивото в околност на зареждане/източване. (За да изчертаете крива на горивото трябва да изберете ред от таблицата отговарящ на зареждане/източване)";
texts['en'].fuel_level_tooltip = "Click here to see a chart representing the fuel curve in the vicinity of refuel/drain.";
texts['de'].fuel_level_tooltip = "Hier klicken, um eine graphische Darstellung des Kraftstoffniveaus in den Tankzonen oder in Zonen, wo vermutlich Kraftstoff entwendet wurde, zu erstellen. Zuerst mussen Sie ein Ereignis aus der Tabelle  auswahlen."; 
texts['ro'].fuel_level_tooltip = "Click aici pentru a vizualiza graficul care reprezintă curba de combustibil în zona punctelor de realimentare.";
texts['es'].fuel_level_tooltip = "Haga clic aquí para ver la curva de combustible en el perimetro de  repostaje / sustracción.";
texts['sr'].fuel_level_tooltip = "Kliknite ovde da vidite grafik koji predstavlja krivulju za gorivo u blizini točenja/istakanja.";
texts['gr'].fuel_level_tooltip = "Πατήστε  εδώ για να δείτε ένα διάγραμμα που αντιπροσωπεύει την καμπύλη καυσίμου στην περιοχή του ανεφοδιασμού / αφαίρεση.(Για να σχεδιάσετε καμπύλη καυσίμου, πρέπει να επιλέξετε μια σειρά από τον πίνακα φόρτωσης / αποστράγγισης)";

texts['bg'].noSuchChartForAccount = "Не сте абониран за тази справка";
texts['en'].noSuchChartForAccount = "You don't have permission to do this report!";
texts['de'].noSuchChartForAccount = "Sie sind nicht autorisiert diesen Bericht zu erstellen!"; 
texts['ro'].noSuchChartForAccount = "Nu aveți abonament de acest raport!"; 
texts['es'].noSuchChartForAccount = "Usted no está suscrito para este informe!"; 
texts['sr'].noSuchChartForAccount = "Nemate dozvolu da uradite ovaj izveštaj!"; 
texts['gr'].noSuchChartForAccount = "Δεν είστε εγγεγραμμένοι για  αυτήν την αναφορά";

texts['bg'].inputStopsFilterValue = "Въведете филтър за престоите. Например: 00:15:00";
texts['en'].inputStopsFilterValue = "Enter stops filter. For example: 00:15:00";
texts['de'].inputStopsFilterValue = "Bestimmen Sie die Aufenthaltsdauer. Zum Beispiel langer als: 00:15:00"; 
texts['ro'].inputStopsFilterValue = "Introduceți filtru pentru perioade de oprire. De exemplu: 00:15:00"; 
texts['es'].inputStopsFilterValue = "Introducir filtro de las paradas:. Por ejemplo: 00:15:00"; 
texts['sr'].inputStopsFilterValue = "Unesite filter za pauze. Na primer: 00:15:00"; 
texts['gr'].inputStopsFilterValue = "Εισαγάγετε ένα φίλτρο στάσης. Για παράδειγμα: 00:15:00";

texts['bg'].properTimeFormatAlert = "Форматът на въвеждане трябва да е във вида: hh:mm:ss !";
texts['en'].properTimeFormatAlert = "You must use time format like hh:mm:ss !";
texts['de'].properTimeFormatAlert = "Sie mussen die Zeit im Format hh:mm:ss eingeben!";
texts['ro'].properTimeFormatAlert = "Trebuie sa folositi formatul orar de felul : hh:mm:ss !";
texts['es'].properTimeFormatAlert = "¡El formato de tiempo debe ser: hh: mm: ss!";
texts['sr'].properTimeFormatAlert = "Morate koristiti format vremena: hh: mm: ss!";
texts['gr'].properTimeFormatAlert = "Πρέπει να χρησιμοποιήσετε τη μορφή ώρας όπως ωω: λλ: δδ!";

texts['bg'].selectBypassMode = "Изберете режим за байпас";
texts['en'].selectBypassMode = "Choose bypass mode";
texts['de'].selectBypassMode = "Wahlen Sie einen Bypassmodus aus"; 
texts['ro'].selectBypassMode = "Alegeți regim de bypass"; 
texts['es'].selectBypassMode = "Seleccione modo de derivación";
texts['sr'].selectBypassMode = "Izaberite režim za bajpas";
texts['gr'].selectBypassMode = "Επιλέξτε τη λειτουργία για την παράκαμψη";

texts['bg'].show_over_trace = "Покажи върху следата";
texts['en'].show_over_trace = "Show over trace";
texts['de'].show_over_trace = "Uber der Spur zeigen"; 
texts['ro'].show_over_trace = "Arată pe urmă traseu"; 
texts['es'].show_over_trace = "Visualizar sobre el rastro";
texts['sr'].show_over_trace = "Prikaži iznad putanje";
texts['gr'].show_over_trace = "Εμφάνιση πάνω από ίχνος";

texts['bg'].listOfOptionsChoice = "Имате следните опции на разположение";
texts['en'].listOfOptionsChoice = "You can choose from the following list of options";
texts['de'].listOfOptionsChoice = "Folgende Optionen stehen zur Verfugung"; 
texts['ro'].listOfOptionsChoice = "Aveți următoarele opțiunii disponibile "; 
texts['es'].listOfOptionsChoice = "Seleccione de las siguientes opciones disponibles ";
texts['sr'].listOfOptionsChoice = "Možete izabrati sledeće opcije ";
texts['gr'].listOfOptionsChoice = "Έχετε τις ακόλουθες επιλογές διαθέσιμες";

texts['bg'].nextDayReport = "Справка за следващия ден";
texts['en'].nextDayReport = "Next day";
texts['de'].nextDayReport = "Bericht fur den nachsten Tag"; 
texts['ro'].nextDayReport = "Rapor pentru ziua următoare"; 
texts['es'].nextDayReport = "Informe para el día siguiente"; 
texts['sr'].nextDayReport = "Izveštaj za sledeći dan"; 
texts['gr'].nextDayReport = "Αναφορά για την επόμενη ημέρα";

texts['bg'].previousDayReport = "Справка за предишния ден";
texts['en'].previousDayReport = "Previous day";
texts['de'].previousDayReport = "Bericht fur den vorigen Tag"; 
texts['ro'].previousDayReport = "Raport pentru ziua precedenta"; 
texts['es'].previousDayReport = "Informe para el día anterior";
texts['sr'].previousDayReport = "Izveštaj za prethodni dan";
texts['gr'].previousDayReport = "Αναφορά για την  προηγούμενης ημέρας";

texts['bg'].startOfNewReport = "Започване на нова справка";
texts['en'].startOfNewReport = "New report";
texts['de'].startOfNewReport = "Bericht erstellen"; 
texts['ro'].startOfNewReport = "Raport nou"; 
texts['es'].startOfNewReport = "Nuevo informe"; 
texts['sr'].startOfNewReport = "Novi izveštaj"; 
texts['gr'].startOfNewReport = "Νέα αναφορά";

texts['bg'].doIt_fashionWay = "~~ Изпълни ~~";
texts['en'].doIt_fashionWay = "~~ Accept ~~";
texts['de'].doIt_fashionWay = "Erstellen"; 
texts['ro'].doIt_fashionWay = "Acceptă"; 
texts['es'].doIt_fashionWay = "~~Aceptar~~";
texts['sr'].doIt_fashionWay = "~~Prihvati~~";
texts['gr'].doIt_fashionWay = "~ ~ Αποδοχή ~~";

texts['bg'].sum_stops_time = "Сумарно време на престойте";
texts['en'].sum_stops_time = "Stops sum time";
texts['de'].sum_stops_time = "Gesamtzeit der Aufenthalte"; 
texts['ro'].sum_stops_time = "Timpul total de ședere"; 
texts['es'].sum_stops_time = "Tiempo total de las paradas";
texts['sr'].sum_stops_time = "Ukupno vreme pauza";
texts['gr'].sum_stops_time = "Συνολικός χρόνος παραμονής";

texts['bg'].day_work_time = "Работно време за деня";
texts['en'].day_work_time = "Work time";
texts['de'].day_work_time = "Arbeitszeit fur den Tag"; 
texts['ro'].day_work_time = "Timpul de lucru pentru o zi"; 
texts['es'].day_work_time = "THorario laboral"; 
texts['sr'].day_work_time = "Vreme rada";
texts['gr'].day_work_time = "Χρόνος εργασίας της ημέρας";

texts['bg'].date_time_inside = "Дата/час на влизане";
texts['en'].date_time_inside = "Inside date/time";
texts['de'].date_time_inside = "Angekommen Datum/Uhrzeit"; 
texts['ro'].date_time_inside = "Data/ora intrării"; 
texts['es'].date_time_inside = "Fecha/hora de entrada ";
texts['sr'].date_time_inside = "Datum/vreme na ulasku ";
texts['gr'].date_time_inside = "Ημερομηνία /ώρα εισόδου";

texts['bg'].date_time_outside = "Дата/час на излизане";
texts['en'].date_time_outside = "Outside date/time";
texts['de'].date_time_outside = "Abgefahren Datum/Uhrzeit"; 
texts['ro'].date_time_outside = "Dată/oră ieșirii"; 
texts['es'].date_time_outside = "Fecha/hora de salida";
texts['sr'].date_time_outside = "Datum/vreme na izlasku";
texts['gr'].date_time_outside = "Ημερομηνία /ώρα εξόδου";

texts['bg'].day_of_month = "Ден от месец";
texts['en'].day_of_month = "Day of month";
texts['de'].day_of_month = "Tag des Monats"; 
texts['ro'].day_of_month = "Zi a lunii"; 
texts['es'].day_of_month = "Día del mes";
texts['sr'].day_of_month = "Dan u mesecu";
texts['gr'].day_of_month = "Ημέρα του μήνα";

texts['bg'].poi_name = "Наименование на посетения обект/терен";
texts['en'].poi_name = "Name of visited place";
texts['de'].poi_name = "Objekt/Gebiet"; 
texts['ro'].poi_name = "Denumire locului/terenului vizitat"; 
texts['es'].poi_name = "Nombre de los objetos/terrenos visitados"; 
texts['sr'].poi_name = "Naziv posećenog mesta/terena"; 
texts['gr'].poi_name = "Όνομα του τόπου που επισκεφθήκατε";

texts['bg'].station_leave = "Напускане на завод";
texts['en'].station_leave = "Station leave";
texts['de'].station_leave = "Abfahrt"; 
texts['ro'].station_leave = "Parasire stație";
texts['es'].station_leave = "Salga de la estación";
texts['sr'].station_leave = "Napuštanje stanice";
texts['gr'].station_leave = "Αναχώρηση σταθμού";

texts['bg'].zone_arrival = "Пристигане в обект";
texts['en'].zone_arrival = "Zone arrival";
texts['de'].zone_arrival = "Ankunft"; 
texts['ro'].zone_arrival = "Zonă sosire"; 
texts['es'].zone_arrival = "Zona de llegada";
texts['sr'].zone_arrival = "Dolazak u zonu";
texts['gr'].zone_arrival = "Άφιξη ζώνη";

texts['bg'].zone_leave = "Напускане на обект";
texts['en'].zone_leave = "Zone leaving";
texts['de'].zone_leave = "Abfahrt"; 
texts['ro'].zone_leave = "Plecare"; 
texts['es'].zone_leave = "Zona de salida"; 
texts['sr'].zone_leave = "Napuštanje zone"; 
texts['bg'].zone_leave = "Αναχώρηση ζωνη";

texts['bg'].discharge_start = "Начало на разтоварване";
texts['en'].discharge_start = "Start of discharging";
texts['de'].discharge_start = "Anfang des Entladens"; 
texts['ro'].discharge_start = "Începutul descarcării"; 
texts['es'].discharge_start = "Comience de descarga";
texts['sr'].discharge_start = "Početak istovara";
texts['gr'].discharge_start = "Έναρξη εκφόρτωσης";

texts['bg'].discharge_end = "Край на разтоварване";
texts['en'].discharge_end = "End of discharging";
texts['de'].discharge_end = "Ende des Entladens"; 
texts['ro'].discharge_end = "Terminarea descarcării"; 
texts['es'].discharge_end = "Fin de descarga";
texts['sr'].discharge_end = "Kraj istovara";
texts['gr'].discharge_end = "Τέλος εκφόρτωσης";

texts['bg'].discharge_duration = "Разтоварване";
texts['en'].discharge_duration = "Duration";
texts['de'].discharge_duration = "Entladen"; 
texts['ro'].discharge_duration = "Descarcare Durată";
texts['es'].discharge_duration = "Descargar";
texts['sr'].discharge_duration = "Trajanje istovara";
texts['gr'].discharge_duration = "Εκφόρτωση";

texts['bg'].stop_time_before_discharge = "Престой преди разтоварване";
texts['en'].stop_time_before_discharge = "Outage time before discharging";
texts['de'].stop_time_before_discharge = "Aufenthaltsdauer vor dem Entladen"; 
texts['ro'].stop_time_before_discharge = "Perioadă de oprire înaintea descarcarii"; 
texts['es'].stop_time_before_discharge = "Estacionamiento antes de descarga";
texts['sr'].stop_time_before_discharge = "Vreme pauze pre istovara";
texts['gr'].stop_time_before_discharge = "Χρόνος διακοπής πριν από την εκφόρτωση";

texts['bg'].refuel_date = "Дата на зареждане";
texts['en'].refuel_date = "Refuel date";
texts['de'].refuel_date = "Tankdatum"; 
texts['ro'].refuel_date = "Dată realimentare combustibil"; 
texts['es'].refuel_date = "Fecha de repostaje"; 
texts['sr'].refuel_date = "Datum punjenja gorivom"; 
texts['gr'].refuel_date = "Ημερομηνία ανεφοδιασμού";

texts['bg'].refuel = "Заредено гориво";
texts['en'].refuel = "Refuel";
texts['de'].refuel = "Kraftstoff tanken"; 
texts['ro'].refuel = "Combustibil alimentat"; 
texts['es'].refuel = "Repostaje";
texts['sr'].refuel = "Punjenje gorivom";
texts['gr'].refuel = "Ανεφοδιασμός";

texts['bg'].errorOneCheckNeeded = "Нужно е да отметнете поне един ред!";
texts['en'].errorOneCheckNeeded = "You must check at least one row!";
texts['de'].errorOneCheckNeeded = "Sie mussen mindestens ein Ereignis auswahlen!"; 
texts['ro'].errorOneCheckNeeded = "Trebuie să selectați cel puțin un eveniment!"; 
texts['es'].errorOneCheckNeeded = "Debe seleccionar al menos un evento!";
texts['sr'].errorOneCheckNeeded = "Morate da izaberete najmanje jedan red!";
texts['gr'].errorOneCheckNeeded = "Πρέπει να επιλέξετε τουλάχιστον ένα συμβάν!";

texts['bg'].city_outside_city_interrelation_title = "Графика за съотношението градско / извънградско движение";
texts['en'].city_outside_city_interrelation_title = "Urban / Suburban correlation chart";
texts['de'].city_outside_city_interrelation_title = "Graphische Darstellung  der Bewegung innerorts/auserorts "; 
texts['ro'].city_outside_city_interrelation_title = "Grafic corelare urban/suburban ";
texts['es'].city_outside_city_interrelation_title = "Gráfica de correlación entre trafico urbano y extraurbano ";
texts['sr'].city_outside_city_interrelation_title = "Grafik za odnos gradskog/vangradskog saobraćaja ";
texts['gr'].city_outside_city_interrelation_title = "Αστικό/προαστιακό διάγραμμα συσχέτισης";

texts['bg'].move_stop_time_correlation_title = "Графика за съотношението движение / престои";
texts['en'].move_stop_time_correlation_title = "Movement / outage duration correlation chart";
texts['de'].move_stop_time_correlation_title = "Graphische Darstellung Bewegung / Aufenthalte"; 
texts['ro'].move_stop_time_correlation_title = "Grafic corelare mișcare/staționare "; 
texts['es'].move_stop_time_correlation_title = "Gráfica de correlación entre movimiento y estacionamiento "; 
texts['sr'].move_stop_time_correlation_title = "Grafik za odnos trajanja kretanja/pauze "; 
texts['gr'].move_stop_time_correlation_title = "Διάγραμμα συσχέτισης διάρκειας κίνησης /διακοπής";

texts['bg'].stops_time_title = "Графика за продължителността на престоите";
texts['en'].stops_time_title = "Outage duration chart";
texts['de'].stops_time_title = "Graphische Darstellung der Zeitdauer der Aufenthalte"; 
texts['ro'].stops_time_title = "Grafică pentru durată perioadă de oprire"; 
texts['es'].stops_time_title = "Gráfica para la duración de las paradas"; 
texts['sr'].stops_time_title = "Grafik za trajanje pauze"; 
texts['gr'].stops_time_title = "Διάγραμμα για τη διάρκεια της διαμονής";

texts['bg'].stop_duration = "Продължителност на престоя";
texts['en'].stop_duration = "Outage duration";
texts['de'].stop_duration = "Dauer des Aufenthalts"; 
texts['ro'].stop_duration = "Durata perioadei de oprire"; 
texts['es'].stop_duration = "Duración del estacionamiento";
texts['sr'].stop_duration = "Trajanje pauze";
texts['gr'].stop_duration = "Διάρκεια διαμονής";

texts['bg'].movements_analyse_title = "Графика за анализ на движенията";
texts['en'].movements_analyse_title = "Movement analysis chart";
texts['de'].movements_analyse_title = "Graphische Darstellung der Bewegungsanalyse"; 
texts['ro'].movements_analyse_title = "Grafic pentru analiza mișcării"; 
texts['es'].movements_analyse_title = "Análisis gráfico del movimiento";
texts['sr'].movements_analyse_title = "Grafik za analizu kretanja";
texts['gr'].movements_analyse_title = "Διάγραμμα ανάλυσης κίνησεων";

texts['bg'].daily_move_idle_title = "Графично изобразяване на времетраенето на движенията и престоите за всеки от дните";
texts['en'].daily_move_idle_title = "Graphical representation of movement and outage for each day";
texts['de'].daily_move_idle_title = "Graphische Darstellung der Bewegungs/Aufenthaltsdauer fur jeden Tag"; 
texts['ro'].daily_move_idle_title = "Grafic reprezentând mișcarea și staționarea pentru fiecare zi";
texts['es'].daily_move_idle_title = "Gráfico diario para duración de movimiento y estacionamiento";
texts['sr'].daily_move_idle_title = "Grafički prikaz trajanja kretanja i pauza za svaki dan";
texts['gr'].daily_move_idle_title = "Γραφική απεικόνιση της διάρκειας των κινήσεων και των στάσεων για κάθε μία από τις ημέρες";

texts['bg'].fuel_level_chart = "Графично изобразяване за нивото на гориво в резервоара";
texts['en'].fuel_level_chart = "Graphical representation of the vehicle's fuel level";
texts['de'].fuel_level_chart = "Graphische Darstellung des Kraftstoffstands im Tank"; 
texts['ro'].fuel_level_chart = "Grafic reprezentând nivelul combustibilului în rezervor";
texts['es'].fuel_level_chart = "Gráfico de nivel de combustible en el tanque";
texts['sr'].fuel_level_chart = "Grafički prikaz nivoa goriva u rezervoaru";
texts['gr'].fuel_level_chart = "Γραφική απεικόνιση της στάθμης καυσίμου της δεξαμενής";

texts['bg'].speed_level_chart = "Графично изобразяване на скоростта";
texts['en'].speed_level_chart = "Graphical representation of the vehicle's speed";
texts['de'].speed_level_chart = "Graphische Darstellung der Geschwindigkeit"; 
texts['ro'].speed_level_chart = "Grafic reprezentând viteza vehiclului";
texts['es'].speed_level_chart = "Gráfico de la velocidad";
texts['sr'].speed_level_chart = "Grafički prikaz brzine";
texts['gr'].speed_level_chart = "Γραφική απεικόνιση της ταχύτητας";

texts['bg'].temperature_level_chart = "Графично изобразяване на температурата";
texts['en'].temperature_level_chart = "Graphical representation of the vehicle's temperature";
texts['de'].temperature_level_chart = "Graphische Darstellung der Temperatur"; 
texts['ro'].temperature_level_chart = "Grafic reprezentând temperatura vehiclului ";
texts['es'].temperature_level_chart = "Gráfico de la temperatura ";
texts['sr'].temperature_level_chart = "Grafički prikaz temperature ";
texts['gr'].temperature_level_chart = "Γραφική απεικόνιση της θερμοκρασίας";

texts['bg'].accumulator_level_chart = "Графично изобразяване на акумулатора";
texts['en'].accumulator_level_chart = "Graphical representation of the vehicle's accumulator";
texts['de'].accumulator_level_chart = "Graphische Darstellung der Akkuleistung"; 
texts['ro'].accumulator_level_chart = "Grafic reprezentând acumulatorul vehiclului";
texts['es'].accumulator_level_chart = "Gráfico del acumulador";
texts['sr'].accumulator_level_chart = "Grafički prikaz akumulatora";
texts['gr'].accumulator_level_chart = "Γραφική απεικόνιση της μπαταρίας";

texts['bg'].day_time_duration = "Изминало време от денонощието";
texts['en'].day_time_duration = "Time";
texts['de'].day_time_duration = "Uhrzeit"; 
texts['ro'].day_time_duration = "Timp"; 
texts['es'].day_time_duration = "Timpo"; 
texts['sr'].day_time_duration = "Vreme"; 
texts['gr'].day_time_duration = "Χρόνος ";

texts['bg'].plot_trace_before_stops = "За да разгледате престоите за избрания ден трябва първо да изчертаете следата!";
texts['en'].plot_trace_before_stops = "In order to see the stops details on the map for this day you must first plot the vehicle's trace.";
texts['de'].plot_trace_before_stops = "Um die Aufenthalte fur den ausgewahlten Tag zu sehen, mussen Sie zuerst die Spur der Route aktivieren!"; 
texts['ro'].plot_trace_before_stops = "pentru a vedea perioada de oprire pentru ziua alesa trebuie mai intai să vizualizați rutele vehiculelor!";
texts['es'].plot_trace_before_stops = "¡Para ver las paradas del día seleccionado primero  tenga que cargar el rastro!";
texts['sr'].plot_trace_before_stops = "Da bi ste videli detalje pauza na mapi za ovaj dan morate prvo da prikažete putanju vozila!";
texts['gr'].plot_trace_before_stops = "Για να δείτε τις λεπτομέρειες σταματήματος στο χάρτη για αυτήν την ημέρα, πρέπει πρώτα να σχεδιάσετε το ίχνος του οχήματος!";

texts['bg'].noSuchVehicleInTheFirm = "Въвели сте автомобил, който не принадлежи на вашата фирма!";
texts['en'].noSuchVehicleInTheFirm = "You have entered a vehicle that does not belong to this firm!";
texts['de'].noSuchVehicleInTheFirm = "Sie haben ein Fahrzeug ausgewahlt, das nicht zu Ihrer Firma gehort!"; 
texts['ro'].noSuchVehicleInTheFirm = "Ați introdus vehicule, care nu sunt de la firma dvs .!"; 
texts['es'].noSuchVehicleInTheFirm = "Ha introducido un coche que no pertenece a su empresa .!"; 
texts['sr'].noSuchVehicleInTheFirm = "Uneli ste vozilo koje ne pripada Vašoj firmi!"; 
texts['gr'].noSuchVehicleInTheFirm = "Έχετε εισάγει ένα όχημα  που δεν ανήκει στην εταιρεία σας!";

texts['bg'].wrongDataInserted = "Имате грешно попълнени данни!";
texts['en'].wrongDataInserted = "You have entered invalid data!";
texts['de'].wrongDataInserted = "Sie haben ungultige Daten eingegeben!"; 
texts['ro'].wrongDataInserted = "Ați completat date greșite!"; 
texts['es'].wrongDataInserted = "Ha introducido datos no válidos!"; 
texts['sr'].wrongDataInserted = "Uneli ste pogrešne podatke!"; 
texts['gr'].wrongDataInserted = "Έχετε συμπληρώσει εσφαλμένα τα δεδομένα!";

texts['bg'].noStartStationSelected = "Не сте избрали начална станция!";
texts['en'].noStartStationSelected = "You did not choose a starting point!";
texts['de'].noStartStationSelected = "Sie haben keine Anfangsposition ausgewahlt!"; 
texts['ro'].noStartStationSelected = "Nu ati ales un punct de plecare!";
texts['es'].noStartStationSelected = "Usted no ha elegido un punto de inicio!";
texts['sr'].noStartStationSelected = "Niste izabrali početnu tačku!";
texts['gr'].noStartStationSelected = "Δεν έχετε επιλέξει σταθμό εκκίνησης!";

texts['bg'].noCargoZonesSelected = "Не сте избрали зони за разтоварване!";
texts['en'].noCargoZonesSelected = "You did not select discharging zones!";
texts['de'].noCargoZonesSelected = "Sie haben keine Ausladezonen ausgewahlt!"; 
texts['ro'].noCargoZonesSelected = "Nu ați ales zonele de descarcare!"; 
texts['es'].noCargoZonesSelected = "Usted no ha elegido  zona de descarga!";
texts['sr'].noCargoZonesSelected = "Niste izabrali zone za istovar!";
texts['gr'].noCargoZonesSelected = "Δεν έχετε επιλέξει περιοχές εκφόρτωσης!";

texts['bg'].noCitiesSelectedForStopsFilter = "Избрали сте филтър за престоите, но не сте избрали градове/села!";
texts['en'].noCitiesSelectedForStopsFilter = "You have choosed а filter but did not specify living places!";
texts['de'].noCitiesSelectedForStopsFilter = "Sie haben einen Filter der Aufenthalte ausgewahlt, aber keinen Ort!"; 
texts['ro'].noCitiesSelectedForStopsFilter = "Ați ales un filtru de opriri, dar nu ați ales localitățile (orașe/sate)!"; 
texts['es'].noCitiesSelectedForStopsFilter = "Usted ha elegido filtro de paradas, pero no ha elegido ciudad/pueblo!";
texts['sr'].noCitiesSelectedForStopsFilter = "Izabrali ste filter za pauze ali niste izabrali gradove/sela!";
texts['gr'].noCitiesSelectedForStopsFilter = "Έχετε επιλέξει ένα φίλτρο για τη διαμονή σας, αλλά δεν έχετε επιλέξει πόλεις/χωριά!";

texts['bg'].noMarkedPlacesSelectedForStopsFilter = "Избрали сте филтър за престоите, но не сте избрали маркирани обекти!";
texts['en'].noMarkedPlacesSelectedForStopsFilter = "You have choosed а filter but did not specify marked places!";
texts['de'].noMarkedPlacesSelectedForStopsFilter = "Sie haben einen Filter der Aufenthalte aktiviert, aber keine markierten Objekte ausgewahlt!"; 
texts['ro'].noMarkedPlacesSelectedForStopsFilter = "Ați ales un filtru de opriri, dar nu ați ales locuri marcate!"; 
texts['es'].noMarkedPlacesSelectedForStopsFilter = "Usted ha elegido filtro de paradas, pero no ha elegido filtro de objetos marcados!"; 
texts['sr'].noMarkedPlacesSelectedForStopsFilter = "Izabrali ste filtere za pauze ali niste izabrali obeležena mesta!"; 
texts['gr'].noMarkedPlacesSelectedForStopsFilter = "Έχετε επιλέξει ένα φίλτρο για τη διαμονή σας, αλλά δεν έχετε επιλέξει επισημασμένες  τοποθεσίες!";

texts['bg'].noMarkedPolygonsSelectedForStopsFilter = "Избрали сте филтър за престоите, но не сте избрали маркирани полигони!";
texts['en'].noMarkedPolygonsSelectedForStopsFilter = "You have choosed а filter but did not specify marked terrains!";
texts['de'].noMarkedPolygonsSelectedForStopsFilter = "Sie haben einen Filter der Aufenthalte aktiviert, aber keine markierten Gebiete ausgewahlt!"; 
texts['ro'].noMarkedPolygonsSelectedForStopsFilter = "Ați ales filtru pentru perioade de oprire, dar nu ați ales aria/zona marcată!";
texts['es'].noMarkedPolygonsSelectedForStopsFilter = "Usted ha elegido filtro de paradas, pero no ha elegido filtro de polígonos marcados!";
texts['sr'].noMarkedPolygonsSelectedForStopsFilter = "Izabrali ste filter za pauze ali niste izabrali obeležene terene!";
texts['gr'].noMarkedPolygonsSelectedForStopsFilter = "Έχετε επιλέξει ένα φίλτρο για τη διαμονή σας, αλλά δεν έχετε επιλέξει σημαδεμένα πολύγωνα!";

texts['bg'].noMarkedPlacesPolygonsSelectedForStopsFilter = "Избрали сте филтър за престоите, но не сте избрали нито един маркиран терен/полигон!";
texts['en'].noMarkedPlacesPolygonsSelectedForStopsFilter = "You have choosed а filter but did not specify marked places/terrains!";
texts['de'].noMarkedPlacesPolygonsSelectedForStopsFilter = "Sie haben einen Filter der Aufenthalte aktiviert, aber keinen markierten Ort/kein markiertes Gebiet ausgewahlt!"; 
texts['ro'].noMarkedPlacesPolygonsSelectedForStopsFilter = "Ați ales filtru pentru perioade de oprire, dar nu ați ales zonele/locurile marcate!";
texts['es'].noMarkedPlacesPolygonsSelectedForStopsFilter = "Usted ha elegido filtro de paradas, pero no ha elegido filtro de terrenos marcados!";
texts['sr'].noMarkedPlacesPolygonsSelectedForStopsFilter = "Izabrali ste filter za pauze ali niste izabrali nijedno obeleženo mesto/teren!";
texts['gr'].noMarkedPlacesPolygonsSelectedForStopsFilter = "Έχετε επιλέξει ένα φίλτρο για τη διαμονή σας, αλλά δεν έχετε επιλέξει κανένα επιλεγμένο έδαφος / πολύγωνο!";

texts['bg'].stops_filter_selection = "Избор на обекти, терени, градове и села за филтриране на пътен лист за престои";
texts['en'].stops_filter_selection = "Selection of places, terrains and living places";
texts['de'].stops_filter_selection = "Auswahl der Objekte, Gebiete, Orte, um die Aufenthalte in einem Fahrtenbuch zu filtern"; 
texts['ro'].stops_filter_selection = "Alegere locuri, terenuri , orașe și sate pentru filtrare de foaie parcursa penru perioadă de oprire"; 
texts['es'].stops_filter_selection = "Seleccionar  objetos, terreno, pueblos y ciudades para filtración de 'informe de paradas'"; 
texts['sr'].stops_filter_selection = "Izbor mesta, terena i naseljenih mesta za filtriranje u izveštaju o pauzama'"; 
texts['gr'].stops_filter_selection = "Επιλογή τοποθεσιών,πόλεις και χωριά ";

texts['bg'].noStopsFilterSelected = "Не сте избрали филтър за престоите!";
texts['en'].noStopsFilterSelected = "No stops filter selected!";
texts['de'].noStopsFilterSelected = "Sie haben keinen Aufenthaltsfilter ausgewahlt"; 
texts['ro'].noStopsFilterSelected = "Nu ați ales filtru pentru perioada de oprire"; 
texts['es'].noStopsFilterSelected = "No ha seleccionado filtro de paradas"; 
texts['sr'].noStopsFilterSelected = "Niste izabrali filter za pauze!"; 
texts['gr'].noStopsFilterSelected = "Δεν έχετε επιλέξει φίλτρο για τη διαμονή σας!";

texts['bg'].noVehiclesSelected = "Не сте избрали превозни средства!";
texts['en'].noVehiclesSelected = "No vehicles selected!";
texts['de'].noVehiclesSelected = "Sie haben keine Fahrzeuge ausgewahlt!"; 
texts['ro'].noVehiclesSelected = "Nu ați selectat vehicule!"; 
texts['es'].noVehiclesSelected = "No ha seleccionado vehículo!"; 
texts['sr'].noVehiclesSelected = "Niste izabrali vozilo!"; 
texts['gr'].noVehiclesSelected = "Δεν έχετε επιλέξει οχήματα!";

texts['bg'].vehicles_pois_selection = "Избор на превозни средства,обекти и терени.";
texts['en'].vehicles_pois_selection = "Selection of vehicles, places and terrains.";
texts['de'].vehicles_pois_selection = "Auswahl der Fahrzeuge, Objekte und Gebiete"; 
texts['ro'].vehicles_pois_selection = "Alegere de vehicule, locuri și terenuri"; 
texts['es'].vehicles_pois_selection = "Selección de vehículos, lugares y terrenos";
texts['sr'].vehicles_pois_selection = "Izbor vozila, mesta i terena.";
texts['gr'].vehicles_pois_selection = "Επιλογή οχημάτων, τοποθεσιών  και εδαφών.";

texts['bg'].vehicleAlias_simbolsLimit = "Псевдоним (до 30 символа)";
texts['en'].vehicleAlias_simbolsLimit = "Alias (up to 30 symbols)";
texts['de'].vehicleAlias_simbolsLimit = "Deckname (max. 30 Symbole)"; 
texts['ro'].vehicleAlias_simbolsLimit = "Pseudonime (max. 30 Simbole)";
texts['es'].vehicleAlias_simbolsLimit = "Alias (hasta 30 símbolos)";
texts['sr'].vehicleAlias_simbolsLimit = "Pseudonim (do 30 símbola)";
texts['gr'].vehicleAlias_simbolsLimit = "Ψευδώνυμο (έως και 30 χαρακτήρες)";

texts['bg'].currentPassword = "Текуща парола";
texts['en'].currentPassword = "Current password";
texts['de'].currentPassword = "Aktuelles Passwort"; 
texts['ro'].currentPassword = "Parolă curentă"; 
texts['es'].currentPassword = "Contraseña actual";
texts['sr'].currentPassword = "Trenutna lozinka";
texts['gr'].currentPassword = "Τρέχων κωδικός πρόσβασης";

texts['bg'].newPassword = "Нова парола";
texts['en'].newPassword = "New password";
texts['de'].newPassword = "Neues Passwort"; 
texts['ro'].newPassword = "Parolă noua"; 
texts['es'].newPassword = "Nueva contraseña"; 
texts['sr'].newPassword = "Nova lozinka"; 
texts['gr'].newPassword = "Νέος κωδικός πρόσβασης";

texts['bg'].newPasswordConfirm = "Повтори новата парола";
texts['en'].newPasswordConfirm = "Repeat password";
texts['de'].newPasswordConfirm = "Das neue Passwort bestatigen"; 
texts['ro'].newPasswordConfirm = "Repetă parolă noua"; 
texts['es'].newPasswordConfirm = "Repita su nueva contraseña"; 
texts['sr'].newPasswordConfirm = "Ponovite lozinku"; 
texts['gr'].newPasswordConfirm = "Επαναλάβετε τον νέο κωδικό πρόσβασης";

texts['bg'].passwordChange = "Промяна на паролата";
texts['en'].passwordChange = "Change password";
texts['de'].passwordChange = "Passwort andern"; 
texts['ro'].passwordChange = "Schimbă parola"; 
texts['es'].passwordChange = "Modificar contraseña";
texts['sr'].passwordChange = "Promenite lozinku";
texts['gr'].passwordChange = "Αλλαγή κωδικό πρόσβασης";

texts['bg'].nonEqualPasswords = "Паролите не съвпадат!";
texts['en'].nonEqualPasswords = "The passwords you've entered are not the same!";
texts['de'].nonEqualPasswords = "Passworter stimmen nicht uberein!"; 
texts['ro'].nonEqualPasswords = "Parolele pe care le-ati introdus nu sunt identice!";
texts['es'].nonEqualPasswords = "¡Las contraseñas no coinciden!";
texts['sr'].nonEqualPasswords = "Lozinke koje ste uneli nisu iste!";
texts['gr'].nonEqualPasswords = "Οι κωδικοί πρόσβασης δεν ταιριάζουν!";

texts['bg'].form_pass_change_label = "Формуляр за промяна на текущата парола за вход в системата.";
texts['en'].form_pass_change_label = "Change login password";
texts['de'].form_pass_change_label = "Andern des aktuellen Loginpassworts"; 
texts['ro'].form_pass_change_label = "Schimbă parola de logare";
texts['es'].form_pass_change_label = "Modificar la contraseña de inicio de sesión";
texts['sr'].form_pass_change_label = "Obrazac za promenu trenutne lozinke za ulaz u sistem";
texts['gr'].form_pass_change_label = "Φόρμα τροποποίησης του τρέχοντος κωδικού πρόσβασης σύνδεσης στο σύστημα.";

texts['bg'].form_firm_data_change_label = "Формуляр за промяна на текущата парола за модифициране на споделени фирмени ресурси.";
texts['en'].form_firm_data_change_label = "Change modification data password.";
texts['de'].form_firm_data_change_label = "Anderung des aktuellen Passworts zur Modifizierung der Fahrzeugdaten."; 
texts['ro'].form_firm_data_change_label = "Schimbați parola curentă pentru a modifica datele vehiculor/parcului ."; 
texts['es'].form_firm_data_change_label = "Cambiar la contraseña de modificación de datos."; 
texts['sr'].form_firm_data_change_label = "Obrazac za promenu trenutne lozinke za izmenu podataka."; 
texts['gr'].form_firm_data_change_label = "Μορφή για να αλλάξετε τον τρέχοντα κωδικό πρόσβασης για την τροποποίηση των κοινών εταιρικών πόρων.";

texts['bg'].remonts_view_correction = "Преглед и корекция на ремонти";
texts['en'].remonts_view_correction = "View and edit vehicle repairs";
texts['de'].remonts_view_correction = "Uberblick / Eingabe der Reparaturen"; 
texts['ro'].remonts_view_correction = "Vizualizare si editare reparații vehicul"; 
texts['es'].remonts_view_correction = "Visualización y edición de reparación de vehículos";
texts['sr'].remonts_view_correction = "Pregled i izmena popravke vozila";
texts['gr'].remonts_view_correction = "Προβολή και επεξεργασία επιδιορθώσεων οχημάτων";

texts['bg'].oil_change_view_correction = "Преглед и корекция на смени на масла";
texts['en'].oil_change_view_correction = "View and edit oil change";
texts['de'].oil_change_view_correction = "Uberblick / Eingabe der Olwechsel"; 
texts['ro'].oil_change_view_correction = "Vizualizare si editare schimb de ulei"; 
texts['es'].oil_change_view_correction = "Visualización y edición de cambio de aceite";
texts['sr'].oil_change_view_correction = "Pregled i izmena promene ulja";
texts['gr'].oil_change_view_correction = "Προβολή και επεξεργασία αλλαγής λαδιού";

texts['bg'].civil_insurance_view_correction = "Преглед и корекция на граждански застраховки";
texts['en'].civil_insurance_view_correction = "View and edit civil ensurances";
texts['de'].civil_insurance_view_correction = "Uberblick / Eingabe der Versicherungen"; 
texts['ro'].civil_insurance_view_correction = "Vizualizare si editare asigurării civile"; 
texts['es'].civil_insurance_view_correction = "Visualización y edición de seguro civil"; 
texts['sr'].civil_insurance_view_correction = "Pregled i izmena osiguranja građana"; 
texts['gr'].civil_insurance_view_correction = "Προβολή και επεξεργασία αστικών ασφαλίσεων";

texts['bg'].autokasko_view_correction = "Преглед и корекция на автокаско";
texts['en'].autokasko_view_correction = "View and edit a vehicle insurance";
texts['de'].autokasko_view_correction = "Uberblick / Eingabe der Kaskoversicherungen";
texts['ro'].autokasko_view_correction = "Vizualizare si editare asigurării CASCO";
texts['es'].autokasko_view_correction = "Visualización y edición de seguro";
texts['sr'].autokasko_view_correction = "Pregled i izmena osiguranja vozila";
texts['gr'].autokasko_view_correction = "Προβολή και επεξεργασία ασφάλισης οχήματος";

texts['bg'].vingette_view_correction = "Преглед и корекция на винетки";
texts['en'].vingette_view_correction = "View and edit vignettes";
texts['de'].vingette_view_correction = "Uberblick / Eingabe der Mautgebuhren"; 
texts['ro'].vingette_view_correction = "Vizualizare si editare roviniete"; 
texts['es'].vingette_view_correction = "Visualización y edición de telepeaje"; 
texts['sr'].vingette_view_correction = "Pregled i izmena vinjeta";
texts['gr'].vingette_view_correction = "Προβολή και επεξεργασία βινιέτες";

texts['bg'].gtp_view_correction = "Преглед и корекция на Г.Т.П.";
texts['en'].gtp_view_correction = "View and edit A.T.R.";
texts['de'].gtp_view_correction = "Uberblick / Eingabe des TUVs"; 
texts['ro'].gtp_view_correction = "Vizualizare si editare ITP-uri";  
texts['es'].gtp_view_correction = "Visualización y edición de I.T.V.";  
texts['sr'].gtp_view_correction = "Pregled i izmena A.D.R.";  
texts['gr'].gtp_view_correction = "Προβολή και επεξεργασία Κ.Τ.Ε.Ο.";

texts['bg'].tachometer_view_correction = "Преглед и корекция на тахометри";
texts['en'].tachometer_view_correction = "View and edit tachometers";
texts['de'].tachometer_view_correction = "Uberblick / Eingabe der Tachokalibrierung"; 
texts['ro'].tachometer_view_correction = "Vizualizare si editare tahometru"; 
texts['es'].tachometer_view_correction = "Visualización y edición de tacómetros"; 
texts['sr'].tachometer_view_correction = "Pregled i izmena tahometara"; 
texts['gr'].tachometer_view_correction = "Προβολή και επεξεργασία ταχομέτρων";

texts['bg'].data_view_correction = "Преглед и корекция на данни";
texts['en'].data_view_correction = "View and edit data";
texts['de'].data_view_correction = "Uberblick / Eingabe / Korrektur der Daten"; 
texts['ro'].data_view_correction = "Vizualizare si editare date"; 
texts['es'].data_view_correction = "Visualización y edición de datos";
texts['sr'].data_view_correction = "Pregled i izmena podataka";
texts['gr'].data_view_correction = "Προβολή και επεξεργασία δεδομένων";

texts['bg'].errorAlert = "Възникна грешка";
texts['en'].errorAlert = "Error occured!";
texts['de'].errorAlert = "Es ist einen Fehler aufgetreten!"; 
texts['ro'].errorAlert = "Eroare apărută!"; 
texts['es'].errorAlert = "¡Se ha producido un error!";
texts['sr'].errorAlert = "Pojavila se greška!";
texts['gr'].errorAlert = "Παρουσιάστηκε σφάλμα";

texts['bg'].errorDateCheck = "ГРЕШКА! Проверете последователността на датите!";
texts['en'].errorDateCheck = "ERROR! Check the sequence of entered data!";
texts['de'].errorDateCheck = "Fehler! Die Reihenfolge der Daten uberprufen!"; 
texts['ro'].errorDateCheck = "Eroare !Verificați secvența de date introduse!"; 
texts['es'].errorDateCheck = "¡ERROR! Compruebe secuencia de las fechas!"; 
texts['sr'].errorDateCheck = "GREŠKA! Proverite redosled unetih podataka!"; 
texts['gr'].errorDateCheck = "ΣΦΑΛΜΑ! Ελέγξτε την ακολουθία ημερομηνιών!";

texts['bg'].wrongPass = "Въвели сте грешна парола!";
texts['en'].wrongPass = "You have entered a wrong password!";
texts['de'].wrongPass = "Passwort falsch!"; 
texts['ro'].wrongPass = "Ați introdus parola incorectă!"; 
texts['es'].wrongPass = "Ha introducido una contraseña incorrecta!"; 
texts['sr'].wrongPass = "Uneli ste pogrešnu lozinku!"; 
texts['gr'].wrongPass = "Έχετε εισάγει λάθος κωδικό πρόσβασης!";

texts['bg'].deleteSuccessful = "Изтриването беше успешно!";
texts['en'].deleteSuccessful = "Successfully deleted!";
texts['de'].deleteSuccessful = "Die Loschung war erfolgreich!"; 
texts['ro'].deleteSuccessful = "Ștergere reușită!"; 
texts['es'].deleteSuccessful = "Eliminado correctamente!";
texts['sr'].deleteSuccessful = "Uspešno izbrisano!";
texts['gr'].deleteSuccessful = "Η διαγραφή ήταν επιτυχής!";

texts['bg'].insertSuccessful = "Добавянето беше успешно!";
texts['en'].insertSuccessful = "Successfully added!";
texts['de'].insertSuccessful = "Erfolgreich eingefugt!"; 
texts['ro'].insertSuccessful = "Adăugare succesibilă!"; 
texts['es'].insertSuccessful = "Se ha añadido con éxito!"; 
texts['sr'].insertSuccessful = "Uspešno dodato!"; 
texts['gr'].insertSuccessful = "Η προσθήκη ήταν επιτυχής!";

texts['bg'].fuel_fill_check = "Зареждане на гориво";
texts['en'].fuel_fill_check = "Refuel";
texts['de'].fuel_fill_check = "Tanken"; 
texts['ro'].fuel_fill_check = "Alimentare combustibil"; 
texts['es'].fuel_fill_check = "Repostaje"; 
texts['sr'].fuel_fill_check = "Sipanje goriva"; 
texts['gr'].fuel_fill_check = "Ανεφοδιασμός";

texts['bg'].montly_refuel_checks = "Зареждания на гориво за месец"; //Януари...
texts['en'].montly_refuel_checks = "Refuels for month"; //Януари...
texts['de'].montly_refuel_checks = "Monatsbericht Tanken";
texts['ro'].montly_refuel_checks = "Alimentari cu combustibil pentru lună";
texts['es'].montly_refuel_checks = "Repostajes mensuales";
texts['sr'].montly_refuel_checks = "Sipanje goriva za mesec";
texts['gr'].montly_refuel_checks = "Ανεφοδιασμός για ton μήνα "; 

texts['bg'].route_pattern = "Шаблон за маршрут";
texts['en'].route_pattern = "Route pattern";
texts['de'].route_pattern = "Muster der Route"; 
texts['ro'].route_pattern = "Modele de traseu";
texts['es'].route_pattern = "patrón de ruta";
texts['sr'].route_pattern = "Šablon maršrute";
texts['gr'].route_pattern = "Πρότυπο διαδρομής";

texts['bg'].route_pattern_comparement = "Сравнение по шаблон";
texts['en'].route_pattern_comparement = "Pattern comparement";
texts['de'].route_pattern_comparement = "Route mit einem Muster vergleichen"; 
texts['ro'].route_pattern_comparement = "Compara rută cu un model";
texts['es'].route_pattern_comparement = "Comparación por patrón";
texts['sr'].route_pattern_comparement = "Upoređivanje šablona";
texts['gr'].route_pattern_comparement = "Σύγκριση προτύπων";

texts['bg'].routeCreatorExplain = "Натиснете върху картата за да отбележите началното място. Натиснете отново за да получите маршрут от предишно избраната точка до текущата.Преместете точка от маршрута за да промените кривата.";
texts['en'].routeCreatorExplain = "Click on the map to mark a starting point. Click again on a different place to get a route from the previous point to the last one. Drag an intermediate point to change the route curve.";
texts['de'].routeCreatorExplain = "Klicken Sie auf die Landkarte, um die Anfangsposition der Route zu bestimmen. Klicken Sie noch einmal auf die Landkarte, um eine Route vom vorigen bis zum gegenwartigen Punkt zu erstellen. Ziehen Sie einen Zwischenpunkt( z.B. uber einen Ort), um die Route zu verandern."; 
texts['ro'].routeCreatorExplain = "Click pe harta pentru marcare punct de plecare.Click dinou intr-un alt loc pentru a genera un traseu din punctul precedent. alege un punct intermediar pentru a schimba traseul ";
texts['es'].routeCreatorExplain = "Haga clic sobre el mapa para marcar un punto de inicio.Haga clic de nuevo de un lugar diferente para obtener una ruta desde el punto anterior hasta punto actual. Arrastre un punto intermedio para cambiar la curva de la ruta";
texts['sr'].routeCreatorExplain = "Kliknite na mapu da obeležite početnu tačku. Kliknite opet na drugo mesto da dobijete maršrutu od prethodne tačke do trenutne. Prevucite tačku maršrute da promenite krivulju maršrute";
texts['gr'].routeCreatorExplain = "Πατήστε  στο χάρτη για να επισημάνετε το σημείο εκκίνησης. Πιέστε ξανά για να φτάσετε μια διαδρομή από το προηγουμένως επιλεγμένο σημείο στο τρέχον. Μετακινήστε ένα σημείο διαδρομής για να αλλάξετε την καμπύλη.";

texts['bg'].new_report = "Нова справка";
texts['en'].new_report = "New report";
texts['de'].new_report = "Neuer Bericht"; 
texts['ro'].new_report = "Raport nou"; 
texts['es'].new_report = "Nuevo informe";
texts['sr'].new_report = "Nov izveštaj";
texts['gr'].new_report = "Νέα αναφορά";

texts['bg'].show_driver_stops = "Покажи престойте на водача";
texts['en'].show_driver_stops = "Show driver's stops";
texts['de'].show_driver_stops = "Die Aufenthalte des Fahrers anzeigen"; 
texts['ro'].show_driver_stops = "Arată perioadele de oprire ale șoferului"; 
texts['es'].show_driver_stops = "Mostrar las paradas del conductor";
texts['sr'].show_driver_stops = "Prikaži pauze vozača.";
texts['gr'].show_driver_stops = "Εμφάνιση παραμονής του οδηγού";

texts['bg'].load_pattern = "Зареди шаблон";
texts['en'].load_pattern = "Load a pattern";
texts['de'].load_pattern = "Muster einfugen"; 
texts['ro'].load_pattern = "Încarcă un model de traseul"; 
texts['es'].load_pattern = "Cargar patrón"; 
texts['sr'].load_pattern = "Učitaj šablon."; 
texts['gr'].load_pattern = "Τοποθετήστε ένα πρότυπο";

texts['bg'].new_report_confirm = "Сигурни ли сте, че искате да започнете нова справка?";
texts['en'].new_report_confirm = "Are you sure you want to begin new report?";
texts['de'].new_report_confirm = "Sind Sie sicher, dass Sie einen neuen Bericht erstellen mochten?"; 
texts['ro'].new_report_confirm = "Sunteți sigur că doriți să înceapă un raport nou?"; 
texts['es'].new_report_confirm = "¿Seguro que deseas comenzar nuevo informe?"; 
texts['sr'].new_report_confirm = "Da li ste sigurni da želite da započnete nov izveštaj?"; 
texts['gr'].new_report_confirm = "Είστε βέβαιοι ότι θέλετε να ξεκινήσετε μια νέα αναφορά?";

texts['bg'].add_to_pois = "Съхранение в маркирани обекти";
texts['en'].add_to_pois = "Save into the marked objects";
texts['de'].add_to_pois = "In den markierten Objekten speichern"; 
texts['ro'].add_to_pois = "Salvare locuri marcate"; 
texts['es'].add_to_pois = "Guardar en los objetos marcados"; 
texts['sr'].add_to_pois = "Sačuvaj u obeležena mesta"; 
texts['gr'].add_to_pois = "Αποθηκευστε στις επισημασμένες τοποθεσίες ";

texts['bg'].form_addNewRoute_label = "Формуляр за добавяне на нов маршрут в системата.";
texts['en'].form_addNewRoute_label = "Form for adding a new route";
texts['de'].form_addNewRoute_label = "Neue Route erstellen"; 
texts['ro'].form_addNewRoute_label = "Formular pentru adaugare traseu nou"; 
texts['es'].form_addNewRoute_label = "Formulario para añadir una nueva ruta"; 
texts['sr'].form_addNewRoute_label = "Obrazac za dodavanje nove maršrute"; 
texts['gr'].form_addNewRoute_label = "Φόρμα προσθήκης νέας διαδρομής στο σύστημα.";

texts['bg'].pattern_stop = "Престой по шаблон";
texts['en'].pattern_stop = "Pattern outage";
texts['de'].pattern_stop = "Aufenthalt nach dem Muster";
texts['ro'].pattern_stop = "Model opriri/staționari"; 
texts['es'].pattern_stop = "Paradas de patrón"; 
texts['sr'].pattern_stop = "Pauza po šablonu";
texts['gr'].pattern_stop = "Πρότυπο στάσεις ";

texts['bg'].radius_stop = "Престой в радиус";
texts['en'].radius_stop = "Radius outage";
texts['de'].radius_stop = "Aufenthalt im Radius"; 
texts['ro'].radius_stop = "Rază de oprire/staționare"; 
texts['es'].radius_stop = "Parada en radio"; 
texts['sr'].radius_stop = "Pauza u radijusu"; 
texts['gr'].radius_stop = "Παραμονές  σε ακτίνα ";

texts['bg'].show_on_map_tooltip = "Натиснете тук за да видите мястото на картата.";
texts['en'].show_on_map_tooltip = "Click here to see this place on the map.";
texts['de'].show_on_map_tooltip = "Hier klicken, um den Ort auf der Landkarte zu sehen."; 
texts['ro'].show_on_map_tooltip = "Apasați aici ca să vedeți acest loc pe harta."; 
texts['es'].show_on_map_tooltip = "Haga clic aquí para ver este lugar en el mapa."; 
texts['sr'].show_on_map_tooltip = "Kliknite ovde da vidite ovo mesto na mapi."; 
texts['gr'].show_on_map_tooltip = "Πατήστε εδώ για να δείτε το μέρος στο χάρτη.";

texts['bg'].clickOnlyOnceAlert = "Не е необходимо да се клика два пъти на едно и също място!";
texts['en'].clickOnlyOnceAlert = "Do not click twice on the same place!";
texts['de'].clickOnlyOnceAlert = "Es ist nicht notig, auf einen Punkt zweimal zu klicken!"; 
texts['ro'].clickOnlyOnceAlert = "Nu este necesar să dea click de doua ori pe acest loc!"; 
texts['es'].clickOnlyOnceAlert = "¡No hay necesidad de hacer clic dos veces en el mismo lugar!"; 
texts['sr'].clickOnlyOnceAlert = "Nemojte da kliknete dvaput na isto mesto!"; 
texts['gr'].clickOnlyOnceAlert = "Δεν χρειάζεται να κάνετε διπλό κλικ στην ίδια θέση!";

texts['bg'].badCoordinatesSelected = "Избрали сте неподходяща точка!";
texts['en'].badCoordinatesSelected = "You have clicked on a place that can not be a part of a route!";
texts['de'].badCoordinatesSelected = "Sie klicken auf einem Punkt, der kein Teil der Route ist!"; 
texts['ro'].badCoordinatesSelected = "Ași dat click pe un loc care nu face parte din ruta/traseu!"; 
texts['es'].badCoordinatesSelected = "¡Usted ha hecho clic en un lugar que no puede ser parte de una ruta!"; 
texts['sr'].badCoordinatesSelected = "Kliknuli ste na mesto koje nije deo maršrute!"; 
texts['gr'].badCoordinatesSelected = "Έχετε επιλέξει ένα ακατάλληλο σημείο!";

texts['bg'].current_distance = "Текущо разстояние";
texts['en'].current_distance = "Current distance";
texts['de'].current_distance = "Aktuelle Entfernung"; 
texts['ro'].current_distance = "Distanță curentă"; 
texts['es'].current_distance = "Distancia actual"; 
texts['sr'].current_distance = "Trenutno rastojanje"; 
texts['gr'].current_distance = "Τρέχουσα απόσταση";

texts['bg'].centerObjectOnMap = "Натиснете тук за да позиционирате обекта в центъра на екрана";
texts['en'].centerObjectOnMap = "Click here to center on screen";
texts['de'].centerObjectOnMap = "Hier klicken, um das Objekt auf der Landkarte zu zentrieren"; 
texts['ro'].centerObjectOnMap = "Click aici pentru centrare harta in ecran"; 
texts['es'].centerObjectOnMap = "Haga clic aquí para centrar en la pantalla"; 
texts['sr'].centerObjectOnMap = "Kliknite ovde da centrirate objekat na ekranu"; 
texts['gr'].centerObjectOnMap = "Πατήστε  εδώ για να τοποθετήσετε το μέρος  στο κέντρο της οθόνης";

texts['bg'].routeNameLengthError = "Желателно е името да бъде между 3 и 20 символа!";
texts['en'].routeNameLengthError = "The name should be between 3 and 20 symbols!";
texts['de'].routeNameLengthError = "Der Name soll 3 bis 20 Symbole beinhalten!"; 
texts['ro'].routeNameLengthError = "Se recomandă numele să fie între 3 și 20 simbole!"; 
texts['es'].routeNameLengthError = "El nombre tiene que ser entre 3 y 20 símbolos!!"; 
texts['sr'].routeNameLengthError = "Ime treba da sadrži između 3 i 20 simbola!"; 
texts['gr'].routeNameLengthError = "Είναι επιθυμητό το όνομα να είναι μεταξύ 3 και 20 χαρακτήρων!";

texts['bg'].routeSaveNonNameAlert = "Tъй като не сте въвели име за маршрута той ще бъде обновен с новите данни. Сигурни ли сте, че искате да продължите?";
texts['en'].routeSaveNonNameAlert = "You have made modifications on the route and did not specify a name for the new route. This means that the existing one will be updates in the system database. Are you sure you want to continue?";
texts['de'].routeSaveNonNameAlert = "Sie haben Anderungen an der Route vorgenommen, aber keinen neuen Namen gegeben. Das bedeutet, dass die aktuelle Route geandert wird. Sind Sie sicher, dass Sie weitermachen mochten?"; 
texts['ro'].routeSaveNonNameAlert = "Pentru că nu ațiintrodus nume pentru itinerariul , el o să fie remodificat cu date noi. Dase Route geandert wird. Sunteți sigur că doriți să continuați?"; 
texts['es'].routeSaveNonNameAlert = "Hay una modificación de la ruta actual. Si no introduce un nombre nuevo (para crear una ruta nueva), esta modificación se grabará en la ruta actual. ¿Estás seguro que quieres continuar?"; 
texts['sr'].routeSaveNonNameAlert = "Napravili ste izmene na maršruti i niste odredili ime za novu maršrutu. To znači da će postojeća maršruta biti ažurirana u sistemu baze podataka. Da li ste sigurni da želite da nastavite?"; 
texts['gr'].routeSaveNonNameAlert = "Δεν  έχετε εισάγει ένα όνομα διαδρομής, θα ενημερωθεί με τα νέα δεδομένα. Είστε βέβαιοι ότι θέλετε να συνεχίσετε?";


texts['bg'].routeSaveExisitngNameAlert = "Задали сте име за маршрут, който съществува в системата, но е модифициран от вас. Ако приемете да запаметите маршура, то той ще бъде съхранен като нов в системата. Ако искате да промените предишния оставете полето с името празно. Сигурни ли сте, че искате да продължите?";
texts['en'].routeSaveExisitngNameAlert = "You have entered a name for a route that already exists in the system database. This makes sense if you want to modify this route and save it as a new pattern in the system. If you simply want to edit the existing route and the save the changes just leave the name field empty. Are you sure you want to continue?";
texts['de'].routeSaveExisitngNameAlert = "Sie haben einen Namen der Route eingegeben, der bereits in den Systemdaten existiert. Wenn Sie die Route sichern, so wird Sie als eine neue Route im System gespeichert. Wenn Sie die vorige Route nur verandern mochten, dann lassen Sie das Namensfenster leer. Sind Sie sicher, dass Sie weitermachen mochten?"; 
texts['ro'].routeSaveNonNameAlert = "Ați introdus nume de itinerariu, care există deja în sistem. Aceasta are sens dacă doriți sa-l modificați și sa-l salvați ca un nou model in sistem. Dacă doar doriți sa editați ruta existentă și sa salvați schimbările lăsați câmpul numelui gol.Sunteți sigur că doriți să continuați?";
texts['es'].routeSaveNonNameAlert = "Se ha introducido nombre de ruta que ya existe en la base de datos del sistema. Si usted hace modificaciones por la ruta ella se guardará como nueva. Si usted quiere hacer modificaciones de la ruta sin guardarla como nueva, simplemente dejar el campo del nombre de la ruta  vacío.¿Estás seguro que quieres continuar?";
texts['sr'].routeSaveNonNameAlert = "Uneli ste ime za maršrutu koja već postoji u sistemu baze podataka. Ovo ima smisla ako želite da izmenite ovu maršrutu i sačuvate je kao nov šablon u sistemu. Ako samo želite da izmenite postojeću maršrutu i sačuvate izmene onda ostavite polje za ime prazno. Da li ste sigurni da želite da nastavite?";
texts['gr'].routeSaveExisitngNameAlert = "Έχετε ορίσει ένα όνομα διαδρομής που υπάρχει στο σύστημα αλλά έχει τροποποιηθεί από εσάς. Αν δεχτείτε να αποθηκεύσετε μια πορεία, θα αποθηκευτεί ως νέα στο σύστημα. Εάν θέλετε να αλλάξετε την προηγούμενη, αφήστε το πεδίο ονόματος κενό. Είστε βέβαιοι ότι θέλετε να συνεχίσετε?";


texts['bg'].noRouteAlert = "Няма наличен маршрут!";
texts['en'].noRouteAlert = "No route available!";
texts['de'].noRouteAlert = "Keine Route verfugbar!"; 
texts['ro'].noRouteAlert = "Nu este itinerariu disponibil!";
texts['es'].noRouteAlert = "No hay  ruta disponible!";
texts['sr'].noRouteAlert = "Nema dostupne maršrute!"
texts['gr'].noRouteAlert = "Δεν υπάρχει διαθέσιμη διαδρομή!";

texts['bg'].routeSaveSuccessful = "Съхраняването на маршрута приключи успешно!";
texts['en'].routeSaveSuccessful = "The route was successfully saved!";
texts['de'].routeSaveSuccessful = "Die Route wurde erfolgreich gespeichert!"; 
texts['ro'].routeSaveSuccessful = "Salvarea itinerariului a fost cu succes!";
texts['ro'].routeSaveSuccessful = "Almacenamiento de la ruta completado con éxito!";
texts['sr'].routeSaveSuccessful = "Maršruta je uspešno sačuvana!";
texts['gr'].routeSaveSuccessful = "Αποθήκευση της διαδρομής ολοκληρώθηκε με επιτυχία!";


texts['bg'].routeSaveError = "Възникна грешка при опит да бъде съхранен маршрута в базата данни!";
texts['en'].routeSaveError = "Database error ocurred and the route was not saved!";
texts['de'].routeSaveError = "Fehler! Die Route wurde nicht gespeichert!"; 
texts['ro'].routeSaveError = " Eroare in baza de date si ruta nu a fost salvată!"; 
texts['es'].routeSaveError = " Se ha producido un error, la ruta no esta guardada !"; 
texts['sr'].routeSaveError = "Pojavila se greška u bazi podataka i maršruta nije sačuvana!"; 
texts['gr'].routeSaveError = "Παρουσιάστηκε σφάλμα κατά την προσπάθεια αποθήκευσης της διαδρομής στη βάση δεδομένων!";

texts['bg'].routeDeleteSuccessful = "Изтриването на маршрута от системата приключи успешно!";
texts['en'].routeDeleteSuccessful = "The route was successfully deleted!";
texts['de'].routeDeleteSuccessful = "Die Route wurde erfolgreich geloscht!"; 
texts['ro'].routeDeleteSuccessful = "Ștergerea rutei s-a facut cu succes!"; 
texts['es'].routeDeleteSuccessful = "La ruta se ha eliminado correctamente!";
texts['sr'].routeDeleteSuccessful = "Maršruta je uspešno izbrisana!";
texts['gr'].routeDeleteSuccessful = "Η διαγραφή της διαδρομής από το σύστημα ολοκληρώθηκε με επιτυχία!";

texts['bg'].routeDeleteError = "Възникна грешка при опит да бъде изтрит маршрута в базата данни!";
texts['en'].routeDeleteError = "Database error ocurred and the route was not deleted!";
texts['de'].routeDeleteError = "Fehler! Die Route wurde nicht geloscht!"; 
texts['ro'].routeDeleteError = "Eroare in baza de date si ruta nu a fost ștearsă!"; 
texts['es'].routeDeleteError = "¡Se ha producido un error, la ruta no esta eliminada!";
texts['sr'].routeDeleteError = "Pojavila se greška u bazi podataka i maršruta nije izbrisana!";
texts['gr'].routeDeleteError = "Παρουσιάστηκε σφάλμα κατά την προσπάθεια διαγραφής της διαδρομής στη βάση δεδομένων!";

texts['bg'].pattern_info = "Информация за шаблон";
texts['en'].pattern_info = "Pattern information";
texts['de'].pattern_info = "Musterinfo"; 
texts['ro'].pattern_info = "Informații model";
texts['es'].pattern_info = "Informacion del patrón";
texts['sr'].pattern_info = "Informacija o šablonu";
texts['gr'].pattern_info = "Πληροφορίες προτύπου";

texts['bg'].successful_unsuccessful_stops_radius = "успешни/неуспешни престои в радиус";
texts['en'].successful_unsuccessful_stops_radius = "regular/irregular stops inside radius";
texts['de'].successful_unsuccessful_stops_radius = "geplante/ungeplante Aufenthalte im Radius"; 
texts['ro'].successful_unsuccessful_stops_radius = "Opriri regulate/neregulate in interiorul razei/zonei";
texts['es'].successful_unsuccessful_stops_radius = "Paradas regulares / irregulares dentro del radio ";
texts['sr'].successful_unsuccessful_stops_radius = "pravilne/nepravilne pauze u radijusu ";
texts['gr'].successful_unsuccessful_stops_radius = "επιτυχής / ανεπιτυχής διαμονή σε ακτίνα";

texts['bg'].time_successful_unsucessful_stops_driver_pattern = "Времетраене на успешните престои на водач / шаблон";
texts['en'].time_successful_unsucessful_stops_driver_pattern = "Duration of the regular outages for driver / pattern";
texts['de'].time_successful_unsucessful_stops_driver_pattern = "Zeitdauer der geplanten Aufenthalte eines Fahrers"; 
texts['ro'].time_successful_unsucessful_stops_driver_pattern = "Durata timpului de staționare pentru șofer/model";
texts['es'].time_successful_unsucessful_stops_driver_pattern = "Duración de las paradas regulares del conductor / patrón";
texts['sr'].time_successful_unsucessful_stops_driver_pattern = "Trajanje pravilnih pauza vozača / šablon";
texts['gr'].time_successful_unsucessful_stops_driver_pattern = "Διάρκεια επιτυχούς διαμονής οδηγού / προτύπου";

texts['bg'].route_delete_part_1 = "Тази команда ще изтрие маршрута '";
texts['en'].route_delete_part_1 = "This command will delete the route '";
texts['de'].route_delete_part_1 = "Dieser Befehl wird die Route  "; 
texts['ro'].route_delete_part_1 = "Această comandă o să șterge itinerariul "; 
texts['es'].route_delete_part_1 = "Este comando eliminará  la ruta "; 
texts['sr'].route_delete_part_1 = "Ova komanda će izbrisati maršrutu "; 
texts['gr'].route_delete_part_1 = "Αυτή η εντολή θα διαγράψει τη διαδρομή '";

texts['bg'].route_delete_part_2 = "' не само от картата но и от системата. Сигурни ли сте, че искате да продължите?";
texts['en'].route_delete_part_2 = "' not only from the map but also from the system database. Are you sure you want to continue?";
texts['de'].route_delete_part_2 = "' nicht nur auf der Landkarte, sondern auch in den Systemdaten loschen. Sind Sie sicher, dass Sie weitermachen mochten?"; 
texts['ro'].route_delete_part_2 = "' nu numai de la harta ,ci și din baza de date a sistemului. Sunteți sigur că doriți să continuați?";
texts['es'].route_delete_part_2 = "' no sólo desde el mapa sino también desde el sistema.¿Estás seguro que quieres continuar?";
texts['sr'].route_delete_part_2 = "' ne samo sa mape već i iz sistema baze podataka. Da li ste sigurni da želite da nastavite?";
texts['gr'].route_delete_part_2 = "' όχι μόνο στο χάρτη αλλά και στο σύστημα. Είστε βέβαιοι ότι θέλετε να συνεχίσετε?";

texts['bg'].confirm_route_map_plot = "В момента има изчертан маршрут върху картата, който ще бъде изтрит ако добавите избрания. Сигурни ли сте, че искате да завършите операцията?";
texts['en'].confirm_route_map_plot = "Currently there is a route on the map and will be replaced with the one you want to show on the map. Are you sure you want to continue?";
texts['de'].confirm_route_map_plot = "Im Augenblick wird eine andere Route auf der Landkarte angezeigt. Sie wird geloscht, wenn Sie eine neue Route auswahlen. Sind Sie sicher, dass Sie weitermachen mochten?"; 
texts['ro'].confirm_route_map_plot = "Momentan este desenat itinerariu pe harta, care o să fie inlocuit cu cel care este aratat pe hartă. Sunteți sigur ca doriți să terminați operațiunea?"; 
texts['es'].confirm_route_map_plot = "Actualmente hay una ruta en el mapa y se reemplazará con el que desea mostrar en el mapa. ¿Seguro que quiere terminar la operación?";
texts['sr'].confirm_route_map_plot = "Trenutno se na mapi nalazi maršruta i biće zamenjena maršrutom koju želite da prikažete na mapi. Da li ste sigurni da želite da nastavite?";
texts['gr'].confirm_route_map_plot = "Υπάρχει μια αντιστοιχισμένη διαδρομή στο χάρτη που θα διαγραφεί αν προσθέσετε την επιλεγμένη. Είστε βέβαιοι ότι θέλετε να ολοκληρώσετε τη λειτουργία;";

texts['bg'].fillTableWithSelectedRow = "Попълни таблицата с данните от избрания ред";
texts['en'].fillTableWithSelectedRow = "Fill the table using the data from the selected row";
texts['de'].fillTableWithSelectedRow = "Fullen Sie die  Tabelle mit den Daten der ausgewahlten Zelle aus."; 
texts['ro'].fillTableWithSelectedRow = "Completați tabelul cu datele de pe rândul ales."; 
texts['es'].fillTableWithSelectedRow = "Rellene la tabla con los datos de la fila seleccionada.";
texts['sr'].fillTableWithSelectedRow = "Popunite tabelu podacima iz izabranog reda.";
texts['gr'].fillTableWithSelectedRow = "Συμπληρώστε τον πίνακα δεδομένων της επιλεγμένης σειράς";

texts['bg'].markRowAsPrimaryData = "Маркиране на ред служещ за база на входните данни, с които може да бъде попълнена таблицата";
texts['en'].markRowAsPrimaryData = "Mark of a row whose data will be used for filling the rest of the table.";
texts['de'].markRowAsPrimaryData = "Markieren Sie eine Zelle, die als Datengrundlage zum Ausfullen der Tabelle verwendet wird."; 
texts['ro'].markRowAsPrimaryData = "Marcheză randul ale cărui date for fi folosite pentru a completa restul tabelului."; 
texts['es'].markRowAsPrimaryData = "Marcación de una fila cuyos datos se utiliza para llenar el resto de la tabla."; 
texts['sr'].markRowAsPrimaryData = "Podaci iz označenog reda biće upotrebljeni za popunjavanje ostatka tabele."; 
texts['gr'].markRowAsPrimaryData = "Σημείωση μιας σειράς των οποίων τα δεδομένα θα χρησιμοποιηθούν για την πλήρωση του υπόλοιπου πίνακα.";

texts['bg'].markRowForPrintOrExport = "Отметнете редовете, които искате да принтирате и/или експортнете.";
texts['en'].markRowForPrintOrExport = "Check the rows you'd like to print or export.";
texts['de'].markRowForPrintOrExport = "Markieren Sie eine Zelle, die Sie drucken und/oder exportieren mochten."; 
texts['ro'].markRowForPrintOrExport = "Marcați rândurile care doriți să fie printate/exportate.";
texts['es'].markRowForPrintOrExport = "Marcar las filas que desea imprimir o exportar.";
texts['sr'].markRowForPrintOrExport = "Označite redove koje želite da odštampate ili izvezete.";
texts['gr'].markRowForPrintOrExport = "Ελέγξτε τις σειρές που θέλετε να εκτυπώσετε ή να εξαγάγετε.";

texts['bg'].reportHaltedBecauseOfDataChanged = "Тъй като променихте входящите данни за извършена справка то тя беше анулирана!";
texts['en'].reportHaltedBecauseOfDataChanged = "You changed an input data of finished report so the report was cancelled.";
texts['de'].reportHaltedBecauseOfDataChanged = "Sie haben die Eingabedaten des fertigen Berichts geandert, darum wurde die Fertigstellung  des Berichts  abgesagt."; 
texts['ro'].reportHaltedBecauseOfDataChanged = "Ați schimbat o dată de intrare al raportului final, deci raportul a fost anulat.";
texts['es'].reportHaltedBecauseOfDataChanged = "Se han cambiado unos datos de entrada del informe final, por lo que el informe fue cancelado.";
texts['sr'].reportHaltedBecauseOfDataChanged = "Izmenili ste ulazne podatke završenog izveštaja pa je izveštaj zbog toga otkazan.";
texts['gr'].reportHaltedBecauseOfDataChanged = "Έχετε αλλάξει τα δεδομένα εισόδου της ολοκληρωμένης αναφοράς έτσι η αναφορά θα ακυρωθεί.";

texts['bg'].doReportFirst = "Първо трябва да направите справка за това превозно средство!";
texts['en'].doReportFirst = "First you have to do a report for this vehicle!";
texts['de'].doReportFirst = "Sie sollten zuerst einen Bericht uber dieses Fahrzeug erstellen!"; 
texts['ro'].doReportFirst = "Mai întai trebuie să faceți un raport pentru acest vehicul!"; 
texts['es'].doReportFirst = "¡En primer lugar tiene que hacer un informe para este vehículo!";
texts['sr'].doReportFirst = "Prvo morate da uradite izveštaj za ovo vozilo!";
texts['gr'].doReportFirst = "Πρώτα πρέπει να κάνετε αναφορά για αυτό το όχημα!";

texts['bg'].spentFuelReportForRoute = "Справка за изразходеното гориво на превозни средства за маршрут";
texts['en'].spentFuelReportForRoute = "Spent fuel route report";
texts['de'].spentFuelReportForRoute = "Bericht uber den Kraftstoffverbrauch der Fahrzeuge fur eine bestimmte Route"; 
texts['ro'].spentFuelReportForRoute = "Raport privind consumul de carburant al vehiculelor pe rută specifică"; 
texts['es'].spentFuelReportForRoute = "Informe consumo de combustible de los vehículos por una ruta"; 
texts['sr'].spentFuelReportForRoute = "Izveštaj o potrošenom gorivu za maršrutu"; 
texts['gr'].spentFuelReportForRoute = "Αναφορά κατανάλωσης καυσίμου οχημάτων σε μια διαδρομη";

texts['bg'].checkAtleastOneRow = "Минимум един ред е нужно да бъде отметнат!";
texts['en'].checkAtleastOneRow = "At least one row must be checked!";
texts['de'].checkAtleastOneRow = "Mindestens eine Zelle muss markiert werden!"; 
texts['ro'].checkAtleastOneRow = "Cel puțin un rând trebuie să fie marcat!"; 
texts['es'].checkAtleastOneRow = "¡Al menos una fila debe ser marcada!"; 
texts['sr'].checkAtleastOneRow = "Minimum jedan red mora da bude označen!"; 
texts['gr'].checkAtleastOneRow = "Πρέπει να σημειωθεί τουλάχιστον μία σειρά!";

texts['bg'].willBeCheckedOnlyRowsWithFinishedReport = "Ще бъдат отметнати само тези редове, за които е направена справка!";
texts['en'].willBeCheckedOnlyRowsWithFinishedReport = "Only some of the rows will be checked because not all vehicles have finished report!";
texts['de'].willBeCheckedOnlyRowsWithFinishedReport = "Es werden nur die Zellen markiert, fur die bereits ein Bericht erstellt wurde."; 
texts['ro'].willBeCheckedOnlyRowsWithFinishedReport= "Doar unele dintre liniile vor fi verificate: pentru că nu toate vehiculele au raportul."; 
texts['es'].willBeCheckedOnlyRowsWithFinishedReport= "¡Sólo algunas de las filas se comprobarán porque no todos los vehículos han terminado su informe!";
texts['sr'].willBeCheckedOnlyRowsWithFinishedReport= "Biće označeni samo oni redovi za koje je urađen izveštaj!";
texts['gr'].willBeCheckedOnlyRowsWithFinishedReport = "Μόνο οι σειρές στις οποίες γίνεται αναφορά θα ελεγχθούν!";

texts['bg'].noRowSelected_selectRow = "Не сте избрали ред. Натиснете върху някой ред за да го маркирате като избран! Алтернативно може да натиснете радио бутонът намиращ се в последната колона!";
texts['en'].noRowSelected_selectRow = "You have not selected a row. Click the radio button or the row itself to make it selected!";
texts['de'].noRowSelected_selectRow = "Sie haben keine Zelle ausgewahlt! Klicken Sie auf eine Zelle oder auf den Knopf in der letzten Spalte, um sie zu markieren!"; 
texts['ro'].noRowSelected_selectRow = "Nu a-ți selectat un rând. Faceți clic pe butonul radio sau rândul în sine pentru a face o selecție!"; 
texts['es'].noRowSelected_selectRow = "No ha seleccionado una fila. ¡Haga clic en una fila para marcarla como seleccionada!";
texts['sr'].noRowSelected_selectRow = "Niste izabrali red. Kliknite na svaki red da ga označite! Alternativno možete da pritisnete radio dugme koje se nalazi u poslednjoj koloni!";
texts['gr'].noRowSelected_selectRow = "Δεν έχετε επιλέξει μια σειρά. Κάντε κλικ σε μια σειρά για να την επισημάνετε ως την επιλογή σας! Εναλλακτικά, μπορείτε να πατήσετε το κουμπί επιλογής που βρίσκεται στην τελευταία στήλη!";

texts['bg'].driver_road_list = "Пътен лист за маршрут на водача";
texts['en'].driver_road_list = "Driver's route road list";
texts['de'].driver_road_list = "Fahrtenbuch fur eine Route"; 
texts['ro'].driver_road_list = "Foaie parcursa pentru itinerariu șoferului"; 
texts['es'].driver_road_list = "Cuaderno de bitácora de ruta del chofer";
texts['sr'].driver_road_list = "Izveštaj za maršrutu vozača";
texts['gr'].driver_road_list = "Οδικό χαρτί διαδρομής του οδηγού ";

texts['bg'].max_speed_city_runway = "Максимална скорост за градско/извънградско";
texts['en'].max_speed_city_runway = "Max allowed speed at urban / suburban movement";
texts['de'].max_speed_city_runway = "Hochste Geschwindigkeit innerorts/au?erorts"; 
texts['ro'].max_speed_city_runway = "Viteză maximă pentru urban/extraurban"; 
texts['es'].max_speed_city_runway = "Velocidad máxima permitida de tráfico urbano/extraurbano";
texts['sr'].max_speed_city_runway = "Maksimalna dozvoljena brzina za gradski/vangradski saobraćaj";
texts['gr'].max_speed_city_runway = "Μέγιστη επιτρεπόμενη ταχύτητα στην αστική / προαστιακή κίνηση";

texts['bg'].working_hours = "Часове работа";
texts['en'].working_hours = "Working hours";
texts['de'].working_hours = "Arbeitsstunden"; 
texts['ro'].working_hours = "ore de lucru"; 
texts['es'].working_hours = "ore de lucru";
texts['sr'].working_hours = "Sati rada";
texts['gr'].working_hours = "Ώρες εργασίας";

texts['bg'].moving_hours = "Часове движение";
texts['en'].moving_hours = "Movement hours";
texts['de'].moving_hours = "In Bewegung"; 
texts['ro'].moving_hours = "Ore de mișcare"; 
texts['es'].moving_hours = "Tiempo laboral"; 
texts['sr'].moving_hours = "Sati kretanja"; 
texts['gr'].moving_hours = "Ώρες κίνησης";

texts['bg'].idle_hours = "Часове престой";
texts['en'].idle_hours = "Outage hours";
texts['de'].idle_hours = "Aufenthaltsdauer"; 
texts['ro'].idle_hours = "Ore de oprire/staționare"; 
texts['es'].idle_hours = "Horas de parada";
texts['sr'].idle_hours = "Sati pauziranja";
texts['gr'].idle_hours = "Ώρες διαμονής";

texts['bg'].procent_error_city_runway = "Процент нарушение в градско/извънградско";
texts['en'].procent_error_city_runway = "Violation procent at urban/suburban";
texts['de'].procent_error_city_runway = "Verkehrsversto?e innerorts/au?erorts"; 
texts['ro'].procent_error_city_runway = "Procent încalcări în urban / suburban";
texts['es'].procent_error_city_runway = "Porcentaje de Infracciones en tráfico urbano/extraurbano";
texts['sr'].procent_error_city_runway = "Procenat prekoračenja u gradskom / vangradskom ";
texts['gr'].procent_error_city_runway = "Ποσοστό παραβίαση σε αστικές/προαστιακές";

texts['bg'].common_violations_report = "Обобщен доклад за нарушения";
texts['en'].common_violations_report = "Summery violations report";
texts['de'].common_violations_report = "Zusammenfassung der Versto?e"; 
texts['ro'].common_violations_report = "Raport sintezat pentru încălcări"; 
texts['es'].common_violations_report = "Informe resumen de infracciones"; 
texts['sr'].common_violations_report = "Ukupan izveštaj prekoračenja";
texts['gr'].common_violations_report = "Συνοπτική αναφορά για παραβιάσεις";

texts['bg'].group_by_zone_leaving = "Групирай по напускане";
texts['en'].group_by_zone_leaving = "Group by leaving";
texts['de'].group_by_zone_leaving = "Nach Abfahrt gruppieren"; 
texts['ro'].group_by_zone_leaving = "Grupează după plecare/părasire ";
texts['es'].group_by_zone_leaving = "Agrupar por salidas ";
texts['sr'].group_by_zone_leaving = "Grupiši po napuštanju ";
texts['gr'].group_by_zone_leaving = "Ομαδοποίηση κατά την αναχώρηση";

texts['bg'].group_by_zones = "Групирай по зони";
texts['en'].group_by_zones = "Group by zones";
texts['de'].group_by_zones = "Nach Zonen gruppieren"; 
texts['ro'].group_by_zones = "Grupează după zone"; 
texts['es'].group_by_zones = "Agrupar por zonas"; 
texts['sr'].group_by_zones = "Grupiši po zonama"; 
texts['gr'].group_by_zones = "Ομαδοποίηση ανά ζώνη";

texts['bg'].speed_graphic_win_title = "Графичен анализ на скоростта за марштур";
texts['en'].speed_graphic_win_title = "Graphical analisys of the speed";
texts['de'].speed_graphic_win_title = "Graphische Analyse der Geschwindigkeit"; 
texts['ro'].speed_graphic_win_title = "Analiză grafică vitezei pentru itinerariu/traseu"; 
texts['es'].speed_graphic_win_title = "Análisis gráfico de velocidad por ruta";
texts['sr'].speed_graphic_win_title = "Grafička analiza brzine za maršrutu";
texts['gr'].speed_graphic_win_title = "Γραφική ανάλυση ταχύτητας ανά δρομολογίου";

texts['bg'].day_road_list_for_driver = "Дневен пътен лист за водач:";
texts['en'].day_road_list_for_driver = "Day road list for driver:";
texts['de'].day_road_list_for_driver = "Tagesfahrtenbuch:"; 
texts['ro'].day_road_list_for_driver = "Foaie parcursa zilnică pentru șofer:"; 
texts['es'].day_road_list_for_driver = "Informe hoja de ruta diaria para chofer:";
texts['sr'].day_road_list_for_driver = "Dnevni izveštaj za vozača:";
texts['gr'].day_road_list_for_driver = "Ημερήσιο οδικό χαρτί οδηγού:";

texts['bg'].discharges_terrains_places_win_text = "Избор на обекти и терени за разтоварване";
texts['en'].discharges_terrains_places_win_text = "Selection the places and terrains for discharging";
texts['de'].discharges_terrains_places_win_text = "Auswahl der Objekte und Gebiete zum Ausladen"; 
texts['ro'].discharges_terrains_places_win_text = "Alegere locurilor și terenurilor pentru descărcare"; 
texts['es'].discharges_terrains_places_win_text = "Selección de lugares y terrenos para descargar"; 
texts['sr'].discharges_terrains_places_win_text = "Izbor mesta i terena za istovar"; 
texts['gr'].discharges_terrains_places_win_text = "Επιλογή των τόπων και των τεραμάτων για εκφόρτωση";

texts['bg'].remont_price_filter_accept_tooltip = "Натиснете тук за да филтрирате ремонтите по цената им";
texts['en'].remont_price_filter_accept_tooltip = "Click here to filter the vehicle repairs by their price";
texts['de'].remont_price_filter_accept_tooltip = "Hier klicken, um die Reparaturen nach Preise einzuordnen."; 
texts['ro'].remont_price_filter_accept_tooltip = "Apasați aici să filtrați repărțiile după prețul lor ."; 
texts['es'].remont_price_filter_accept_tooltip = "Haga clic aquí para filtrar las reparaciones de vehículos por su precio.";
texts['sr'].remont_price_filter_accept_tooltip = "Kliknite ovde da filtrirate popravke vozila po ceni.";
texts['gr'].remont_price_filter_accept_tooltip = "Πατήστε  εδώ για να φιλτράρετε τις επισκευές του οχήματος ανάλογα με την τιμή τους";

texts['bg'].remont_price_filter_cancel_tooltip = "Натиснсте тук за да премахнете филтъра за цена";
texts['en'].remont_price_filter_cancel_tooltip = "Click here to remove the price filter";
texts['de'].remont_price_filter_cancel_tooltip = "Hier klicken, um den Preisfilter zu entfernen"; 
texts['ro'].remont_price_filter_cancel_tooltip = "Apasați aici ca să eliminați filtrul pentru preț"; 
texts['es'].remont_price_filter_cancel_tooltip = "AHaga clic aquí para eliminar el filtro de precio"; 
texts['sr'].remont_price_filter_cancel_tooltip = "Kliknite ovde da uklonite filter za cenu"; 
texts['gr'].remont_price_filter_cancel_tooltip = "Πατήστε εδώ για να καταργήσετε το φίλτρο κόστους";

texts['bg'].remont_report_for_vehicles = "Справка за ремонтирани автомобили за периода:";
texts['en'].remont_report_for_vehicles = "Report for repaired vehicles for the period:";
texts['de'].remont_report_for_vehicles = "Bericht uber reparierte Fahrzeuge fur eine Periode:"; 
texts['ro'].remont_report_for_vehicles = "Raport pentru vehicule repărate pentru o perioadă:"; 
texts['es'].remont_report_for_vehicles = "Informe para vehículos reparados en el periodo:";
texts['sr'].remont_report_for_vehicles = "Izveštaj za popravljena vozila za period:";
texts['gr'].remont_report_for_vehicles = "Αναφορά για επισκευασμένα αυτοκίνητα για την περίοδο:";

texts['bg'].xml_error = "Поради липса на данни справката не може да бъде завършена!";
texts['en'].xml_error = "Due to not enough data the report can not be finished!";
texts['de'].xml_error = "Wegen Datenmangel kann der Bericht nicht erstellt werden!"; 
texts['ro'].xml_error = "Din cauză lipsei detelor raportul nu poate fi terminat!"; 
texts['es'].xml_error = "¡Debido a la falta de datos no se puede completar el informe!";
texts['sr'].xml_error = "Zbog nedovoljno podataka izveštaj ne može biti završen!";
texts['gr'].xml_error = "Λόγω έλλειψης δεδομένων, η αναφορά  δεν μπορεί να ολοκληρωθεί!";

texts['bg'].spent_fuel_report = "Справка за изразходвано гориво";
texts['en'].spent_fuel_report = "Spent fuel report";
texts['de'].spent_fuel_report = "Bericht uber den Kraftstoffverbrauch"; 
texts['ro'].spent_fuel_report = "Raport pentru combustibil consumat"; 
texts['es'].spent_fuel_report = "Informe consumo de combustible"; 
texts['sr'].spent_fuel_report = "Izveštaj za potrošeno gorivo"; 
texts['gr'].spent_fuel_report = "Αναφορά κατανάλωσης καυσίμου";


texts['bg'].max_speed_report = "Справка за максимални и средни стойности за скоростта на превозни средства";
texts['en'].max_speed_report = "Max and average speed report";
texts['de'].max_speed_report = "Bericht uber die hochste und durchschnittliche Geschwindigkeit der Fahrzeuge"; 
texts['ro'].max_speed_report = "Raport pentru valori maxime și medii ale vitezei vehiculelor"; 
texts['es'].max_speed_report = "Informe de velocidad máxima y media de los vehículos"; 
texts['sr'].max_speed_report = "Izveštaj za maksimalnu i prosečnu brzinu"; 
texts['gr'].max_speed_report = "Αναφορά στις μέγιστες και τις μέσες τιμές στροφών για τα οχήματα";

texts['bg'].driver_day_road_list = "Дневен пътен лист за водач";
texts['en'].driver_day_road_list = "Driver day road list";
texts['de'].driver_day_road_list = "Tagesfahrtenbuch"; 
texts['ro'].driver_day_road_list = "Foaie de parcur zilnică pentru șofer"; 
texts['es'].driver_day_road_list = "Informe hoja de ruta diaria para chofer";
texts['sr'].driver_day_road_list = "Dnevni izveštaj za vozača";
texts['gr'].driver_day_road_list = "Ημερήσιο οδικό χαρτί οδηγού:";

texts['bg'].show_table = "Покажи таблицата";
texts['en'].show_table = "Show table";
texts['de'].show_table = "Tabelle einblenden"; 
texts['ro'].show_table = "Arată tabelul"; 
texts['es'].show_table = "Mostrar tabla";
texts['sr'].show_table = "Prikaži tabelu";
texts['gr'].show_table = "Εμφάνιση του πίνακα";

texts['bg'].distance_fuel_chart = "Графика за разтояние и гориво";
texts['en'].distance_fuel_chart = "Chart representing the distance and fuel";
texts['de'].distance_fuel_chart = "Graphische Darstellung von gefahrenen Strecken und Kraftstoffverbrauch"; 
texts['ro'].distance_fuel_chart = "Grafic reprezentând distanța și combustibilul"; 
texts['es'].distance_fuel_chart = "Gráfico de  distancia y combustible"; 
texts['sr'].distance_fuel_chart = "Grafik za rastojanje i gorivo";
texts['gr'].distance_fuel_chart = "Γραφική απεικόνιση των διαδρομών και κατανάλωσης καυσίμου";

texts['bg'].price_chart = "Графика за цена";
texts['en'].price_chart = "Chart representing the price";
texts['de'].price_chart = "Graphische Darstellung der Preise"; 
texts['ro'].price_chart = "Reprezentare grafică pentru preț"; 
texts['es'].price_chart = "representación gráfica del costo";
texts['sr'].price_chart = "Grafik za cenu";
texts['gr'].price_chart = "Γραφική απεικόνιση των τιμών";

texts['bg'].fuel_chart = "Графика за гориво";
texts['en'].fuel_chart = "Chart representing the fuel";
texts['de'].fuel_chart = "Graphische Darstellung Kraftstoff"; 
texts['ro'].fuel_chart = "Reprezentare grafică pentru combustibil"; 
texts['es'].fuel_chart = "Gráfico de combustible"; 
texts['sr'].fuel_chart = "Grafik za gorivo"; 
texts['gr'].fuel_chart = "Γραφική απεικόνιση των καυσίμων";

texts['bg'].price_filter_explain_error = "Въведете филтър за цена. Ако искате всички ремонти струващи повече от 100 лв. въведете '>100'. Ако искате всички ремонти струващи по-малко от 100 лв. въведете '<100'. Ако искате всички ремонти струващи м/у 100 и 300 лв въведете '100..300' ";
texts['en'].price_filter_explain_error = "Enter a price filter. If you want all the vehicle repairs costing more than a fixed price enter '> fixed_price'. If you want all the vehicle repairs costing less than a fixed price enter '< fixed_price'. If you want all the vehicle repairs costing between two fixed prices enter 'fixed_price_1..fixed_price2' ";
texts['de'].price_filter_explain_error = "Geben Sie einen Preisfilter ein .(zum Beispiel: Wenn Sie alle Reparaturen teurer als 100 Euro sehen mochten, geben Sie '>100' ein. Wenn Sie alle Reparaturen billiger als 100 Euro sehen mochten, geben Sie '<100' ein. Wenn Sie alle Reparaturen zwischen 100 und 300 Euro sehen mochten, geben Sie ein '100..300'."; 
texts['ro'].price_filter_explain_error = "Introduceți filtru pentru preț .Dacă doriți toate reparațiile cu preț mai mult de 100 de euro introduceți'>100'. Dacă doriți toate reparațiile cu preț mai mic de 100 de Euro introduceți '<100' . Dacă doriți toate reparațiile cu preț între 100 Euro și 300 Euro introduceți '100..300'."; 
texts['es'].price_filter_explain_error = "Introducir filtro para precio.Si desea ver todas las reparaciones más de 100 euros, intrudocir  '> 100'. Si desea ver todas las reparaciones  menos de 100 euros, intrudocir  '< 100'. Si desea ver todas las reparaciones entre 100 euros y 300 euros , intrudocir '100..300'."; 
texts['sr'].price_filter_explain_error = "Unesite filter za cenu. Ako želite sve popravke vozila koje koštaju više od fiksne cene unesite '>fiksna_cena'. Ako želite sve popravke koje koštaju manje od fiksne cene unesite '< fiksna_cena'. Ako želite sve popravke koje koštaju između dve fiksne cene unesite 'fiksna_cena_1..fiksna_cena2' "; 
texts['gr'].price_filter_explain_error = "Εισαγάγετε ένα φίλτρο κόστους. Εάν θέλετε όλες τις επισκευές να κοστίζουν περισσότερα από  100, πληκτρολογήστε '> 100'. Αν θέλετε όλες τις επισκευές να κοστίζουν λιγότερο από 100, πληκτρολογήστε '<100'. Αν θέλετε όλες τις επισκευές να κοστίζουν  μεταξύ  100 και 300 να πληκτρολογήσετε '100..300'";

texts['bg'].refuel_report_period = "Отчет на заредено гориво за периода";  
texts['en'].refuel_report_period = "Refuel report between the period";  
texts['de'].refuel_report_period = "Bericht uber die getankten Kraftstoffmengen fur eine Periode"; 
texts['ro'].refuel_report_period = "Raport pentru combustibilul alimentat pentru o perioada"; 
texts['es'].refuel_report_period = "Informe de repostaje por un período"; 
texts['sr'].refuel_report_period = "Izveštaj o sipanju goriva za period"; 
texts['gr'].refuel_report_period = "Αναφορά καυσίμων για την περίοδο";  

texts['bg'].drain_report_period = "Отчет на източено гориво за периода";  
texts['en'].drain_report_period = "Drain report between the period";  
texts['de'].drain_report_period = ""; 
texts['ro'].drain_report_period = "Raport pentru combustibil consumat pentru o perioadă";
texts['es'].drain_report_period = "Informe de sustraccion por un período";
texts['sr'].drain_report_period = "Izveštaj o istakanju goriva za period";
texts['gr'].drain_report_period = "Αναφορά αφαίρεση καυσίμου για περιόδο";  

texts['bg'].correlation_vehicle_fuel_report_chart = "Графика за съотношението на зареденото гориво на превозните средства";
texts['en'].correlation_vehicle_fuel_report_chart = "Graphical correlation of the vehicles' refuels";
texts['de'].correlation_vehicle_fuel_report_chart = "Verhaltnisdarstellung der getankten Kraftstoffsmengen aller Fahrzeuge"; 
texts['ro'].correlation_vehicle_fuel_report_chart = "Corelare grafică a alimentarilor vehiculelor"; 
texts['es'].correlation_vehicle_fuel_report_chart = "Gráfico de correlación de repostaje de los vehículos"; 
texts['sr'].correlation_vehicle_fuel_report_chart = "Grafik za odnos sipanog goriva u vozilima"; 
texts['gr'].correlation_vehicle_fuel_report_chart = "Γραφική ανεφοδιασμού καυσίμου οχήματος";

texts['bg'].distance_fuel_report_chart = "Графичен анализ на движението и изразходваното гориво на автомобилите";
texts['en'].distance_fuel_report_chart = "Graphical display of vehicles movement and spent fuel";
texts['de'].distance_fuel_report_chart = "Verhaltnisdarstellung von gefahrenen Strecken zum Kraftstoffverbrauch"; 
texts['ro'].distance_fuel_report_chart = "Analiză grafică a mișcării și combustibilului consumat de vehicule"; 
texts['es'].distance_fuel_report_chart = "Análisis gráfico de movimiento y consumo de combustible de los vehículos";
texts['sr'].distance_fuel_report_chart = "Grafički prikaz kretanja vozila i potrošenog goriva";
texts['gr'].distance_fuel_report_chart = "Γραφική ανάλυση της κυκλοφορίας και της κατανάλωσης καυσίμων των οχημάτων";

texts['bg'].refuel_spent_fuel_report_chart = "Графично изобразяване на зареденото и изразходвано гориво на превозните средства";
texts['en'].refuel_spent_fuel_report_chart = "Graphical display of the refuel and spent fuel of the vehicles";
texts['de'].refuel_spent_fuel_report_chart = "Verhaltnisdarstellung von getankten und verbrauchten Kraftstoffsmengen"; 
texts['ro'].refuel_spent_fuel_report_chart = "Reprezentare grafică a combustibilului alimentat și consumat de vehicul/e"; 
texts['es'].refuel_spent_fuel_report_chart = "Visualización gráfico de los repostajes y el combustible gastado";
texts['sr'].refuel_spent_fuel_report_chart = "Grafički prikaz sipanog goriva i potrošenog goriva u vozilima ";
texts['gr'].refuel_spent_fuel_report_chart = "Γραφική απεικόνιση του ανεφοδιασμού και αναλωμένου καυσίμου στα οχήματα";

texts['bg'].data_loading = "Моля изчакайте да се зареди графиката...";
texts['en'].data_loading = "Please wait while the report is being calculated...";
texts['de'].data_loading = "Bitte warten. Die graphische Darstellung wird geladen..."; 
texts['ro'].data_loading = "Vă rog așteptați să se genereze raportul grafic..."; 
texts['es'].data_loading = "Por favor, espere el informe se esta calculando ...";
texts['sr'].data_loading = "Molimo sačekajte dok se učita izveštaj...";
texts['gr'].data_loading = "Παρακαλώ περιμένετε να φορτωθεί το γράφημα ...";

texts['bg'].speed_interrelation_chart = "Графичен анализ за съотношенията на скоростта";
texts['en'].speed_interrelation_chart = "Graphical information about the speed correlations";
texts['de'].speed_interrelation_chart = "Graphische Analyse der Geschwindigkeit"; 
texts['ro'].speed_interrelation_chart = "Analiză grafică a vitezei"; 
texts['es'].speed_interrelation_chart = "Análisis gráfico para las correlaciones de la velocidad"; 
texts['sr'].speed_interrelation_chart = "Grafička informacija o odnosu brzine"; 
texts['gr'].speed_interrelation_chart = "Γραφική ανάλυση αναλογιών ταχύτητας";

texts['bg'].city_outside_city_fuel_interrelation = "Графичен анализ на изразходваното гориво в градско/извънградско движение";
texts['en'].city_outside_city_fuel_interrelation = "Graphical analysis of urban / suburban spent fuel correlation";
texts['de'].city_outside_city_fuel_interrelation = "Graphische Analyse des Kraftstoffverbrauchs innerorts/au?erorts"; 
texts['ro'].city_outside_city_fuel_interrelation = "Analiză grafică combustibilului consumat în trafic urban/extraurban";
texts['es'].city_outside_city_fuel_interrelation = "Análisis gráfico de consumo de combustible en el tráfico urbano / extraburbano";
texts['sr'].city_outside_city_fuel_interrelation = "Grafička analiza odnosa potrošenog goriva u gradskom/vangradskom saobraćaju";
texts['gr'].city_outside_city_fuel_interrelation = "Γραφική ανάλυση αναλωμένου καυσίμου στην αστική / προαστιακή  κυκλοφορία";

texts['bg'].noMoreZoomOut = "Графиката е в първоначалния си вид. За да разгледате някой регион от избрания интервал трябва да селектирате мястото с мишката!";
texts['en'].noMoreZoomOut = "The graphical curve can not be zoomed out any more. If you want to explore some region simply select it while holding the left mouse button pressed.";
texts['de'].noMoreZoomOut = "Die graphische Darstellung ist in ihrem ursprunglichen Format. Um ein bestimmtes Detail genauer darzustellen, wahlen Sie es zuerst mit der Maus aus!"; 
texts['ro'].noMoreZoomOut = "Reprezentarea grafică nu mai poate fi micșorată mai mult. Dacă doriți să explorați anumite zone, selectați-le in timp ce țineți butonul stâng apăsat!";
texts['es'].noMoreZoomOut = "La curva gráfica no se puede alejar más. Si desea explorar algunas áreas, seleccione y mientras mantiene pulsado el botón izquierdo!";
texts['sr'].noMoreZoomOut = "Grafička krivulja ne može više da se smanjuje. Ako želite da istražite neki region jednostavno ga izaberite dok držite levi taster miša!";
texts['gr'].noMoreZoomOut = "Ο χάρτης είναι στην αρχική του μορφή. Εάν θέλετε να εξερευνήσετε κάποια περιοχή, απλά επιλέξτε την ενώ κρατάτε πατημένο το αριστερό πλήκτρο του ποντικιού.";

texts['bg'].temperature_report_chart = "Графична справка за температура";
texts['en'].temperature_report_chart = "Graphical information about the vehicle's temperature";
texts['de'].temperature_report_chart = "Graphische Darstellung der Temperatur"; 
texts['ro'].temperature_report_chart = "Raport grafic pentru temperatura vehiculelor"; 
texts['ro'].temperature_report_chart = "Informe gráfico para el temperatura del vehículo"; 
texts['sr'].temperature_report_chart = "Grafička informacija o temperaturi"; 
texts['gr'].temperature_report_chart = "Γραφική αναφορά θερμοκρασίας";

texts['bg'].accumulator_report_chart = "Графична справка за акумулатор";
texts['en'].accumulator_report_chart = "Graphical information about the vehicle's accumulator";
texts['de'].accumulator_report_chart = "Graphische Darstellung der Akkuleistung"; 
texts['ro'].accumulator_report_chart = "Raport grafic pentru acumulatorul vehiculelor";
texts['es'].accumulator_report_chart = "Informe gráfico de acumulador";
texts['sr'].accumulator_report_chart = "Grafička informacija o akumulatoru";
texts['gr'].accumulator_report_chart = "Αναφορά γραφικών συσσωρευτή ";

texts['bg'].now_engine_off = "В момента автомобилът е с изключен двигател!";
texts['en'].now_engine_off = "At the moment vehicle's engine is off!";
texts['de'].now_engine_off = "Im Augenbick ist der Motor aus!"; 
texts['ro'].now_engine_off = "În acest moment vehiculul este cu motor oprit!"; 
texts['es'].now_engine_off = "¡En este momento el motor del vehículo esta apagado!";
texts['sr'].now_engine_off = "Motor vozila je trenutno ugašen!";
texts['gr'].now_engine_off = "Αυτή τη στιγμή ο κινητήρας του οχήματος είναι απενεργοποιημένος!";

texts['bg'].add_new_account = "Добавяне на нов акаунт в системата";
texts['en'].add_new_account = "Account registration";
texts['de'].add_new_account = "Account registration";
texts['ro'].add_new_account = "Adaugați cont nou în sistem";
texts['es'].add_new_account = "Adaugați cont nou în sistem";
texts['sr'].add_new_account = "Registracija naloga";
texts['gr'].add_new_account = "Προσθήκη νέου λογαριασμού στο σύστημα ";

texts['bg'].registered_accounts = "Регистрирани акаунти";
texts['en'].registered_accounts = "Registered accounts";
texts['de'].registered_accounts = "Registered accounts";
texts['ro'].registered_accounts = "Conturi registrate";
texts['es'].registered_accounts = "Registrar nueva cuenta";
texts['sr'].registered_accounts = "Registrovani nalozi";
texts['gr'].registered_accounts = "Εγγεγραμμένοι λογαριασμοί";

texts['bg'].firm_account =  "Фирма притежаваща акаунта";
texts['en'].firm_account =  "The firm associated with the account";
texts['de'].firm_account =  "The firm associated with the account";
texts['ro'].firm_account =  "Firmă care are un cont";
texts['es'].firm_account =  "La firma asociada con la cuenta";
texts['sr'].firm_account =  "Firma povezana sa nalogom";
texts['gr'].firm_account =  "Εταιρεία που κατέχει τον λογαριασμό";

texts['bg'].user_name =  "Потребителско име";
texts['en'].user_name =  "Username";
texts['de'].user_name =  "Username";
texts['ro'].user_name =  "Nume utilizător";
texts['es'].user_name =  "Usuario";
texts['sr'].user_name =  "Korisničko ime";
texts['gr'].user_name =  "Όνομα χρήστη";

texts['bg'].password = "Парола";
texts['en'].password = "Password";
texts['de'].password = "Password";
texts['ro'].password = "Parolă";
texts['es'].password = "Contraseña";
texts['sr'].password = "Lozinka";
texts['gr'].password = "Κωδικός πρόσβασης";

texts['bg'].expire_date = "Платено до";
texts['en'].expire_date = "Expire date";
texts['de'].expire_date = "Expire date";
texts['ro'].expire_date = "Dată expirării";
texts['es'].expire_date = "Fecha de caducidad";
texts['sr'].expire_date = "Datum isteka";
texts['gr'].expire_date = "Ημερομηνία λήξης";

texts['bg'].choose_account_status = "Изберете статус на акаунта";
texts['en'].choose_account_status = "Choose account status";
texts['de'].choose_account_status = "Choose account status";
texts['ro'].choose_account_status = "Alege starea contului";
texts['es'].choose_account_status = "Seleccione estado de la cuenta";
texts['sr'].choose_account_status = "Izaberite status naloga";
texts['gr'].choose_account_status = "Επιλέξτε κατάσταση λογαριασμού";

texts['bg'].active = "активен";
texts['en'].active = "activated";
texts['de'].active = "activated";
texts['ro'].active = "Activat";
texts['es'].active = "activado";
texts['sr'].active = "aktivan";
texts['gr'].active = "ενεργός";

texts['bg'].c_active2 = "Активно";
texts['en'].c_active2 = "Active";
texts['de'].c_active2 = "";
texts['ro'].c_active2 = "Activ";
texts['es'].c_active2 = "Activo";
texts['sr'].c_active2 = "Aktivno";
texts['gr'].c_active2 = "Ενεργός";

texts['bg'].freeze = "замразен";
texts['en'].freeze = "freezed";
texts['de'].freeze = "freezed";
texts['ro'].freeze = "Înghețat";
texts['es'].freeze = "congelado";
texts['sr'].freeze = "zamrznut";
texts['gr'].freeze = "φραγή";

texts['bg'].system_register = "Регистрирай в системата";
texts['en'].system_register = "Register";
texts['de'].system_register = "Register";
texts['ro'].system_register = "Înregistrat în sistem";
texts['es'].system_register = "Registrarse";
texts['sr'].system_register = "Prijavi u sistem";
texts['gr'].system_register = "Εγγραφή στο σύστημα ";

texts['bg'].account_added_successfully_please_select_option = "След като акаунтът беше успешно добавен в системата може да изберете една от следващите опции, с която да продължите напред";
texts['en'].account_added_successfully_please_select_option = "The account was successfully added to the system. Now you can choose one of the following options";
texts['de'].account_added_successfully_please_select_option = "The account was successfully added to the system. Now you can choose one of the following options";
texts['ro'].account_added_successfully_please_select_option = "Acest cont a fost adaugat cu succes in sistem.Acum puteți alege una din următoarele obțiuni";
texts['es'].account_added_successfully_please_select_option = "La cuenta se ha añadido con éxito.Ahora usted puede elegir una de las siguientes opciones";
texts['sr'].account_added_successfully_please_select_option = "Nalog je uspešno dodat u sistem. Sada možete izabrati jednu od sledećih opcija";
texts['gr'].account_added_successfully_please_select_option = "Ο λογαριασμός προστέθηκε με επιτυχία στο σύστημα, μπορείτε να επιλέξετε μία από τις παρακάτω επιλογές για να προχωρήσετε";

texts['bg'].i_want_to_add_new_account = "Искам да добавя нов акаунт в системата.";
texts['en'].i_want_to_add_new_account = "I want to register new account.";
texts['de'].i_want_to_add_new_account = "I want to register new account.";
texts['ro'].i_want_to_add_new_account = "Vreau sa înregistrez un cont nou.";
texts['es'].i_want_to_add_new_account = "Quiero registrar nueva cuenta.";
texts['sr'].i_want_to_add_new_account = "Želim da dodam novi nalog.";
texts['gr'].i_want_to_add_new_account = "Θέλω να προσθέσω ένα νέο λογαριασμό στο σύστημα.";

texts['bg'].i_want_to_change_account_data = "Искам да коригирам данните на акаунта, който току що добавих в системата.";
texts['en'].i_want_to_change_account_data = "I want to modify the data of the account i've just registered.";
texts['de'].i_want_to_change_account_data = "I want to modify the data of the account i've just registered.";
texts['ro'].i_want_to_change_account_data = "Vreau să modific datele din contul pe care tocmai l-am înregistrat.";
texts['es'].i_want_to_change_account_data = "Quiero corregir los datos de la cuenta que acabo de agregar en el sistema.";
texts['sr'].i_want_to_change_account_data = "Želim da izmenim podatke naloga koji sam upravo dodao.";
texts['gr'].i_want_to_change_account_data = "Θέλω να διορθώσω τα στοιχεία του λογαριασμού που μόλις πρόσθεσα στο σύστημα.";

texts['bg'].i_want_to_add_vehicles_to_account = "Искам да добавя автомобили към акаунта, който току що добавих в системата.";
texts['en'].i_want_to_add_vehicles_to_account = "I want to add vehicles to the account i've just registered.";
texts['de'].i_want_to_add_vehicles_to_account = "I want to add vehicles to the account i've just registered.";
texts['ro'].i_want_to_add_vehicles_to_account = "Vreau să adaug vehicule pe contul nou , pe care tocmai l-am înregistrat .";
texts['es'].i_want_to_add_vehicles_to_account = "Quiero añadir vehículos al cuenta que acabo de registrar en el sistema.";
texts['sr'].i_want_to_add_vehicles_to_account = "Želim da dodam vozila nalogu koji sam upravo dodao.";
texts['gr'].i_want_to_add_vehicles_to_account = "Θέλω να προσθέσω οχήματα στο λογαριασμό που μόλις πρόσθεσα στο σύστημα.";

texts['bg'].i_want_to_go_back_to_online_monitoring = "Искам да се върна обратно към онлайн режима.";
texts['en'].i_want_to_go_back_to_online_monitoring = "I want to go back to the online monitoring center.";
texts['de'].i_want_to_go_back_to_online_monitoring = "I want to go back to the online monitoring center.";
texts['ro'].i_want_to_go_back_to_online_monitoring = "Vreau să întorc înapoi pe regim online";
texts['es'].i_want_to_go_back_to_online_monitoring = "Quiero volver al modo online";
texts['sr'].i_want_to_go_back_to_online_monitoring = "Želim da se vratim na onlajn režim.";
texts['gr'].i_want_to_go_back_to_online_monitoring = "Θέλω να επιστρέψω στο ηλεκτρονικό κέντρο παρακολούθησης.";

texts['bg'].view_modify_account = "Преглед и корекция на акаунти";
texts['en'].view_modify_account = "View and modify accounts";
texts['de'].view_modify_account = "View and modify accounts";
texts['ro'].view_modify_account = "Vizualizare și editare de conturi";
texts['es'].view_modify_account = "Ver y modificar cuentas";
texts['sr'].view_modify_account = "Pregledaj i izmeni naloge";
texts['gr'].view_modify_account = "Προβολή και επεξεργασία λογαριασμών";

texts['bg'].account = "Акаунт";
texts['en'].account = "Account";
texts['de'].account = "Account";
texts['ro'].account = "Cont";
texts['es'].account = "Cuenta";
texts['sr'].account = "Nalog";
texts['gr'].account = "Λογαριασμός";

texts['bg'].freeze2 = "Замразяване";
texts['en'].freeze2 = "Freeze";
texts['de'].freeze2 = "Freeze";
texts['ro'].freeze2 = "Înghetat";
texts['es'].freeze2 = "Congelar";
texts['sr'].freeze2 = "Zamrzavanje";
texts['gr'].freeze2 = "Φραγή";

texts['bg'].confirm_account_delete = "Сигурни ли сте, че искате да изтриете този акаунт от системата?";
texts['en'].confirm_account_delete = "Are you sure you want to delete this account from the system?";
texts['de'].confirm_account_delete = "Are you sure you want to delete this account from the system?";
texts['ro'].confirm_account_delete = "Sunteți sigur că doriți să ștergeți acest cont din sistemul?";
texts['es'].confirm_account_delete = "¿Seguro que quieres eliminar esta cuenta del sistema?";
texts['sr'].confirm_account_delete = "Da li ste sigurni da želite da izbrišete ovaj nalog iz sistema?";
texts['gr'].confirm_account_delete = "Είστε βέβαιοι ότι θέλετε να διαγράψετε αυτόν το λογαριασμό από το σύστημα?";

texts['bg'].all_vehicles = "Всички превозни средства"; 
texts['en'].all_vehicles = "All available vehicles"; 
texts['de'].all_vehicles = "All available vehicles"; 
texts['ro'].all_vehicles = "Toate vehiculele disponibile"; 
texts['es'].all_vehicles = "Todos los vehículos disponibles"; 
texts['sr'].all_vehicles = "Sva dostupna vozila"; 
texts['gr'].all_vehicles = "Όλα τα οχήματα"; 

texts['bg'].accounts_administration = "Администриране на акаунт";
texts['en'].accounts_administration = "Account administration";
texts['de'].accounts_administration = "Account administration";
texts['ro'].accounts_administration = "Administrare cont ";
texts['es'].accounts_administration = "Administración de cuentas ";
texts['sr'].accounts_administration = "Administracija naloga ";
texts['sr'].accounts_administration = "Administracija naloga ";
texts['gr'].accounts_administration = "Διαχείριση  λογαριασμού";

texts['bg'].firms_administration = "Администриране на фирми";
texts['en'].firms_administration = "Firms administration";
texts['de'].firms_administration = "Firms administration";
texts['ro'].firms_administration = "Administrare firma";
texts['es'].firms_administration = "administración de empresa";
texts['sr'].firms_administration = "Administracija firme";
texts['gr'].firms_administration = "Διαχείριση  επιχειρήσεων";

texts['bg'].vehicles_administration = "Администриране на превозни средства";
texts['en'].vehicles_administration = "Vehicles administration";
texts['de'].vehicles_administration = "Vehicles administration";
texts['ro'].vehicles_administration = "Administrare vehicule";
texts['es'].vehicles_administration = "Administración de vehículo";
texts['sr'].vehicles_administration = "Administracija vozila";
texts['gr'].vehicles_administration = "Διαχείριση οχημάτων";

texts['bg'].SIM_cards_administration = "Администриране на SIM карти";
texts['en'].SIM_cards_administration = "SIM cards administration";
texts['de'].SIM_cards_administration = "SIM cards administration";
texts['ro'].SIM_cards_administration = "Administrare cartelă SIM";
texts['es'].SIM_cards_administration = "Administración de tarjeta SIM";
texts['sr'].SIM_cards_administration = "Administracija SIM kartica";
texts['gr'].SIM_cards_administration = "Διαχείριση καρτών SIM";

texts['bg'].managers_administration = "Администриране на мениджъри";
texts['en'].managers_administration = "Managers administration";
texts['de'].managers_administration = "Managers administration";
texts['ro'].managers_administration = "Administrare manageri";
texts['es'].managers_administration = "Administración de Gerentes";
texts['sr'].managers_administration = "Administracija menadžera";
texts['gr'].managers_administration = "Διαχείριση διαχειριστών";

texts['bg'].deals_administration = "Администриране на сделки";
texts['en'].deals_administration = "Deals administration";
texts['de'].deals_administration = "Deals administration";
texts['ro'].deals_administration = "Administrare negocieri";
texts['es'].deals_administration = "Administración de ofertas";
texts['sr'].deals_administration = "Administracija ponuda";
texts['gr'].deals_administration = "Διαχείριση συναλλαγών";

texts['bg'].reports_administration = "Администриране на справки";
texts['en'].reports_administration = "Reports administration";
texts['de'].reports_administration = "Reports administration";
texts['ro'].reports_administration = "Administrare rapoarte";
texts['es'].reports_administration = "Administración de informes";
texts['sr'].reports_administration = "Administracija izveštaja";
texts['gr'].reports_administration = "Διαχείριση αναφορών ";

texts['bg'].firm_added_successfully_please_select_option = "След като фирмата беше успешно добавена в системата може да изберете една от следващите опции, с която да продължите напред";
texts['en'].firm_added_successfully_please_select_option = "The firm was successfully added to the system. Now you can choose one of the following options";
texts['de'].firm_added_successfully_please_select_option = "The firm was successfully added to the system. Now you can choose one of the following options";
texts['ro'].firm_added_successfully_please_select_option = "Firma a fost adăugată cu succes în sistem.Acum puteți să alegeți una dintre următoarele opțiuni , cu care să continuați";
texts['es'].firm_added_successfully_please_select_option = "La empresa se ha añadido con éxito.Ahora usted puede elegir una de las siguientes opciones";
texts['sr'].firm_added_successfully_please_select_option = "Firma je uspešno dodata u sistem. Sada možete izabrati jednu od sledećih opcija";
texts['gr'].firm_added_successfully_please_select_option = "Η εταιρεία προστέθηκε με επιτυχία στο σύστημα,μπορείτε να επιλέξετε μία από τις παρακάτω επιλογές για να προχωρήσετε";

texts['bg'].i_want_to_add_new_firm = "Искам да добавя нова фирма в системата.";
texts['en'].i_want_to_add_new_firm = "I want to register new firm.";
texts['de'].i_want_to_add_new_firm = "I want to register new firm.";
texts['ro'].i_want_to_add_new_firm = "Vreau să adaug o firma nouă în sistem.";
texts['es'].i_want_to_add_new_firm = "Quiero registrarme nueva empresa.";
texts['sr'].i_want_to_add_new_firm = "Želim da dodam novu firmu.";
texts['gr'].i_want_to_add_new_firm = "Θέλω να προσθέσω μια νέα εταιρεία στο σύστημα.";


texts['bg'].i_want_to_change_firm_data = "Искам да коригирам данните на фирмата, която току що добавих в системата.";
texts['en'].i_want_to_change_firm_data = "I want to modify the data of the firm i've just registered.";
texts['de'].i_want_to_change_firm_data = "I want to modify the data of the firm i've just registered.";
texts['ro'].i_want_to_change_firm_data = "Vreau să modific datele firmei, pe care tocmai am înregistrat-o în sistem.";
texts['es'].i_want_to_change_firm_data = "Quiero corregir los datos de la empresa que acabo de agregar en el sistema.";
texts['sr'].i_want_to_change_firm_data = "Želim da izmenim podatke firme koju sam upravo dodao.";
texts['gr'].i_want_to_change_firm_data = "Θέλω να διορθώσω τα δεδομένα της εταιρείας που μόλις πρόσθεσα στο σύστημα.";

texts['bg'].i_want_to_add_vehicles_to_firm = "Искам да добавя автомобили към фирмата, която току що добавих в системата.";
texts['en'].i_want_to_add_vehicles_to_firm = "I want to add vehicles to the firm i've just registered.";
texts['de'].i_want_to_add_vehicles_to_firm = "I want to add vehicles to the firm i've just registered.";
texts['ro'].i_want_to_add_vehicles_to_firm = "Vreau să adaug vehicule la firma,pe care tocmai am înregistrat-o în sistemul";
texts['es'].i_want_to_add_vehicles_to_firm = "Quiero añadir vehículo al emresa que acabo de agregar en el sistema";
texts['sr'].i_want_to_add_vehicles_to_firm = "Želim da dodam vozila firmi koju sam upravo dodao.";
texts['gr'].i_want_to_add_vehicles_to_firm = "Θέλω να προσθέσω οχήματα  στην εταιρεία που μόλις πρόσθεσα στο σύστημα.";

texts['bg'].i_want_to_register_accounts_to_firm = "Искам да регистрирам акаунт/и към фирмата, която току що добавих в системата.";
texts['en'].i_want_to_register_accounts_to_firm = "I want to associate new accounts with the firm, i've just created.";
texts['de'].i_want_to_register_accounts_to_firm = "I want to associate new accounts with the firm, i've just created.";
texts['ro'].i_want_to_register_accounts_to_firm = "Vreau să înregistrez conturi noi la firma pe care tocmai am înregistrat-o în sistem.";
texts['es'].i_want_to_register_accounts_to_firm = "Quiero registrar cuenta al empresa que acabo de agregar en el sistema.";
texts['sr'].i_want_to_register_accounts_to_firm = "Želim da povežem nove naloge sa firmom koje sam upravo napravio.";
texts['gr'].i_want_to_register_accounts_to_firm = "Θέλω να καταχωρήσω ένα λογαριασμό στην εταιρεία που μόλις πρόσθεσα στο σύστημα.";


texts['bg'].new_firm = "Добавяне на фирма";
texts['en'].new_firm = "Add new firm";
texts['de'].new_firm = "Add new firm";
texts['ro'].new_firm = "Adăugare firmă nouă";
texts['es'].new_firm = "Añadir nueva empresa";
texts['sr'].new_firm = "Dodaj novu firmu";
texts['gr'].new_firm = "Προσθήκη  εταιρεία";

texts['bg'].view_modify_firm = "Преглед и корекции на фирми";
texts['en'].view_modify_firm = "View and modify firms";
texts['de'].view_modify_firm = "View and modify firms";
texts['ro'].view_modify_firm = "Vizualizare și editare firme";
texts['es'].view_modify_firm = "Ver y modificar empresa";
texts['sr'].view_modify_firm = "Pregledaj i izmeni firme";
texts['gr'].view_modify_firm = "Αναθεώρηση και διόρθωση των επιχειρήσεων";

texts['bg'].month_payment = "Месечни разплащания";
texts['en'].month_payment = "Month payment";
texts['de'].month_payment = "Month payment";
texts['ro'].month_payment = "Plăți lunare";
texts['es'].month_payment = "Pagos mensuales";
texts['sr'].month_payment = "Mesečno plaćanje";
texts['gr'].month_payment = "Μηνιαίες πληρωμές";

texts['bg'].contact_person = "Лице за контакти";
texts['en'].contact_person = "Contact person";
texts['de'].contact_person = "Contact person";
texts['ro'].contact_person = "Persoană de contact";
texts['es'].contact_person = "Persona de contactos";
texts['sr'].contact_person = "Kontakt osoba";
texts['gr'].contact_person = "Υπεύθυνος επικοινωνίας";

texts['bg'].gsm = "GSM";
texts['en'].gsm = "GSM";
texts['de'].gsm = "GSM";
texts['ro'].gsm = "Mobil";
texts['es'].gsm = "Movil";
texts['sr'].gsm = "GSM";
texts['gr'].gsm = "Κινητό";

texts['bg'].bulstat = "Булстат";
texts['en'].bulstat = "Bulstat";
texts['de'].bulstat = "Bulstat";
texts['ro'].bulstat = "Bulstat";
texts['es'].bulstat = "CIF";
texts['sr'].bulstat = "Bulstat";
texts['gr'].bulstat = "ΑΦΜ"

texts['bg'].activity = "Дейност";
texts['en'].activity = "Activity";
texts['de'].activity = "Activity";
texts['ro'].activity = "Activitate";
texts['es'].activity = "Actividad";
texts['sr'].activity = "Aktivnost";
texts['gr'].activity = "Δραστηριότητα";

texts['bg'].payment = "Такса";
texts['en'].payment = "Payment";
texts['de'].payment = "Payment";
texts['ro'].payment = "Taxă";
texts['es'].payment = "Pago";
texts['sr'].payment = "Plaćanje";
texts['gr'].payment = "Πληρωμή";

texts['bg'].manager = "Мениджър";
texts['en'].manager = "Manager";
texts['de'].manager = "Manager";
texts['ro'].manager = "Manager";
texts['es'].manager = "Gerente";
texts['sr'].manager = "Menadžer";
texts['gr'].manager = "Διευθυντής";

texts['bg'].reportName = "Име на справката";
texts['en'].reportName = "Report name";
texts['de'].reportName = "Report name";
texts['ro'].reportName = "Denumire raportului ";
texts['es'].reportName = "Nombre del informe ";
texts['sr'].reportName = "Ime izveštaja ";
texts['gr'].reportName = "Όνομα αναφοράς ";

texts['bg'].category = "Категория";
texts['en'].category = "Category";
texts['de'].category = "Category";
texts['ro'].category = "Categorie";
texts['es'].category = "Categoría";
texts['sr'].category = "Kategorija";
texts['gr'].category = "Κατηγορία";

texts['bg'].simcard_registration = "Регистриране на SIM карти";
texts['en'].simcard_registration = "Add new SIM card";
texts['de'].simcard_registration = "Add new SIM card";
texts['ro'].simcard_registration = "Registare cartele SIM";
texts['es'].simcard_registration = "Registrar nueva tarjeta SIM";
texts['sr'].simcard_registration = "Dodaj novu SIM karticu";
texts['gr'].simcard_registration = "Καταχώρηση καρτών SIM";


texts['bg'].view_modify_simcard = "Преглед и корекции на SIM карти";
texts['en'].view_modify_simcard = "View and modify SIM cards";
texts['de'].view_modify_simcard = "View and modify SIM cards";
texts['ro'].view_modify_simcard = "Vizualizare și editare cartele SIM";
texts['es'].view_modify_simcard = "Ver y modificar tarjeta SIM";
texts['sr'].view_modify_simcard = "Pregledaj i izmeni SIM kartice";
texts['gr'].view_modify_simcard = "Προβολή και διορθώσεις των καρτών SIM";

texts['bg'].manager_registration = "Регистриране на мениджър за продажби";
texts['en'].manager_registration = "Add new manager";
texts['de'].manager_registration = "Add new manager";
texts['ro'].manager_registration = "Registrare manager vânzării";
texts['es'].manager_registration = "Añadir nuevo gerente de ventas";
texts['sr'].manager_registration = "Dodaj novog menadžera";
texts['gr'].manager_registration = "Προσθήκη νέου διαχειριστή";

texts['bg'].view_modify_manager = "Преглед и корекции на мениджъри";
texts['en'].view_modify_manager = "View and modify managers";
texts['de'].view_modify_manager = "View and modify managers";
texts['ro'].view_modify_manager = "Vizualizare și editare manageri";
texts['es'].view_modify_manager = "Ver y modificar gerente de ventas";
texts['sr'].view_modify_manager = "Pregledaj i izmeni menadžere";
texts['gr'].view_modify_manager = "Προβάλετε και διορθώστε τους διαχειριστές";

texts['bg'].enter_manager_names = "Въведете име и фамилия на мениджъра:";
texts['en'].enter_manager_names = "Enter manager's first and last name:";
texts['de'].enter_manager_names = "Enter manager's first and last name:";
texts['ro'].enter_manager_names = "Introduceți nume și prenumele managerului:";
texts['es'].enter_manager_names = "Introducir nombres del gerente:";
texts['sr'].enter_manager_names = "Unesite ime i prezime menadžera:";
texts['gr'].enter_manager_names = "Καταχωρίστε το όνομα και το επώνυμο του διαχειριστή:";

texts['bg'].enter_gsm_number = "Въведете GSM номер:";
texts['en'].enter_gsm_number = "Enter GSM number:";
texts['de'].enter_gsm_number = "Enter GSM number:";
texts['ro'].enter_gsm_number = "Introduceți număr de mobil GSM";
texts['es'].enter_gsm_number = "Introducir un número móvil";
texts['sr'].enter_gsm_number = "Unesite GSM broj";
texts['gr'].enter_gsm_number = "Καταχωρήστε αριθμού κινητού:";

texts['bg'].enter_manager_firm = "Въведете фирма, към която принадлежи мениджъра";
texts['en'].enter_manager_firm = "Enter the manager's firm:";
texts['de'].enter_manager_firm = "Enter the manager's firm:";
texts['ro'].enter_manager_firm = "Introduceți mnagerii firmei";
texts['es'].enter_manager_firm = "Introducir empresa que pertenece del gerente";
texts['sr'].enter_manager_firm = "Unesite firmu menadžera";
texts['gr'].enter_manager_firm = "Καταχωρίστε  επιχείρηση στην οποία ανήκει ο διαχειριστής";

texts['bg'].system_enrol =  "Запиши в системата";
texts['en'].system_enrol =  "Enrol in the system";
texts['de'].system_enrol =  "Enrol in the system";
texts['ro'].system_enrol =  "Înregistrează în sistem";
texts['es'].system_enrol =  "Registrar en el sistema";
texts['sr'].system_enrol =  "Upiši u sistem";
texts['gr'].system_enrol =  "Εγγραφείτε στο σύστημα";

texts['bg'].manager_name = "Име на мениджър";
texts['en'].manager_name = "Manager name";
texts['de'].manager_name = "Manager name";
texts['ro'].manager_name = "Nume manager";
texts['es'].manager_name = "Nombre del gerente";
texts['sr'].manager_name = "Ime menadžera";
texts['gr'].manager_name = "Όνομα διαχειριστή";

texts['bg'].manager_phone_number = "Телефон за връзка";
texts['en'].manager_phone_number = "Phone number";
texts['de'].manager_phone_number = "Phone number";
texts['ro'].manager_phone_number = "Număr de contact";
texts['es'].manager_phone_number = "Número de contacto";
texts['sr'].manager_phone_number = "Broj telefona";
texts['gr'].manager_phone_number = "Τηλέφωνο επικοινωνίας";

texts['bg'].technical_data = "Технически данни";
texts['en'].technical_data = "Technical data";
texts['de'].technical_data = "Technical data";
texts['ro'].technical_data = "Date tehnice";
texts['es'].technical_data = "Datos técnicos";
texts['sr'].technical_data = "Tehnički podaci";
texts['gr'].technical_data = "Τεχνικά δεδομένα";

texts['bg'].devices_description = "Характеристика на устройства";
texts['en'].devices_description = "Devices performance";
texts['de'].devices_description = "Devices performance";
texts['ro'].devices_description = "Caracteristicile dispozitivelor";
texts['es'].devices_description = "Características de los dispositivos";
texts['sr'].devices_description = "Karakteristike uređaja";
texts['gr'].devices_description = "Χαρακτηριστικά συσκευών ";

texts['bg'].add_vehicles_firm = "Добавяне на автомобили към фирма";
texts['en'].add_vehicles_firm = "Add vehicles to firm";
texts['de'].add_vehicles_firm = "Add vehicles to firm";
texts['ro'].add_vehicles_firm = "Adăugare de vehicule pe firma";
texts['es'].add_vehicles_firm = "Añadir vehículos al empresa";
texts['sr'].add_vehicles_firm = "Dodajte vozila firmi";
texts['gr'].add_vehicles_firm = "Προσθήκη όχημα σε επιχειρηση";

texts['bg'].add_vehicles_account = "Добавяне на автомобили към акаунт";
texts['en'].add_vehicles_account = "Add vehicles to account";
texts['de'].add_vehicles_account = "Add vehicles to account";
texts['ro'].add_vehicles_account = "Adăugare vehicule la cont";
texts['es'].add_vehicles_account = "Añadir vehículos al cuenta";
texts['sr'].add_vehicles_account = "Dodajte vozila nalogu";
texts['gr'].add_vehicles_account = "Προσθήκη όχημα στο λογαριασμό ";

texts['bg'].remove_vehicles_firm = "Изтриване на автомобили от фирма";
texts['en'].remove_vehicles_firm = "Delete vehicles from firm";
texts['de'].remove_vehicles_firm = "Delete vehicles from firm";
texts['ro'].remove_vehicles_firm = "Șterge firma/vehiculele firmei";
texts['es'].remove_vehicles_firm = "Eliminar vehículos de la empresa";
texts['sr'].remove_vehicles_firm = "Izbrišite vozila iz firme";
texts['gr'].remove_vehicles_firm = "Διαγραφή οχημάτων από επιχειρηση";

texts['bg'].remove_vehicles_account = "Изтриване на автомобили от акаунт";
texts['en'].remove_vehicles_account = "Delete vehicles from account";
texts['de'].remove_vehicles_account = "Delete vehicles from account";
texts['ro'].remove_vehicles_account = "Șterge vehicule din cont";
texts['es'].remove_vehicles_account = "Eliminar vehículos de la cuenta";
texts['sr'].remove_vehicles_account = "Izbrišite vozila iz naloga";
texts['gr'].remove_vehicles_account = "Διαγραφή οχημάτων από λογαριασμό";

texts['bg'].registered_vehicles = "Регистрирани автомобили";
texts['en'].registered_vehicles = "Registered vehicles";
texts['de'].registered_vehicles = "Registered vehicles";
texts['ro'].registered_vehicles = "Vehicule înregistrate";
texts['es'].registered_vehicles = "Vehículos registrados";
texts['sr'].registered_vehicles = "Registrovana vozila";
texts['gr'].registered_vehicles = "Εγγεγραμμένων οχημάτων";

texts['bg'].account_association_vehicle = "Акаунт асоцииран с автомобилът";
texts['en'].account_association_vehicle = "Selected account";
texts['de'].account_association_vehicle = "Selected account";
texts['ro'].account_association_vehicle = "Contul asociat cu vehiculul";
texts['es'].account_association_vehicle = "Cuenta asociada con  coche";
texts['sr'].account_association_vehicle = "Izabrani nalog";
texts['gr'].account_association_vehicle = "Λογαριασμός που σχετίζεται με το αυτοκίνητο";

texts['bg'].free_vehicles_firm = "Свободни автомобили във фирмата";
texts['en'].free_vehicles_firm = "Unused vehicles in the firm";
texts['de'].free_vehicles_firm = "Unused vehicles in the firm";
texts['ro'].free_vehicles_firm = "Vehicule libere în firmă";
texts['es'].free_vehicles_firm = "Vehículos no utilizados en la empresa";
texts['sr'].free_vehicles_firm = "Slobodna vozila u firmi";
texts['gr'].free_vehicles_firm = "Ελευθερα  οχήματα στην εταιρεία ";

texts['bg'].make_association = "Асоцииране";
texts['en'].make_association = "Association";
texts['de'].make_association = "Association";
texts['ro'].make_association = "Asociere";
texts['es'].make_association = "Asociación";
texts['sr'].make_association = "Udruživanje";
texts['gr'].make_association = "Ένωση";

texts['bg'].confirm_vehicle_to_account_association = "Сигурни ли сте, че искате да добавите превозното средство към избраният акаунт?";
texts['en'].confirm_vehicle_to_account_association = "Are you sure you want to associate vehicle @ with the selected account?";
texts['de'].confirm_vehicle_to_account_association = "Are you sure you want to associate vehicle @ with the selected account?";
texts['ro'].confirm_vehicle_to_account_association = "Sunteți sigur că doriți să adăugați vehiculul ... pe contul ales?";
texts['es'].confirm_vehicle_to_account_association = "¿Seguro que desee asociar vehículo @ al cuenta seleccionada?";
texts['sr'].confirm_vehicle_to_account_association = "Da li ste sigurni da želite da povežete vozilo sa izabranim nalogom?";
texts['gr'].confirm_vehicle_to_account_association = "Είστε βέβαιοι ότι θέλετε να προσθέσετε το όχημα στον επιλεγμένο λογαριασμό?";

texts['bg'].firm_association_vehicle = "Фирма асоциирана с автомобила";
texts['en'].firm_association_vehicle = "Selected firm";
texts['de'].firm_association_vehicle = "Selected firm";
texts['ro'].firm_association_vehicle = "Firma asociată cu vehiculul";
texts['es'].firm_association_vehicle = "Empresa asociada con vehículo";
texts['sr'].firm_association_vehicle = "Izabrana firma";
texts['gr'].firm_association_vehicle = "Εταιρεία που συνδέεται με το όχημα";

texts['bg'].unused_vehicles = "Свободни автомобили";
texts['en'].unused_vehicles = "Unused vehicles";
texts['de'].unused_vehicles = "Unused vehicles";
texts['ro'].unused_vehicles = "Vehicule libere";
texts['es'].unused_vehicles = "Vehículos no utilizados";
texts['sr'].unused_vehicles = "Slobodna vozila";
texts['gr'].unused_vehicles = "Ελευθερα όχημα";

texts['bg'].confirm_vehicle_to_firm_association = "Сигурни ли сте, че искате да добавите превозно средство @ към избраната фирма?";
texts['en'].confirm_vehicle_to_firm_association = "Are you sure you want to associate vehicle @ with the selected firm?";
texts['de'].confirm_vehicle_to_firm_association = "Are you sure you want to associate vehicle @ with the selected firm?";
texts['ro'].confirm_vehicle_to_firm_association = "Sunteți sigur că doriți să adăugați vehiculul @ pe firma alesa?";
texts['es'].confirm_vehicle_to_firm_association = "¿Seguro que desee asociar vehículo @ al empresa seleccionada?";
texts['sr'].confirm_vehicle_to_firm_association = "Da li ste sigurni da želite da povežete vozilo sa izabranom firmom?";
texts['gr'].confirm_vehicle_to_firm_association = "Είστε βέβαιοι ότι θέλετε να προσθέσετε ένα όχημα @ στην επιλεγμένη επιχείρηση?";

texts['bg'].confirm_vehicle_account_delete = "Сигурни ли сте, че искате да премахнете превозно средство @ от избрания акаунт?";
texts['en'].confirm_vehicle_account_delete = "Are you sure you want to remove vehicle @ from the selected account?";
texts['de'].confirm_vehicle_account_delete = "Are you sure you want to remove vehicle @ from the selected account?";
texts['ro'].confirm_vehicle_account_delete = "Sunteți sigur că doriți să ștergeți vehiculul @ din contul selectat ?";
texts['es'].confirm_vehicle_account_delete = "¿Seguro que desee eliminar vehículo @ de la cuenta seleccionada?";
texts['sr'].confirm_vehicle_account_delete = "Da li ste sigurni da želite da uklonite vozilo iz izabranog naloga?";
texts['gr'].confirm_vehicle_account_delete = "Είστε βέβαιοι ότι θέλετε να καταργήσετε ένα όχημα @ από τον επιλεγμένο λογαριασμό?";

texts['bg'].confirm_vehicle_firm_delete = "Сигурни ли сте, че искате да премахнете превозно средство @ от избраната фирма?";
texts['en'].confirm_vehicle_firm_delete = "Are you sure you want to remove vehicle @ from the selected firm?";
texts['de'].confirm_vehicle_firm_delete = "Are you sure you want to remove vehicle @ from the selected firm?";
texts['ro'].confirm_vehicle_firm_delete = "Sunteți sigur că doriți să ștergeți vehiculul @ din firma aleasă?";
texts['es'].confirm_vehicle_firm_delete = "¿Seguro que desee eliminar vehículo @ de la empresa seleccionada?";
texts['sr'].confirm_vehicle_firm_delete = "Da li ste sigurni da želite da uklonite vozilo iz izabrane firme?";
texts['gr'].confirm_vehicle_firm_delete = "Είστε βέβαιοι ότι θέλετε να καταργήσετε ένα όχημα @ από την επιλεγμένη επιχείρηση?";

texts['bg'].fuel_meter = "Разходомер";
texts['en'].fuel_meter = "Fuel meter";
texts['de'].fuel_meter = "Fuel meter";
texts['ro'].fuel_meter = "Debimetru";
texts['es'].fuel_meter = "Medidor de flujo";
texts['sr'].fuel_meter = "Merač goriva";
texts['gr'].fuel_meter = "Μετρητής καυσίμου";

texts['bg'].fuel_meter2 = "Рзх-мер";
texts['en'].fuel_meter2 = "Fuel mtr";
texts['de'].fuel_meter2 = "Fuel mtr";
texts['ro'].fuel_meter2 = "Fuel mtr";
texts['es'].fuel_meter2 = "Medidor flj.";
texts['sr'].fuel_meter2 = "Mer goriva.";
texts['gr'].fuel_meter2 = "Μετρητής καυσίμου";

texts['bg'].ltr_city_highway = "Лтр. в гр./извн.гр.";
texts['en'].ltr_city_highway = "City/highway ltr.";
texts['de'].ltr_city_highway = "City/highway ltr.";
texts['ro'].ltr_city_highway = "Litri Urban/Exraurban.";
texts['es'].ltr_city_highway = "Urbano/suburbano(ltr).";
texts['sr'].ltr_city_highway = "Grad/autoput (ltr).";
texts['gr'].ltr_city_highway = "Αστικό/προαστιακό λ. .";

texts['bg'].ltr_h = "Л/час";
texts['en'].ltr_h = "Ltr/h.";
texts['de'].ltr_h = "Ltr/h.";
texts['ro'].ltr_h = "Litri/oră.";
texts['es'].ltr_h = "Ltr./h.";
texts['sr'].ltr_h = "Ltr./h.";
texts['gr'].ltr_h = "Λτρ /ώρα";

texts['bg'].fuel_tank = "Р-ар"; //Съкратено от РЕЗЕРВОАР
texts['en'].fuel_tank = "Tank"; //Съкратено от РЕЗЕРВОАР
texts['de'].fuel_tank = "Tank"; //Съкратено от РЕЗЕРВОАР
texts['ro'].fuel_tank = "Rezervor"; //Съкратено от РЕЗЕРВОАР
texts['es'].fuel_tank = "Tanque"; //Съкратено от РЕЗЕРВОАР
texts['sr'].fuel_tank = "Rezervoar"; //Съкратено от РЕЗЕРВОАР
texts['gr'].fuel_tank = "Δεξαμενή";

texts['bg'].fuel_tank2 = "Резервоар";
texts['en'].fuel_tank2 = "Tank";
texts['de'].fuel_tank2 = "Tank";
texts['ro'].fuel_tank2 = "Rezervor";
texts['es'].fuel_tank2 = "Tanque";
texts['sr'].fuel_tank2 = "Rezervoar";
texts['gr'].fuel_tank2 = "Δεξαμενή";

texts['bg'].sonda_support = "Поддръжка на сонда";
texts['en'].sonda_support = "Fuel support";
texts['de'].sonda_support = "Fuel support";
texts['ro'].sonda_support = "Sondă suport";
texts['es'].sonda_support = "Ajustes de sonda";
texts['sr'].sonda_support = "Podrška goriva";
texts['gr'].sonda_support = "Υποστήριξη καυσίμου";

texts['bg'].acml = "Акмл."; //Съкратено от АКУМУЛАТОР
texts['en'].acml = "Acml.->"; //Съкратено от АКУМУЛАТОР
texts['de'].acml = "Acml.->"; //Съкратено от АКУМУЛАТОР
texts['ro'].acml = "Baterie.->"; //Съкратено от АКУМУЛАТОР ******
texts['es'].acml = "Acml.->"; //Съкратено от АКУМУЛАТОР ******
texts['sr'].acml = "Akml.->"; //Съкратено от АКУМУЛАТОР ******
texts['gr'].acml = "Acml.";

texts['bg'].bypass = "Байпас";
texts['en'].bypass = "Bypass";
texts['de'].bypass = "Bypass";
texts['ro'].bypass = "Bypass";
texts['es'].bypass = "Derivación";
texts['sr'].bypass = "Bajpas";
texts['gr'].bypass = "Παράκαμψη";

texts['bg'].event1 = "Събитие1";
texts['en'].event1 = "Event1";
texts['de'].event1 = "Event1";
texts['ro'].event1 = "Eveniment1";
texts['es'].event1 = "Evento1";
texts['sr'].event1 = "Događaj1";
texts['gr'].event1 = "Συμβάν1";

texts['bg'].event2 = "Събитие2";
texts['en'].event2 = "Event2";
texts['de'].event2 = "Event2";
texts['ro'].event2 = "Eveniment2";
texts['es'].event2 = "Evento2";
texts['sr'].event2 = "Događaj2";
texts['gr'].event2 = "Συμβάν2";

texts['bg'].sim_card = "SIM Карта";
texts['en'].sim_card = "SIM Card";
texts['de'].sim_card = "SIM Card";
texts['ro'].sim_card = "Cartelă SIM ";
texts['es'].sim_card = "Tarjeta SIM ";
texts['sr'].sim_card = "SIM kartica";
texts['gr'].sim_card = "Κάρτα SIM";

texts['bg'].c_device = "Устройство";
texts['en'].c_device = "Device";
texts['de'].c_device = "Device";
texts['ro'].c_device = "Dispozitiv";
texts['es'].c_device = "Dispositivo";
texts['sr'].c_device = "Uređaj";
texts['gr'].c_device = "Συσκευή";

texts['bg'].version = "Версия";
texts['en'].version = "Version";
texts['de'].version = "Version";
texts['ro'].version = "Versiune";
texts['es'].version = "Versión";
texts['sr'].version = "Verzija";
texts['gr'].version = "Έκδοση";

texts['bg'].spent_fuel_travelled_distance_period = "Разход на гориво и изминати километри за период";
texts['en'].spent_fuel_travelled_distance_period = "Spent fuel and travelled distance";
texts['de'].spent_fuel_travelled_distance_period = "Spent fuel and travelled distance";
texts['ro'].spent_fuel_travelled_distance_period = "Consum combustibil și kilometrii parcurși ";
texts['es'].spent_fuel_travelled_distance_period = "Consumo de combustible y distancia recorrida ";
texts['sr'].spent_fuel_travelled_distance_period = "Potrošeno gorivo i pređena kilometraža ";
texts['gr'].spent_fuel_travelled_distance_period = "Κατανάλωση καυσίμου και χιλιομετρική ανά περίοδο";


texts['bg'].add_remove_reports = "Добавяне/премахване на справки към/от акаунт";
texts['en'].add_remove_reports = "Adding/removing reports";
texts['de'].add_remove_reports = "Adding/removing reports";
texts['ro'].add_remove_reports = "Adăugare/ștergere rapoarte pe/de cont";
texts['es'].add_remove_reports = "Añadir/eliminar informes a/de cuenta";
texts['sr'].add_remove_reports = "Dodavanje/uklanjanje izveštaja";
texts['gr'].add_remove_reports = "Προσθήκη / κατάργηση αναφορών σε / από έναν λογαριασμό";

texts['bg'].checks_administration = "Администриране на касови бележки"; 
texts['en'].checks_administration = "Checks administration"; 
texts['de'].checks_administration = "Bearbeitung der Kassenzettel"; 
texts['ro'].checks_administration = "Administrare de bonuri fiscale";
texts['es'].checks_administration = "Administración de recibos";
texts['sr'].checks_administration = "Administracija priznanica";
texts['gr'].checks_administration = "Διαχείριση αποδείξεων"; 

texts['bg'].vehicles_remont_report = "Справка за ремонт на автомобили";
texts['en'].vehicles_remont_report = "Vehicle repairs report";
texts['de'].vehicles_remont_report = "";
texts['ro'].vehicles_remont_report = "Raport reparare vehicul";
texts['es'].vehicles_remont_report = "Informe reparacion de vehículo";
texts['sr'].vehicles_remont_report = "Izveštaj o popravkama vozila";
texts['gr'].vehicles_remont_report = "Αναφορά για επισκευές οχημάτων ";

texts['bg'].month_report_spent_fuel = "Месечен отчет на заредено гориво";
texts['en'].month_report_spent_fuel = "Montly report for spent fuel";
texts['de'].month_report_spent_fuel = "Montly report for spent fuel";
texts['ro'].month_report_spent_fuel = "Raport lunar pentru combustibulul consumat";
texts['es'].month_report_spent_fuel = "Informe mensual de repostajes";
texts['sr'].month_report_spent_fuel = "Mesečni izveštaj o potrošenom gorivu";
texts['gr'].month_report_spent_fuel = "Μηνιαία αναφορά για αναλωμένα καύσιμα";

texts['bg'].nipoelectronics = "Нипо Електроникс";
texts['en'].nipoelectronics = "Nipo Electronics";
texts['de'].nipoelectronics = "Nipo Elektroniks";
texts['ro'].nipoelectronics = "Nipo Elektronics";
texts['es'].nipoelectronics = "Nipo Elektronics";
texts['sr'].nipoelectronics = "Nipo Elektronics";
texts['gr'].nipoelectronics = "Nipo Electronics";

texts['bg'].nipoelectronics_description = "Нипо Електроникс - Web базирана система за наблюдение в реално време и контрол върху разхода на гориво";
texts['en'].nipoelectronics_description = "Nipo Electronics - Web based system for managing vehicles fleet and controling fuel consumption.";
texts['de'].nipoelectronics_description = "Nipo Electronics - Web based system for managing vehicles fleet and controling fuel consumption.";
texts['ro'].nipoelectronics_description = "Nipo Electronics - Sistem Web pentru monitorizare în timp real și control de consumului de combustibil.";
texts['es'].nipoelectronics_description = "Nipo Electronics - Sistema Web para el monitoreo en tiempo real y control de combustible.";
texts['sr'].nipoelectronics_description = "Nipo Electronics - Web bazirani sistem za upravljanje voznim parkom i kontrolu potrošnje goriva.";
texts['gr'].nipoelectronics_description = "Nipo Electronics - Σύστημα παρακολούθησης σε πραγματικό χρόνο και σύστημα ελέγχου κατανάλωσης καυσίμου στο Web";

texts['bg'].last_session = "Последна сесия";
texts['en'].last_session = "Active sessions";
texts['de'].last_session = "Active sessions";
texts['ro'].last_session = "Ultima sesiune";
texts['es'].last_session = "Última sesión";
texts['sr'].last_session = "Poslednja sesija";
texts['gr'].last_session = "Τελευταία συνεδρία";

texts['bg'].reportsUsage = "Използване на справки";
texts['en'].reportsUsage = "Reports usage";
texts['de'].reportsUsage = "Reports usage";
texts['ro'].reportsUsage = "Folosirea rapoartelor";
texts['es'].reportsUsage = "Uso de informes";
texts['sr'].reportsUsage = "Upotreba izveštaja";
texts['gr'].reportsUsage = "Χρήση αναφορών";

texts['bg'].sessionHistory = "История на сесии";
texts['en'].sessionHistory = "Session history";
texts['de'].sessionHistory = "Session history";
texts['ro'].sessionHistory = "Istoria sesiunii";
texts['es'].sessionHistory = "Historia de sesión";
texts['sr'].sessionHistory = "Istorija sesija";
texts['gr'].sessionHistory = "Ιστορία συνεδρίες ";

texts['bg'].table = "Таблица";
texts['en'].table = "Show table";
texts['de'].table = "Show table";
texts['ro'].table = "Arată tabel";
texts['es'].table = "Tabla";
texts['sr'].table = "Tabela";
texts['gr'].table = "Πίνακας";

texts['bg'].sessionActiveTime = "Графика за продължителност";
texts['en'].sessionActiveTime = "Sessions active time chart";
texts['de'].sessionActiveTime = "Sessions active time chart";
texts['ro'].sessionActiveTime = "Grafic de durată sesiune";
texts['es'].sessionActiveTime = "Gráfico de actividad de sesión";
texts['sr'].sessionActiveTime = "Grafik za aktivne sesije";
texts['gr'].sessionActiveTime = "Διάγραμμα διάρκειας";

texts['bg'].sessionInactiveTime = "Графика за неактивност";
texts['en'].sessionInactiveTime = "Sessions inactive time chart";
texts['de'].sessionInactiveTime = "Sessions inactive time chart";
texts['ro'].sessionInactiveTime = "Grafic de inactivitate sesiune ";
texts['es'].sessionInactiveTime = "Gráfico de inactividad de sesión ";
texts['sr'].sessionInactiveTime = "Gráfik za neaktivne sesije ";
texts['gr'].sessionInactiveTime = "Γραφικά για αδράνεια";

texts['bg'].groupByAccounts = "Групирай по акаунти";
texts['en'].groupByAccounts = "Group by accounts";
texts['de'].groupByAccounts = "Group by accounts";
texts['ro'].groupByAccounts = "Grupează după conturi";
texts['es'].groupByAccounts = "Agrupar por cuentas";
texts['sr'].groupByAccounts = "Grupiši po nalozima";
texts['gr'].groupByAccounts = "Ομαδοποίηση ανά λογαριασμούς ";

texts['bg'].groupByFirms = "Групирай по фирми";
texts['en'].groupByFirms = "Group by firms";
texts['de'].groupByFirms = "Group by firms";
texts['ro'].groupByFirms = "Grupează după firme";
texts['es'].groupByFirms = "Agrupar por empresas";
texts['sr'].groupByFirms = "Grupiši po firmama";
texts['gr'].groupByFirms = "Ομαδοποίηση ανά εταιρείες";

texts['bg'].groupByActiveTime = "Групирай по активност";
texts['en'].groupByActiveTime = "Group by activity";
texts['de'].groupByActiveTime = "Group by activity";
texts['ro'].groupByActiveTime = "Grupează după activitate";
texts['es'].groupByActiveTime = "Agrupar por actividad";
texts['sr'].groupByActiveTime = "Grupiši po aktivnosti";
texts['gr'].groupByActiveTime = "Ομαδοποίηση ανά δραστηριότητα";

texts['bg'].loginTime = "Вход";
texts['en'].loginTime = "Login";
texts['de'].loginTime = "Login";
texts['ro'].loginTime = "Intra";
texts['es'].loginTime = "Iniciar Sesión";
texts['sr'].loginTime = "Prijavljivanje";
texts['gr'].loginTime = "Σύνδεση";


texts['bg'].inactivityTime = "Неактивност";
texts['en'].inactivityTime = "Inactivity";
texts['de'].inactivityTime = "Inactivity";
texts['ro'].inactivityTime = "Inactivitate";
texts['es'].inactivityTime = "Inactividad";
texts['sr'].inactivityTime = "Neaktivnost";
texts['gr'].inactivityTime = "Αδράνεια";

texts['bg'].ip_address = "IP Адрес";
texts['en'].ip_address = "IP Address";
texts['de'].ip_address = "IP Address";
texts['ro'].ip_address = " Adresa IP";
texts['es'].ip_address = " Dirección IP";
texts['sr'].ip_address = "IP adresa";
texts['gr'].ip_address = "Διεύθυνση IP";

texts['bg'].last_report = "Последно гледал";
texts['en'].last_report = "Last report";
texts['de'].last_report = "Last report";
texts['ro'].last_report = " Raportul ultim";
texts['es'].last_report = " Último informe";
texts['sr'].last_report = " Poslednji izveštaj";
texts['gr'].last_report = "Τελευταία παρακολούθηση";

texts['bg'].session_successfully_droped = "Сесията бе успешно прекратена";
texts['en'].session_successfully_droped = "Session was successfully dropped";
texts['de'].session_successfully_droped = "Session was successfully dropped";
texts['ro'].session_successfully_droped = "Sesiunea a fost oprită cu succes";
texts['es'].session_successfully_droped = "La sesión ha finalizado con éxito";
texts['sr'].session_successfully_droped = "Sesija je uspešno prekinuta";
texts['gr'].session_successfully_droped = "Η συνεδρία ολοκληρώθηκε με επιτυχία";

texts['bg'].confirmSessionDrop = "Сигурни ли сте, че искате да изхвърлите този потребител от системата?";
texts['en'].confirmSessionDrop = "Are you sure that you want to drop this session?";
texts['de'].confirmSessionDrop = "Are you sure that you want to drop this session?";
texts['ro'].confirmSessionDrop = "Sunteți sigur, că doriți eliminați acest utilizator din sistem?";
texts['es'].confirmSessionDrop = "¿Seguro que quiera eliminar esta persona del sistema?";
texts['sr'].confirmSessionDrop = "Da li ste sigurni da želite da prekinete ovu sesiju?";
texts['gr'].confirmSessionDrop = "Είστε βέβαιοι ότι θέλετε να απορρίψετε αυτόν τον χρήστη από το σύστημα?";

texts['bg'].sessionActiveTimeChart = "Графика за съотношението на продължителността на сесиите";
texts['en'].sessionActiveTimeChart = "Sessions active time chart";
texts['de'].sessionActiveTimeChart = "Sessions active time chart";
texts['ro'].sessionActiveTimeChart = "Grafic pentru corelarea duratei sesiunilor";
texts['es'].sessionActiveTimeChart = "Gráfico actividad de las sesiónes";
texts['sr'].sessionActiveTimeChart = "Grafik za aktivne sesije";
texts['gr'].sessionActiveTimeChart = "Χρονοδιάγραμμα διάρκειας περιόδου σύνδεσης";

texts['bg'].sessionInactiveTimeChart = "Графика за съотношението на времето на неактивност на сесиите";
texts['en'].sessionInactiveTimeChart = "Sessions inactive time chart";
texts['de'].sessionInactiveTimeChart = "Sessions inactive time chart";
texts['ro'].sessionInactiveTimeChart = "Grafic cu timpul de inactivitate al sesiunilor";
texts['es'].sessionInactiveTimeChart = "Gráfico inactividad de las sesiónes";
texts['sr'].sessionInactiveTimeChart = "Grafik za neaktivne sesije";
texts['gr'].sessionInactiveTimeChart = "Χρονοδιάγραμμα χρονισμού αδράνειας περιόδου λειτουργίας";


texts['bg'].reports_statistics_all_time = "Статистика от началото до днес";
texts['en'].reports_statistics_all_time = "Statistics for the whole period";
texts['de'].reports_statistics_all_time = "Statistics for the whole period";
texts['ro'].reports_statistics_all_time = "Statistică de la început până astăzi";
texts['es'].reports_statistics_all_time = "Estadística desde el principio hasta hoy";
texts['sr'].reports_statistics_all_time = "Statistika za ceo period";
texts['gr'].reports_statistics_all_time = "Στατιστικές από την αρχή μέχρι σήμερα";

texts['bg'].reports_statistics_today = "Статистика за днес";
texts['en'].reports_statistics_today = "Statistics for today";
texts['de'].reports_statistics_today = "Statistics for today";
texts['ro'].reports_statistics_today = "Statistică pentru astăzi";
texts['es'].reports_statistics_today = "Estadística de hoy";
texts['sr'].reports_statistics_today = "Statistika za danas";
texts['gr'].reports_statistics_today = "Στατιστικά για σήμερα";

texts['bg'].reports_statistics_accounts = "Статистика за акаунти";
texts['en'].reports_statistics_accounts = "Statistics for accounts";
texts['de'].reports_statistics_accounts = "Statistics for accounts";
texts['ro'].reports_statistics_accounts = "Statistică pentru conturi";
texts['es'].reports_statistics_accounts = "Estadística de cuentas";
texts['sr'].reports_statistics_accounts = "Statistika za naloge";
texts['gr'].reports_statistics_accounts = "Στατιστικά λογαριασμού";

texts['bg'].report_name = "Име на справка";
texts['en'].report_name = "Report name";
texts['de'].report_name = "Report name";
texts['ro'].report_name = "Denumirea raport";
texts['es'].report_name = "Nombre de informe";
texts['sr'].report_name = "Ime izveštaja";
texts['gr'].report_name = "Όνομα αναφοράς";

texts['bg'].visits_count = "Брой посещения";
texts['en'].visits_count = "Usage count";
texts['de'].visits_count = "Usage count";
texts['ro'].visits_count = "Numărul de vizite";
texts['es'].visits_count = "Número de visitas";
texts['sr'].visits_count = "Broj poseta";
texts['gr'].visits_count = "Αριθμός επισκέψεων";

texts['bg'].reports_usage_chart = "Графика на съотношението за използваемостта на справките";
texts['en'].reports_usage_chart = "Reports usage correlation chart";
texts['de'].reports_usage_chart = "Reports usage correlation chart";
texts['ro'].reports_usage_chart = "Grafic corelare folosir rapoarte";
texts['es'].reports_usage_chart = "Gráfico de los Informes más utilizados";
texts['sr'].reports_usage_chart = "Grafik za odnos upotrebe izveštaja";
texts['gr'].reports_usage_chart = "Γράφημα του λόγου χρηστικότητας των αναφορών";

texts['bg'].period_whole_time = "Период: От началото до днес";
texts['en'].period_whole_time = "Period: From the beginning...";
texts['de'].period_whole_time = "Period: From the beginning...";
texts['ro'].period_whole_time = "Perioadă: De la început până astăzi...";
texts['es'].period_whole_time = "Periodo: Desde el principio hasta hoy...";
texts['sr'].period_whole_time = "Period: Od početka do danas...";
texts['gr'].period_whole_time = "Περίοδος: Από την αρχή μέχρι σήμερα";

texts['bg'].session_start = "Начало на сесията";
texts['en'].session_start = "Start of session";
texts['de'].session_start = "Start of session";
texts['ro'].session_start = "Începutul sesiunii";
texts['es'].session_start = "Inicio de sesión";
texts['sr'].session_start = "Početak sesije";
texts['gr'].session_start = "Έναρξη συνεδρίασης";

texts['bg'].session_end = "Край на сесията";
texts['en'].session_end = "End of session";
texts['de'].session_end = "End of session";
texts['ro'].session_end = "Sfârșitul sesiunii";
texts['es'].session_end = "Fin de sesión";
texts['sr'].session_end = "Kraj sesije";
texts['gr'].session_end = "Τέλος συνεδρίασης";

texts['bg'].events_c = "Събития";
texts['en'].events_c = "Events";
texts['de'].events_c = "";
texts['ro'].events_c = "Evenimente";
texts['es'].events_c = "Eventos";
texts['sr'].events_c = "Događaju";
texts['gr'].events_c = "Συμβάντα ";

texts['bg'].service2_c = "Услуга";
texts['en'].service2_c = "Service";
texts['de'].service2_c = "";
texts['ro'].service2_c = "Servicii";
texts['es'].service2_c = "Servicio";
texts['sr'].service2_c = "Usluga";
texts['gr'].service2_c = "Υπηρεσία";

texts['bg'].info_c = "Информация";
texts['en'].info_c = "Information";
texts['de'].info_c = "";
texts['ro'].info_c = "Informație";
texts['es'].info_c = "Información";
texts['sr'].info_c = "Informacija";
texts['gr'].info_c = "Πληροφορίες";

texts['bg'].zoomInfo = "Задръжте левия бутон на мишката натиснат и очертайте района, който искате да увеличите";
texts['en'].zoomInfo = "Press and hold the left mouse button while dragging a rectangle. Release the button to zoom on the selected area.";
texts['de'].zoomInfo = "";
texts['ro'].zoomInfo = "Apasă si mentine apasat butonul stâng al mouse-ului si creaza un dreptunghi.Eliberați butonul pentru zoom pe zona selectată";
texts['es'].zoomInfo = "Mantenga pulsado el botón izquierdo del ratón y dibujar el área que desea ampliar";
texts['sr'].zoomInfo = "Pritisnite i držite levi taster miša i obeležite oblast koju želite da uveličate.";
texts['gr'].zoomInfo = "Κρατήστε πατημένο το αριστερό πλήκτρο του ποντικιού και περιγράψτε την περιοχή που θέλετε να μεγεθύνετε";

texts['bg'].eta_h_km = "е.т.а ч/км";
texts['en'].eta_h_km = "e.t.a h/km";
texts['de'].eta_h_km = "";
texts['ro'].eta_h_km = "e.t.a oră/km";
texts['es'].eta_h_km = "e.t.a h/km";
texts['sr'].eta_h_km = "e.t.a h/km";
texts['gr'].eta_h_km = "e.t.a h/km";

texts['bg'].garmin_channel_not_open_error = "Не сте отворили канал за комуникация с това превозно средство";
texts['en'].garmin_channel_not_open_error = "You have not started a communication session with this vehicle";
texts['de'].garmin_channel_not_open_error = "";
texts['ro'].garmin_channel_not_open_error = "Nu ați început sesiune de comunicare cu acest vehicul ";
texts['es'].garmin_channel_not_open_error = "No tienes abierto un canal de comunicación con el vehículo ";
texts['sr'].garmin_channel_not_open_error = "Niste započeli sesiju komunikacije sa ovim vozilom ";
texts['gr'].garmin_channel_not_open_error = "Δεν έχετε ανοίξει κανάλι επικοινωνίας με αυτό το όχημα";

texts['bg'].garmin_open_channel = "Отвори канал за комуникация";
texts['en'].garmin_open_channel = "Start a communication session";
texts['de'].garmin_open_channel = "";
texts['ro'].garmin_open_channel = "Începe o sesiune de comunicare";
texts['es'].garmin_open_channel = "Abrir un canal de comunicación";
texts['sr'].garmin_open_channel = "Započnite sesiju komunikacije";
texts['gr'].garmin_open_channel = "Ανοίξτε  κανάλι επικοινωνίας";

texts['bg'].garmin_session_busy_error = "За съжаление друг акаунт от вашата фирма е отворил канал за комуникация с това превозно средство. С цел да не може едновременно няколко човека да изпращат съобщения системата позволява само един потребител да държи отворена сесия за комуникация!";
texts['en'].garmin_session_busy_error = "Another firm user has started a communication session with this vehicle. In order to restrict multiple users to send messages you need to wait for the other user to close the session or force him to get disconnected from it!";
texts['de'].garmin_session_busy_error = "";
texts['ro'].garmin_session_busy_error = "Un alt user al firmei voastră a început sesiune de comunicare cu acest vehicul.Pentru a resticționa mai mulți userii să trimită mesaje, trebuie să așteptați ca celalalt user să închidă sesiunea sau forțați-l să se deconecteze de la ea.";
texts['es'].garmin_session_busy_error = "Desafortunadamente otra cuenta de vuestra compañía ha abierto un canal de comunicación con el vehículo.Por motivos preventivamente sólo una cuenta puede mantener la sesión abierta.";
texts['sr'].garmin_session_busy_error = "Drugi korisnik firme je započeo sesiju komunikacije sa ovim vozilom. Da biste ograničili više korisnika da šalju poruke morate da sačekate drugog korisnika da zatvori sesiju ili da ga naterate da se diskonektuje!";
texts['gr'].garmin_session_busy_error = "Δυστυχώς,  άλλος λογαριασμός από την εταιρεία σας έχει  άνοιξε κανάλι επικοινωνίας με αυτό το όχημα.Για να αποφύγετε την αποστολή μηνυμάτων από πολλά άτομα, το σύστημα επιτρέπει σε έναν μόνο χρήστη να κρατήσει μια ανοιχτή συνεδρία για επικοινωνία!";

texts['bg'].force_garmin_session_kill = "Освободи канала за комуникация";
texts['en'].force_garmin_session_kill = "Kill user's session";
texts['de'].force_garmin_session_kill = "";
texts['ro'].force_garmin_session_kill = "Eliberează sesiunea de comunicare";
texts['es'].force_garmin_session_kill = "Liberar el canal de comunicación";
texts['sr'].force_garmin_session_kill = "Zatvori sesiju korisnika";
texts['gr'].force_garmin_session_kill = "Απελευθερώστε το κανάλι επικοινωνίας";

texts['bg'].garmin_start_chat_session_info = "Изберете превозно средство от таблицата в ляво за да започнете чат с шофьора.</br> (Ако друг акаунт вече е започнал чат с него <span style='color:red'>няма</span> да бъде отворен допълнителен канал за комуникация!";
texts['en'].garmin_start_chat_session_info = "Select a vehicle from the table on the left to open a communication session. </br> (If another firm user has started a chat session with this vehicle you will <span style='color:red'>not</span> be able to start a new one!)";
texts['de'].garmin_start_chat_session_info = "";
texts['ro'].garmin_start_chat_session_info = "Alegeți un vehicul din tabelul din stângă ca să începeți comunicație cu șoferul.</br> (Dacă un alt user al firmei a început deja comunicare cu el <span style='color:red'>nu</span> o să puteți să deschideți o alta nouă )";
texts['es'].garmin_start_chat_session_info = "Seleccione un vehículo de la tabla de la izquierda para iniciar charlar con el conductor.</br> (Si otra cuenta ha iniciado charlar con él <span style='color:red'>no</span> estará abierto un canal adicional para la comunicación )";
texts['sr'].garmin_start_chat_session_info = "Izaberite vozilo sa leve strane tabele da započnete komunikaciju sa vozačem.</br> (Ako je drugi korisnik započeo komunikaciju sa njim <span style='color:red'>no</span> da otvorite dodatni kanal za komunikaciju!)";
texts['gr'].garmin_start_chat_session_info = "Επιλέξτε ένα όχημα από τον πίνακα στα αριστερά για να ξεκινήσετε μια συνομιλία με τον οδηγό.</br>(Εάν ένας άλλος λογαριασμός έχει ήδη αρχίσει συνομιλία μαζί του<span style='color:red'>δεν</span>θα ανοίξει κανένα επιπλέον κανάλι επικοινωνίας!) ";

texts['bg'].garmin_send_text_restriction = "Въведете текст до 100 символа";
texts['en'].garmin_send_text_restriction = "Enter text less than 100 chars";
texts['de'].garmin_send_text_restriction = "";
texts['ro'].garmin_send_text_restriction = "Introduceți text de până la 100 de caractere";
texts['es'].garmin_send_text_restriction = "Introduzca  texto  hasta 100 caracteres";
texts['sr'].garmin_send_text_restriction = "Unesite tekst do 100 karaktera";
texts['gr'].garmin_send_text_restriction = "Εισαγάγετε κείμενο έως 100 χαρακτήρων";

texts['bg'].send_text_to_garmin = "Изпрати към навигатора";
texts['en'].send_text_to_garmin = "Send to garmin";
texts['de'].send_text_to_garmin = "";
texts['ro'].send_text_to_garmin = "Trimite șoferului";
texts['es'].send_text_to_garmin = "Enviar a garmin";
texts['sr'].send_text_to_garmin = "Pošalji garminu";
texts['gr'].send_text_to_garmin = "Αποστολή στον πλοηγό ";

texts['bg'].chat_with_garmin_vehicle = "Чат с водачът на превозно средство:";
texts['en'].chat_with_garmin_vehicle = "Chat with vehicle:";
texts['de'].chat_with_garmin_vehicle = "";
texts['ro'].chat_with_garmin_vehicle = "Comunică cu șoferul vehicului";
texts['es'].chat_with_garmin_vehicle = "Chatee con vehículo";
texts['sr'].chat_with_garmin_vehicle = "Komuniciraj sa vozilom";
texts['gr'].chat_with_garmin_vehicle = "Συνομιλία με τον οδηγό του οχήματος:";

texts['bg'].garmin_cached_messages_install = "Бързите съобщения, които може да инсталирате в навигатора на избраното превозно средство.";
texts['en'].garmin_cached_messages_install = "The cached messages which are available for installing into the garmin device of this vehicle.";
texts['de'].garmin_cached_messages_install = "";
texts['ro'].garmin_cached_messages_install = "Mesajele instantanee care pot fi instalate în aparatul Garmin al vehiculului selectat";
texts['es'].garmin_cached_messages_install = "Mensajes instantáneos que se pueden instalar en el dispositivo Garmin de  vehículo seleccionado ";
texts['sr'].garmin_cached_messages_install = "Keširane poruke koje su dostupne za instaliranje u navigaciju ovog vozila. ";
texts['gr'].garmin_cached_messages_install = "Τα αποθηκευμένα μηνύματα που μπορείτε να εγκαταστήσετε στον πλοηγό του επιλεγμένου οχήματος.";

texts['bg'].garmin_cached_messages_list_update = "Натиснете тук за да обновите списъка. Тази команда има смисъл само ако сте добавили нови съобщения.";
texts['en'].garmin_cached_messages_list_update = "Press here to update the list. This command will be executed only if you have added new messages.";
texts['de'].garmin_cached_messages_list_update = "";
texts['ro'].garmin_cached_messages_list_update = "Apasați aici ca să actualizezi lista. Această comandă va fi executată numai dacă ați adăugat mesaje noi ";
texts['es'].garmin_cached_messages_list_update = "Haga clic para actualizar la lista. Este comando se ejecutará sólo si se han añadido nuevos mensajes ";
texts['sr'].garmin_cached_messages_list_update = "Kliknite ovde da ažurirate listu. Ova komanda će biti izvršena samo ako ste dodali nove poruke. ";
texts['gr'].garmin_cached_messages_list_update = "Πατήστε εδώ για να ενημερώσετε τη λίστα. Αυτή η εντολή θα εκτελεστεί μόνο αν έχετε προσθέσει νέα μηνύματα.";

texts['bg'].navigator_data = "Данни от навигатора:";
texts['en'].navigator_data = "Navigator data:";
texts['de'].navigator_data = "";
texts['ro'].navigator_data = "Date de navigație";
texts['es'].navigator_data = "Datos de navegación";
texts['sr'].navigator_data = "Podaci navigacije:";
texts['gr'].navigator_data = "Δεδομένα πλοηγού:";

texts['bg'].navigator_event_sent = "Вече е задействано събитие. Изчакайте да приключи и тогава опитайте пак.";
texts['en'].navigator_event_sent = "There is a pending event. Please try again later.";
texts['de'].navigator_event_sent = "";
texts['ro'].navigator_event_sent = "Deja este un eveniment în desfășurare. Vă rog încercați mai târziu";
texts['es'].navigator_event_sent = "Otro evento se esta ejecutando. Espere hasta termina el proceso y Inténtalo de nuevo.";
texts['sr'].navigator_event_sent = "Već je zadat događaj. Molimo pokušajte kasnije.";
texts['gr'].navigator_event_sent = "Ένα συμβάν έχει ήδη ενεργοποιηθεί. Περιμένετε να ολοκληρωθεί και δοκιμάστε ξανά.";

texts['bg'].search_result = "Резултати от търсенето";
texts['en'].search_result = "Search results";
texts['de'].search_result = "";
texts['ro'].search_result = "Rezultatele căutării";
texts['es'].search_result = "Resultados de la búsqueda";
texts['sr'].search_result = "Rezultati pretrage";
texts['gr'].search_result = "Αποτελέσματα αναζήτησης";

texts['bg'].add_on_map = "Добави на картата";
texts['en'].add_on_map = "Add on map";
texts['de'].add_on_map = "";
texts['ro'].add_on_map = "Adaugă pe hartă";
texts['es'].add_on_map = "Añadir mapa";
texts['sr'].add_on_map = "Dodaj na mapi";
texts['gr'].add_on_map = "Προσθέστε στο χάρτη";

texts['bg'].center_on_map = "Центрирай";
texts['en'].center_on_map = "Center on map";
texts['de'].center_on_map = "";
texts['ro'].center_on_map = "Centrați";
texts['es'].center_on_map = "Centrar";
texts['sr'].center_on_map = "Centriraj";
texts['gr'].center_on_map = "Κέντρο";

texts['bg'].network_signal = "Мрежа/Сигнал";
texts['en'].network_signal = "Network/Signal";
texts['de'].network_signal = "";
texts['ro'].network_signal = "Rețea/Semnal";
texts['es'].network_signal = "Red/Señal";
texts['sr'].network_signal = "Mreža/Signal";
texts['gr'].network_signal = "Δίκτυο / Σήμα";

texts['bg'].digital_events = "Цифрови събития";
texts['en'].digital_events = "Digital Events";
texts['de'].digital_events = "";
texts['ro'].digital_events = "Evenimente digitale";
texts['es'].digital_events = "Eventos digitales";
texts['sr'].digital_events = "Digitalni događaji";
texts['gr'].digital_events = "Ψηφιακά γεγονότα";

texts['bg'].analog_events = "Аналогови събития";
texts['en'].analog_events = "Analog Events";
texts['de'].analog_events = "";
texts['ro'].analog_events = "Evenimente analogice.";
texts['es'].analog_events = "Eventos analógico.";
texts['sr'].analog_events = "Analogni događaji.";
texts['gr'].analog_events = "Αναλογικά γεγονότα";

texts['bg'].speeding = "Превишена скорост";
texts['en'].speeding = "Speeding";
texts['de'].speeding = "";
texts['ro'].speeding = "Viteză depășită";
texts['es'].speeding = "Exceso de velocidad";
texts['sr'].speeding = "Ubrzavanje";
texts['gr'].speeding = "Υπερβολική ταχύτητα";

texts['bg'].exceeded_revolutions = "Превишени обороти";
texts['en'].exceeded_revolutions = "Exceeded revolutions";
texts['de'].exceeded_revolutions = "";
texts['ro'].exceeded_revolutions = "Turatii depășite ";
texts['es'].exceeded_revolutions = "Exceso de revoluciones ";
texts['sr'].exceeded_revolutions = "Povećan broj obrtaja ";
texts['gr'].exceeded_revolutions = "Υπέρβαση στροφών";

texts['bg'].temperature_boundaries = "Температурни граници";
texts['en'].temperature_boundaries = "Temperature limits";
texts['de'].temperature_boundaries = "";
texts['ro'].temperature_boundaries = "Limite de temperătură";
texts['es'].temperature_boundaries = "Límites de temperatura";
texts['sr'].temperature_boundaries = "Ograničenja temperature";
texts['gr'].temperature_boundaries = "Όρια θερμοκρασίας";

texts['bg'].vehicle_parking_no_info_available = "На този етап няма информация за последното паркиране на избрания автомобил!";
texts['en'].vehicle_parking_no_info_available = "Currently there is no information available about the last parking time of this vehicle!";
texts['de'].vehicle_parking_no_info_available = "";
texts['ro'].vehicle_parking_no_info_available = "Momentan nu este nici o informație disponibilă despre ultima parcare a vehicului ales !";
texts['es'].vehicle_parking_no_info_available = "De momento no hay información disponible para el último  estacionamiento de este vehículo.!";
texts['sr'].vehicle_parking_no_info_available = "Trenutno ne postoji informacija o poslednjem parkiranju ovog vozila!";
texts['gr'].vehicle_parking_no_info_available = "Επί του παρόντος, δεν υπάρχουν διαθέσιμες πληροφορίες σχετικά με τον τελευταίο χρόνο στάθμευσης αυτού του οχήματος!";

texts['bg'].routes_created_and_saved_info = "Предварително създадени маршрути";
texts['en'].routes_created_and_saved_info = "Firm's available routes";
texts['de'].routes_created_and_saved_info = "";
texts['ro'].routes_created_and_saved_info = "Itinerarii prealabile făcute de firmă";
texts['es'].routes_created_and_saved_info = "Rutas anteriormente creadas";
texts['sr'].routes_created_and_saved_info = "Prethodno napravljene maršrute";
texts['gr'].routes_created_and_saved_info = "Προ-δημιουργημένες διαδρομές";

texts['bg'].routes_created_and_saved = "Запаметени маршрути в системата";
texts['en'].routes_created_and_saved = "Available routes";
texts['de'].routes_created_and_saved = "";
texts['ro'].routes_created_and_saved = "Itinerarii salvate în sistem";
texts['es'].routes_created_and_saved = "Rutas disponibles";
texts['sr'].routes_created_and_saved = "Sačuvane maršrute u sistemu";
texts['gr'].routes_created_and_saved = "Αποθηκευμένα δρομολόγια στο σύστημα";

texts['bg'].dynamic_routes_info = "Динамично създадени маршрути, който се използват само за текущата сесия";
texts['en'].dynamic_routes_info = "Temporary routes available only during the user's session";
texts['de'].dynamic_routes_info = "";
texts['ro'].dynamic_routes_info = "Itinerarii temporare făcute, care se folosesc numai pentru sesiune curentă";
texts['es'].dynamic_routes_info = "Rutas temporales disponibles sólo durante la sesión del usuario";
texts['sr'].dynamic_routes_info = "Privremene maršrute dostupne samo tokom sesije korisnika ";
texts['gr'].dynamic_routes_info = "Δυναμικά δημιουργημένες διαδρομές που χρησιμοποιούνται μόνο για την τρέχουσα περίοδο λειτουργίας";

texts['bg'].dynamic_route = "Временен маршрут";
texts['en'].dynamic_route = "Temporary route";
texts['de'].dynamic_route = "";
texts['ro'].dynamic_route = "Itinerariu temporar";
texts['es'].dynamic_route = "Rutas temporales";
texts['es'].dynamic_route = "Privremena maršruta";
texts['gr'].dynamic_route = "Προσωρινή διαδρομή";

texts['bg'].dynamic_routes = "Динамично създадени маршрути";
texts['en'].dynamic_routes = "Temporary routes";
texts['de'].dynamic_routes = "";
texts['ro'].dynamic_routes = "Itinerarii temporar făcute";
texts['es'].dynamic_routes = "Rutas dinámicamente creadas";
texts['sr'].dynamic_routes = "Privremene maršrute";
texts['gr'].dynamic_routes = "Δυναμικά δημιουργούμενες διαδρομές";

texts['bg'].waypoint = "Междинна точка";
texts['en'].waypoint = "Waypoint";
texts['de'].waypoint = "";
texts['ro'].waypoint = "Punct intermediar";
texts['es'].waypoint = "Punto intermedio";
texts['sr'].waypoint = "Srednja tačka";
texts['gr'].waypoint = "Ενδιάμεσο σημείο";

texts['bg'].waypoints = "Междинни точки";
texts['en'].waypoints = "Waypoints";
texts['de'].waypoints = "";
texts['ro'].waypoints = "Puncte intermediare";
texts['es'].waypoints = "Puntos intermedios";
texts['sr'].waypoints = "Srednje tačke";
texts['gr'].waypoints = "Ενδιάμεσα σημεία";

texts['bg'].vehicles_parking_state = "Състояние на превозните средства";
texts['en'].vehicles_parking_state = "Vehicles' parking state";
texts['de'].vehicles_parking_state = "";
texts['ro'].vehicles_parking_state = "Starea vehiculelor";
texts['es'].vehicles_parking_state = "Estado del vehículo";
texts['sr'].vehicles_parking_state = "Parking mesto vozila";
texts['gr'].vehicles_parking_state = "Κατάσταση των οχημάτων";

texts['bg'].parking_idle = "Паркинг/престой";
texts['en'].parking_idle = "Parking/idle";
texts['de'].parking_idle = "";
texts['ro'].parking_idle = "Parcat/Inactiv";
texts['es'].parking_idle = "Aparcamiento/estacionamiento";
texts['sr'].parking_idle = "Parking/pauza";
texts['gr'].parking_idle = "Πάρκινγκ / διαμονή";

texts['bg'].moving_work = "Работа/движение";
texts['en'].moving_work = "Moving/working";
texts['de'].moving_work = "";
texts['ro'].moving_work = "Lucrează/În mișcare";
texts['es'].moving_work = "Trabajo/movimiento";
texts['sr'].moving_work = "Kretanje/rad";
texts['gr'].moving_work = "Εργασία / κίνηση";

texts['bg'].update_data = "Обнови данните";
texts['en'].update_data = "Update data";
texts['de'].update_data = "";
texts['ro'].update_data = "Actualizează datele";
texts['es'].update_data = "Actualizar datos";
texts['sr'].update_data = "Ažuriraj podatke";
texts['gr'].update_data = "Ανανέωση δεδομένων";

texts['bg'].new_polygon = "Нов полигон";
texts['en'].new_polygon = "New polygon";
texts['de'].new_polygon = "";
texts['ro'].new_polygon = "Perimetru/Poligon nou";
texts['es'].new_polygon = "Nuevo polígono";
texts['sr'].new_polygon = "Nov poligon";
texts['gr'].new_polygon = "Νέο πολύγωνο";

texts['bg'].server_connection_lost = "Връзката със сървъра беше прекъсната.";
texts['en'].server_connection_lost = "Could not establish connection with the server!";
texts['de'].server_connection_lost = "";
texts['ro'].server_connection_lost = "Conectarea cu server a fost întreruptă";
texts['es'].server_connection_lost = "No se pudo establecer conexión con el servidor";
texts['sr'].server_connection_lost = "Prekinuta veza sa serverom";
texts['gr'].server_connection_lost = "Η σύνδεση με τον διακομιστή διακόπηκε.";

texts['bg'].session_ended_on_purpose = "Вашата сесия беше служебно прекратена.\nПричината може да е поради опит да се създаде дублираща се сесия.";
texts['en'].session_ended_on_purpose = "Your session was ended.\nMost probably you tried to login twice from the same browser.";
texts['de'].session_ended_on_purpose = "";
texts['ro'].session_ended_on_purpose = "Sesiunea dvs.a fost întreruptă.\nProbabil a-ti încercat să vă logați de două ori de la același browser";
texts['es'].session_ended_on_purpose = "Su sesión ha terminado.\nLa razón puede ser debido a un intento de entrar dos veces el mismo navegador";
texts['sr'].session_ended_on_purpose = "Vaša sesija je prekinuta.\nVerovatno ste probali da se dvaput prijavite sa istog pretraživača.";
texts['gr'].session_ended_on_purpose = "Η συνεδρία σας ακυρώθηκε αυτεπαγγέλτως.\nΟ λόγος μπορεί να οφείλεται σε προσπάθειες δημιουργίας διπλής περιόδου σύνδεσης.";

texts['bg'].fuel_drain = "Източване";
texts['en'].fuel_drain = "Fuel draining";
texts['de'].fuel_drain = "";
texts['ro'].fuel_drain = "Golire";
texts['es'].fuel_drain = "Sustracción";
texts['sr'].fuel_drain = "Istakanje goriva";
texts['gr'].fuel_drain = "Αφαίρεση καυσίμου ";

texts['bg'].fuel_fill = "Зареждане";
texts['en'].fuel_fill = "Fueling";
texts['de'].fuel_fill = "";
texts['ro'].fuel_fill = "Alimentare";
texts['es'].fuel_fill = "Repostaje";
texts['sr'].fuel_fill = "Sipanje goriva";
texts['gr'].fuel_fill = "Ανεφοδιασμός ";

texts['bg'].inactive_sended_point = "Неактивна изпратена точка.";
texts['en'].inactive_sended_point = "Inactive sent point.";
texts['de'].inactive_sended_point = "";
texts['ro'].inactive_sended_point = "Punct inactiv trimis";
texts['es'].inactive_sended_point = "Punto enviado Inactivo";
texts['sr'].inactive_sended_point = "Neaktivna poslata tačka.";
texts['gr'].inactive_sended_point = "Αδρανές σημείο αποστολής.";

texts['bg'].active_point = "Активна точка.";
texts['en'].active_point = "Active point.";
texts['de'].active_point = "";
texts['ro'].active_point = "Punct activ";
texts['es'].active_point = "Punto activo";
texts['sr'].active_point = "Aktivna tačka.";
texts['gr'].active_point = "Ενεργό σημείο.";

texts['bg'].inactive_point = "Неактивна точка.";
texts['en'].inactive_point = "Inactive point.";
texts['de'].inactive_point = "";
texts['ro'].inactive_point = "Punct inactiv";
texts['es'].inactive_point = "Punto inactivo";
texts['sr'].inactive_point = "Neaktivna tačka.";
texts['gr'].inactive_point = "Αδρανές σημείο.";

texts['bg'].completed_point = "Отработена точка.";
texts['en'].completed_point = "Completed point.";
texts['de'].completed_point = "";
texts['ro'].completed_point = "Punct finalizat";
texts['es'].completed_point = "Punto completado";
texts['sr'].completed_point = "Završena tačka.";
texts['gr'].completed_point = "Ολοκληρωμένο σημείο.";

texts['bg'].deleted_point = "Изтрита точка.";
texts['en'].deleted_point = "Deleted point.";
texts['de'].deleted_point = "";
texts['ro'].deleted_point = "Punct șters";
texts['es'].deleted_point = "Punto eliminado";
texts['sr'].deleted_point = "Izbrisana tačka.";
texts['gr'].deleted_point = "Διαγραμμένο σημείο.";

texts['bg'].sent_to_navigator = "Изпратено е към навигатора на това превозно средство.";
texts['en'].sent_to_navigator = "Sent to the navigator.";
texts['de'].sent_to_navigator = "";
texts['ro'].sent_to_navigator = "A fost trimis la navigatorul vehiculului";
texts['es'].sent_to_navigator = "ha enviado al navegación";
texts['sr'].sent_to_navigator = "Poslato navigaciji.";
texts['gr'].sent_to_navigator = "Αποστέλλεται στον πλοηγό του οχήματος.";

texts['bg'].not_sent_to_navigator = "Не е изпратено към навигатора на това превозно средство.";
texts['en'].not_sent_to_navigator = "Not sent to the navigator.";
texts['de'].not_sent_to_navigator = "";
texts['ro'].not_sent_to_navigator = "Nu a fost trimis pe navigatorul vehiculului";
texts['es'].not_sent_to_navigator = "No se ha enviado al navegación";
texts['sr'].not_sent_to_navigator = "Nije poslato navigaciji.";
texts['gr'].not_sent_to_navigator = "Δεν αποστέλλονται στον πλοηγό αυτού του οχήματος.";

texts['bg'].check_navigator_connection = "Проверка на връзката с навигатора";
texts['en'].check_navigator_connection = "Check the connection with the navigator.";
texts['de'].check_navigator_connection = "";
texts['ro'].check_navigator_connection = "Verificare conectarea cu navigatorul";
texts['es'].check_navigator_connection = "Compruebe la conexión con el navegador";
texts['sr'].check_navigator_connection = "Proverite konekciju sa navigacijom.";
texts['gr'].check_navigator_connection = "Ελέγξτε τη σύνδεση με τον πλοηγό";

texts['bg'].navigator_points_sort = "Сортиране на точките в навигатора";
texts['en'].navigator_points_sort = "Sort the navigator's points.";
texts['de'].navigator_points_sort = "";
texts['ro'].navigator_points_sort = "Sortare elementele/punctelor din navigato";
texts['es'].navigator_points_sort = "Clasificación de los puntos en la navegación";
texts['sr'].navigator_points_sort = "Sortirajte tačke navigacije.";
texts['gr'].navigator_points_sort = "Ταξινόμηση σημείων στον πλοηγό";

texts['bg'].navigator_delete_all_points = "Изтриване на всички точки";
texts['en'].navigator_delete_all_points = "Delete all points.";
texts['de'].navigator_delete_all_points = "";
texts['ro'].navigator_delete_all_points = "Șterge toate puncte";
texts['es'].navigator_delete_all_points = "Eliminar todos los puntos";
texts['sr'].navigator_delete_all_points = "Izbrišite sve tačke.";
texts['gr'].navigator_delete_all_points = "Διαγραφή όλων των σημείων";

texts['bg'].navigator_delete_all_msgs = "Изтриване на всички съобщения.";
texts['en'].navigator_delete_all_msgs = "Delete all messages.";
texts['de'].navigator_delete_all_msgs = "";
texts['ro'].navigator_delete_all_msgs = "Șterge toate mesaje";
texts['es'].navigator_delete_all_msgs = "Eliminar todos los mensajes";
texts['sr'].navigator_delete_all_msgs = "Izbrišite sve poruke.";
texts['gr'].navigator_delete_all_msgs = "Διαγραφή όλων των μηνυμάτων.";

texts['bg'].navigator_delete_current_route = "Изтриване на активната навигация.";
texts['en'].navigator_delete_current_route = "Delete the current navigation.";
texts['de'].navigator_delete_current_route = "";
texts['ro'].navigator_delete_current_route = "Ștergeți navgarea activă";
texts['es'].navigator_delete_current_route = "Eliminar la navegación actual";
texts['sr'].navigator_delete_current_route = "Izbrišite trenutnu navigaciju.";
texts['gr'].navigator_delete_current_route = "Διαγραφή ενεργής πλοήγησης.";

texts['bg'].navigator_delete_cached_messages = "Изтриване на бързите съобщения.";
texts['en'].navigator_delete_cached_messages = "Delete the cached messages.";
texts['de'].navigator_delete_cached_messages = "";
texts['ro'].navigator_delete_cached_messages = "Șterge mesajele rapide/memorate";
texts['es'].navigator_delete_cached_messages = "Eliminar los mensajes rápidos";
texts['sr'].navigator_delete_cached_messages = "Izbrišite keširane poruke.";
texts['gr'].navigator_delete_cached_messages = "Διαγραφή των αποθηκευμένων μηνυμάτων.";

texts['bg'].navigator_delete_cached_responses = "Изтриване на бързите отговори.";
texts['en'].navigator_delete_cached_responses = "Delete the cached responses.";
texts['de'].navigator_delete_cached_responses = "";
texts['ro'].navigator_delete_cached_responses = "Șterge răspunsurile rapide/memorate";
texts['es'].navigator_delete_cached_responses = "Eliminar las respuestas rápidas";
texts['sr'].navigator_delete_cached_responses = "Izbrišite keširane odgovore.";
texts['gr'].navigator_delete_cached_responses = "Διαγραφή των αποθηκευμένων απαντήσεων.";

texts['bg'].navigator_delete_gpi_file = "Изтриване на GPI file.";
texts['en'].navigator_delete_gpi_file = "Delete the GPI file.";
texts['de'].navigator_delete_gpi_file = "";
texts['ro'].navigator_delete_gpi_file = "Șterge fișier GPI ";
texts['es'].navigator_delete_gpi_file = "Eliminar el archivo PGI ";
texts['sr'].navigator_delete_gpi_file = "Izbrišite GPI fajl. ";
texts['gr'].navigator_delete_gpi_file = "Διαγραφή αρχείου GPI.";

texts['bg'].navigator_delete_id = "Изтриване на ID.";
texts['en'].navigator_delete_id = "Delete ID.";
texts['de'].navigator_delete_id = "";
texts['ro'].navigator_delete_id = "Ștergere ID";
texts['es'].navigator_delete_id = "Eliminar ID";
texts['sr'].navigator_delete_id = "Izbrišite ID.";
texts['gr'].navigator_delete_id = "Διαγραφή ID.";

texts['bg'].navigator_delete_waypoints = "Изтриване на междинните точки.";
texts['en'].navigator_delete_waypoints = "Delete the waypoints.";
texts['de'].navigator_delete_waypoints = "";
texts['ro'].navigator_delete_waypoints = "Șterge punctele intermediare";
texts['es'].navigator_delete_waypoints = "Eliminar los puntos intermedios";
texts['sr'].navigator_delete_waypoints = "Izbrišite medjutačke.";
texts['gr'].navigator_delete_waypoints = "Διαγραφή ενδιάμεσα σημεία.";

texts['bg'].navigator_fmi_deny = "Забрана на FMI.";
texts['en'].navigator_fmi_deny = "FMI deny.";
texts['de'].navigator_fmi_deny = "";
texts['ro'].navigator_fmi_deny = "Șterge FMI";
texts['es'].navigator_fmi_deny = "Deshabilitar  FMI";
texts['sr'].navigator_fmi_deny = "FMI odbijen.";
texts['gr'].navigator_fmi_deny = "Απαγόρευση του FMI.";

texts['bg'].navigator_message_from_driver = "Съобщение от водача";
texts['en'].navigator_message_from_driver = "Message from the driver";
texts['de'].navigator_message_from_driver = "";
texts['ro'].navigator_message_from_driver = "Mesaj de la șofer";
texts['es'].navigator_message_from_driver = "Mensaje del conductor";
texts['sr'].navigator_message_from_driver = "Poruka od vozača.";
texts['gr'].navigator_message_from_driver = "Μήνυμα από τον οδηγό";

texts['bg'].navigator_data = "Данни от навигатора";
texts['en'].navigator_data = "Navigator data";
texts['de'].navigator_data = "";
texts['ro'].navigator_data = "Date din navigator";
texts['es'].navigator_data = "Datos de la navegación";
texts['sr'].navigator_data = "Podaci navigacije";
texts['gr'].navigator_data = "Δεδομένα πλοηγού";

texts['bg'].calculating = "Обработва се...";
texts['en'].calculating = "Calculating...";
texts['de'].calculating = "";
texts['ro'].calculating = "Prelucrare date.....";
texts['es'].calculating = "Calculando.....";
texts['sr'].calculating = "Obrada.....";
texts['gr'].calculating = "Επεξεργασία ...";

texts['bg'].remaining_time = "Оставащо време...";
texts['en'].remaining_time = "Remaining time...";
texts['de'].remaining_time = "";
texts['ro'].remaining_time = "Timp rămas";
texts['es'].remaining_time = "Tiempo restante...";
texts['sr'].remaining_time = "Preostalo vreme...";
texts['gr'].remaining_time = "Χρόνος που απομένει ...";

texts['bg'].no_data = "Няма данни";
texts['en'].no_data = "No data";
texts['de'].no_data = "";
texts['ro'].no_data = "Nu sunt date";
texts['es'].no_data = "Sin datos";
texts['sr'].no_data = "Nema podataka";
texts['gr'].no_data = "Δεν υπάρχουν  δεδομένα";

texts['bg'].enter_navigator_point_name_info = "Натиснете тук за да въведете име на точката.";
texts['en'].enter_navigator_point_name_info = "Click here to enter a name for this point.";
texts['de'].enter_navigator_point_name_info = "";
texts['ro'].enter_navigator_point_name_info = "Apasați aici ca să introduceți denumirea punctului";
texts['es'].enter_navigator_point_name_info = "Haga click para introducir nombre del punto";
texts['sr'].enter_navigator_point_name_info = "Kliknite ovde da unesete ime za ovu tačku.";
texts['gr'].enter_navigator_point_name_info = "Πατήστε  εδώ για να εισάγετε όνομα σημείου.";

texts['bg'].enter_navigator_point_text_info = "Натиснете тук за да въведете текст, който искате да покажете на шофиора при изпращане на точката.";
texts['en'].enter_navigator_point_text_info = "Click here to enter additional text message.";
texts['de'].enter_navigator_point_text_info = "";
texts['ro'].enter_navigator_point_text_info = "Apasați aici ca să introduceți text suplimentar, prin care vreți să indicați șoferului punctul de trimitere ";
texts['es'].enter_navigator_point_text_info = "Haga click para introducir un texto que se mostrará del conductor junto con el envio del punto ";
texts['sr'].enter_navigator_point_text_info = "Kliknite ovde da unesete dodatni tekst poruke.";
texts['gr'].enter_navigator_point_text_info = "Πατήστε  εδώ για να εισαγάγετε το κείμενο που θέλετε να εμφανίζεται στο πρόγραμμα οδήγησης κατά την αποστολή του σημείου.";

texts['bg'].message_sent_by_you = "Съобщение изпратено от вас.";
texts['en'].message_sent_by_you = "Message sent by you.";
texts['de'].message_sent_by_you = "";
texts['ro'].message_sent_by_you = "Mesaj trimis de dvs";
texts['es'].message_sent_by_you = "Mensaje enviado por usted";
texts['sr'].message_sent_by_you = "Poruka koju ste poslali.";
texts['gr'].message_sent_by_you = "Μήνυμα σταλμένο από εσάς";

texts['bg'].message_pending = "Съобщението се изпраща...";
texts['en'].message_pending = "The message is being sent...";
texts['de'].message_pending = "";
texts['ro'].message_pending = "Mesajul se trimite...";
texts['es'].message_pending = "El mensaje se esta enviando ...";
texts['sr'].message_pending = "Poruka se šalje ...";
texts['gr'].message_pending = "Αποστολή μηνύματος ...";

texts['bg'].point_pending = "Точката се изпраща...";
texts['en'].point_pending = "The point is being sent...";
texts['de'].point_pending = "";
texts['ro'].point_pending = "Punctul se trimite...";
texts['es'].point_pending = "El punto se esta enviando ...";
texts['sr'].point_pending = "Tačka se šalje ...";
texts['gr'].point_pending = "Σημείο αποστολής ...";

texts['bg'].received = "Получено";
texts['en'].received = "Delivered";
texts['de'].received = "";
texts['ro'].received = "Trimis";
texts['es'].received = "Entregado";
texts['sr'].received = "Primljeno";
texts['gr'].received = "Παραδόθηκε";

texts['bg'].undelivered = "Не получено";
texts['en'].undelivered = "Undelivered";
texts['de'].undelivered = "";
texts['ro'].undelivered = "Nu a fost trimis";
texts['es'].undelivered = "No entregado";
texts['sr'].undelivered = "Nije primljeno";
texts['gr'].undelivered = "Δεν έχει παραδοθεί";

texts['bg'].navigator_system_command_pending = "В момента се обработва системна команда изпратена към навигатора на избраното превозно средство...";
texts['en'].navigator_system_command_pending = "System command is currently being processed...";
texts['de'].navigator_system_command_pending = "";
texts['ro'].navigator_system_command_pending = "Comanda trimisă pe navigatorul vehicului ales, este în curs de procesare...";
texts['es'].navigator_system_command_pending = "Comando del sistema se encuentra en trámite ...";
texts['sr'].navigator_system_command_pending = "Sistemska komanda se trenutno obrađuje ...";
texts['gr'].navigator_system_command_pending = "Μια εντολή συστήματος που αποστέλλεται στον πλοηγό του επιλεγμένου οχήματος βρίσκεται υπό επεξεργασία ...";

texts['bg'].wrong_firm_password_entered = "Не сте въвели правилна парола за манупилиране на данни!";
texts['en'].wrong_firm_password_entered = "You have not entered the correct password for manipulation of firm's data";
texts['de'].wrong_firm_password_entered = "";
texts['ro'].wrong_firm_password_entered = "Ați introdus parola greșită pentru manipularea datelor firmei";
texts['es'].wrong_firm_password_entered = "¡No ha introducido la contraseña correcta para  manipulación de datos!";
texts['sr'].wrong_firm_password_entered = "Niste uneli ispravnu lozinku za upravljanje podacima firme!";
texts['gr'].wrong_firm_password_entered = "Δεν έχετε εισάγει τον σωστό κωδικό πρόσβασης για τον χειρισμό δεδομένων!";

texts['bg'].service_rule_violation = "Тази услуга не е предназначена за фиктивни цели. Използвайте е само по нужда. За целта изберете не повече от 3 превозни средства. Ако имате добра причина за повече сметки свържете се с вашия представител на софтуера.";
texts['en'].service_rule_violation = "This service is only for real case scenarious so please do not choose more than 3 vehicles. If you do have a good reason to use it for more than 3 vehicles at a time please contact us.";
texts['de'].service_rule_violation = "";
texts['ro'].service_rule_violation = "Acest serviciu este doar pentru scenarii reale, vă rog nu alegeți mai mult de 3 vehicule.Dacă aveți un motiv de al folosiii pentru mai mult de 3 vehicule in același timp vă rog să ne contactați";
texts['es'].service_rule_violation = "Este servicio es sólo para los escenarios reales, por favor no elegir más de 3 vehículos. Si usted tiene una razón para usarlo durante más de 3 vehículos al mismo tiempo, por favor póngase en contacto con nosotros";
texts['sr'].service_rule_violation = "Ova usluga je namenjena samo za prave scenarije pa nemojte izabrati više od 3 vozila. Ako imate dobre razloge da je koristite za više od 3 vozila odjednom, molimo kontaktirajte nas.";
texts['gr'].service_rule_violation = "Αυτή η υπηρεσία είναι για πραγματικά σενάρια, επομένως παρακαλούμε να μην επιλέξετε περισσότερα από 3 οχήματα. Εάν έχετε καλό λόγο να το χρησιμοποιήσετε για περισσότερα από 3 οχήματα τη φορά, επικοινωνήστε μαζί μας.";

texts['bg'].name_edit = "Редактиране на името";
texts['en'].name_edit = "Edit route's name";
texts['de'].name_edit = "";
texts['ro'].name_edit = "Editează numele itinerariului";
texts['es'].name_edit = "Editar el nombre";
texts['sr'].name_edit = "Izmenite ime maršrute";
texts['gr'].name_edit = "Επεξεργασία ονόματος";

texts['bg'].of_8 = "от 8";
texts['en'].of_8 = "of 8";
texts['de'].of_8 = "";
texts['ro'].of_8 = "de 8";
texts['es'].of_8 = "de 8";
texts['sr'].of_8 = "od 8";
texts['gr'].of_8 = "από 8";

texts['bg'].delete_route = "Натиснете тук за да изтриете маршрута";
texts['en'].delete_route = "Click here to delete route";
texts['de'].delete_route = "";
texts['ro'].delete_route = "Apasați aici ca să ștergeți itinerariu";
texts['es'].delete_route = "Haga clic para eliminar la ruta";
texts['sr'].delete_route = "Kliknite ovde da izbrišete maršrutu";
texts['gr'].delete_route = "Πατήστε  εδώ για να διαγράψετε τη διαδρομή";

texts['bg'].route_invalid_start_end_point = "При така зададените начална и крайна точка не съществува маршрут!";
texts['en'].route_invalid_start_end_point = "In both initial and end point there is no route!";
texts['de'].route_invalid_start_end_point = "";
texts['ro'].route_invalid_start_end_point = "Între punctul inițial si cel de final nu este o rută ";
texts['es'].route_invalid_start_end_point = "Los puntos adjuntos de inicio y final no son parte de una ruta existente ";
texts['sr'].route_invalid_start_end_point = "Nema maršrute ni na početnoj ni na krajnjoj tački!";
texts['gr'].route_invalid_start_end_point = "Δεν υπάρχει διαδρομή σε αυτό το σημείο έναρξης και λήξης!";

texts['bg'].driver_name = "Име на водач";
texts['en'].driver_name = "Driver";
texts['de'].driver_name = "";
texts['ro'].driver_name = "Nume de șofer";
texts['es'].driver_name = "Nombre del conductor";
texts['sr'].driver_name = "Ime vozača";
texts['gr'].driver_name = "Όνομα  οδηγού";

texts['bg'].filter_color = "Цвят на филтъра";
texts['en'].filter_color = "Filter color";
texts['de'].filter_color = "";
texts['ro'].filter_color = "Filtru de culoare";
texts['es'].filter_color = "Color del filtro";
texts['sr'].filter_color = "Boja filtera";
texts['gr'].filter_color = "Χρώμα φίλτρου";

texts['bg'].filter = "Филтър";
texts['en'].filter = "Filter";
texts['de'].filter = "";
texts['ro'].filter = "Filtru";
texts['es'].filter = "Filtro";
texts['sr'].filter = "Filter";
texts['gr'].filter = "Φίλτρο";

texts['bg'].tank_1 = "Резервоар - 1";
texts['en'].tank_1 = "Fuel tank - 1";
texts['de'].tank_1 = "";
texts['ro'].tank_1 = "Rezervor - 1";
texts['es'].tank_1 = "Tanque - 1";
texts['sr'].tank_1 = "Rezervoar - 1";
texts['gr'].tank_1 = "Резервоар - 1";
texts['gr'].tank_1 = "Δεξαμενή - 1";

texts['bg'].tank_2 = "Резервоар - 2";
texts['en'].tank_2 = "Fuel tank - 2";
texts['de'].tank_2 = "";
texts['ro'].tank_2 = "Rezervor - 2";
texts['es'].tank_2 = "Tanque - 2";
texts['sr'].tank_2 = "Rezervoar - 2";
texts['gr'].tank_2 = "Δεξαμενή - 2";

texts['bg'].check_rows_print_export = "Отметнете редовете, които искате да принтирате и/или експотнете.";
texts['en'].check_rows_print_export = "Mark the rows you wish to print or export.";
texts['de'].check_rows_print_export = "";
texts['ro'].check_rows_print_export = "Selectați rândurile, pe care doriți să le printați și/sau exportați";
texts['es'].check_rows_print_export = "Seleccione las filas que desea imprimir o exportar";
texts['sr'].check_rows_print_export = "Označite redove koje želite da odštampate i/ili izvezete.";
texts['gr'].check_rows_print_export = "Επιλέξτε τις σειρές που θέλετε να εκτυπώσετε ή να εξαγάγετε.";

texts['bg'].work_time_period_norm = "Период на работа по зададена норма";
texts['en'].work_time_period_norm = "Work time period based on norm";
texts['de'].work_time_period_norm = "";
texts['ro'].work_time_period_norm = "Perioadă de lucrare după norm a setată";
texts['es'].work_time_period_norm = "Período de trabajo sobre una norma determinada ";
texts['sr'].work_time_period_norm = "Vreme rada na osnovu norme ";
texts['gr'].work_time_period_norm = "Περίοδος εργασίας που βασίζεται στον κανόνα";

texts['bg'].under_norm_work_duration = "Работа под норма";
texts['en'].under_norm_work_duration = "Work under norm";
texts['de'].under_norm_work_duration = "";
texts['ro'].under_norm_work_duration = "Lucrare conform normă setată";
texts['sr'].under_norm_work_duration = "Rad ispod norme";
texts['gr'].under_norm_work_duration = "Εργασία σύμφωνα με τον κανόνα";

texts['bg'].under_norm_work_consumption = "Разход под норма";
texts['en'].under_norm_work_consumption = "Consumption under norm";
texts['de'].under_norm_work_consumption = "";
texts['ro'].under_norm_work_consumption = "Consum conform normă setată";
texts['sr'].under_norm_work_consumption = "Potrošnja ispod norme";
texts['gr'].under_norm_work_consumption = "Κατανάλωση σύμφωνα με τον κανόνα";

texts['bg'].under_norm_work_duration_consumption = "Работа под норма<br>Продължителност / разход (л)";
texts['en'].under_norm_work_duration_consumption = "Work under norm<br>Duration / consumption (l)";
texts['de'].under_norm_work_duration_consumption = "";
texts['ro'].under_norm_work_duration_consumption = "Lucrare conform normă setată <br>Durată / consum (l) ";
texts['es'].under_norm_work_duration_consumption = "Rendimiento bajo la norma <br>Duración / consumo (l) ";
texts['sr'].under_norm_work_duration_consumption = "Rad ispod norme <br>Trajanje / potrošnja (l) ";
texts['gr'].under_norm_work_duration_consumption = "Εργασία κάτω από τον κανόνα <br> Διάρκεια / κόστος (λ)";


texts['bg'].zero_revolutions_work_duration = "Работа при празен ход";
texts['en'].zero_revolutions_work_duration = "Work for idling";
texts['de'].zero_revolutions_work_duration = "";
texts['ro'].zero_revolutions_work_duration = "";
texts['sr'].zero_revolutions_work_duration = "Rad pri mirovanju";
texts['gr'].zero_revolutions_work_duration = "Αδράνεια εργασίας";

texts['bg'].zero_revolutions_work_consumption = "Разход при празен ход";
texts['en'].zero_revolutions_work_consumption = "Consumption when idle";
texts['de'].zero_revolutions_work_consumption = "";
texts['ro'].zero_revolutions_work_consumption = "";
texts['sr'].zero_revolutions_work_consumption = "Potrošnja pri mirovanju";
texts['gr'].zero_revolutions_work_consumption = "Κατανάλωση σε αδράνεια";

texts['bg'].zero_revolutions_work_duration_consumption = "Работа при празен ход<br>Продължителност / разход (л)";
texts['en'].zero_revolutions_work_duration_consumption = "Work for idling<br>Duration / consumption (l)";
texts['de'].zero_revolutions_work_duration_consumption = "";
texts['ro'].zero_revolutions_work_duration_consumption = "Locuri de muncă inactive<br>Durata / cheltuială (l) ";
texts['es'].zero_revolutions_work_duration_consumption = "Trabajo de ralentí<br>Duración / consumo (l) ";
texts['sr'].zero_revolutions_work_duration_consumption = "Rad pri mirovanju<br>Trajanje / potrošnja (l) ";
texts['gr'].zero_revolutions_work_duration_consumption = "Αδράνεια εργασίας<br>Διάρκεια / κόστος (λ)";


texts['bg'].load_work_duration = "Работа при натоварване";
texts['en'].load_work_duration = "Work for loading";
texts['de'].load_work_duration = "";
texts['ro'].load_work_duration = "";
texts['sr'].load_work_duration = "Rad pri utovaru";
texts['gr'].load_work_duration = "Φόρτωση εργασίας";

texts['bg'].load_work_consumption = "Разход при натоварване";
texts['en'].load_work_consumption = "Consumption for loading";
texts['de'].load_work_consumption = "";
texts['ro'].load_work_consumption = "";
texts['sr'].load_work_consumption = "Potrošnja pri utovaru";
texts['gr'].load_work_consumption = "Φόρτωση κατανάλωσης";

texts['bg'].load_work_duration_consumption = "Работа при натоварване<br>Продължителност / разход (л)";
texts['en'].load_work_duration_consumption = "Work for loading<br>Duration / consumption (l)";
texts['de'].load_work_duration_consumption = "";
texts['ro'].load_work_duration_consumption = "Lucrare in sarcină<br>Durată / consum(l)";
texts['es'].load_work_duration_consumption = "Trabajo en modo ocupado<br>Duración / consumo(l)";
texts['sr'].load_work_duration_consumption = "Rad pri utovaru<br>Trajanje / potrošnja(l)";
texts['gr'].load_work_duration_consumption = "Φόρτωση εργασίας<br>Διάρκεια / κόστος (λ)";

texts['bg'].over_norm_work_duration = "Работа над норма";
texts['en'].over_norm_work_duration = "Work over norm";
texts['de'].over_norm_work_duration = "";
texts['ro'].over_norm_work_duration = "";
texts['sr'].over_norm_work_duration = "Rad preko norme";
texts['gr'].over_norm_work_duration = "Εργασία πάνω κανόνας";

texts['bg'].over_norm_work_consumption = "Разход над норма";
texts['en'].over_norm_work_consumption = "Consumption over norm";
texts['de'].over_norm_work_consumption = "";
texts['ro'].over_norm_work_consumption = "";
texts['sr'].over_norm_work_consumption = "Potrošnja preko norme";
texts['gr'].over_norm_work_consumption = "Κατανάλωση πάνω κανόνας";

texts['bg'].over_norm_work_duration_consumption = "Работа над норма<br>Продължителност / разход (л)";
texts['en'].over_norm_work_duration_consumption = "Work over norm<br>Duration / consumption (l)";
texts['de'].over_norm_work_duration_consumption = "";
texts['ro'].over_norm_work_duration_consumption = "Lucru peste program <br> Durata/ Consum (l)";
texts['es'].over_norm_work_duration_consumption = "Trabajo sobre la norma <br> Duración/ consumo (l)";
texts['sr'].over_norm_work_duration_consumption = "Rad preko norme <br> Trajanje/ potrošnja (l)";
texts['gr'].over_norm_work_duration_consumption = "Εργασία πάνω κανόνας<br>Διάρκεια / κόστος (λ)";

texts['bg'].summery_data = "Обобщени данни";
texts['en'].summery_data = "Summery data";
texts['de'].summery_data = "";
texts['ro'].summery_data = "Rezumat Date";
texts['es'].summery_data = "Datos resumen";
texts['sr'].summery_data = "Rezime podataka";
texts['gr'].summery_data = "Συνοπτικά δεδομένα";

texts['bg'].dynamic_fuel_consumption_period = "Динамичен разход на гориво за периода";
texts['en'].dynamic_fuel_consumption_period = "Dynamic fuel consumption for period";
texts['de'].dynamic_fuel_consumption_period = "";
texts['ro'].dynamic_fuel_consumption_period = "Consum dinamic combustibilului pentru perioadă";
texts['es'].dynamic_fuel_consumption_period = "Consumo dinámico de combustible para el periodo";
texts['sr'].dynamic_fuel_consumption_period = "Dinamična potrošnja goriva za period";
texts['gr'].dynamic_fuel_consumption_period = "Δυναμική κατανάλωση καυσίμου για την περίοδο";

texts['bg'].days_of_month = "Номерация на дните за избрания месец";
texts['en'].days_of_month = "Days of month";
texts['de'].days_of_month = "";
texts['ro'].days_of_month = "Numarul de zile pentru luna alesă";
texts['es'].days_of_month = "Numeración de los días para el mes seleccionado";
texts['sr'].days_of_month = "Dani u mesecu";
texts['gr'].days_of_month = "Αρίθμηση ημερών για τον επιλεγμένο μήνα";

texts['bg'].route_pattern_match_selection = "Избор на шаблон за сравнение с маршрута на превозно средство";
texts['en'].route_pattern_match_selection = "Route pattern match selection";
texts['de'].route_pattern_match_selection = "";
texts['ro'].route_pattern_match_selection = "Alegerea unui șablon/modelul pentru comparație cu traseul vehiculului";
texts['es'].route_pattern_match_selection = "Elección de un patrón para comparación con la ruta";
texts['sr'].route_pattern_match_selection = "Izbor šablona za poređenje sa maršrutom";
texts['gr'].route_pattern_match_selection = "Επιλέξτε ένα πρότυπο για σύγκριση με τη διαδρομή ενός οχήματος";

texts['bg'].start_point = "Начална точка";
texts['en'].start_point = "Start point";
texts['de'].start_point = "";
texts['ro'].start_point = "Punct de plecare";
texts['es'].start_point = "Punto de inicio";
texts['sr'].start_point = "Početna tačka";
texts['gr'].start_point = "Σημείο εκκίνησης";

texts['bg'].end_point = "Крайна точка";
texts['en'].end_point = "End point";
texts['de'].end_point = "";
texts['ro'].end_point = "Punct final";
texts['es'].end_point = "Punto de final";
texts['sr'].end_point = "Krajnja tačka";
texts['gr'].end_point = "Τελικού σημείου";

texts['bg'].range_m = "Обхват (м)";
texts['en'].range_m = "Range (m)";
texts['de'].range_m = "";
texts['ro'].range_m = "Semnal(m)";
texts['es'].range_m = "Alcance(m)";
texts['sr'].range_m = "Opseg(m)";
texts['gr'].range_m = "Εύρος(μ)";

texts['bg'].range_m = "Обхват (м)";
texts['en'].range_m = "Range (m)";
texts['de'].range_m = "";
texts['ro'].range_m = "Semnal(m)";
texts['es'].range_m = "Alcance(m)";
texts['sr'].range_m = "Opseg(m)";
texts['gr'].range_m = "Εύρος(μ)";

texts['bg'].regulated_stops = "Регламентирани престои";
texts['en'].regulated_stops = "Regulated stops";
texts['de'].regulated_stops = "";
texts['ro'].regulated_stops = "Perioade de opriri reglementate";
texts['es'].regulated_stops = "Paradas reglamentadas";
texts['sr'].regulated_stops = "Regulisane pauze";
texts['gr'].regulated_stops = "Κανονισμένες διαμονές";

texts['bg'].click_here_to_make_report_for_this_event = "Натиснете тук за да направите справка за това събитие";
texts['en'].click_here_to_make_report_for_this_event = "Click here to create a report based on this event";
texts['de'].click_here_to_make_report_for_this_event = "";
texts['ro'].click_here_to_make_report_for_this_event = "Apasați aici ca să faceți raport pentru acest eveniment";
texts['es'].click_here_to_make_report_for_this_event = "Haga clic para preparar un informe de este evento";
texts['sr'].click_here_to_make_report_for_this_event = "Kliknite ovde da napravite izveštaj za ovaj događaj";
texts['gr'].click_here_to_make_report_for_this_event = "Πατήστε εδώ για να δημιουργήσετε μια αναφορά με βάση αυτό το συμβάν";

texts['bg'].no_zero_revolution_accumulator_norm = "Превозното средство, което сте избрали няма норма за максимална стойност на акумулатора при невъртящ момент!\nИскате ли да зададете норма?";
texts['en'].no_zero_revolution_accumulator_norm = "The vehicle that you have chosen does not have a norm for maximum accumulator value when the engine is idle!\nWould you like to set a norm?";
texts['de'].no_zero_revolution_accumulator_norm = "";
texts['ro'].no_zero_revolution_accumulator_norm = "Vehiculul, pe care l-ați ales nu are normă pentru valoare maxima a acumulatorului când motorul este oprit.\nDoriți să stabiliți o normă ?";
texts['es'].no_zero_revolution_accumulator_norm = "El vehículo seleccionado no hay valor maximo definido del acumulador en estado de inactividad del motor/alternador.\n¿Quieres hacer una norma?";
texts['sr'].no_zero_revolution_accumulator_norm = "Vozilo koje ste izabrali nema normu za maksimalnu vrednost akumulatora kada je motor u stanju mirovanja!\nDa li želite da postavite normu?";
texts['gr'].no_zero_revolution_accumulator_norm = "Το όχημα που έχετε επιλέξει δεν έχει πρότυπο για μέγιστη τιμή συσσωρευτή όταν ο κινητήρας είναι σε αδρανία!\nΘέλετε να ορίσετε  έναν κανόνα?";

texts['bg'].admin_no_zero_revolution_accumulator_norm = "Администраторът на това превозно средство не е задал норма за максимална стойност на акумулатора при невъртящ момент.\nИскате ли да зададете норма вместо него?";
texts['en'].admin_no_zero_revolution_accumulator_norm = "The administrator of this vehicle has not entered a norm for maximum accumulator value when the engine is idle!\nWould you like to set a norm?";
texts['de'].admin_no_zero_revolution_accumulator_norm = "";
texts['ro'].admin_no_zero_revolution_accumulator_norm = "Administratorul acestui vehicul nu a stabilit o normă de valoare maximă a acumulatorului pentru vehicul oprit!\nDoriți sa stabiliți o normă? ";
texts['es'].admin_no_zero_revolution_accumulator_norm = "¡El administrador de este vehículo no ha establecido una norma para valor maximo definido del acumulador en estado de inactividad del motor/alternador!\n¿Quieres hacer una norma? ";
texts['sr'].admin_no_zero_revolution_accumulator_norm = "Administrator ovog vozila nije uneo normu za maksimalnu vrednost akumulatora kada je motor u stanju mirovanja!\nDa li želite da postavite normu umesto njega? ";
texts['gr'].admin_no_zero_revolution_accumulator_norm = "Ο διαχειριστής αυτού του οχήματος δεν έχει εισαγάγει πρότυπο για μέγιστη τιμή συσσωρευτή όταν ο κινητήρας είναι σε αδράνεια!\nΘέλετε να ορίσετε έναν κανόνα?";

texts['bg'].select_event = "Изберете измежду наличните събития";
texts['en'].select_event = "Select one of the following events";
texts['de'].select_event = "";
texts['ro'].select_event = "Alegeți dintre evenimentele disponibile";
texts['es'].select_event = "Seleccione entre los eventos disponibles";
texts['sr'].select_event = "Izaberite jedan od sledećih događaja";
texts['gr'].select_event = "Επιλέξτε μεταξύ των διαθέσιμων συμβάντων";

texts['bg'].hide = "Скриване";
texts['en'].hide = "Hide";
texts['de'].hide = "";
texts['ro'].hide = "Ascunde";
texts['es'].hide = "Ocultar";
texts['sr'].hide = "Sakrij";
texts['gr'].hide = "Απόκρυψη";

texts['bg'].workingTime_kilometres = "Часове работа и километри";
texts['en'].workingTime_kilometres = "Working time and kilometres";
texts['de'].workingTime_kilometres = "";
texts['ro'].workingTime_kilometres = "Ore de lucru și kilometrii";
texts['es'].workingTime_kilometres = "Horas de trabajo y kilómetros";
texts['sr'].workingTime_kilometres = "Vreme rada i kilometri";
texts['gr'].workingTime_kilometres = "Ώρες εργασίας και χιλιόμετρα";

texts['bg'].additional_workingTime_kilometres = "Д-ни часове и километри";
texts['en'].additional_workingTime_kilometres = "Overwork and kilometres";
texts['de'].additional_workingTime_kilometres = "";
texts['ro'].additional_workingTime_kilometres = "Date ore și kilometrii";
texts['es'].additional_workingTime_kilometres = "Horas extras  y kilómetros";
texts['sr'].additional_workingTime_kilometres = "Prekovremeni rad i kilometri";
texts['gr'].additional_workingTime_kilometres = "Επιπλέον ώρες και χιλιόμετρα";

texts['bg'].add_repair = "Добавяне на ремонт";
texts['en'].add_repair = "Add new repair";
texts['de'].add_repair = "";
texts['ro'].add_repair = "Adăugare repărare nouă";
texts['es'].add_repair = "Añadir reparación";
texts['sr'].add_repair = "Dodaj novu popravku";
texts['gr'].add_repair = "Προσθέστε επισκευή";

texts['bg'].group_by_vehicles = "Групиране по превозно средство";
texts['en'].group_by_vehicles = "Group by vehicle";
texts['de'].group_by_vehicles = "";
texts['ro'].group_by_vehicles = "Grupează după vehicule";
texts['es'].group_by_vehicles = "Agrupar por vehículo";
texts['sr'].group_by_vehicles = "Grupiši po vozilima";
texts['gr'].group_by_vehicles = "Ομαδοποίηση ανά όχημα";

texts['bg'].add_oil_change = "Нова смяна на масло";
texts['en'].add_oil_change = "Add new oil change";
texts['de'].add_oil_change = "";
texts['ro'].add_oil_change = "Adaugă schimb nou de ulei";
texts['es'].add_oil_change = "Añadir nuevo cambio de aceite";
texts['sr'].add_oil_change = "Dodaj novu zamenu ulja";
texts['gr'].add_oil_change = "Νέα αλλαγή λαδιού";

texts['bg'].add_new_civil_insurance = "Нова гражданска застраховка";
texts['en'].add_new_civil_insurance = "Add a new civil liability insurance";
texts['de'].add_new_civil_insurance = "";
texts['ro'].add_new_civil_insurance = "Asigurare o nouă asigurare de raspundere civilă";
texts['es'].add_new_civil_insurance = "Añadir nuevo seguro civil";
texts['sr'].add_new_civil_insurance = "Dodaj novo osiguranje građana od odgovornosti";
texts['gr'].add_new_civil_insurance = "Νέα αστική ασφάλιση";

texts['bg'].error_start_end_date = "ГРЕШКА! Крайната дата не може да е преди началната.";
texts['en'].error_start_end_date = "ERROR! End date must be bigger than start date.";
texts['de'].error_start_end_date = "";
texts['ro'].error_start_end_date = "Eroare! Data finală trebuie să fi mai mare decat ce de la debut ";
texts['es'].error_start_end_date = "¡UN ERROR! La fecha de final no puede ser antes de fecha de inicio ";
texts['sr'].error_start_end_date = "GREŠKA! Krajnji datum mora biti veći od početnog. ";
texts['gr'].error_start_end_date = "ΣΦΑΛΜΑ! Η ημερομηνία λήξης δεν μπορεί να είναι πριν από την ημερομηνία έναρξης.";

texts['bg'].add_new_autokasko = "Ново автокаско";
texts['en'].add_new_autokasko = "Add new vehicle insurance";
texts['de'].add_new_autokasko = ""; 
texts['ro'].add_new_autokasko = "Adaugă o nouă asigurare"; 
texts['es'].add_new_autokasko = "Añadir nuevo seguro";
texts['sr'].add_new_autokasko = "Dodaj novo osiguranje vozila";
texts['gr'].add_new_autokasko = "Νέα ασφάλεια οχημάτων";

texts['bg'].add_new_vignette = "Добавяне на нова винетка";
texts['en'].add_new_vignette = "Add new vignette";
texts['de'].add_new_vignette = ""; 
texts['ro'].add_new_vignette = "Adăugare de rovinetă nouă"; 
texts['es'].add_new_vignette = "Añadir nuevo telepeaje"; 
texts['sr'].add_new_vignette = "Dodaj novu vinjetu"; 
texts['gr'].add_new_vignette = "Προσθήκη νεα βινιέτα";

texts['bg'].add_new_gtp = "Нов Г.Т.П.";
texts['en'].add_new_gtp = "Add new A.T.R.";
texts['de'].add_new_gtp = ""; 
texts['ro'].add_new_gtp = "Adăugați ITP nou"; 
texts['es'].add_new_gtp = "Añadir nuevo I.T.V.";
texts['sr'].add_new_gtp = "Dodaj novi A.D.R.";
texts['gr'].add_new_gtp = "Προσθήκη  Κ.Τ.Ε.Ο.";

texts['bg'].add_new_tachometer = "Нова заверка на тахограф";
texts['en'].add_new_tachometer = "Add new tachometer validation";
texts['de'].add_new_tachometer = ""; 
texts['ro'].add_new_tachometer = "Agăugați verificare nouă a tahografului"; 
texts['es'].add_new_tachometer = "Nueva certificación de tacógrafo"; 
texts['sr'].add_new_tachometer = "Dodaj novu potvrdu tahografa"; 
texts['gr'].add_new_tachometer = "Νέα πιστοποίηση ταχογράφων";

texts['bg'].add_new_tire_change = "Нова смяна на гума";
texts['en'].add_new_tire_change = "Add new tire change";
texts['de'].add_new_tire_change = ""; 
texts['ro'].add_new_tire_change = "Agăugați schimbare nouă a anvelopelor"; 
texts['es'].add_new_tire_change = "Añadir nuevo cambio de neumaticos";
texts['sr'].add_new_tire_change = "Dodaj novu zamenu gume";
texts['gr'].add_new_tire_change = "Νέα αλλαγή ελαστικών";

texts['bg'].tire_change_index = "Сменена гума";
texts['en'].tire_change_index = "Tire type";
texts['de'].tire_change_index = ""; 
texts['ro'].tire_change_index = "Anvelopa schimbată";
texts['es'].tire_change_index = "El neumático se ha sustituido";
texts['sr'].tire_change_index = "Guma je zamenjena";
texts['gr'].tire_change_index = "Αλλαγμένο ελαστικό ";

texts['bg'].confirmation = "Потвърждение";
texts['en'].confirmation = "Confirmation";
texts['de'].confirmation = ""; 
texts['ro'].confirmation = "Confirmare"; 
texts['es'].confirmation = "Conformación";
texts['sr'].confirmation = "Potvrda";
texts['gr'].confirmation = "Επιβεβαίωση";

texts['bg'].confirm_delete = "Потвърдете изтриването";
texts['en'].confirm_delete = "Confirm the deletion";
texts['de'].confirm_delete = ""; 
texts['ro'].confirm_delete = "Confirmați ștergerea"; 
texts['es'].confirm_delete = "Confirmar eliminación";
texts['sr'].confirm_delete = "Potvrdite brisanje";
texts['gr'].confirm_delete = "Επιβεβαιώστε τη διαγραφή";

texts['bg'].vehicle_start_of_parking_definition = "Дефиниция за начало на паркинг на превозно средство";
texts['en'].vehicle_start_of_parking_definition = "Definition of when the vehicle is on parking";
texts['de'].vehicle_start_of_parking_definition = ""; 
texts['ro'].vehicle_start_of_parking_definition = "Definește când vehiculul este in parcare."; 
texts['es'].vehicle_start_of_parking_definition = "Definición comienzo de estacionar un vehículo."; 
texts['sr'].vehicle_start_of_parking_definition = "Definicija za početak parkiranja vozila."; 
texts['gr'].vehicle_start_of_parking_definition = "Ορισμός του χρόνου στάθμευσης του οχήματος";

texts['bg'].double_quote_error = "Използвайте единични кавички вместо двойни!";
texts['en'].double_quote_error = "Use single quotes!";
texts['de'].double_quote_error = ""; 
texts['ro'].double_quote_error = "Folosiți ghilimele simple în loc de dublu";
texts['es'].double_quote_error = "Use comillas simples";
texts['sr'].double_quote_error = "Koristite jednostruke navodnike!";
texts['gr'].double_quote_error = "Χρησιμοποιήστε μονά εισαγωγικά αντί διπλα!";

texts['bg'].litres_hour_working_mode_non_working_mode = "Литри(ч) п.х. / р.р.";
texts['en'].litres_hour_working_mode_non_working_mode = "Litres(h) - modes";
texts['de'].litres_hour_working_mode_non_working_mode = ""; 
texts['ro'].litres_hour_working_mode_non_working_mode = "Litri(oră)";
texts['es'].litres_hour_working_mode_non_working_mode = "Litros(h)";
texts['sr'].litres_hour_working_mode_non_working_mode = "Litri(h)";
texts['gr'].litres_hour_working_mode_non_working_mode = "Λίτρα (ω)";

texts['bg'].end_bofore_start_work_time_error = "Началото на работното време не може да е след края на работното време!";
texts['en'].end_bofore_start_work_time_error = "The beginning of the work time must be before the end of the work time!";
texts['de'].end_bofore_start_work_time_error = ""; 
texts['ro'].end_bofore_start_work_time_error = "Începutul orei de lucru nu poate fi pus după sfârșitul timpului de lucru"; 
texts['es'].end_bofore_start_work_time_error = "¡El inicio del tiempo laboral no puede ser después de final de la jornada laboral!";
texts['sr'].end_bofore_start_work_time_error = "Početak vremena rada mora biti pre kraja vremena rada!";
texts['gr'].end_bofore_start_work_time_error = "Η έναρξη του εργασιακού χρόνου δεν μπορεί να είναι μετά το τέλος του χρόνου εργασίας!";

texts['bg'].end_bofore_start_work_time_error2 = "Края на работното време не може да е преди началото на работното време!";
texts['en'].end_bofore_start_work_time_error2 = "The end of the work time must be after the beginning of the work time!";
texts['de'].end_bofore_start_work_time_error2 = ""; 
texts['ro'].end_bofore_start_work_time_error2 = "Sfârșitul timpului de lucru trebuie sa fie după începutul timpului de lucru"; 
texts['es'].end_bofore_start_work_time_error2 = "El final del tiempo laboral no puede ser antes de inicio de la jornada laboral"; 
texts['sr'].end_bofore_start_work_time_error2 = "Kraj vremena rada mora biti posle početka vremena rada!"; 
texts['gr'].end_bofore_start_work_time_error2 = "Το τέλος του χρόνου εργασίας δεν μπορεί να είναι πριν από την έναρξη του χρόνου εργασίας!";

texts['bg'].input_time_format_tooltip = "Формат на въвеждане: чч:мм:сс";
texts['en'].input_time_format_tooltip = "Input format: hh:mm:ss";
texts['de'].input_time_format_tooltip = ""; 
texts['ro'].input_time_format_tooltip = "Format de introducere: hh:mm:ss"; 
texts['es'].input_time_format_tooltip = "Formato de introducir: hh:mm:ss"; 
texts['sr'].input_time_format_tooltip = "Format unosa: hh:mm:ss"; 
texts['gr'].input_time_format_tooltip = "Μορφή εισόδου: ωω : λλ : δδ";

texts['bg'].apply_value_to_all_vehicles_tooltip = "Приложи въведената стойност за всички превозни средства.";
texts['en'].apply_value_to_all_vehicles_tooltip = "Apply this value to all vehicles.";
texts['de'].apply_value_to_all_vehicles_tooltip = ""; 
texts['ro'].apply_value_to_all_vehicles_tooltip = "Aplicați valoare introdusă pentru toate vehicule"; 
texts['es'].apply_value_to_all_vehicles_tooltip = "Aplicar la valor introducida para todos los vehículos"; 
texts['sr'].apply_value_to_all_vehicles_tooltip = "Dodelite ovu vrednost svim vozilima"; 
texts['gr'].apply_value_to_all_vehicles_tooltip = "Εφαρμόστε την τιμή που έχει εισαχθεί για όλα τα οχήματα.";

texts['bg'].id_driver_associated_error = "Това Id вече е асоциирано с водач!";
texts['en'].id_driver_associated_error = "This Id is already associated with a driver!";
texts['de'].id_driver_associated_error = ""; 
texts['ro'].id_driver_associated_error = "Acest Id este asocializat cu un șofer";
texts['es'].id_driver_associated_error = "Este ID ya no esta asociado con un conductor";
texts['sr'].id_driver_associated_error = "Ovaj Id je već povezan sa vozačem!";
texts['gr'].id_driver_associated_error = "Αυτή η ταυτότητα έχει ήδη συσχετιστεί με οδηγό!";

texts['bg'].id_associated_error = "Това Id вече е асоциирано!";
texts['en'].id_associated_error = "This Id is currently mapped!";
texts['de'].id_associated_error = ""; 
texts['ro'].id_associated_error = "Acest Id este asocializat";
texts['es'].id_associated_error = "Este ID ya no esta asociado";
texts['sr'].id_associated_error = "Ovaj Id je već povezan!";
texts['gr'].id_associated_error = "Αυτή η ταυτότητα έχει ήδη συσχετιστεί!";

texts['bg'].form_driver_card_id_association = "Формуляр за асоцииране на магнитна карта с водач.";
texts['en'].form_driver_card_id_association = "Magnetic card id to driver mapping form";
texts['de'].form_driver_card_id_association = ""; 
texts['ro'].form_driver_card_id_association = "Asociera cartelă magnetică ID cu șoferul";
texts['es'].form_driver_card_id_association = "Asociar una tarjeta magnética con conductor";
texts['sr'].form_driver_card_id_association = "Obrazac za povezivanje magnetne kartice sa vozačem.";
texts['gr'].form_driver_card_id_association = "Φόρμα σύνδεσης κάρτας μαγνητικής  με οδηγού.";

texts['bg'].magnetic_card_id = "Id на магнитна карта";
texts['en'].magnetic_card_id = "Magnetic card id";
texts['de'].magnetic_card_id = ""; 
texts['ro'].magnetic_card_id = "Cartelă Magnetică ";
texts['es'].magnetic_card_id = "ID de tarjeta magnética ";
texts['sr'].magnetic_card_id = "ID magnetna kartica ";
texts['gr'].magnetic_card_id = "Id μαγνητική κάρτα";

texts['bg'].existing_magnetic_card_id_error = "Това Id вече е асоциирано с водач на име"; //imeto na voda4a se slaga dinami4no
texts['en'].existing_magnetic_card_id_error = "This id is already associated with driver named";
texts['de'].existing_magnetic_card_id_error = ""; 
texts['ro'].existing_magnetic_card_id_error = "Acest Id este deja asocializat cu numele șoferului"; 
texts['es'].existing_magnetic_card_id_error = "Este ID ya esta asociado con conductor";
texts['sr'].existing_magnetic_card_id_error = "Ovaj ID je već povezan sa imenom vozača";
texts['gr'].existing_magnetic_card_id_error = "Αυτό το Id έχει ήδη συσχετιστεί με ένα όνομα οδηγού "; 

texts['bg'].incorrect_asc_gps_signal_error = "Степента на нарастване не е коректна!";
texts['en'].incorrect_asc_gps_signal_error = "Incorrect ascending data entered!";
texts['de'].incorrect_asc_gps_signal_error = ""; 
texts['ro'].incorrect_asc_gps_signal_error = "Datele recepționate incoerente";
texts['es'].incorrect_asc_gps_signal_error = "Los datos recibidos son incorrectos";
texts['sr'].incorrect_asc_gps_signal_error = "Uneti su netačni rastući podaci";
texts['gr'].incorrect_asc_gps_signal_error = "Έχουν εισαχθεί εσφαλμένα δεδομένα ανόδου!";

texts['bg'].event = "Събитие";
texts['en'].event = "Event";
texts['de'].event = ""; 
texts['ro'].event = "Eveniment"; 
texts['es'].event = "Eventos"; 
texts['sr'].event = "Događaj"; 
texts['gr'].event = "Συμβάν";

texts['bg']._3_15_chars_incorrect_name_error = "Името трябва да бъде между 3 и 15 символа!";
texts['en']._3_15_chars_incorrect_name_error = "The name must be between 3 and 15 characters long!";
texts['de']._3_15_chars_incorrect_name_error = ""; 
texts['ro']._3_15_chars_incorrect_name_error = "Numele trebuie să fie între 3 și 15 simbole"; 
texts['es']._3_15_chars_incorrect_name_error = "El nombre debe tener entre 3 y 15 caracteres!";
texts['sr']._3_15_chars_incorrect_name_error = "Ime mora da sadrži između 3 i 15 karaktera!";
texts['gr']._3_15_chars_incorrect_name_error = "Το όνομα πρέπει να είναι μεταξύ 3 και 15 χαρακτήρων!";

texts['bg'].notification_period = "Период за известяване";
texts['en'].notification_period = "Notification period";
texts['de'].notification_period = ""; 
texts['ro'].notification_period = "Perioada de notificare"; 
texts['es'].notification_period = "Período de notificación "; 
texts['sr'].notification_period = "Period za obaveštenja "; 
texts['gr'].notification_period = "Περίοδος ειδοποίησης";

texts['bg'].notification_type = "Тип на известие";
texts['en'].notification_type = "Notification type";
texts['de'].notification_type = ""; 
texts['ro'].notification_type = "Tip de notificare"; 
texts['es'].notification_type = "Tipo de notificación ";
texts['sr'].notification_type = "Tip obaveštenja ";
texts['gr'].notification_type = "Είδος ειδοποίησης";

texts['bg'].notification_target = "Таргет за получаване на известие";
texts['en'].notification_target = "Notification target settings";
texts['de'].notification_target = ""; 
texts['ro'].notification_target = "Vizați pentru a primi accept "; 
texts['es'].notification_target = "Configuración de destino de notificación ";
texts['sr'].notification_target = "Podešavanja za ciljna obaveštenja";
texts['gr'].notification_target = "Ρυθμίσεις στόχου ειδοποίησης";

texts['bg'].c_inactive = "Неактивно";
texts['en'].c_inactive = "Inactive";
texts['de'].c_inactive = ""; 
texts['ro'].c_inactive = "Inactiv"; 
texts['es'].c_inactive = "Inactivo";
texts['sr'].c_inactive = "Neaktivno";
texts['gr'].c_inactive = "Αδρανής";

texts['bg'].fill_value_above = "Попълни нагоре с тази стойност";
texts['en'].fill_value_above = "Fill above with this value";
texts['de'].fill_value_above = ""; 
texts['ro'].fill_value_above = "Completați mai sus cu această valoare"; 
texts['es'].fill_value_above = "Rellenar arriba con ese valor";
texts['sr'].fill_value_above = "Popuni iznad ovom vrednošću";
texts['gr'].fill_value_above = "Συμπληρώστε παραπάνω με αυτήν την τιμή";

texts['bg'].fill_value_all = "Попълни всичко с тази стойност";
texts['en'].fill_value_all = "Fill all with this value";
texts['de'].fill_value_all = "";
texts['ro'].fill_value_all = "Completa totul cu această valoare";
texts['es'].fill_value_all = "Rellenar todo con ese valor";
texts['sr'].fill_value_all = "Popuni sve ovom vrednošću";
texts['gr'].fill_value_all = "Συμπληρώστε όλα  με αυτήν την τιμή";

texts['bg'].fill_value_below = "Попълни надолу с тази стойност";
texts['en'].fill_value_below = "Fill below with this value";
texts['de'].fill_value_below = "";
texts['ro'].fill_value_below = "Completați mai jos cu această valoare";
texts['es'].fill_value_below = "Rellenar abajo con ese valor";
texts['sr'].fill_value_below = "Popuni ispod ovom vrednošću";
texts['gr'].fill_value_below = "Συμπληρώστε παρακάτω με αυτήν την τιμή";

texts['bg'].speed_value = "Стойност (км/ч)";
texts['en'].speed_value = "Value (km/h)";
texts['de'].speed_value = ""; 
texts['ro'].speed_value = "Valoare (km/h)"; 
texts['es'].speed_value = "Valor (km/h)"; 
texts['sr'].speed_value = "Vrednost (km/h)";
texts['gr'].speed_value = "Τιμή (χλμ/ω)";

texts['bg'].min_time = "Мин. време";
texts['en'].min_time = "Min. time";
texts['de'].min_time = ""; 
texts['ro'].min_time = "Timp minim "; 
texts['es'].min_time = "Tiempo mínimo ";
texts['sr'].min_time = "Min. vreme ";
texts['gr'].min_time = "Ελάχιστος χρόνος";

texts['bg'].min_distance = "Мин. разст.(м)";
texts['en'].min_distance = "Min. dist.(m)";
texts['de'].min_distance = ""; 
texts['ro'].min_distance = "Distanță minimă"; 
texts['es'].min_distance = "Dist. mínima (m) ";
texts['sr'].min_distance = "Min. rast.(m) ";
texts['gr'].min_distance = "Ελάχιστη απόσταση(μ)";

texts['bg'].invalid_data_entered = "Въвели сте навилидни данни!";
texts['en'].invalid_data_entered = "Invalid data entered!";
texts['de'].invalid_data_entered = ""; 
texts['ro'].invalid_data_entered = "Ați introdus date invalide"; 
texts['es'].invalid_data_entered = "Se han introducido datos no válidos";
texts['sr'].invalid_data_entered = "Uneli ste nevažeće podatke!";
texts['gr'].invalid_data_entered = "Έχετε εισάγει μη έγκυρα δεδομένα!";

texts['bg'].c_below = "Под";
texts['en'].c_below = "Below";
texts['de'].c_below = ""; 
texts['ro'].c_below = ""; 
texts['es'].c_below = "Abajo de"; 
texts['sr'].c_below = "Ispod"; 
texts['gr'].c_below = "Παρακάτω";

texts['bg'].c_under = "Под";
texts['en'].c_under = "Under";
texts['de'].c_under = ""; 
texts['ro'].c_under = "Sub"; 
texts['es'].c_under = "Abajo";
texts['sr'].c_under = "Ispod";
texts['gr'].c_under = "Κάτω από";

texts['bg'].c_over = "Над";
texts['en'].c_over = "Over";
texts['de'].c_over = ""; 
texts['ro'].c_over = "Peste"; 
texts['es'].c_over = "Encima";
texts['sr'].c_over = "Iznad";
texts['gr'].c_over = "Πάνω";

texts['bg'].poi_entrance = "Вход в обекта";
texts['en'].poi_entrance = "Entrance";
texts['de'].poi_entrance = ""; 
texts['ro'].poi_entrance = "Întrare"; 
texts['es'].poi_entrance = "Entrada";
texts['sr'].poi_entrance = "Ulaz";
texts['gr'].poi_entrance = "Είσοδος";

texts['bg'].poi_exit = "Изход от обекта";
texts['en'].poi_exit = "Exit ";
texts['de'].poi_exit = ""; 
texts['ro'].poi_exit = "Ieșire"; 
texts['es'].poi_exit = "Salida"; 
texts['sr'].poi_exit = "Izlaz"; 
texts['gr'].poi_exit = "Εξοδος";

texts['bg'].add_new_email_tooltip = "Натиснете тук за да добавите нов е-майл адрес.";
texts['en'].add_new_email_tooltip = "Click here to enter new email.";
texts['de'].add_new_email_tooltip = ""; 
texts['ro'].add_new_email_tooltip = "Apasați aici ca să adaăugați o adresa de e-mail nouă"; 
texts['es'].add_new_email_tooltip = "Haga clic para añadir nuevo correo electrónico"; 
texts['sr'].add_new_email_tooltip = "Kliknite ovde da unesete novu e-mail adresu."; 
texts['gr'].add_new_email_tooltip = "Πατήστε  εδώ για να προσθέσετε μια νέα διεύθυνση ηλεκτρονικού ταχυδρομείου.";

texts['bg'].add_new_phone_tooltip = "Натиснете тук за да добавите нов телефонен номер.";
texts['en'].add_new_phone_tooltip = "Click here to enter new phone number.";
texts['de'].add_new_phone_tooltip = "";
texts['ro'].add_new_phone_tooltip = "Apasați aici ca să adăugați un nou număr de telefon";
texts['es'].add_new_phone_tooltip = "Haga clic para añadir nuevo número de telefono";
texts['sr'].add_new_phone_tooltip = "Kliknite ovde da unesete nov broj telefona";
texts['gr'].add_new_phone_tooltip = "Πατήστε  εδώ για να προσθέσετε έναν νέο αριθμό τηλεφώνου.";

texts['bg'].event_notification_email = "Е-майл адрес <span style='color:grey'>(за получаване на известия генерирани от събития)</span>";
texts['en'].event_notification_email = "Email <span style='color:grey'>(for receiving notification about fired events)</span>";
texts['de'].event_notification_email = ""; 
texts['ro'].event_notification_email = "Adresă de e-mail <span style='color:grey'>(pentru a primi notificări generate de evenimente.)</span>"; 
texts['es'].event_notification_email = "Correo electrónico <span style='color:grey'>(para recibir notificaciones generadas por eventos.)</span>";
texts['sr'].event_notification_email = "E-mail adresa <span style='color:grey'>(za primanje obaveštenja generisanih od događaja.)</span>";
texts['gr'].event_notification_email = "Διεύθυνση ηλεκτρονικού ταχυδρομείου <span style ='color: gray'>(για να λαμβάνετε ειδοποιήσεις που δημιουργούνται από συμβάντα) </span>";

texts['bg'].invalid_email_entered = "Въвели сте низ не отговарящ на спецификацията за валиден е-майл адрес!\nВъведете валиден е-майл адрес.";
texts['en'].invalid_email_entered = "You have entered invalid email!\nPlease make sure the email you enter is valid.";
texts['de'].invalid_email_entered = ""; 
texts['ro'].invalid_email_entered = "Ati introdus un email invalid";
texts['es'].invalid_email_entered = "Se ha introducido correo electrónico no válido";
texts['sr'].invalid_email_entered = "Uneli ste neispravnu e-mail adresu!\nMolimo proverite da li je e-mail adresa koju ste uneli ispravna";
texts['gr'].invalid_email_entered = "Έχετε εισάγει μια συμβολοσειρά που δεν ταιριάζει με τις ισχύουσες προδιαγραφές της διεύθυνσης ηλεκτρονικού ταχυδρομείου!\n Εισαγάγετε μια έγκυρη διεύθυνση ηλεκτρονικού ταχυδρομείου.";

texts['bg'].email_exists = "Този е-майл присъства в списъка!";
texts['en'].email_exists = "This email already exists in the list!";
texts['de'].email_exists = ""; 
texts['ro'].email_exists = "Această adresă de mail nu există în listă"; 
texts['es'].email_exists = "Este correo electrónico ya existe en la lista";
texts['sr'].email_exists = "Ovaj e-mail već postoji u spisku!";
texts['gr'].email_exists = "Αυτό το  ηλεκτρονικού ταχυδρομείου υπάρχει ήδη στη λίστα!";

texts['bg'].error_saving_data_existing_email = "Възникна грешка и данните не бяха запаметени. Не се изключва възможността такъв е-майл да съществува в системата!";
texts['en'].error_saving_data_existing_email = "Error occurred while saving the data. The reason migh be because of the email you have entered already exists into the system!";
texts['de'].error_saving_data_existing_email = ""; 
texts['ro'].error_saving_data_existing_email = "O eroare a apărut in timplul salvării datelor. Un motiv poate fi introducerea aceluiași email în sistem, cu unul deja existent"; 
texts['es'].error_saving_data_existing_email = "Se ha producido un error, los datos no se pueden guardar. La razón puede ser que este correo electrónico ya existe"; 
texts['sr'].error_saving_data_existing_email = "Došlo je do greške prilikom čuvanja podataka. Može biti zbog toga što e-mail koji ste uneli već postoji u sistemu! ";
texts['gr'].error_saving_data_existing_email = "Παρουσιάστηκε σφάλμα κατά την αποθήκευση των δεδομένων.Ο λόγος μπορεί να οφείλεται στο ότι το  ηλεκτρονικού ταχυδρομείου που έχετε εισάγει υπάρχει ήδη στο σύστημα!";

texts['bg'].click_here_to_enter_verification_code = "Натиснете тук за да въведете кода за потвърждение.";
texts['en'].click_here_to_enter_verification_code = "Click here to enter the verification code.";
texts['de'].click_here_to_enter_verification_code = ""; 
texts['ro'].click_here_to_enter_verification_code = "Apasați aici ca să introduceți codul de confirmare"; 
texts['es'].click_here_to_enter_verification_code = "Haga click para introducir el código de verificación"; 
texts['sr'].click_here_to_enter_verification_code = "Kliknite ovde da unesete verifikacioni kod"; 
texts['gr'].click_here_to_enter_verification_code = "Πατήστε  εδώ για να εισάγετε τον κωδικό επαλήθευσης.";

texts['bg'].click_here_to_resend_verification_code = "Натиснете тук за да изпратите нов код за потвърждение в случай, че не сте получили е-майл.";
texts['en'].click_here_to_resend_verification_code = "Click here to resend the verification code in case you didn't receive email.";
texts['de'].click_here_to_resend_verification_code = ""; 
texts['ro'].click_here_to_resend_verification_code = "Apăsați aici pentru a retrimite codul de confirmare în cazul în care nu ați primit email"; 
texts['es'].click_here_to_resend_verification_code = "Haga clic para enviar nuevo código de verificación en caso que usted no ha recibido mensaje del correo electrónico.";
texts['sr'].click_here_to_resend_verification_code = "Kliknite ovde da pošaljete nov verifikacioni kod u slučaju da niste primili e-mail.";
texts['gr'].click_here_to_resend_verification_code = "Πατήστε  εδώ για να στείλετε έναν νέο κωδικό επαλήθευσης σε περίπτωση που δεν λάβετε μήνυμα ηλεκτρονικού ταχυδρομείου.";

texts['bg'].click_here_to_delete_email = "Натиснете тук за да изтриете този е-майл.";
texts['en'].click_here_to_delete_email = "Click here to delete this email.";
texts['de'].click_here_to_delete_email = ""; 
texts['ro'].click_here_to_delete_email = "Apasați aici ca să ștergeți acest e-mail"; 
texts['es'].click_here_to_delete_email = "Haga clic para eliminar este correo electrónico";
texts['sr'].click_here_to_delete_email = "Kliknite ovde da izbrišete ovaj e-mail";
texts['gr'].click_here_to_delete_email = "Πατήστε  εδώ για να διαγράψετε αυτό το  ηλεκτρονικού ταχυδρομείου.";

texts['bg'].verification_code_sent_email = "Код за потвърждение беше изпратен на е-майл"; //nqkakav email
texts['en'].verification_code_sent_email = "The verification code was sent to the email";
texts['de'].verification_code_sent_email = ""; 
texts['ro'].verification_code_sent_email = "Codul de confirmare a fost trimis la adresa e-mail"; 
texts['es'].verification_code_sent_email = "El código de verificación habia enviado del correo electrónico";
texts['gr'].verification_code_sent_email = "Ο κωδικός επαλήθευσης στάλθηκε στο ηλεκτρονικού ταχυδρομείου";

texts['bg'].to_use_email_enter_verification_code = "За да използвате е-майлът трябва да въведете кода за да докажете, че вие сте неговият собственик!";
texts['en'].to_use_email_enter_verification_code = "To use this email you need to enter the verification code in order to prove that you are its owner!";
texts['de'].to_use_email_enter_verification_code = ""; 
texts['ro'].to_use_email_enter_verification_code = "Pentru a putea utiliza acest e-mailul trebuie să introduceți codul de confirmare, pentru a dovedi că dvs. sunteți proprieterul lui";
texts['es'].to_use_email_enter_verification_code = "Para utilizar el correo electrónico debe introducir el código para probar que usted es el dueño!";
texts['sr'].to_use_email_enter_verification_code = "Da biste koristili ovaj e-mail potrebno je da unesete verifikacioni kod da biste dokazali da ste Vi njegov vlasnik!";
texts['gr'].to_use_email_enter_verification_code = "Για να χρησιμοποιήσετε το ηλεκτρονικό ταχυδρομείο πρέπει να εισάγετε τον κωδικό για να αποδείξετε ότι είστε ιδιοκτήτης του!";

texts['bg'].notification_resiving_phone_number = "Телефонен номер <span style='color:grey'>(за получаване на известия)</span>";
texts['en'].notification_resiving_phone_number = "Phone number <span style='color:grey'>(for receiving notifications)</span>";
texts['de'].notification_resiving_phone_number = ""; 
texts['ro'].notification_resiving_phone_number = "Număr de telefon <span style='color:grey'>(pentru primirea mesajelor de notificare)</span>"; 
texts['es'].notification_resiving_phone_number = "Número de telefono <span style='color:grey'>(para recibir notificaciones)</span>"; 
texts['sr'].notification_resiving_phone_number = "Broj telefona <span style='color:grey'>(za primanje obaveštenja)</span>"; 
texts['gr'].notification_resiving_phone_number = "Αριθμός τηλεφώνου<span style='color:grey'> (για να λαμβάνετε ειδοποιήσεις)</span>";

texts['bg'].sms_price = "Смс цена (&#128)";
texts['en'].sms_price = "Sms price (&#128)";
texts['de'].sms_price = ""; 
texts['ro'].sms_price = "Preț sms (&#128)"; 
texts['es'].sms_price = "Precio por mensaje (&#128)";
texts['sr'].sms_price = "SMS cena (&#128)";
texts['gr'].sms_price = "Τιμή SMS (&#128)";

texts['bg'].quota_money = "Квота (&#128)";
texts['en'].quota_money = "Quota (&#128)";
texts['de'].quota_money = ""; 
texts['ro'].quota_money = "Contor (&#128)";
texts['es'].quota_money = "Cuota (&#128)";
texts['sr'].quota_money = "Kvota (&#128)";
texts['gr'].quota_money = "Ποσόστωση (&#128)";

texts['bg'].expenditure = "Разход (&#128)";
texts['en'].expenditure = "Expend. (&#128)";
texts['de'].expenditure = ""; 
texts['ro'].expenditure = "Cost(&#128) "; 
texts['es'].expenditure = "Costo(&#128) ";
texts['sr'].expenditure = "Trošak(&#128) ";
texts['gr'].expenditure = "Εξόδων (&#128)";

texts['bg'].click_to_edit = "Натиснете за да редактирате";
texts['en'].click_to_edit = "Click to enter edit mode";
texts['de'].click_to_edit = ""; 
texts['ro'].click_to_edit = "Apasați pentru modul de editare"; 
texts['es'].click_to_edit = "Clic para editar"; 
texts['sr'].click_to_edit = "Kliknite za izmene";
texts['gr'].click_to_edit = "Πατήστε για επεξεργασία"; 

texts['bg'].cached_responses = "Бързи отговори";
texts['en'].cached_responses = "Cached messages";
texts['de'].cached_responses = ""; 
texts['ro'].cached_responses = "Răspunsuri rapide"; 
texts['es'].cached_responses = "Respuestas rápidas"; 
texts['sr'].cached_responses = "Keširane poruke"; 
texts['gr'].cached_responses = "Αποθηκευμένα μηνύματα";

texts['bg'].cached_response = "Бърз отговор";
texts['en'].cached_response = "Cached message";
texts['de'].cached_response = ""; 
texts['ro'].cached_response = "Răspuns rapid/predefinit"; 
texts['es'].cached_response = "Respuesta rápida";
texts['sr'].cached_response = "Keširana poruka";
texts['gr'].cached_response = "Αποθηκευμένο μήνυμα";

texts['bg'].add_new_cached_responses = "Натиснете тук за да добавите нов бърз отговор.";
texts['en'].add_new_cached_responses = "Click here to add new cached message.";
texts['de'].add_new_cached_responses = ""; 
texts['ro'].add_new_cached_responses = "Apasați aici ca să adăugați un nou răspuns rapid/predefinit"; 
texts['es'].add_new_cached_responses = "Haga clic para añadir nueva respuesta rápida";
texts['sr'].add_new_cached_responses = "Kliknite ovde da dodate novu keširanu poruku";
texts['gr'].add_new_cached_responses = "Πατήστε  εδώ για να προσθέσετε νέο αποθηκευμένο μήνυμα.";

texts['bg'].cached_responses_from_navigator = "Бързи отговори от навигатора";
texts['en'].cached_responses_from_navigator = "Cached responses installed/to be installed in the navigator";
texts['de'].cached_responses_from_navigator = "";
texts['ro'].cached_responses_from_navigator = "Răspunsuri rapide/predefinite care să fie introduse in navigator";
texts['es'].cached_responses_from_navigator = "Respuestas rápidas de la navegación";
texts['sr'].cached_responses_from_navigator = "Instalirani su keširani odgovori od navigacije";
texts['gr'].cached_responses_from_navigator = "Αποθηκευμένες απαντήσεις από τον πλοηγό";

texts['bg'].add_new_cached_response_form = "Формуляр за добавяне на бързи отговори от навигатора.";
texts['en'].add_new_cached_response_form = "Form for adding new cached messages.";
texts['de'].add_new_cached_response_form = "";
texts['ro'].add_new_cached_response_form = "Formular pentru adăugarea răspunsurile rapide/predefinite pe navigator";
texts['es'].add_new_cached_response_form = "Formulario para añadir respuestas rápidas de la navegación.";
texts['sr'].add_new_cached_response_form = "Obrazac za dodavanje novih keširanih poruka.";
texts['gr'].add_new_cached_response_form = "Έντυπο για την προσθήκη νέων αποθηκευμένων μηνυμάτων.";

texts['bg'].note = "Забележка";
texts['en'].note = "Note";
texts['de'].note = "";
texts['ro'].note = "Notă";
texts['es'].note = "Nota";
texts['sr'].note = "Beleška";
texts['gr'].note = "Σημείωση";

texts['bg'].cached_message_delete_note = "Изтриването на бърз отговор от системата няма да се отрази на устройствата, към които е изпратен. Ако сте изпратили този бърз отговор към някоя от навигациите трябва за всяка по отделно да изпратите команда за изтриване!";
texts['en'].cached_message_delete_note = "Deleting a cached message from the system wont affect the one installed into the navigators. So if you have sent this cached message to one or more than one navigators you need to send a delete command to each one from the chat interface!";
texts['de'].cached_message_delete_note = "";
texts['ro'].cached_message_delete_note = "Ștergerea unui mesaj memorat din sistem nu va afecta pe cele instalate pe navigator. Deci dacă ați trimis acest mesaj memorat la unul sau mai multe nevigatoare, trebuie să trimiteți o comandă de ștergere la fiecare din interfața de chat";
texts['es'].cached_message_delete_note = "Eliminación de una respuesta rápida del sistema no afectará a los dispositivos a los que se envía. Si usted ha enviado esta respuesta rápida a algunos dispositivos de navegación, tiene que enviar un comando de eliminacion para cada uno.";
texts['sr'].cached_message_delete_note = "Brisanje keširane poruke iz sistema neće uticati na onu koja je instalirana u navigaciji. Ako ste poslali keširanu poruku jednoj ili više navigacija potrebno je da pošaljete komandu brisanja svakoj ponaosob sa čet interfejsa";
texts['gr'].cached_message_delete_note = "Η διαγραφή ενός αποθηκευμένου μηνύματος από το σύστημα δεν επηρεάζει αυτό που έχει εγκατασταθεί στους πλοηγούς. Επομένως, αν έχετε στείλει αυτό το αποθηκευμένο μήνυμα σε έναν ή περισσότερους από έναν πλοηγούς θα πρέπει να στείλετε μια εντολή διαγραφής σε κάθε μία από τη διεπαφή συνομιλίας!";

texts['bg'].delete_cached_message_from_system_tooltip = "Изтриване на бърз отговор от системата";
texts['en'].delete_cached_message_from_system_tooltip = "Delete the  cached message from the system";
texts['de'].delete_cached_message_from_system_tooltip = "";
texts['ro'].delete_cached_message_from_system_tooltip = "Ștergere mesaj rapid/predefinit din sistem";
texts['es'].delete_cached_message_from_system_tooltip = "Eliminar respuesta rápida del sistema";
texts['sr'].delete_cached_message_from_system_tooltip = "Izbriši keširanu poruku iz sistema";
texts['gr'].delete_cached_message_from_system_tooltip = "Διαγραφή  αποθηκευμένο μήνυμα από το σύστημα";

texts['bg'].notification_code_sent_succesfully_to_email = "Кодът за потвърждение беше изпратен успешно на посочения е-майл.";
texts['en'].notification_code_sent_succesfully_to_email = "The notification code was sent successfully to your email";
texts['de'].notification_code_sent_succesfully_to_email = "";
texts['ro'].notification_code_sent_succesfully_to_email = "Codul de confirmare a fost trimis cu succes către e-mailul dvs. ";
texts['es'].notification_code_sent_succesfully_to_email = "El código de confirmación se  ha sido enviada a su correo electrónico correctamente. ";
texts['sr'].notification_code_sent_succesfully_to_email = "Kod za obaveštenja je uspešno poslat na Vaš e-mail. ";
texts['gr'].notification_code_sent_succesfully_to_email = "Ο κωδικός επιβεβαίωσης στάλθηκε με επιτυχία στη διεύθυνση ηλεκτρονικού ταχυδρομείου που δώσατε.";

texts['bg'].enter_verification_code_to_activate_email = "Въведете кода за потвърждение за да активирате е-майл адреса!";
texts['en'].enter_verification_code_to_activate_email = "Enter the verification code to activate the email";
texts['de'].enter_verification_code_to_activate_email = "";
texts['ro'].enter_verification_code_to_activate_email = "Introduceți codul de confirmare pentru a activa adresă de e-mail";
texts['es'].enter_verification_code_to_activate_email = "El código de confirmación  ha sido enviada a su correo electrónico con éxito";
texts['sr'].enter_verification_code_to_activate_email = "Unesite verifikacioni kod da aktivirate e-mail";
texts['gr'].enter_verification_code_to_activate_email = "Εισαγάγετε τον κωδικό επαλήθευσης για να ενεργοποιήσετε τη διεύθυνση ηλεκτρονικού ταχυδρομείου!";

texts['bg'].unsuccessful_verification = "Потвърждението не беше успешно!";
texts['en'].unsuccessful_verification = "The verification was not successful";
texts['de'].unsuccessful_verification = "";
texts['ro'].unsuccessful_verification = "Confirmarea nu s-a făcut cu succes";
texts['es'].unsuccessful_verification = "La confirmación  ha sido con éxito";
texts['sr'].unsuccessful_verification = "Neuspešna verifikacija";
texts['gr'].unsuccessful_verification = "Η επιβεβαίωση απέτυχε!";

texts['bg'].successful_verification = "Успешно потвърдихте е-майл адреса.";
texts['en'].successful_verification = "The email was successfully verified";
texts['de'].successful_verification = "";
texts['ro'].successful_verification = "E-mailul a fost confirmat cu succes";
texts['es'].successful_verification = "El correo electrónico se ha verificado con éxito";
texts['sr'].successful_verification = "E-mail je uspešno verifikovan";
texts['gr'].successful_verification = "Έχετε επιβεβαιώσει με επιτυχία τη διεύθυνση ηλεκτρονικού ταχυδρομείου.";

texts['bg'].confirm_digital_event_rename = "Сигурни ли сте, че искате да преименувате всички събития от тип: @_1 като: @_2 ?";
texts['en'].confirm_digital_event_rename = "Are you sure you want to rename all the events of type @_1 as: @_2 ?";
texts['de'].confirm_digital_event_rename = "";
texts['ro'].confirm_digital_event_rename = "Sunteți sigur că doriți să redenumiți toate evenimente de acest fel @_1 ca: @_2 ?";
texts['es'].confirm_digital_event_rename = "¿Seguro que desea cambiar el nombre de todos los eventos de tipo @_1 a: @_2 ?";
texts['sr'].confirm_digital_event_rename = "Da li ste sigurni da želite da preimenujete sve događaje tipa @_1 u: @_2 ?";
texts['gr'].confirm_digital_event_rename = "Είστε βέβαιοι ότι θέλετε να μετονομάσετε όλα τα συμβάντα του τύπου: @_1 ως: @_2 ?";

texts['bg'].select_alias = "Изберете псевдоним!";
texts['en'].select_alias = "Please select an alias";
texts['de'].select_alias = "";
texts['ro'].select_alias = "Alegeți un pseudonim!";
texts['es'].select_alias = "¡Por favor, seleccione un alias!";
texts['sr'].select_alias = "Izaberite pseudonim!";
texts['gr'].select_alias = "Επιλέξτε ένα ψευδώνυμο!";

texts['bg'].phone_number_not_mapped_to_sms_provider = "За този номер не е зададен SMS провайдер. Причината може да е в това, че не сте закупили SMS-и или с цел минимални разходи към мобилни оператори администратора на системата трябва ръчно да асоциира въведения телефонен номер с подходящ SMS провайдер.";
texts['en'].phone_number_not_mapped_to_sms_provider = "This phone number is not mapped to an sms provider. The reason may be that you didn't by sms credit or the system administrator didn't map it to an sms provider yet. The reason to be selected an sms provider for each phone number is because of the sms taxes.";
texts['de'].phone_number_not_mapped_to_sms_provider = "";
texts['ro'].phone_number_not_mapped_to_sms_provider = "Acest număr de telefon nu aparține unui operator de SMS. Motivul poate fi faptul ca nu ați cumparat credit sau administratorul de sistem nu l-a inregistrat înca la un operator. Motivul pentru care trebuie ales un operator pentru fiecare telefon este costul SMS-urilor.";
texts['es'].phone_number_not_mapped_to_sms_provider = "Para este número no se ha establecido el proveedor de SMS. la razón puede ser que usted no ha comprado mensajes por un costo mínimo a los operadores móviles.El administrador del sistema debe asociar manualmente el número de teléfono con el proveedor de mesajes.";
texts['sr'].phone_number_not_mapped_to_sms_provider = "Za ovaj broj telefona ne postoji SMS provajder. Razlog može biti taj što niste dopunili kredit ili ga administrator sistema jos nije prijavio sms provajderu. Razlog da se izabere sms provajder za svaki broj telefona je zbog sms poreza.";
texts['gr'].phone_number_not_mapped_to_sms_provider = "Ο παροχέας SMS δεν έχει καθοριστεί για αυτόν τον αριθμό. Ο λόγος μπορεί να είναι ότι δεν έχετε αγοράσει πίστωση ή ο διαχειριστής του συστήματος δεν το έχει ακόμη καταχωρήσει σε έναν φορέα εκμετάλλευσης.Ο διαχειριστής συστήματος πρέπει να συνδέσει χειροκίνητα  τον αριθμό τηλεφώνου με τον παροχέα μηνυμάτων.";

texts['bg'].option_currently_unavailable = "За момента не може да използвате тази опция.";
texts['en'].option_currently_unavailable = "This option is currently unavailable.";
texts['de'].option_currently_unavailable = "";
texts['ro'].option_currently_unavailable = "Momentan nu puteți folosii acesată opțiune";
texts['es'].option_currently_unavailable = "Esta opción no está disponible actualmente";
texts['sr'].option_currently_unavailable = "Ova opcija trenutno nije dostupna";
texts['gr'].option_currently_unavailable = "Αυτήν τη στιγμή δεν μπορείτε να χρησιμοποιήσετε αυτήν την επιλογή.";


texts['bg'].delete_phone_number_tooltip = "Натиснете тук за да изтриете този номер.";
texts['en'].delete_phone_number_tooltip = "Click here to delete this phone number.";
texts['de'].delete_phone_number_tooltip = "";
texts['ro'].delete_phone_number_tooltip = "Apasați aici ca să ștergeți acest număr";
texts['es'].delete_phone_number_tooltip = "Haga clic aquí para eliminar este número";
texts['sr'].delete_phone_number_tooltip = "Kliknite ovde da izbrišete ovaj broj telefona";
texts['gr'].delete_phone_number_tooltip = "Πατήστε εδώ για να διαγράψετε αυτόν τον αριθμό.";

texts['bg'].not_mobile_phone_number_entered = "Номерът, който сте въвели не е мобилен!";
texts['en'].not_mobile_phone_number_entered = "The number you have entered is not mobile!";
texts['de'].not_mobile_phone_number_entered = "";
texts['ro'].not_mobile_phone_number_entered = "Numărul care l-ați introdus nu este de mobil!";
texts['es'].not_mobile_phone_number_entered = "¡El número que usted ha introducido no es movil!";
texts['sr'].not_mobile_phone_number_entered = "Broj koji ste uneli nije mobilni!";
texts['gr'].not_mobile_phone_number_entered = "Ο αριθμός που καταχωρίσατε δεν είναι κινητός!";

texts['bg'].invalid_phone_number = "Номерът, който сте въвели не е мобилен!";
texts['en'].invalid_phone_number = "The number you have entered is invalid!";
texts['de'].invalid_phone_number = "";
texts['ro'].invalid_phone_number = "Numărul care -ați introdus nu este valid!";
texts['es'].invalid_phone_number = "El número que usted ha introducido no es valido";
texts['sr'].invalid_phone_number = "Broj koji ste uneli je nevažeći";
texts['gr'].invalid_phone_number = "Ο αριθμός που έχετε εισάγει δεν είναι έγκυρος!";

texts['bg'].phone_number_wrong_country = "Номерът, който сте въвели изглежда валиден, но не съотвества на избраната държава!";
texts['en'].phone_number_wrong_country = "The number you have entered seems to be valid but does not match the country prefix!";
texts['de'].phone_number_wrong_country = "";
texts['ro'].phone_number_wrong_country = "Numărul pe care l-ați introdus pare a valabil, dar nu se potrivește cu prefixul de țară ";
texts['es'].phone_number_wrong_country = "El número que usted ha introducido  es valido, pero no coincide del país elegida ";
texts['sr'].phone_number_wrong_country = "Broj koji ste uneli je važeći ali ne odgovara izabranoj državi!";
texts['gr'].phone_number_wrong_country = "Ο αριθμός που έχετε εισάγει φαίνεται να είναι έγκυρος αλλά δεν αντιστοιχεί στην επιλεγμένη χώρα!";

texts['bg'].enter_verification_code = "Въведете кода за потвърждение.";
texts['en'].enter_verification_code = "Enter the verification code.";
texts['de'].enter_verification_code = "";
texts['ro'].enter_verification_code = "Introduceți codul de confirmare";
texts['es'].enter_verification_code = "Introducir el código de verificación";
texts['sr'].enter_verification_code = "Unesite verifikacioni kod";
texts['gr'].enter_verification_code = "Καταχωρίστε τον κωδικό επαλήθευσης.";

texts['bg'].write_access_wrong_pass = "Не сте въвели правилна парола за модифициране на фирмените данни!";
texts['en'].write_access_wrong_pass = "Wrong firm data modification password entered!";
texts['de'].write_access_wrong_pass = ""; 
texts['ro'].write_access_wrong_pass = "Ați introdus parolă incorectă pentru modificarea datelor de firma"; 
texts['es'].write_access_wrong_pass = "La contraseña para 'modificación' de los datos de la empresa no esta introducida correctamente";
texts['sr'].write_access_wrong_pass = "Pogrešna lozinka za izmenu podataka firme";
texts['gr'].write_access_wrong_pass = "Δεν εισαγάγατε τον σωστό κωδικό πρόσβασης για να τροποποιήσετε τα δεδομένα της εταιρείας!";

texts['bg'].start_end_point_no_route_exists = "Не съществува маршрут измежду така зададените начална и крайна точка!";
texts['en'].start_end_point_no_route_exists = "No route could be calculated between the start and end point!";
texts['de'].start_end_point_no_route_exists = ""; 
texts['ro'].start_end_point_no_route_exists = "Nu poate fi găsită între punctul de plecare și punctul final";
texts['es'].start_end_point_no_route_exists = "Ruta no existente entre los puntos introducidos de inicio y final";
texts['sr'].start_end_point_no_route_exists = "Ne postoji maršruta između zadate početne i krajnje tačke!";
texts['gr'].start_end_point_no_route_exists = "Καμία διαδρομή δεν μπορεί να υπολογιστεί μεταξύ των σημείων έναρξης και τέλους!";

texts['bg'].routePlanTooltip = "Планиране на маршрут.";
texts['en'].routePlanTooltip = "Plan a new route";
texts['de'].routePlanTooltip = "";
texts['ro'].routePlanTooltip = "Planificare un nou itinerariul/rută";
texts['es'].routePlanTooltip = "Planificación de rutas";
texts['sr'].routePlanTooltip = "Planiranje nove maršrute";
texts['gr'].routePlanTooltip = "Σχεδιασμός διαδρομής.";

texts['bg'].historyTrace = "Наслагване на следа от реален маршрут.";
texts['en'].historyTrace = "Project a vehicle history trace.";
texts['de'].historyTrace = "";
texts['ro'].historyTrace = "Afișează istoria rutei reale a vehiculului";
texts['es'].historyTrace = "Visualización de rastro del historial de vehículo";
texts['sr'].historyTrace = "Pregled istorije maršrute vozila";
texts['gr'].historyTrace = "Παρακολούθηση επικάλυψης από πραγματική διαδρομή.";

texts['bg'].c_leg = "Отсечка";
texts['en'].c_leg = "Leg";
texts['de'].c_leg = "";
texts['ro'].c_leg = "Segment";
texts['es'].c_leg = "Tramo";
texts['sr'].c_leg = "Deonica";
texts['gr'].c_leg = "Τμήμα";

texts['bg'].c_legs = "Отсечки";
texts['en'].c_legs = "Legs";
texts['de'].c_legs = "";
texts['ro'].c_legs = "Segmente";
texts['es'].c_legs = "Tramo";
texts['sr'].c_legs = "Deonice";
texts['gr'].c_legs = "Τμήματα";

texts['bg'].route_bone = "(Скелет на маршрут)";
texts['en'].route_bone = "(Route's waypoints)";
texts['de'].route_bone = "";
texts['ro'].route_bone = "Puncte intermediare ale rutelor";
texts['es'].route_bone = "Punto intermedios de ruta";
texts['sr'].route_bone = "Međutačke maršrute";
texts['gr'].route_bone = "(Σκελετός διαδρομής)";

texts['bg'].c_up = "Нагоре";
texts['en'].c_up = "Up";
texts['de'].c_up = "";
texts['ro'].c_up = "Sus";
texts['es'].c_up = "Arriba";
texts['sr'].c_up = "Nagore";
texts['gr'].c_up = "Επάνω";

texts['bg'].c_down = "Надолу";
texts['en'].c_down = "Down";
texts['de'].c_down = "";
texts['ro'].c_down = "Jos";
texts['es'].c_down = "Abajo";
texts['sr'].c_down = "Nadole";
texts['gr'].c_down = "Κάτω";

texts['bg'].route_bone_point_drag = "Изберете дадена точка и е завлачете ВЪРХУ точката, ПОД която искате да отиде!";
texts['en'].route_bone_point_drag = "Select a waypoint and drag it OVER the point UNDER which you'd like to place it!";
texts['de'].route_bone_point_drag = "";
texts['ro'].route_bone_point_drag = "Alegeți un anumit punct intermediar și trageți-l peste punctul unde doriți sa-l plasați.";
texts['es'].route_bone_point_drag = "Seleccionar un punto intermedio y arrástralo sobre el punto BAJO que desea colocarlo!.";
texts['sr'].route_bone_point_drag = "Izaberite datu tačku i prevucite je PREKO tačke ISPOD koje želite da je smestite!";
texts['gr'].route_bone_point_drag = "Επιλέξτε ένα σημείο και σύρετε το σημείο κάτω από το οποίο θέλετε να πάτε!";

texts['bg'].waypoint_drag_error = "Преместването не може да бъде направено защото избраната точка вече се намира под посочената!";
texts['en'].waypoint_drag_error = "It is not neccessary to drag this point here because it would not take any effect!";
texts['de'].waypoint_drag_error = "";
texts['ro'].waypoint_drag_error = "Nu este necesar să trageți acest punct aici deoarece nu o sa aibă nici un efect";
texts['es'].waypoint_drag_error = "No es necesario arrastrar este punto aquí, porque no tendría ningún efecto";
texts['sr'].waypoint_drag_error = "Nije potrebno da prevlačite ovu tačku ovde zato što nema efekta! ";
texts['gr'].waypoint_drag_error = "Η μετακίνηση δεν μπορεί να γίνει επειδή το επιλεγμένο σημείο είναι ήδη κάτω από το καθορισμένο!";

texts['bg'].vehicle_trace = "Следа на превозно средство";
texts['en'].vehicle_trace = "Vehicle's trace";
texts['de'].vehicle_trace = "";
texts['ro'].vehicle_trace = "Urmă vehicule";
texts['es'].vehicle_trace = "Rastro del vehículo";
texts['sr'].vehicle_trace = "Putanja vozila";
texts['gr'].vehicle_trace = "Ίχνος του οχήματος";

texts['bg'].route_direction = "Упътване/навигиране по маршрута";
texts['en'].route_direction = "Route's directions";
texts['de'].route_direction = "";
texts['ro'].route_direction = "Îndrumare/navigare după itinerariul";
texts['es'].route_direction = "Direcciones de ruta";
texts['sr'].route_direction = "Uputstva za maršrutu";
texts['gr'].route_direction = "Κατεύθυνση / πλοήγηση κατά μήκος της διαδρομής";

texts['bg'].select_route_view_edit = "Избор на маршрут за преглед/редактиране";
texts['en'].select_route_view_edit = "Select a route to view or edit";
texts['de'].select_route_view_edit = "";
texts['ro'].select_route_view_edit = "Alegere un itinerariu pentru vizualizare/editare";
texts['es'].select_route_view_edit = "Seleccionar una ruta para ver/editar";
texts['sr'].select_route_view_edit = "Izaberite maršrutu za pregled/izmenu";
texts['gr'].select_route_view_edit = "Επιλέξτε μια διαδρομή για προβολή ή επεξεργασία";

texts['bg'].search_address_places = "Търсене на адреси, места и др...";
texts['en'].search_address_places = "Search for addresses and places";
texts['de'].search_address_places = "";
texts['ro'].search_address_places = "Cautare adrese, locuri, etc...";
texts['es'].search_address_places = "Buscar direcciones, lugares , e.t.c...";
texts['sr'].search_address_places = "Pretražite adresu, mesta i dr...";
texts['gr'].search_address_places = "Αναζήτηση διευθύνσεων, τοποθεσιών κλπ ...";

texts['bg'].c_place = "Място";
texts['en'].c_place = "Place";
texts['de'].c_place = "";
texts['ro'].c_place = "Loc";
texts['es'].c_place = "Lugar";
texts['sr'].c_place = "Mesto";
texts['gr'].c_place = "Τόπος ";

texts['bg'].c_places = "Места";
texts['en'].c_places = "Places";
texts['de'].c_places = "";
texts['ro'].c_places = "Locuri";
texts['es'].c_places = "Lugares";
texts['sr'].c_places = "Mesta";
texts['gr'].c_places = "Τοποθεσίες ";

texts['bg'].c_coordinates = "Координати";
texts['en'].c_coordinates = "Coordinates";
texts['de'].c_coordinates = "";
texts['ro'].c_coordinates = "Coordonate";
texts['es'].c_coordinates = "Coordenadas";
texts['sr'].c_coordinates = "Koordinate";
texts['gr'].c_coordinates = "Συντεταγμένες";

texts['bg'].enter_desired_address = "Въведете адрес за търсене.\nЗа оптимален резултат използвайте формат: \"улица, град, държава.\"";
texts['en'].enter_desired_address = "Enter the desired address.\nFor best results use the format: \"street,city,country\"";
texts['de'].enter_desired_address = "";
texts['ro'].enter_desired_address = "Introduceți adresă de cautare.\n Pentru rezultate optimale folosiți formatul: \"stradă, oraș,țară\"";
texts['es'].enter_desired_address = "Introducir dirección de búsqueda.\nPara resultado optimal use este formato: \"calle, ciudad, país\"";
texts['sr'].enter_desired_address = "Unesite željenu adresu.\nZa najbolje rezultate koristite format: \"ulica, grad, država\"";
texts['gr'].enter_desired_address = "Εισαγάγετε μια διεύθυνση αναζήτησης. \nΓια καλύτερα αποτελέσματα χρησιμοποιήστε τη μορφή: \"οδός, πόλη, χώρα .\"";

texts['bg'].search = "Търси";
texts['en'].search = "Search";
texts['de'].search = "";
texts['ro'].search = "Caută";
texts['es'].search = "Buscar";
texts['sr'].search = "Traži";
texts['gr'].search = "Αναζήτηση";

texts['bg'].clear = "Изчисти";
texts['en'].clear = "Clear";
texts['de'].clear = "";
texts['ro'].clear = "Șterge";
texts['es'].clear = "Borrar";
texts['sr'].clear = "Izbriši";
texts['gr'].clear = "Καθαρίστε";

texts['bg'].zoom_in_map_to_search = "Приближете картата достатъчно близко до някой град или квартал за да активирате търсенето на обекти.\nИмате две възможности за търсене:\n1) По ключова дума(и) - въвеждането става в прав текст.\n2) По име на обекта - името трябва да бъде въведено в кавички.";
texts['en'].zoom_in_map_to_search = "Zoom in the map close enough to cover a city or neighbourhood in order to activate the search of places.\nYou can search in two different ways:\n1) Using a key word(s) - simply entering it.\n2) By a place's name - the name must be entered in quotes.";
texts['de'].zoom_in_map_to_search = "";
texts['ro'].zoom_in_map_to_search = "Zoom pe hartă suficient încât să acoperiți un oraș sau ămprejurimile pentru a activa căutarea de locuri/adrese.\nPuteți căuta in două feluri diferite: \n Folosind cuvinte cheie -simplu il indroduceți.\n2)După numele locului -numele trebuie introdus ghilimele.";
texts['es'].zoom_in_map_to_search = "Amplíe el mapa hasta ciudad o barrio para activar búsqueda de objetos.\nTenga dos opciones por búsqueda: \n Por palabra/s clave -simplemente introduzca la palabra.\n2)Por nombre del lugar -introduzca el nombre entre comillas.";
texts['sr'].zoom_in_map_to_search = "Uveličajte mapu dovoljno blizu da prekrijete grad ili susedstvo da biste aktivirali pretragu mesta.\nImate dve opcije za pretragu: \n Koristeći ključnu reč(i) -jednostavno je unesite.\n2)Po imenu mesta -ime mora da se unese pod navodnike.";
texts['gr'].zoom_in_map_to_search = "Μεγέθυνση στο χάρτη αρκετά κοντά για να καλύψει μια πόλη ή μια γειτονιά για να ενεργοποιήσετε την αναζήτηση των τόπων.\nΜπορείτε να αναζητήσετε με δύο διαφορετικούς τρόπους: \ n1)χρησιμοποιώντας λέξεις-κλειδιά - απλά εισάγοντάς την.\ n 2) Με το όνομα ενός τόπου - το όνομα πρέπει να εισαχθεί σε εισαγωγικά. ";

texts['bg'].choose_place_to_display_the_search_result = "Изберете къде да бъдат показани намерените обекти:";
texts['en'].choose_place_to_display_the_search_result = "Choose how to proceed with the search results:";
texts['de'].choose_place_to_display_the_search_result = "";
texts['ro'].choose_place_to_display_the_search_result = "Alegeți cum unde să fie arătate obiectele gasite";
texts['es'].choose_place_to_display_the_search_result = "Elegir donde estarán visualizados los objetos encontrados";
texts['sr'].choose_place_to_display_the_search_result = "Izaberite gde će se prikazati pronađeni rezultati";
texts['gr'].choose_place_to_display_the_search_result = "Επιλέξτε πώς να προχωρήσετε με τα αποτελέσματα αναζήτησης:";

texts['bg'].show_all_places_on_map = "Покажи всички обекти на картата";
texts['en'].show_all_places_on_map = "Load all places on the map";
texts['de'].show_all_places_on_map = "";
texts['ro'].show_all_places_on_map = "Arată toate locurile de pe hartă";
texts['es'].show_all_places_on_map = "Mostrar todo los objetos sobre el mapa";
texts['sr'].show_all_places_on_map = "Prikaži sva mesta na mapi";
texts['gr'].show_all_places_on_map = "Εμφάνιση όλα τα μέρη στο χάρτη";

texts['bg'].show_additional_interface_to_select_places = "Покажи интерфейс за избор на обекти";
texts['en'].show_additional_interface_to_select_places = "Display additional interface with the search results in order to select some of them";
texts['de'].show_additional_interface_to_select_places = "";
texts['ro'].show_additional_interface_to_select_places = "Arată interfața adițională cu rezultatele cautarii pentru a putea selecta pe unele dintre ele";
texts['es'].show_additional_interface_to_select_places = "Mostrar la interfaz de selección de objetos";
texts['sr'].show_additional_interface_to_select_places = "Prikaži dodatni interfejs za izbor mesta";
texts['gr'].show_additional_interface_to_select_places = "Εμφάνιση διεπαφής για  επιλογή θέσης ";

texts['bg'].route_builder_options = "Опции за изграждането на маршрут по зададени адреси (междинни точки):";
texts['en'].route_builder_options = "Options for building a route based on waypoints:";
texts['de'].route_builder_options = "";
texts['ro'].route_builder_options = "Opțiuni pentru construirea itinerariului dupa adrese cerute (puncte intermediare)";
texts['es'].route_builder_options = "Opciones de construir una ruta por puntos intermedios o objetos marcados";
texts['sr'].route_builder_options = "Opcije za pravljenje maršrute na osnovu zadate adrese (međutačaka)";
texts['gr'].route_builder_options = "Επιλογές για την κατασκευή μιας διαδρομής με διευθύνσεις (ενδιάμεσα σημεία):";

texts['bg'].avoid_highways = "Ако е възможно да се избягват магистрали и/или високоскоростни пътища.";
texts['en'].avoid_highways = "If possible avoid highways and high speed roads.";
texts['de'].avoid_highways = "";
texts['ro'].avoid_highways = "Dacă este posibil să fie evitate autostrazile si drumurile rapide de viteză ";
texts['es'].avoid_highways = "Si es posible evitar autopistas y carreteras de alta velocidad";
texts['sr'].avoid_highways = "Ako je moguće izbegavajte autoputeve i/ili brze puteve";
texts['gr'].avoid_highways = "Αν είναι δυνατόν να αποφευχθούν αυτοκινητόδρομοι και / ή δρόμοι υψηλής ταχύτητας.";

texts['bg'].avoid_roads_with_taxes = "Ако е възможно да се избягват пътищя с платен режим и/или изсикване на винетка.";
texts['en'].avoid_roads_with_taxes = "If possible avoid roads with taxes and necessity of vignette.";
texts['de'].avoid_roads_with_taxes = "";
texts['ro'].avoid_roads_with_taxes = "Dacaă este posibil sa fie evitate drumurile cu taxe si care necesită vignete";
texts['es'].avoid_roads_with_taxes = "Si es posible evitar autopistas y carreteras con peaje";
texts['sr'].avoid_roads_with_taxes = "Ako je moguće izbegavajte puteve sa porezima i/ili zahtevima za vinjetu";
texts['gr'].avoid_roads_with_taxes = "Αν είναι δυνατόν να αποφευχθεί δρόμους που απαιτούν φόρους και βινιέτες";

texts['bg'].route_travel_mode = "Режим на пътуване (Ако ще съхранявате маршрута в системата е задължително да изберете 1-вото):";
texts['en'].route_travel_mode = "Route travel mode (If you plan to save the route into the system you must select the first one):";
texts['de'].route_travel_mode = "";
texts['ro'].route_travel_mode = "Modul de deplasare Rută (Dacă aveți de gând să salvați traseul în sistem trebuie să-l selectați pe primul)";
texts['es'].route_travel_mode = "Modo de Viajes (Si usted quiere almacenar la ruta en el sistema, tenga que seleccionar el primero)";
texts['sr'].route_travel_mode = "Režim putovanja (Ako planirate da sačuvate maršrutu u sistem, morate izabrati prvo)";
texts['gr'].route_travel_mode = "Λειτουργία δρομολογίου (Αν σκοπεύετε να αποθηκεύσετε τη διαδρομή στο σύστημα θα πρέπει να επιλέξετε το πρώτο):";

texts['bg'].route_travel_mode_vehicle = "Шофиране с превозно средство от категория А нагоре.";
texts['en'].route_travel_mode_vehicle = "Driving with a vehicle.";
texts['de'].route_travel_mode_vehicle = "";
texts['ro'].route_travel_mode_vehicle = "Conducerea unui vehicul din categoria A și următoarele după categoria A";
texts['es'].route_travel_mode_vehicle = "Conducción de un vehículo con categoría A o superior ";
texts['sr'].route_travel_mode_vehicle = "Vožnja vozilom od kategorije A nagore. ";
texts['gr'].route_travel_mode_vehicle = "Οδήγηση με όχημα κατηγορίας Α και άνω.";

texts['bg'].route_travel_mode_bicycling = "Предвижване с мини моторче или колело.";
texts['en'].route_travel_mode_bicycling = "Using a bicycle or mini motor.";
texts['de'].route_travel_mode_bicycling = "";
texts['ro'].route_travel_mode_bicycling = "Mișcarea cu mini moped sau bicicletă";
texts['es'].route_travel_mode_bicycling = "Uso de una bicicleta o mini moto";
texts['sr'].route_travel_mode_bicycling = "Prevoz biciklom ili mini motorom.";
texts['gr'].route_travel_mode_bicycling = "Κίνηση με μηχανή ή ποδήλατο.";

texts['bg'].route_travel_mode_transit = "Транзитно.";
texts['en'].route_travel_mode_transit = "Transit.";
texts['de'].route_travel_mode_transit = "";
texts['ro'].route_travel_mode_transit = "Tranzit";
texts['es'].route_travel_mode_transit = "Tránsito";
texts['sr'].route_travel_mode_transit = "Tranzit";
texts['gr'].route_travel_mode_transit = "Transit.";

texts['bg'].finish_previous_task_first = "Първо довършете предишната задача!";
texts['en'].finish_previous_task_first = "You must finish the previous task!";
texts['de'].finish_previous_task_first = "";
texts['ro'].finish_previous_task_first = "Trebuie să terminați sarcina precedentă";
texts['es'].finish_previous_task_first = "Usted debe terminar la tarea anterior";
texts['sr'].finish_previous_task_first = "Morate završiti prethodni zadatak!";
texts['gr'].finish_previous_task_first = "Πρώτα ολοκληρώστε την προηγούμενη εργασία σας!";

texts['bg'].new_document_confirm = "Сигурни ли сте, че искате да започнете нов документ?";
texts['en'].new_document_confirm = "Are you sure you want to start a new document?";
texts['de'].new_document_confirm = "";
texts['ro'].new_document_confirm = "Sunteți sigur că doriți începeți un nou document";
texts['es'].new_document_confirm = "¿Seguro que quieres empezar un nuevo documento?";
texts['sr'].new_document_confirm = "Da li ste sigurni da želite da započnete novi dokument?";
texts['gr'].new_document_confirm = "Είστε βέβαιοι ότι θέλετε να ξεκινήσετε ένα νέο έγγραφο;";

texts['bg'].route_not_enough_points_error = "По дефиниция един маршрут трябва да има минимум 2 (две) точки!";
texts['en'].route_not_enough_points_error = "Each route must have atleast 2 (two) points!";
texts['de'].route_not_enough_points_error = "";
texts['ro'].route_not_enough_points_error = "Potrivit definiției un itinerariu trebuie să aibă minim 2 (două) puncte";
texts['es'].route_not_enough_points_error = "Cada ruta debe tener mínimo dos(2) puntos";
texts['sr'].route_not_enough_points_error = "Svaka maršruta mora imati najmanje 2 (dve) tačke!";
texts['gr'].route_not_enough_points_error = "Εξ ορισμού, μια διαδρομή πρέπει να έχει τουλάχιστον 2 (δύο) σημεία!";

texts['bg'].route_missing_start_end_point = "Маршрута няма начална или крайна точка.";
texts['en'].route_missing_start_end_point = "This route does not have a start or end point.";
texts['de'].route_missing_start_end_point = "";
texts['ro'].route_missing_start_end_point = "Itinerariul acesta nu are puncte de plecare și punct de final";
texts['es'].route_missing_start_end_point = "La ruta no hay punto de inicio o punto de final";
texts['sr'].route_missing_start_end_point = "Ova maršruta nema početnu i krajnju tačku.";
texts['gr'].route_missing_start_end_point = "Η διαδρομή δεν έχει αρχικό ή τελικό σημείο.";

texts['bg'].enter_route_name = "Въведете име на маршрута:";
texts['en'].enter_route_name = "Enter route name:";
texts['de'].enter_route_name = "";
texts['ro'].enter_route_name = "Introduceți numele itinerariului/rutei";
texts['es'].enter_route_name = "Introducir nombre de ruta";
texts['sr'].enter_route_name = "Unesite ime maršrute";
texts['gr'].enter_route_name = "Εισαγάγετε όνομα διαδρομής:";

texts['bg'].route_name_less_than_three_chars_error = "Името трябва да е минимум 3 символа!";
texts['en'].route_name_less_than_three_chars_error = "The route name's length must be atleast 3 characters";
texts['de'].route_name_less_than_three_chars_error = "";
texts['ro'].route_name_less_than_three_chars_error = "Numele traseului/rutei trebuie să fie de minim 3 caractere";
texts['es'].route_name_less_than_three_chars_error = "El nombre debe ser mínimo tres carácteres";
texts['sr'].route_name_less_than_three_chars_error = "Ime maršrute mora da sadrži najmanje 3 karaktera! ";
texts['gr'].route_name_less_than_three_chars_error = "Το όνομα πρέπει να είναι τουλάχιστον 3 χαρακτήρες!";

texts['bg'].create_route_before_save_error = "Преди да запаметите първо трябва да изградите маршрут!";
texts['en'].create_route_before_save_error = "You need to build a route before using this function!";
texts['de'].create_route_before_save_error = "";
texts['ro'].create_route_before_save_error = "Înainte de a folosii această funcție trebuie să construiți itinerariul";
texts['es'].create_route_before_save_error = "Antes de almacenar en el sistema usted tenga que construir la ruta";
texts['sr'].create_route_before_save_error = "Morate prvo da napravite maršrutu pre nego što je sačuvate!";
texts['gr'].create_route_before_save_error = "Πριν αποθηκεύσετε πρέπει πρώτα να δημιουργήσετε μια διαδρομή!";

texts['bg'].click_over_marker_load_additional_info = "Натиснете върху маркера за да се зареди допълнителна информация за това място.";
texts['en'].click_over_marker_load_additional_info = "Click over the marker to load additional information about this place.";
texts['de'].click_over_marker_load_additional_info = "";
texts['ro'].click_over_marker_load_additional_info = "Apasați pe marcaj pentru a încarcă informația suplimentară despre acest loc/locație";
texts['es'].click_over_marker_load_additional_info = "Clic sobre el marcador para visualizarsé información adicional para este lugar";
texts['sr'].click_over_marker_load_additional_info = "Kliknite na marker da učitate dodatnu informaciju o ovom mestu.";
texts['gr'].click_over_marker_load_additional_info = "Πατήστε στο δείκτη για να φορτώσετε πρόσθετες πληροφορίες σχετικά με αυτό το μέρος.";

texts['bg'].if_u_continue_all_gets_nulled = "Ако продължите ще нулирате всичко извършено до момента!";
texts['en'].if_u_continue_all_gets_nulled = "If you continue everything will be erased!";
texts['de'].if_u_continue_all_gets_nulled = "";
texts['ro'].if_u_continue_all_gets_nulled = "Dacă continuați totul ceea ce ați făcut până acum va fi șters ";
texts['es'].if_u_continue_all_gets_nulled = "Si continúa todo se borrará!";
texts['sr'].if_u_continue_all_gets_nulled = "Ako nastavite sve će biti izbrisano!";
texts['gr'].if_u_continue_all_gets_nulled = "Εάν συνεχίσετε, θα επαναφέρετε όλα όσα έχετε κάνει μέχρι τώρα!";

texts['bg'].invalid_coordinates_entered = "Възникна грешка при опит да се разчетът координатите. Проверете дали сте попълнили правилно полетата!";
texts['en'].invalid_coordinates_entered = "The coordinates couldn't be validated. Make sure you have entered valid data into the fields!";
texts['de'].invalid_coordinates_entered = "";
texts['ro'].invalid_coordinates_entered = "Coordonatele nu au putut fi validate. Asigurațivă că ați introdus date valide in câmpurile aferente";
texts['es'].invalid_coordinates_entered = "Se ha producido un error al intentar leer las coordenadas.¡Compruebe que ha rellenado los campos correctamente!";
texts['sr'].invalid_coordinates_entered = "Koordinate nisu potvrđene. Proverite da li ste uneli tačne podatke u polja!";
texts['gr'].invalid_coordinates_entered = "Παρουσιάστηκε σφάλμα κατά την προσπάθεια ανάγνωσης των συντεταγμένων. Βεβαιωθείτε ότι συμπληρώσατε τα πεδία ";

texts['bg'].vehicle_trace_load_error = "Възникна грешка при опит за зареждане на следата. Проверете за по-голям период от време!";
texts['en'].vehicle_trace_load_error = "There was an error while trying to load the vehicle's trace. Please choose bigger period of time!";
texts['de'].vehicle_trace_load_error = "";
texts['ro'].vehicle_trace_load_error = "Eroare la încarcarea urmei vehiculelor. Verificați pentru o perioadă de timp mai mare";
texts['es'].vehicle_trace_load_error = "Se ha producido un error al intentar cargar el rastro. Por favor, elija mayor período de tiempo!";
texts['sr'].vehicle_trace_load_error = "Pojavila se greška prilikom učitavanja putanje vozila. Molimo izaberite veći vremenski period!";
texts['gr'].vehicle_trace_load_error = "Παρουσιάστηκε σφάλμα κατά την προσπάθεια φόρτωσης του ίχνους του οχήματος. Παρακαλώ επιλέξτε μεγαλύτερο χρονικό διάστημα!";


texts['bg'].min_value = "Н.м.с.";
texts['en'].min_value = "Min value";
texts['de'].min_value = "";
texts['ro'].min_value = "";
texts['es'].min_value = "Valor mínimo";
texts['sr'].min_value = "Min. vrednost";
texts['gr'].min_value = "Ελάχιστη αξία";

texts['bg'].most_value = "Н.г.с.";
texts['en'].most_value = "Most value";
texts['de'].most_value = "";
texts['ro'].most_value = "Cele mai de valoare";
texts['ro'].most_value = "Valor normal";
texts['sr'].most_value = "Najveća vrednost";
texts['gr'].most_value = "Μέγιστη αξία";

texts['bg'].total_fuel = "Общо гориво";
texts['en'].total_fuel = "Total fuel";
texts['de'].total_fuel = "";
texts['ro'].total_fuel = "Combustibil total";
texts['es'].total_fuel = "Combustible total";
texts['sr'].total_fuel = "Ukupno gorivo";
texts['gr'].total_fuel = "Συνολικό καύσιμο";

texts['bg'].captured_events = "Прихванати събития";
texts['en'].captured_events = "Captured events";
texts['de'].captured_events = "";
texts['ro'].captured_events = "Evenimente înregistrate";
texts['es'].captured_events = "Eventos Interceptados";
texts['sr'].captured_events = "Zabeleženi događaji";
texts['gr'].captured_events = "Αιχμαλωτισμένα συμβάντα";

texts['bg'].dateTime = "Данни към";
texts['en'].dateTime = "Datetime";
texts['de'].dateTime = "";
texts['ro'].dateTime = "Date pentru";
texts['es'].dateTime = "Datos para";
texts['sr'].dateTime = "Podaci za";
texts['gr'].dateTime = "Δεδομένα προς";

texts['bg'].one_hour_back = "Час назад";
texts['en'].one_hour_back = "One hour back";
texts['de'].one_hour_back = "";
texts['ro'].one_hour_back = "O oră înapoi";
texts['es'].one_hour_back = "Retrasa una hora";
texts['sr'].one_hour_back = "Jedan sat nazad";
texts['gr'].one_hour_back = "Μια ώρα πίσω";

texts['bg'].one_hour_forward = "Час напред";
texts['en'].one_hour_forward = "One hour forward";
texts['de'].one_hour_forward = "";
texts['ro'].one_hour_forward = "O oră înainte";
texts['ro'].one_hour_forward = "Una hora delante";
texts['sr'].one_hour_forward = "Jedan sat napred";
texts['gr'].one_hour_forward = "Μια ώρα μπροστά";

texts['bg'].places = "Обекти";
texts['en'].places = "Marked places";
texts['de'].places = "";
texts['ro'].places = "Locuri marcate";
texts['es'].places = "Lugares marcados";
texts['sr'].places = "Obeležena mesta";
texts['gr'].places = "Τοποθεσίες ";

texts['bg'].dispenser = "Бензиноколонка";
texts['en'].dispenser = "Dispenser";
texts['de'].dispenser = "";
texts['ro'].dispenser = "Distribuitor";
texts['ro'].dispenser = "Dispensador";
texts['sr'].dispenser = "Dispenzer";
texts['gr'].dispenser = "Dispenser";

texts['bg'].mh = "М.ч.";
texts['en'].mh = "W.h.";
texts['de'].mh = "";
texts['ro'].mh = "m.h.";
texts['es'].mh = "h.t.";
texts['sr'].mh = "R.s.";
texts['gr'].mh = "W.h.";

texts['bg'].workingHours = "Моточасове";
texts['en'].workingHours = "Working hours";
texts['de'].workingHours = "Ore lucrate";
texts['ro'].workingHours = "Ore lucrate";
texts['es'].workingHours = "Horas de trabajo";
texts['sr'].workingHours = "Radni sati";
texts['gr'].workingHours = "Ωρες εργασίας";

texts['bg'].working_hours_monthly_report = "Месечен пътен лист за моточасове";
texts['en'].working_hours_monthly_report = "Working hours montly road list";
texts['de'].working_hours_monthly_report = "Raport ore lucrate lunar";
texts['ro'].working_hours_monthly_report = "Raport ore lucrate lunar";
texts['es'].working_hours_monthly_report = "Informe mensual de horas laborales de vehículo";
texts['sr'].working_hours_monthly_report = "Mesečni izveštaj za radne sate";
texts['gr'].working_hours_monthly_report = "Μηνιαία λίστα οδικών ωρών εργασίας";

texts['bg'].signal = "Сигнал";
texts['en'].signal = "Network";
texts['de'].signal = "rețea";
texts['ro'].signal = "Rețea";
texts['es'].signal = "Señal";
texts['sr'].signal = "Signal";
texts['gr'].signal = "Σήμα";

texts['bg'].signal_loss = "Загуба на сигнал";
texts['en'].signal_loss = "Network loss";
texts['de'].signal_loss = "Rețea pierdută";
texts['ro'].signal_loss = "Rețea pierdută";
texts['es'].signal_loss = "Fuera de cobertura";
texts['sr'].signal_loss = "Izgubljen signal";
texts['gr'].signal_loss = "Απώλεια σήματος";

texts['bg'].mileage = "Километраж";
texts['en'].mileage = "Mileage";
texts['de'].mileage = "Kilometraj";
texts['ro'].mileage = "Kilometraj";
texts['es'].mileage = "Kilometraje";
texts['sr'].mileage = "Kilometraža";
texts['gr'].mileage = "Χιλιόμετρα";


texts['bg'].expand_chart = "Уголеми графиката";
texts['en'].expand_chart = "Expand chart";
texts['de'].expand_chart = "";
texts['es'].expand_chart = "Ampliar gráfico";
texts['sr'].expand_chart = "Povećaj grafik";
texts['gr'].expand_chart = "Μεγέθυνση γράφημα ";

texts['bg'].filter_color1 = "Оцвети по филтър";
texts['en'].filter_color1 = "Color by filter";
texts['de'].filter_color1 = "";
texts['ro'].filter_color1 = "";
texts['es'].filter_color1 = "Colorear de litro";
texts['sr'].filter_color1 = "Boja po filteru";
texts['gr'].filter_color1 = "Χρωμάτισε ανά φίλτρο ";

texts['bg'].sidebar_hide = "Скрий левия панел";
texts['en'].sidebar_hide = "Hide sidebar";
texts['de'].sidebar_hide = "";
texts['ro'].sidebar_hide = "";
texts['es'].sidebar_hide = "Ocultar el panel izquierdo";
texts['sr'].sidebar_hide = "Sakrij levi panel";
texts['gr'].sidebar_hide = "Απόκρυψη του αριστερού πλαισίου";

texts['bg'].general_chart = "Обща графика";
texts['en'].general_chart = "General chart";
texts['de'].general_chart = "";
texts['ro'].general_chart = "";
texts['es'].general_chart = "Gráfico generale";
texts['sr'].general_chart = "Opšti grafik";
texts['gr'].general_chart = "Γενικό διάγραμμα";

texts['bg'].filter_correlation_chart = "Съотношение на филтри";
texts['en'].filter_correlation_chart = "Filter correlation chart";
texts['de'].filter_correlation_chart = "";
texts['ro'].filter_correlation_chart = "";
texts['es'].filter_correlation_chart = "Correlación de filtros";
texts['sr'].filter_correlation_chart = "Odnos filtera";
texts['gr'].filter_correlation_chart = "Αναλογία φίλτρων";

texts['bg'].filter_correlation_chart2 = "Графика на съотношението на филтри";
texts['en'].filter_correlation_chart2 = "Filter correlation chart";
texts['de'].filter_correlation_chart2 = "";
texts['ro'].filter_correlation_chart2 = "";
texts['es'].filter_correlation_chart2 = "Grafico de correlación de filtros";
texts['sr'].filter_correlation_chart2 = "Grafik odnosa filtera";
texts['gr'].filter_correlation_chart2 = "Διάγραμμα της αναλογίας των φίλτρων";

texts['bg'].filter_correlation = "Съотношение на филтри";
texts['en'].filter_correlation = "Correlation of filters";
texts['de'].filter_correlation = "";
texts['ro'].filter_correlation = "";
texts['es'].filter_correlation = "Correlación de filtros";
texts['sr'].filter_correlation = "Odnos filtera";
texts['gr'].filter_correlation = "Αναλογία φίλτρων";

texts['bg'].c_average = "Средно";
texts['en'].c_average = "Average";
texts['de'].c_average = "";
texts['ro'].c_average = "";
texts['es'].c_average = "Promedio";
texts['sr'].c_average = "Prosečno";
texts['gr'].c_average = "Μέσος";

texts['bg'].general_month_chart = "Обща графика за месеца";
texts['en'].general_month_chart = "General month chart";
texts['de'].general_month_chart = "";
texts['ro'].general_month_chart = "";
texts['es'].general_month_chart = "Gráfico general mensual";
texts['sr'].general_month_chart = "Opšti mesečni grafik";
texts['gr'].general_month_chart = "Γενικό διάγραμμα για το μήνα";

texts['bg'].general_period_chart = "Обща графика за избрания период";
texts['en'].general_period_chart = "General chart for custom period";
texts['de'].general_period_chart = "";
texts['ro'].general_period_chart = "";
texts['es'].general_period_chart = "Gráfico general del periodo seleccionado";
texts['sr'].general_period_chart = "Opšti grafik za izabrani period";
texts['gr'].general_period_chart = "Γενικό διάγραμμα της επιλεγμένης περιόδου";

texts['bg'].max_min_average_values_filters_correlation = "Максимални, минимални и средни стойности. Съотношение на филтрите.";
texts['en'].max_min_average_values_filters_correlation = "Highest, lowest and average values. Filters correlation.";
texts['de'].max_min_average_values_filters_correlation = "";
texts['ro'].max_min_average_values_filters_correlation = "";
texts['es'].max_min_average_values_filters_correlation = "Valores máximos, mínimos y promedios. Correlación de filtros";
texts['sr'].max_min_average_values_filters_correlation = "Maksimalne, minimalne i srednje vrednosti. Odnos filtera";
texts['gr'].max_min_average_values_filters_correlation = "Μέγιστες, ελάχιστες και μέσες τιμές. Αναλογία των φίλτρων.";

texts['bg'].daily_percentage_devision_temp_filters_custom_period = "Дневно процентно разпределение на температурните филтри за избрания период";
texts['en'].daily_percentage_devision_temp_filters_custom_period = "Daily percentage distribution of temperature filters for the selected period";
texts['de'].daily_percentage_devision_temp_filters_custom_period = "";
texts['ro'].daily_percentage_devision_temp_filters_custom_period = "";
texts['es'].daily_percentage_devision_temp_filters_custom_period = "Distribución porcentual diaria de filtros de temperatura para el periodo seleccionado";
texts['sr'].daily_percentage_devision_temp_filters_custom_period = "Dnevni procenat distribucije temperature filtera za izabrani period";
texts['gr'].daily_percentage_devision_temp_filters_custom_period = "Ημερήσια ποσοστιαία κατανομή των φίλτρων θερμοκρασίας για την επιλεγμένη περίοδο";

texts['bg'].distribution_temp_filters = "Разпределение на температурните филтри";
texts['en'].distribution_temp_filters = "Distribution of temperature filters";
texts['de'].distribution_temp_filters = "";
texts['ro'].distribution_temp_filters = "";
texts['es'].distribution_temp_filters = "Distribución de los filtros de la temperatura";
texts['sr'].distribution_temp_filters = "Distribucija temperature filtera";
texts['gr'].distribution_temp_filters = "Κατανομή των φίλτρων θερμοκρασίας";

texts['bg'].select_export_vehicles = "Изберете превозните средства, които искате да експортнете";
texts['en'].select_export_vehicles = "Select the vehicles that you would like to export";
texts['de'].select_export_vehicles = "";
texts['ro'].select_export_vehicles = "";
texts['es'].select_export_vehicles = "Seleccione los vehículos que desea exportar";
texts['sr'].select_export_vehicles = "Izaberite vozila koja želite da izvezete";
texts['gr'].select_export_vehicles = "Επιλέξτε τα οχήματα που θέλετε να εξάγετε";

texts['bg'].select_export_groups = "Изберете групите, които искате да експортнете";
texts['en'].select_export_groups = "Select the groups that you would like to export";
texts['de'].select_export_groups = "";
texts['ro'].select_export_groups = "";
texts['es'].select_export_groups = "Seleccione los grupos que desea exportar";
texts['sr'].select_export_groups = "Izaberite grupe koje želite da izvezete";
texts['gr'].select_export_groups = "Επιλέξτε τις ομάδες που θέλετε να εξαγάγετε";

texts['bg'].select_atleast_one_vehicle = "Трябва да изберете поне едно превозно средство!";
texts['en'].select_atleast_one_vehicle = "You must select atleast one vehicle!";
texts['de'].select_atleast_one_vehicle = "";
texts['ro'].select_atleast_one_vehicle = "";
texts['es'].select_atleast_one_vehicle = "Debe seleccionar al menos un vehículo!";
texts['sr'].select_atleast_one_vehicle = "Morate izabrati najmanje jedno vozilo!";
texts['gr'].select_atleast_one_vehicle = "Πρέπει να επιλέξετε τουλάχιστον ένα όχημα!";


texts['bg'].kml_export_no_trace = "Нужно е да визуализирате поне една отсечка от следата на превозното средство!";
texts['en'].kml_export_no_trace = "You must plot atleast one leg of the vehicle route!";
texts['de'].kml_export_no_trace = "You must plot atleast one leg of the vehicle route!";
texts['ro'].kml_export_no_trace = "You must plot atleast one leg of the vehicle route!";
texts['es'].kml_export_no_trace = "You must plot atleast one leg of the vehicle route!";
texts['sr'].kml_export_no_trace = "You must plot atleast one leg of the vehicle route!";
texts['gr'].kml_export_no_trace = " Πρέπει να απεικονίσετε  τουλάχιστον ένα ίχνος  της διαδρομής του οχήματος!";


texts['bg'].from = "от";
texts['en'].from = "of";
texts['de'].from = "of";
texts['ro'].from = "of";
texts['es'].from = "of";
texts['sr'].from = "of";
texts['gr'].from = "από";


texts['bg'].sensor = "датчик";
texts['en'].sensor = "sensor";
texts['de'].sensor = "sensor";
texts['ro'].sensor = "sensor";
texts['es'].sensor = "sensor";
texts['sr'].sensor = "sensor";
texts['gr'].sensor = "αισθητήρα";


texts['bg'].c_sensor = "Датчик";
texts['en'].c_sensor = "Sensor";
texts['de'].c_sensor = "Sensor";
texts['ro'].c_sensor = "Sensor";
texts['es'].c_sensor = "Sensor";
texts['sr'].c_sensor = "Sensor";
texts['gr'].c_sensor = "Αισθητήρα";

texts['bg'].working_shift = "Смяна";
texts['en'].working_shift = "Shift";
texts['de'].working_shift = "Shift";
texts['ro'].working_shift = "Schimb";
texts['es'].working_shift = "Shift";
texts['sr'].working_shift = "Shift";
texts['gr'].working_shift = "Βάρδια";

texts['bg'].courses_count = "Бр. курсове";
texts['en'].courses_count = "Courses cnt.";
texts['de'].courses_count = "Courses cnt.";
texts['ro'].courses_count = "Nr. curse";
texts['es'].courses_count = "Courses cnt.";
texts['sr'].courses_count = "Courses cnt.";
texts['gr'].courses_count = "Courses cnt.";

texts['bg'].dynamic_work_time = "Променливо задаване на работното време";
texts['en'].dynamic_work_time = "Dynamic work time";
texts['de'].dynamic_work_time = "Dynamic work time";
texts['ro'].dynamic_work_time = "Setare timp de lucru flexsibil";
texts['es'].dynamic_work_time = "Dynamic work time";
texts['sr'].dynamic_work_time = "Dynamic work time";
texts['gr'].dynamic_work_time = "Μεταβλητή ρύθμιση του χρόνου εργασίας";

texts['bg'].c_weekly = "Седмичен";
texts['en'].c_weekly = "Weekly";
texts['de'].c_weekly = "Weekly";
texts['ro'].c_weekly = "Saptamanal";
texts['es'].c_weekly = "Weekly";
texts['sr'].c_weekly = "Weekly";
texts['gr'].c_weekly = "Εβδομαδιαία";

texts['bg'].c_daily = "Дневен";
texts['en'].c_daily = "Daily";
texts['de'].c_daily = "Daily";
texts['ro'].c_daily = "Zilnic";
texts['es'].c_daily = "Daily";
texts['sr'].c_daily = "Daily";
texts['gr'].c_daily = "Καθημερινά";

texts['bg'].c_scheduler = "График";
texts['en'].c_scheduler = "Scheduler";
texts['de'].c_scheduler = "Scheduler";
texts['ro'].c_scheduler = "Grafic";
texts['es'].c_scheduler = "Scheduler";
texts['sr'].c_scheduler = "Scheduler";
texts['gr'].c_scheduler = "Πρόγραμμα";

texts['bg'].task_description = "Описание на задачата";
texts['en'].task_description = "Task description";
texts['de'].task_description = "Task description";
texts['ro'].task_description = "Drscrierea evenimentului";
texts['es'].task_description = "Task description";
texts['sr'].task_description = "Task description";
texts['gr'].task_description = "Περιγραφή της εργασίας";

texts['bg'].vehicles_custom_work_time_config = "Задаване на променливо работно време на превозните средства";
texts['en'].vehicles_custom_work_time_config = "Custom worktime configuration";
texts['de'].vehicles_custom_work_time_config = "Custom worktime configuration";
texts['ro'].vehicles_custom_work_time_config = "Setare timp de lucru flexibil pentru vehicule";
texts['es'].vehicles_custom_work_time_config = "Custom worktime configuration";
texts['sr'].vehicles_custom_work_time_config = "Custom worktime configuration";
texts['gr'].vehicles_custom_work_time_config = "Καθορισμός μεταβλητών ωρών εργασίας για τα οχήματα";

texts['bg'].avl_snapshot = "Състояние на автопарк";
texts['en'].avl_snapshot = "AVL snapshot";
texts['de'].avl_snapshot = "AVL snapshot";
texts['ro'].avl_snapshot = "Starea parcului auto";
texts['es'].avl_snapshot = "AVL snapshot";
texts['sr'].avl_snapshot = "AVL snapshot";
texts['gr'].avl_snapshot = "Κατάσταση AVL";

texts['bg'].avl_group = "Група на МПС";
texts['en'].avl_group = "AVL group";
texts['de'].avl_group = "AVL group";
texts['ro'].avl_group = "Grupa autovehicule";
texts['es'].avl_group = "AVL group";
texts['sr'].avl_group = "AVL group";
texts['gr'].avl_group = "Ομάδα οχημάτων";

texts['bg'].collapse_expand = "Свий/разтегни групирането";
texts['en'].collapse_expand = "Collapse/expand";
texts['de'].collapse_expand = "Collapse/expand";
texts['ro'].collapse_expand = "Grupat/detailat";
texts['es'].collapse_expand = "Collapse/expand";
texts['sr'].collapse_expand = "Collapse/expand";
texts['gr'].collapse_expand = "Σύμπτυξη / επέκταση";


texts['bg'].c_engine = "Двигател";
texts['en'].c_engine = "Engine";
texts['de'].c_engine = "Engine";
texts['ro'].c_engine = "Motor";
texts['es'].c_engine = "Engine";
texts['sr'].c_engine = "Engine";
texts['gr'].c_engine = "Κινητήρας";

texts['bg'].c_engine = "Двигател";
texts['en'].c_engine = "Engine";
texts['de'].c_engine = "Engine";
texts['ro'].c_engine = "Motor";
texts['es'].c_engine = "Engine";
texts['sr'].c_engine = "Engine";
texts['gr'].c_engine = "Κινητήρας";

texts['bg'].please_fill_all_input_fields_press_submit_button = "Моля попълнете всички полета и натиснете бутона 'Изпрати'.";
texts['en'].please_fill_all_input_fields_press_submit_button = "Please fill all input fields and press 'Submit' button.";
texts['de'].please_fill_all_input_fields_press_submit_button = "";
texts['ro'].please_fill_all_input_fields_press_submit_button = "Va rog completati toate campurile si apasati butonul 'Trimite'.";
texts['es'].please_fill_all_input_fields_press_submit_button = "Por favor, complete todos los campos y haga clic en el botón 'Enviar'.";
texts['sr'].please_fill_all_input_fields_press_submit_button = "Please fill all input fields and press 'Submit' button.";
texts['gr'].please_fill_all_input_fields_press_submit_button = "Συμπληρώστε όλα τα πεδία και πατήστε το κουμπί 'Αποστολή'.";

texts['bg'].display_previous_engaged_driver = "Зареди последно регистрираните водачи";
texts['en'].display_previous_engaged_driver = "Load last checked drivers";
texts['de'].display_previous_engaged_driver = "Load last checked drivers";
texts['ro'].display_previous_engaged_driver = "Incarca ultimele soferi inregistrati";
texts['es'].display_previous_engaged_driver = "Load last checked drivers";
texts['sr'].display_previous_engaged_driver = "Load last checked drivers";
texts['gr'].display_previous_engaged_driver = "Άνοιξε τους τελευταίους εγγεγραμμένους οδηγούς";

texts['bg'].choose_loading_places = "Изберете обекти за товарене";
texts['en'].choose_loading_places = "Select loading places";
texts['de'].choose_loading_places = "Select loading places";
texts['ro'].choose_loading_places = "Alegeti terenuri de incarcare";
texts['es'].choose_loading_places = "Select loading places";
texts['sr'].choose_loading_places = "Select loading places";
texts['gr'].choose_loading_places = "Επιλέξτε τα σημεία φόρτωσης";

texts['bg'].choose_unloading_places = "Изберете обекти за разтоварване";
texts['en'].choose_unloading_places = "Select unloading places";
texts['de'].choose_unloading_places = "Select unloading places";
texts['ro'].choose_unloading_places = "Alegeti terenuri de descarcare";
texts['es'].choose_unloading_places = "Select unloading places";
texts['sr'].choose_unloading_places = "Select unloading places";
texts['gr'].choose_unloading_places = "Επιλέξτε  τα σημεία εκφόρτωσης";

texts['bg'].dynamic_places = "Динамични обекти";
texts['en'].dynamic_places = "Dynamic places";
texts['de'].dynamic_places = "Dynamic places";
texts['ro'].dynamic_places = "Terenuri dinamice";
texts['es'].dynamic_places = "Dynamic places";
texts['sr'].dynamic_places = "Dynamic places";
texts['gr'].dynamic_places = "Δυναμικές θέσεις";

texts['bg'].fuel_level_chart = "Графика за нивото на горивото";
texts['en'].fuel_level_chart = "Fuel level chart";
texts['de'].fuel_level_chart = "Fuel level chart";
texts['ro'].fuel_level_chart = "Grafica nivelului de combustibil";
texts['es'].fuel_level_chart = "Fuel level chart";
texts['sr'].fuel_level_chart = "Fuel level chart";
texts['gr'].fuel_level_chart = "Διάγραμμα στάθμης καυσίμου";

texts['bg'].dynamic_fuel_consumption = "Динамичен разход на гориво";
texts['en'].dynamic_fuel_consumption = "Dynamic fuel consumption";
texts['de'].dynamic_fuel_consumption = "Dynamic fuel consumption";
texts['ro'].dynamic_fuel_consumption = "Consum dinamic combustibil";
texts['es'].dynamic_fuel_consumption = "Dynamic fuel consumption";
texts['sr'].dynamic_fuel_consumption = "Dynamic fuel consumption";
texts['gr'].dynamic_fuel_consumption = "Δυναμική κατανάλωση καυσίμου";

texts['bg'].drivers_days_report = "Справка по дни за водачи и коли";
texts['en'].drivers_days_report = "Drivers daily vehicles usage";
texts['de'].drivers_days_report = "Drivers daily vehicles usage";
texts['ro'].drivers_days_report = "Raport dupa zi pentru soferi si vehicle";
texts['es'].drivers_days_report = "Drivers daily vehicles usage";
texts['sr'].drivers_days_report = "Drivers daily vehicles usage";
texts['gr'].drivers_days_report = "Αναφορά ανά ημέρα οδηγών και οχημάτων ";

texts['bg'].drivers_daily_road_list_period = "Пътен лист за водачи по дни за периода:";
texts['en'].drivers_daily_road_list_period = "Drivers daily road list for dates:";
texts['de'].drivers_daily_road_list_period = "Drivers daily road list for dates:";
texts['ro'].drivers_daily_road_list_period = "Foaie parcursa zilnica  pentru perioada de:";
texts['es'].drivers_daily_road_list_period = "Drivers daily road list for dates:";
texts['sr'].drivers_daily_road_list_period = "Drivers daily road list for dates:";
texts['gr'].drivers_daily_road_list_period = "Οδικό χαρτί οδηγών για περίοδος:";

texts['bg'].contact_key_work = "Работа на базата на контактен ключ";
texts['en'].contact_key_work = "Work based on contact key toggle";
texts['de'].contact_key_work = "Work based on contact key toggle";
texts['ro'].contact_key_work = "La punerea contactului";
texts['es'].contact_key_work = "Work based on contact key toggle";
texts['sr'].contact_key_work = "Work based on contact key toggle";
texts['gr'].contact_key_work = "Λειτουργία βασισμένη στο κλειδί επαφής";

texts['bg'].accumulator_work = "Работа на базата на напрежението на акумулатора";
texts['en'].accumulator_work = "Work based on the accumulator voltage";
texts['de'].accumulator_work = "Work based on the accumulator voltage";
texts['ro'].accumulator_work = "La pornirea motorului";
texts['es'].accumulator_work = "Work based on the accumulator voltage";
texts['sr'].accumulator_work = "Work based on the accumulator voltage";
texts['gr'].accumulator_work = "Λειτουργία με βάση την τάση της μπαταρίας";

texts['bg'].refuels = "Зареждания на гориво";
texts['en'].refuels = "Refueling";
texts['de'].refuels = "Refueling";
texts['ro'].refuels = "Alimentari de combustibil";
texts['es'].refuels = "Refueling";
texts['sr'].refuels = "Refueling";
texts['gr'].refuels = "Ανεφοδιασμού";

texts['bg'].select = "Избери";
texts['en'].select = "Select";
texts['de'].select = "Select";
texts['ro'].select = "Selecta";
texts['es'].select = "Select";
texts['sr'].select = "Select";
texts['gr'].select = "Επιλογή ";

texts['bg'].custom = "По избор";
texts['en'].custom = "Custom range";
texts['de'].custom = "Custom range";
texts['ro'].custom = "Optional";
texts['es'].custom = "Custom range";
texts['sr'].custom = "Custom range";
texts['gr'].custom = "Προαιρετικά";

texts['bg'].today = "За днес";
texts['en'].today = "Today";
texts['de'].today = "Today";
texts['ro'].today = "Astazi";
texts['es'].today = "Today";
texts['sr'].today = "Today";
texts['gr'].today = "Σήμερα";

texts['bg'].since_yesterday = "От вчера";
texts['en'].since_yesterday = "Since yesterday";
texts['de'].since_yesterday = "Since yesterday";
texts['ro'].since_yesterday = "De ieri";
texts['es'].since_yesterday = "Since yesterday";
texts['sr'].since_yesterday = "Since yesterday";
texts['gr'].since_yesterday = "Από χθες";

texts['bg'].since_monday = "От понеделник";
texts['en'].since_monday = "Since monday";
texts['de'].since_monday = "Since monday";
texts['ro'].since_monday = "De luni";
texts['es'].since_monday = "Since monday";
texts['sr'].since_monday = "Since monday";
texts['gr'].since_monday = "Από τη Δευτέρα";

texts['bg'].since_last_seven_days = "За посл. 7 дни";
texts['en'].since_last_seven_days = "Last 7 days";
texts['de'].since_last_seven_days = "Last 7 days";
texts['ro'].since_last_seven_days = "Pentru ultimile 7 zile";
texts['es'].since_last_seven_days = "Last 7 days";
texts['sr'].since_last_seven_days = "Last 7 days";
texts['gr'].since_last_seven_days = "Τελευταίες 7 ημέρες";

texts['bg'].for_this_month = "За този месец";
texts['en'].for_this_month = "This month";
texts['de'].for_this_month = "This month";
texts['ro'].for_this_month = "Pentru luna acesta";
texts['es'].for_this_month = "This month";
texts['sr'].for_this_month = "This month";
texts['gr'].for_this_month = "Για αυτόν τον μήνα";

texts['bg'].work_nocheck = "Работа без оторизация";
texts['en'].work_nocheck = "Work without authorization";
texts['de'].work_nocheck = "Work without authorization"; 
texts['ro'].work_nocheck = "Work without authorization";
texts['es'].work_nocheck = "Work without authorization";
texts['sr'].work_nocheck = "Радите без одобрења";
texts['gr'].work_nocheck = "Work without authorization";

texts['bg'].after_hours_work = "Извън работно време";
texts['en'].after_hours_work = "After hours work";
texts['de'].after_hours_work = "After hours work"; 
texts['ro'].after_hours_work = "After hours work";
texts['es'].after_hours_work = "After hours work";
texts['sr'].after_hours_work = "после сати";
texts['gr'].after_hours_work = "After hours work";