/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var re = /^[0-9]*$/;
var N_LOADING_MAP_INDICATOR = "n_loadingMapIndicator";


//**********************************************************************************************************************************************************
// Променя дължината на div element
//**********************************************************************************************************************************************************

function changeDivHeight(_divId, _height)
{
    document.getElementById(_divId).style.height = _height + "px";
}

//**********************************************************************************************************************************************************
// Центрира div element в центъра на страницата
//**********************************************************************************************************************************************************
function n_centerOnScreen(elementId)
{
    if(typeof(elementId) == "string") elementId = document.getElementById(elementId);
    var _top = window.innerHeight/2 - (50/2);
    var _left = window.innerWidth/2 - (300/2);

    //elementId.style.width = 300 + "px";
    //elementId.style.height = 50 + "px";
    elementId.style.position = "absolute";
    elementId.style.left = _left + "px";
    elementId.style.top = _top + "px";
}

//**********************************************************************************************************************************************************
// Показва скрит div element
//**********************************************************************************************************************************************************
function n_showOnScreen(elementId)
{
    if(typeof(elementId) == "string") elementId = document.getElementById(elementId);
    elementId.style.display = "block";
}

//**********************************************************************************************************************************************************
// Показва и центрира скрит div element
//**********************************************************************************************************************************************************
function n_showAndCenterOnScreen(elementId)
{
    if(typeof(elementId) == "string") elementId = document.getElementById(elementId);
    n_showOnScreen(elementId);
    n_centerOnScreen(elementId);   
}

//**********************************************************************************************************************************************************
// Скрива div element
//**********************************************************************************************************************************************************
function n_hideFromScreen(elementId)
{
    if(typeof(elementId) == "string") elementId = document.getElementById(elementId);
    elementId.style.display = "none";
}

//**********************************************************************************************************************************************************
// Създава и премахва индикатор за прогрес при зареждане на картата на Google Maps.
//**********************************************************************************************************************************************************

function n_createLoadingMapIndicator()
{
    var newdiv = document.createElement('div');
    newdiv.setAttribute('id', N_LOADING_MAP_INDICATOR);

    var _top = window.innerHeight/2 - (50/2);
    var _left = window.innerWidth/2 - (300/2);
 
    newdiv.style.width = 300;
    newdiv.style.height = 50;    
    newdiv.style.position = "absolute";
    newdiv.style.left = _left + "px";
    newdiv.style.top = _top + "px";
    newdiv.style.background = "yellow";
    newdiv.style.border = "1px solid #000";
    newdiv.style.padding = "20px 80px 20px 80px";

    newdiv.innerHTML = "Моля изчакайте...";
   
    document.body.appendChild(newdiv);

}

function n_removeLoadingMapIndicator()
{
    var loadingIndicator = document.getElementById(N_LOADING_MAP_INDICATOR);
    document.body.removeChild(loadingIndicator);
}

//**********************************************************************************************************************************************************
// Взима request parameter отговарящ на името name. Ако няма такъв връща "no"
//**********************************************************************************************************************************************************

function gup(name)
{
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return "no";
    else
        return results[1];
}

//**********************************************************************************************************************************************************
// Пресмята ъгъла в градуси между две точки
//  North = 0, East = 90, South = 180, West = 270.
//**********************************************************************************************************************************************************

function bearing(from, to)
{
    // See T. Vincenty, Survey Review, 23, No 176, p 88-93,1975.
    // Convert to radians.
    var degreesPerRadian = 180.0 / Math.PI;

    var lat1 = from.latRadians();
    var lon1 = from.lngRadians();
    var lat2 = to.latRadians();
    var lon2 = to.lngRadians();

    // Compute the angle.
    var angle = - Math.atan2( Math.sin( lon1 - lon2 ) * Math.cos( lat2 ), Math.cos( lat1 ) * Math.sin( lat2 ) - Math.sin( lat1 ) * Math.cos( lat2 ) * Math.cos( lon1 - lon2 ) );
    if ( angle < 0.0 )
        angle  += Math.PI * 2.0;

    // And convert result to degrees.
    angle = angle * degreesPerRadian;
    angle = angle.toFixed(1);

    return angle;
}


//**********************************************************************************************************************************************************
// Функзия за събиране на Layout компенентна клетка.
// @param id
//**********************************************************************************************************************************************************

function undockReport(id)
{
    layout.cells(id).undock();
}


//**********************************************************************************************************************************************************
// Функция за показване оптимален зоом върху следа
// @param minLat
// @param minLng
// @param maxLat
// @param maxLng
//**********************************************************************************************************************************************************


function n_centerMap(minLat,minLng,maxLat,maxLng)
{
    var latlngbounds = new GLatLngBounds();
    latlngbounds.extend(new GLatLng(minLat,minLng));
    latlngbounds.extend(new GLatLng(maxLat,maxLng));
    map.setCenter(latlngbounds.getCenter(),map.getBoundsZoomLevel(latlngbounds));
}

//**********************************************************************************************************************************************************
// Функция за обратно геокодиране по дадено координати
// @param lat
// @param lng
// @param nodeId
//**********************************************************************************************************************************************************

function n_getAddress(lat,lng,nId)
{
    geocoder.getLocations(new GLatLng(parseFloat(lat),parseFloat(lng)), function(response)
    {
        if (!response || response.Status.code != 200)
        {
            document.getElementById(nId).innerHTML="Неидентифицирано място";
        }
        else
        {
            var place = response.Placemark[0];
            document.getElementById(nId).innerHTML = place.address;
        }
    })
}

//**********************************************************************************************************************************************************
// Функция за преместване на scrollbar-а в дъното на страницата!
// @param divId
//**********************************************************************************************************************************************************
function moveScrollToButtom(divId)
{
    var gridDiv = document.getElementById(divId);
    gridDiv.scrollTop = gridDiv.scrollHeight;
}


//**********************************************************************************************************************************************************
// Функция за локализиране на място по зададен низ
//**********************************************************************************************************************************************************
function n_findLocation(placeName)
{
    var geocoder = new GClientGeocoder();

    geocoder.getLatLng(placeName, function(point)
    {
        if(!point)
        {
            alert("Неидентифицирано място. Проверете дали сте въвели правилно името. \nЖелателно е за по комплексни места въвеждането да става по следният начин:\n1) Номер на улица\n 2) Име на улицата\n 3) Град\n 4) Държава.");
            return;
        }
        else
        {
            map.setCenter(point, 13);
            var marker = new GMarker(point);
            marker.routeIndex = true;
            map.addOverlay(marker);
            marker.openInfoWindowHtml(placeName);
        }
    });
}


//**********************************************************************************************************************************************************
// Функция проверяваща дали едно време е правилно въведено
//**********************************************************************************************************************************************************
function validateTime(time)
{
    var tokens = time.split(":");

    if(tokens.length != 3) return false;

    try
    {
        var hours = parseFloat(tokens[0]); 
        var minutes = parseFloat(tokens[1]); 
        var seconds = parseFloat(tokens[2]);

        if(hours >= 0)
        {
            if(minutes >= 0 && minutes < 60)
            {
                if(seconds>=0 && seconds < 60)
                {
                    return true;
                }
            }
        }
    }
    catch(err)
    {
        return false;
    }

    return false;

}


//**********************************************************************************************************************************************************
// Функция проверяваща дали едно число е положително
//**********************************************************************************************************************************************************
function isPositiveNumber(number)
{
   if(number.match(re)==null) return false;
   return true;
}

function registerReportUsage(reportName)
{
    var url = "/GPS/ReportsUsageServlet?reportName="+reportName;
    new Ajax.Request(url,
    {
        method:"post"

    });
}
