/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function eXcell_editableTime(cell)
{
    if (cell)
    {
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;
    }
    this.setValue = function (val) {
        //alert(1);
        this.setCValue(val);
    };
    this.getValue = function () {

        var value = "";
        try
        {
            value = this.cell.firstChild.childNodes[0].value + ":" + this.cell.firstChild.childNodes[2].value + ":" + this.cell.firstChild.childNodes[4].value;
        } catch (err) {
            value = this.cell.innerHTML;
        }

        return value;
    };
    this.edit = function () {

        var that = this;

        this.val = this.getValue(); //save current value
        var valArr = this.val.split(":");
        if (valArr.length < 3) {
            valArr.length = 0;
            valArr.push("00");
            valArr.push("00");
            valArr.push("00");
        }

        var style = "width:13px; font-size:11px; border:0px; background-color:transparent; cursor:pointer";

        this.cell.innerHTML = "<div title='След промяна натиснете Enter за да запаметите!' style='background-color:white; border:1px solid #888; width:55px; padding-left:3px; padding-right:3px;'><input type='text' style='" + style + "' /><span>:</span><input type='text' style='" + style + "' /><span>:</span><input type='text' style='" + style + "' /></div>";
        this.cell.firstChild.childNodes[0].value = valArr[0];
        this.cell.firstChild.childNodes[2].value = valArr[1];
        this.cell.firstChild.childNodes[4].value = valArr[2];

        var dummy = 0;
        for (var i = 0; i <= 4; i++)
        {

            (function (i, htmlElement) {

                htmlElement.childNodes[i].onclick = function (e) {
                    if (i % 2 === 0) {
                        htmlElement.childNodes[i].select();
                    }

                    (e || event).cancelBubble = true;
                };
                
                htmlElement.childNodes[i].onkeydown = function (event){
                    dummy++;
                };
                
                htmlElement.childNodes[i].onkeyup = function (event) {
                    dummy--;
                    
                    if (i % 2 === 0)
                    {
                        var testAgainst = -1;
                        switch (i) {
                            case 0:
                                testAgainst = 3;
                                break;
                            case 2:
                                testAgainst = 6;
                                break;
                            case 4:
                                testAgainst = 6;
                                break;
                            default:
                                break;
                        }

                        var x = event.which || event.keyCode;
                        if (x >= 48 && x <= 57 || x >= 96 && x <= 105)
                        {
                            if (this.value >= testAgainst)
                            {
                                if (this.value.length < 2)
                                {
                                    this.value = ("0" + this.value);

                                    if (i !== 4) {
                                        
                                        if(dummy === 0) {
                                            htmlElement.childNodes[i + 2].focus();
                                            htmlElement.childNodes[i + 2].select();
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    var oldValue = this.value + "";
                                    if (oldValue.length >= 2) {
                                        
                                        if(i === 0 && this.value > 23 || (i === 2 || i === 4) && this.value > 59) {
                                            this.value = "00";
                                            this.select();
                                            
                                            return;
                                        }
                                        
                                        if (i !== 4) {
                                            
                                            if(dummy === 0) {
                                                htmlElement.childNodes[i + 2].focus();
                                                htmlElement.childNodes[i + 2].select();
                                            }
                                        }
                                    } 
                                }
                            }
                            else
                            {
                                if (this.value.length === 2)
                                {
                                    var selectNext = i;
                                    if (i !== 4) {
                                        selectNext = i + 2;
                                    }
                                    
                                    if(dummy === 0) {
                                        htmlElement.childNodes[selectNext].focus();
                                        htmlElement.childNodes[selectNext].select();
                                    }
                                }
                            }
                        } 
                        else
                        {
                            if (x === 13) {
                                htmlElement.parentNode.click();
                            }
                        }
                    }
                };

                htmlElement.childNodes[i].onmousedown = function (e) {
                    if (i % 2 === 0)
                        return true;
                    return false;
                };
                htmlElement.childNodes[i].onselectstart = function (e) {
                    if (i % 2 === 0)
                        return true;
                    return false;
                };

            })(i, this.cell.firstChild);



        }

        this.cell.firstChild.childNodes[0].focus();
        this.cell.firstChild.childNodes[0].select();
    };
    this.detach = function () {

        this.setValue(this.cell.firstChild.childNodes[0].value + ":" + this.cell.firstChild.childNodes[2].value + ":" + this.cell.firstChild.childNodes[4].value);
        return this.val !== this.getValue();    // compare the new and the _old values
    };

}
eXcell_editableTime.prototype = new eXcell;
