import axios from 'axios';

import {SET_LOGIN_USERS} from '@/store';
import store from '@/store';
// import * as namedExports from '../../store';
// import store, * as namedExports from '../../store';

export function getUsersApi() {
    return axios('http://localhost:3000/users', {
            method: "GET"
        }
    )
        .then((response) => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    store.commit(SET_LOGIN_USERS, response.data);

                    resolve(response.data);
                }, 500);
            })
        })
}
