import axios from 'axios'
import {DatabaseData} from "@/store";
import {addData} from "@/store/mutations";

export function createWaybill(data: machineData): Promise<DatabaseData> {
    return axios.post<DatabaseData>('/create_waybill.php', data)
        .then((r) => {
            addData(r.data);
            return r.data;
        });
}

export function getMaintenance(type: string): Promise<Maintenance[]> {

    return axios.get('/find_maintenance.php', {
        params: {
            type
        }
    })
        .then((res) => {
            // let viewMaintenace = [];
            // for (let key of res.data) {
            //     viewMaintenace.push(
            //         {
            //             id: key.id,
            //             registration: key.registration,
            //             start_date: key.start_date,
            //             end_date: key.end_date
            //         }
            //     )
            // }
            // return viewMaintenace;
            return res.data
        })
        .catch((err) => {
            throw err;
        })
}

export function deleteMaintenanceById(id: number, mainType: string) {
    let deleteEntry = {id, targetTable: mainType};
    return axios.post('/remove_maintenance.php', deleteEntry);
}


export function createTrackerMachineEntry(data: {}) {

    return axios.post('/set_tracking_machine.php', data)
        .then((r) => {
            console.log(r);
        });
}

export function editTrackerMachineEntry(data: any) {
    let destination = data.to;

    return axios.post('/edit_tracking_machine.php', data)
        .then((r) => {
            console.log(r);
        }).then(() => getTrackerMachines(destination));
}


export function updateTrackingTimeIntervals(data: any[]) {
    return axios.post('/edit_target_intervals.php', data)
        .then((r) => {
            console.log(r);
        })
        .catch((e) => console.log(e));
}

export function getTrackerTargetIntervals(type: string) {
    return axios.get('get_tracking_intervals.php', {params: {type}})
        .then((res) => {
            return res;
        })
}

export function deleteTrackerMachineEntry(data: any) {
    let destination = data.from;
    return axios.post('/delete_tracking_machine.php', data)
        .then((r) => {
            console.log(r.data);
        }).then(() => getTrackerMachines(destination))
}

export function getTrackerMachines(type: string) {
    return axios.get('get_tracking_machines.php', {params: {type}})
        .then((res) => {
            return res.data;
        })
        .catch((e) => {
            console.log(e);
            throw e;
        })
}

export function getAuditMachines() {
    return axios.get('get_machines.php')
        .then((res) => res.data)
        .catch(console.log);
}

export function loadAccountingAuditData(data: any) {
    return axios.post('get_accounting_audit.php', data)
        .then((res) => res.data)
        .catch(console.log)
}

export function deleteByLocationWork(id: number) {
    return axios.post('/remove_location_work.php', {
        id
    })

}
