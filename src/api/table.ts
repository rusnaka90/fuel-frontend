

export function createTable(tableData: any, type:string) {

    let xlsx = require('xlsx');

    let jsonMachineData = JSON.stringify(tableData);

    let data = xlsx.utils.sheet_to_json(jsonMachineData);

    let newData = data.map((record: any) => {
        record.Net = record.Sales - record.Cost;
        return record;
    });

    let newWB = xlsx.utils.book_new();
    newWB.Props = {
        Title: 'SheetJS tutorial',
        Subject: 'Register.vue',
        Author: 'Pavel Nistorov',
        CreatedDate: new Date().getFullYear(),
    };
    newWB.SheetNames.push('Test_Sheet');

    let commonMachineData: any = [
        [
            ['NAME'], ['REGISTRATION'], ['FIRST_KM'], ['LAST_KM'], ['TOTAL_KM'], ['CONSUME/100km'], ['CONSUMED_FUEL']
        ]
    ];

    let detailedMachinesData: any = [
        [
            ['NAME'], ['REGISTRATION'], ['FIRST_KM'], ['LAST_KM'], ['TOTAL_KM'], ['DATE'], ['CONSUME/100km'], ['CONSUMED_FUEL']
        ]
    ];

    // @ts-ignore
    let arayToArrayFromCommonMachineData = (): [] => {
        for (let data of tableData) {
            commonMachineData.push([[data.name], [data.registration], [data.first],
                [data.last], [data.total],[data.consum], [data.consumed]]);
        }

        return commonMachineData;
    };
    // @ts-ignore
    let arayToArrayFromDetailedMachineData = (): [] => {
        for (let data of tableData) {
            detailedMachinesData.push([[data.name], [data.registration], [data.start_km],
                [data.end_km], [data.total_km], [data.date], [data.consum], [data.consumed_total]]);
        }

        return detailedMachinesData;
    };
    if (type === "common") {
    newWB.Sheets['Test_Sheet'] = xlsx.utils.aoa_to_sheet(arayToArrayFromCommonMachineData());
    }
    if (type === "detailed") {
    newWB.Sheets['Test_Sheet'] = xlsx.utils.aoa_to_sheet(arayToArrayFromDetailedMachineData());
    }

    let newWS = xlsx.utils.json_to_sheet(newData);
    xlsx.utils.book_append_sheet(newWB, newWS, 'machine_DATA');

    xlsx.writeFile(newWB, 'tableBase.xlsx');

}
