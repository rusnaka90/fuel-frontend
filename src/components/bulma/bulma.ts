import styles from './bulma.module.scss'

export function augmentCss(classes: Record<string, boolean>): Record<string, boolean> {
    return Object.fromEntries(
        Object.entries(classes).map((c) => {
            // TODO Maybe skip missing classes
            if (!styles[c[0]]) {
                console.log(`class "${c[0]}" missing`)
            }

            return [styles[c[0]], c[1]]
        })
    )
}

export function augmentClasses(classes: string[]): Record<string, boolean> {
    return Object.fromEntries(
        classes.map((c) => {
            // todo maybe skip missing classes
            return [styles[c], true]
        })
    )
}

export function augmentClass(c: string): string {
    return styles[c]
}

export { styles }
