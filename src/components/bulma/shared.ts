export const primaryTypes = ['primary', 'info', 'success', 'notification', 'danger']

export const buttonTypes = ['white', 'light', 'dark', 'black', 'text', 'link', ...primaryTypes, undefined]

export const sizes = ['small', 'normal', 'medium', 'large']

export const breadcrumbSeparators = ['arrow', 'bullet', 'dot', 'succeeds']
