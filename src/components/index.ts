import Vue from 'vue';

import CreateVehicle from './CreateVehicle.vue';
import Navbar from './Navbar.vue';
import NavbarItem from './NavbarItem.vue'
import FileInput from './FileInput.vue';
import WelcomeInfo from './WelcomeInfo.vue';
import InfoNavigation from './InfoNavigation.vue';
import Footer from './Footer.vue';
import Warning from './Warning.vue';
import SwitchBtn from './SwitchBtn.vue'
import TableList from './TableList.vue'
import TableGeneral from './TableGeneral.vue'
import Help from './AccessInfo.vue'
import MaintenanceTableList from './MaintenanceTableList.vue'
import Notification from './Notification.vue'

Vue.component('CreateVehicle', CreateVehicle);
Vue.component('Navbar', Navbar);
Vue.component('NavbarItem', NavbarItem);
Vue.component('FileInput', FileInput);
Vue.component('WelcomeInfo', WelcomeInfo);
Vue.component('InfoNavigation', InfoNavigation);
Vue.component('Footer', Footer);
Vue.component('Warning', Warning);
Vue.component('SwitchBtn', SwitchBtn);
Vue.component('TableList', TableList);
Vue.component('TableGeneral', TableGeneral);
Vue.component('Help', Help);
Vue.component('MaintenanceTableList', MaintenanceTableList);
Vue.component('Notification', Notification);

import * as bulma from './bulma'

Vue.component('bulma-button', bulma.Button);
Vue.component('bulma-menu', bulma.Menu);
Vue.component('bulma-menu-list', bulma.MenuList);
Vue.component('bulma-message', bulma.Message);
Vue.component('bulma-select', bulma.Select);
Vue.component('bulma-text-area', bulma.TextArea);
Vue.component('bulma-text-field', bulma.TextField);
