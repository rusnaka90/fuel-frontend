import Vue from 'vue';
import VueRouter, {RouteConfig} from 'vue-router';

import Home from "../views/Home.vue";

import Login from '../views/Login.vue';
import Register from '../views/Register.vue';

import AddWayBill from '../views/AddWayBill.vue';
import WayBillsList from '../views/WayBillsList.vue';
import InputDataManager from '../views/InputDataManager.vue';
import AuditList from '../views/AuditList.vue';
import AccountingAudit from '../views/AccountingAudit.vue';

import AddMaintenance from "../views/AddMaintenance.vue";
import InsuranceMaintenance from "../views/InsuranceMaintenance.vue";
import AccessInfo from '../components/AccessInfo.vue';
import OilMaintenance from "../views/OilMaintenance.vue";
import TaxMaintenance from "../views/TaxMaintenance.vue";
import TiresMaintenance from "../views/TiresMaintenance.vue";
import VignetteMaintenance from "../views/VignetteMaintenance.vue";
import TimingBeltMaintenance from "../views/TimingBeltMaintenance.vue";
import TechnicalMaintenance from "@/views/TechnicalMaintenance.vue";

import TrackedMachinesList from '../views/TrackedMachinesList.vue';
import TrackedMachinesSet from "@/views/TrackedMachinesSet.vue";




Vue.use(VueRouter);

const routes: RouteConfig[] = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            title: "fuelAuditApp",
            anonymous: true
        }
    },
    {
        path: '/AddWayBill',
        name: 'AddWayBill',
        component: AddWayBill,
        meta: {
            title: "fuelAuditApp",
            anonymous: false
        }
    },
    {
        path: '/WayBillsList',
        name: 'WayBillsList',
        component: WayBillsList,
        meta: {
            anonymous: false
        }
    },
    {
        path: '/AuditList',
        name: 'AuditList',
        component: AuditList,
        meta: {
            anonymous: false
        }
    },
    {
        path: '/AccountingAudit',
        name: 'AccountingAudit',
        component: AccountingAudit ,
        meta: {
            anonymous: false
        }
    },
    {
        path: '/TrackedMachinesList',
        name: 'TrackedMachinesList',
        component: TrackedMachinesList,
        meta: {
            anonymous: false
        }
    },
    {
        path: '/TrackedMachinesSet',
        name: 'TrackedMachinesSet',
        component: TrackedMachinesSet,
        meta: {
            anonymous: false,
        }

    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            anonymous: true,
        }
    },
    {
        path: '/help',
        name: 'AccessHelp',
        component: AccessInfo,
        meta: {
            anonymous: true,
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            anonymous: true,
        }
    },
    {
        path: '/InputDataWayBillManager',
        name: 'InputDataManager',
        component: InputDataManager,
        meta: {
            anonymous: false,
        }
    },
    {
        path: '/AddMaintenance',
        name: 'AddMaintenance',
        component: AddMaintenance,
        meta: {
            anonymous: false,
        }
    },
    {
        path: '/InsuranceMaintenance',
        name: 'InsuranceMaintenance',
        component: InsuranceMaintenance,
                meta: {
            anonymous: false,
        }
    },
    {
        path: '/OilMaintenance',
        name: 'OilMaintenance',
        component: OilMaintenance,
        meta: {
            anonymous: false,
        }
    },
    {
        path: '/TaxMaintenance',
        name: 'TaxMaintenance',
        component: TaxMaintenance,
        meta: {
            anonymous: false,
        }
    },
    {
        path: '/TechnicalMaintenance',
        name: 'TechnicalMaintenance',
        component: TechnicalMaintenance,
        meta: {
            anonymous: false,
        }
    },
    {
        path: '/TiresMaintenance',
        name: 'TiresMaintenance',
        component: TiresMaintenance,
        meta: {
            anonymous: false,
        }
    },
    {
        path: '/VignetteMaintenance',
        name: 'VignetteMaintenance',
        component: VignetteMaintenance,
        meta: {
            anonymous: false,
        }
    },
    {
        path: '/TimingBeltMaintenance',
        name: 'TimingBeltMaintenance',
        component: TimingBeltMaintenance,
        meta: {
            anonymous: false,
        }
    },

];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
